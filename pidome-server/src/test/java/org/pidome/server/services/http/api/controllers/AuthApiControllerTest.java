/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.controllers;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.AfterAll;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.pidome.server.services.PiDomeService;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.authentication.AuthenticationService;
import org.pidome.server.services.authentication.PidomeAuthToken;
import org.pidome.server.services.http.api.auth.LoginObject;
import org.pidome.server.services.http.api.controllers.ApiControllerHelper.ReferenceHolder;
import org.pidome.server.services.http.api.response.ApiResponseCode;
import org.pidome.server.services.http.api.response.HttpStatusCodeException;
import org.pidome.server.system.config.SystemConfig;

/**
 * Test authentication API controller.
 *
 * @author johns
 */
@DisplayName("Authentication endpoint tests")
@SuppressWarnings("CPD-START")
@TestMethodOrder(OrderAnnotation.class)
@TestInstance(Lifecycle.PER_CLASS)
@ExtendWith(VertxExtension.class)
public class AuthApiControllerTest {

	/**
	 * Api controller status.
	 */
	private static AuthApiController instance;

	/**
	 * The service handler.
	 */
	private static ServiceHandler handler;

	@BeforeAll
	public static void setUpClass() throws Exception {
		SystemConfig.initialize();
	}

	@AfterAll
	public static void tearDownClass() throws Exception {
		instance = null;
		handler.stopServicesBlocking();
		handler = null;
	}

	@Test
	@Order(0)
	public void testStartServices() throws Exception {
		handler = ServiceHandler.getInstance();
		handler.startServicesBlocking(
				PiDomeService.NETWORK,
				PiDomeService.SECURITY,
				PiDomeService.DATABASESERVICE,
				PiDomeService.USERSERVICE,
				PiDomeService.WEBSERVICE,
				PiDomeService.AUTHENTICATIONSERVICE);
		instance = new AuthApiController();
	}

	/**
	 * Test of authenticate method, of class AuthApiController.
	 */
	@Test
	@DisplayName("Test succesfull login")
	public void testAuthenticateSuccess(Vertx vertx, VertxTestContext testContext) throws Exception {

		HttpServerResponse response = ApiControllerHelper.mockHttpServerResponse();
		HttpServerRequest request = ApiControllerHelper.mockHttpServerRequest();

		LoginObject loginObject = new LoginObject();
		loginObject.setUsername("pidome");
		loginObject.setPassword("pidome");
		loginObject.setLoginSource(ApiControllerHelper.mockAuthTokenReference());

		instance.authenticate(loginObject, response, request).setHandler(
				testContext.succeeding(result -> {
					assertThat(result.getToken(), is(notNullValue()));
					testContext.completeNow();
				})
		);

	}

	/**
	 * Test of authenticate method, of class AuthApiController.
	 */
	@Test
	@DisplayName("Test failing login")
	public void testAuthenticateFailure(Vertx vertx, VertxTestContext testContext) throws Exception {
		HttpServerResponse response = ApiControllerHelper.mockHttpServerResponse();
		HttpServerRequest request = ApiControllerHelper.mockHttpServerRequest();

		LoginObject loginObject = new LoginObject();
		loginObject.setUsername("wronguser");
		loginObject.setPassword("wrongpass");
		loginObject.setLoginSource(ApiControllerHelper.mockAuthTokenReference());

		instance.authenticate(loginObject, response, request).setHandler(
				testContext.failing(result -> {
					assertThat(result, is(instanceOf(HttpStatusCodeException.class)));
					assertThat(((HttpStatusCodeException) result).getStatusCode(), is(equalTo(ApiResponseCode.HTTP_401.getResponseCode())));
					testContext.completeNow();
				})
		);

	}

	/**
	 * Test of logout method, of class AuthApiController.
	 */
	@Test
	@DisplayName("Test logout/session destroy")
	public void testLogout() throws Exception {
		HttpServerResponse response = ApiControllerHelper.mockHttpServerResponse();
		HttpServerRequest request = ApiControllerHelper.mockHttpServerRequest();

		LoginObject loginObject = new LoginObject();
		loginObject.setUsername("pidome");
		loginObject.setPassword("pidome");
		loginObject.setLoginSource(ApiControllerHelper.mockAuthTokenReference());

		final ReferenceHolder<PidomeAuthToken> tokenHolder = new ReferenceHolder<>();

		handler.getService(AuthenticationService.class).get().authenticate(loginObject, resultHandler -> {
			if (resultHandler.failed()) {
				fail(resultHandler.cause());
			}
			tokenHolder.setObject(resultHandler.result());
		});

		assertTrue(tokenHolder.getObject().isPresent());
		Mockito.when(request.getHeader("authorization")).thenReturn(tokenHolder.getObject().get().getToken());

		instance.logout(response, request);

		assertTrue(handler.getService(AuthenticationService.class).get().isTokenRevoked(tokenHolder.getObject().get().getToken()));
	}

}
