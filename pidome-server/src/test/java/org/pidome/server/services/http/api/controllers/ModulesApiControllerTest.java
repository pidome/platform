/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.controllers;

import java.util.List;
import java.util.Map;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.Timeout;
import org.pidome.platform.hardware.driver.Transport;
import org.pidome.platform.modules.ModuleType;
import org.pidome.server.services.PiDomeService;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.modules.ModuleDefinition;
import org.pidome.server.system.config.SystemConfig;

/**
 * Tets module controller classes.
 *
 * @author johns
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SuppressWarnings("CPD-START")
public class ModulesApiControllerTest {

    /**
     * The main test package.
     */
    private static final String testPackageName = "PiDome RFXCom USB";

    /**
     * Api controller status.
     */
    private static ModulesApiController instance;

    /**
     * The service handler.
     */
    private static ServiceHandler handler;

    @BeforeEach
    @Timeout(60)
    void setUp() {
        // fails if execution time exceeds 5 seconds
    }

    @BeforeAll
    public static void setUpClass() throws Exception {
        SystemConfig.initialize();
    }

    @AfterAll
    public static void tearDownClass() throws InterruptedException {
        handler.stopServicesBlocking();
        handler = null;
        instance = null;
    }

    @Test
    @Order(0)
    public void testStartServices() throws Exception {
        handler = ServiceHandler.getInstance();
        handler.startServicesBlocking(
                PiDomeService.MODULESERVICE
        );
        instance = new ModulesApiController();
    }

    /**
     * Test of getModules method, of class ModulesApiController.
     *
     * We know at minimal one devices module is present.
     *
     * As all implementations are generic, returning a devices module is equal
     * to returning any other module.
     *
     * @throws java.lang.Exception On error
     */
    @Test
    @Order(1)
    public void testGetModules() throws Exception {
        Map<ModuleType, List<ModuleDefinition>> result = instance.getModules();

        assertThat(result.keySet().isEmpty(), is(equalTo(false)));
        assertThat(result.keySet().contains(ModuleType.DEVICES), is(equalTo(true)));
        assertThat(result.values().isEmpty(), is(equalTo(false)));

        assertThat(result.values().stream()
                .anyMatch(modules -> modules.stream().anyMatch(module -> testPackageName.equals(module.getName()))), is(equalTo(true)));

    }

    /**
     * Test of getModules method, of class ModulesApiController.
     *
     * We know at minimal one devices module is present. As all implementations
     * are generic, returning a devices module is equal to returning any other
     * module.
     */
    @Test
    @Order(2)
    public void testGetDevicesModules() throws Exception {
        List<ModuleDefinition> result = instance.getModules(ModuleType.DEVICES);
        assertThat(result.isEmpty(), is(equalTo(false)));
        assertThat(result.stream()
                .anyMatch(module -> testPackageName.equals(module.getName())), is(equalTo(true)));

    }

    /**
     * Test of getModules method, of class ModulesApiController.
     *
     * We know at minimal one devices module is present. As all implementations
     * are generic, returning a devices module is equal to returning any other
     * module.
     */
    @Test
    @Order(2)
    public void testGetDevicesSerialTransportModules() throws Exception {
        List<ModuleDefinition> result = instance.getModules(ModuleType.DEVICES, Transport.SubSystem.SERIAL, null);
        assertThat(result.isEmpty(), is(equalTo(false)));
        assertThat(result.stream()
                .anyMatch(module -> testPackageName.equals(module.getName())), is(equalTo(true)));

    }

    /**
     * Test of getModules method, of class ModulesApiController.
     *
     * We know at minimal one devices module is present. As all implementations
     * are generic, returning a devices module is equal to returning any other
     * module.
     */
    @Test
    @Order(3)
    public void testGetDevicesSerialTransportModulesWithPreferredParameter() throws Exception {
        List<ModuleDefinition> result = instance.getModules(ModuleType.DEVICES, Transport.SubSystem.SERIAL, "VID_0403&PID_6015");
        assertThat(result.isEmpty(), is(equalTo(false)));
        assertThat(result.stream()
                .anyMatch(module -> testPackageName.equals(module.getName())), is(equalTo(true)));

    }

    /**
     * Test of getModules method, of class ModulesApiController.
     *
     * We know at minimal one devices module is present. As all implementations
     * are generic, returning a devices module is equal to returning any other
     * module. No module definition should be returned as a module target of "-"
     * should not match any module.
     */
    @Test
    @Order(3)
    public void testGetDevicesSerialTransportModulesWithNonTargetPreferredParameter() throws Exception {
        List<ModuleDefinition> result = instance.getModules(ModuleType.DEVICES, Transport.SubSystem.SERIAL, "-");
        assertThat(result.isEmpty(), is(equalTo(true)));
    }

}
