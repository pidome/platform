/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.controllers;

import org.junit.jupiter.api.AfterAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.pidome.server.services.PiDomeService;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.cluster.ClusterMode;
import org.pidome.server.services.cluster.ClusterService;
import org.pidome.server.services.cluster.ClusterHost;
import org.pidome.server.system.config.SystemConfig;

/**
 * Cluster tests
 *
 * @author johns
 */
@DisplayName("Cluster integration tests")
@TestMethodOrder(OrderAnnotation.class)
@TestInstance(Lifecycle.PER_CLASS)
public class ClusterApiControllerTest {

    /**
     * Api controller status.
     */
    private static ClusterApiController instance;

    /**
     * The service handler.
     */
    private static ServiceHandler handler;

    @BeforeAll
    public static void setUpClass() throws Exception {
        SystemConfig.initialize();
    }

    @AfterAll
    public static void tearDownClass() throws Exception {
        handler.stopServicesBlocking();
        handler = null;
        instance = null;
    }

    @Test
    @Order(0)
    public void testStartServices() throws Exception {
        handler = ServiceHandler.getInstance();
        handler.startServicesBlocking(
                PiDomeService.CLUSTER
        );
        instance = new ClusterApiController();
    }

    /**
     * Test of gethostInfo method, of class ClusterApiController.
     */
    @Test
    @DisplayName("Test cluster host info")
    public void testGethostInfo() throws Exception {
        ClusterHost expResult = handler.getService(ClusterService.class).get().getHostInformation();
        ClusterHost result = instance.gethostInfo();
        assertEquals(expResult, result);
    }

    @Test
    @DisplayName("Test run mode")
    public void testGetRunMode() throws Exception {
        assertEquals(ClusterMode.LOCAL, handler.getService(ClusterService.class).get().getClusterMode());
    }

}
