/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.installer;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.AfterAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.pidome.server.services.PiDomeService;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.system.config.SystemConfig;
import org.pidome.server.system.database.Database;
import org.pidome.server.system.installer.packages.ServerPackage;
import org.pidome.server.system.installer.repositories.RepositoryContainer;

/**
 *
 * @author johns
 */
@SuppressWarnings("CPD-START")
@TestMethodOrder(OrderAnnotation.class)
@TestInstance(Lifecycle.PER_CLASS)
public class InstallerServiceTest {

    /**
     * The service handler.
     */
    private static ServiceHandler handler;

    private long newContainerIdFromTest;

    public InstallerServiceTest() {
    }

    @BeforeAll
    public static void setUpClass() throws Exception {
        SystemConfig.initialize();
    }

    @AfterAll
    public static void tearDownClass() throws Exception {
        handler.stopServicesBlocking();
    }

    @Test
    @Order(0)
    public void testStartServices() throws Exception {
        SystemConfig.initialize();
        handler = ServiceHandler.getInstance();
        handler.startServicesBlocking(
                PiDomeService.INSTALLERSERVICE
        );
    }

    /**
     * Test of getRepositoryContainers method, of class InstallerService.
     */
    @Test
    @Order(1)
    public void testGetRepositoryContainers() throws Exception {
        InstallerService service = handler.getService(InstallerService.class).orElseThrow(() -> new Exception("Service not available"));
        RepositoryContainer repoComponentsContainer = service.getRepositoryContainers().get(0);
        assertThat(repoComponentsContainer.getRepositoryProviderId(), is(equalTo("pidome-core")));
        assertThat(repoComponentsContainer.isReadOnly(), is(equalTo(true)));
        assertThat(repoComponentsContainer.isActive(), is(equalTo(true)));
    }

    /**
     * Test of getRepositoryContainerById method, of class InstallerService.
     */
    @Test
    @Order(2)
    public void testGetRepositoryContainerById() throws Exception {
        InstallerService service = handler.getService(InstallerService.class).orElseThrow();
        RepositoryContainer repoComponentsContainer = service.getRepositoryContainerById(2L);
        assertThat(repoComponentsContainer.getRepositoryProviderId(), is(equalTo("pidome-components")));
        assertThat(repoComponentsContainer.isReadOnly(), is(equalTo(false)));
        assertThat(repoComponentsContainer.isActive(), is(equalTo(true)));
    }

    /**
     * Test of updateRepositoryContainer method, of class InstallerService.
     */
    @Test
    @Order(3)
    public void testUpdateRepositoryContainer() throws Exception {

        final String repoName = "Name changed by PiDome unit test: testUpdateRepositoryContainer";

        InstallerService service = handler.getService(InstallerService.class).orElseThrow();
        RepositoryContainer repoComponentsContainer = service.getRepositoryContainerById(1L);

        repoComponentsContainer.setRepositoryProviderName(repoName);
        /// System containers are not updateable.
        assertThrows(InstallerException.class, () -> service.updateRepositoryContainer(repoComponentsContainer));

        RepositoryContainer repoContainer = new RepositoryContainer();
        repoContainer.setRepositoryProviderId("pidome-core-test");
        repoContainer.setRepositoryProviderName("PiDome platform-test");
        repoContainer.setRepositoryProviderDescription("Official PiDome platform installation locations-test");
        repoContainer.setReadOnly(false);
        repoContainer.setActive(true);

        try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            autoManager.getManager().getTransaction().begin();
            autoManager.getManager().persist(repoContainer);
            autoManager.getManager().getTransaction().commit();
            newContainerIdFromTest = repoContainer.getId();
        }

        RepositoryContainer repoComponentsContainerUpdated = service.getRepositoryContainerById(newContainerIdFromTest);
        repoComponentsContainerUpdated.setRepositoryProviderName(repoName);

        service.updateRepositoryContainer(repoComponentsContainerUpdated);

        assertThat(service.getRepositoryContainerById(newContainerIdFromTest).getRepositoryProviderName(), is(equalTo(repoName)));
    }

    /**
     * Test of getPackage method, of class InstallerService.
     */
    @Test
    @Order(4)
    @SuppressWarnings("unchecked")
    public void testGetPackage() {
        final String packageGroup = "org.pidome.platform.hardware.drivers";
        final String packageName = "pidome-driver-serial";
        final String packageVersion = "1.0.0-SNAPSHOT";

        InstallerService service = handler.getService(InstallerService.class).orElseThrow();
        ServerPackage pack = service.getPackage(packageGroup, packageName, packageVersion);

        assertThat(packageGroup, is(equalTo(pack.getPackageGroup())));
        assertThat(packageName, is(equalTo(pack.getPackageName())));
        assertThat(packageVersion, is(equalTo(pack.getPackageVersion())));

    }

    /**
     * Test of deleteRepositoryContainer method, of class InstallerService.
     */
    @Test
    public void testDeleteRepositoryContainer() throws Exception {
        InstallerService service = handler.getService(InstallerService.class).orElseThrow();

        /// System containers are not deletable.
        assertThrows(InstallerException.class, () -> service.deleteRepositoryContainer(
                service.getRepositoryContainerById(1L)
        ));

        RepositoryContainer repoComponentsContainer = service.getRepositoryContainerById(newContainerIdFromTest);
        service.deleteRepositoryContainer(repoComponentsContainer);
        service.getRepositoryContainerById(newContainerIdFromTest);
    }

}
