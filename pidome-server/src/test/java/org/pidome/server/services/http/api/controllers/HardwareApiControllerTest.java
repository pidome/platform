/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import io.vertx.core.Vertx;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasKey;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.api.condition.DisabledIfSystemProperty;
import org.junit.jupiter.api.extension.ExtendWith;
import org.pidome.platform.hardware.driver.HardwareDriver;
import org.pidome.platform.hardware.driver.Transport;
import org.pidome.platform.presentation.input.InputForm;
import org.pidome.server.env.PlatformOs;
import org.pidome.server.services.PiDomeService;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.hardware.DriverDefinition;
import org.pidome.server.services.hardware.HardwareService;
import org.pidome.server.services.http.api.response.HttpStatusCodeException;
import org.pidome.server.system.config.SystemConfig;
import org.pidome.server.system.hardware.HardwareComponent;
import org.pidome.server.system.hardware.Peripheral;
import org.pidome.server.system.hardware.usb.USBDevices;
import org.pidome.tools.utilities.Serialization;

/**
 *
 * @author johns
 */
@TestMethodOrder(OrderAnnotation.class)
@TestInstance(Lifecycle.PER_CLASS)
@ExtendWith(VertxExtension.class)
@SuppressWarnings("CPD-START")
public class HardwareApiControllerTest {

	/**
	 * Api controller status.
	 */
	private static HardwareApiController instance;

	/**
	 * The service handler.
	 */
	private static ServiceHandler handler;

	/**
	 * The peripheral we test with using windows style key.
	 */
	private static final String PERIPHERAL_TEST_KEY_WINDOWS = "\\\\?\\USB#VID_2458&PID_0001#1#{5f5b9aac-4db5-5aba-89f0-a13651d848c8}";

	/**
	 * The peripheral we test with using *nix style key.
	 */
	private static final String PERIPHERAL_TEST_KEY_NIX = "/devices/pci0000:00/0000:00:06.0/usb2/2-2/2-2:1.0/tty/ttyACM0";

	@BeforeEach
	@Timeout(60)
	void setUp() {
		// fails if execution time exceeds 5 seconds
	}

	@BeforeAll
	public static void setUpClass() throws Exception {
		SystemConfig.initialize();
	}

	@AfterAll
	public static void tearDownClass() throws InterruptedException {
		handler.stopServicesBlocking();
		handler = null;
		instance = null;
	}

	@Test
	@Order(0)
	public void testStartServices() throws Exception {
		handler = ServiceHandler.getInstance();
		handler.startServicesBlocking(
				PiDomeService.HARDWARESERVICE
		);
		instance = new HardwareApiController();
	}

	/**
	 * Test of getHardware method, of class HardwareApiController.
	 */
	@Test
	@DisabledIfSystemProperty(named = "org.pidome.skiptests.hardware", matches = "true")
	public void testGetHardwareOfSerialType() throws Exception {
		List<Peripheral<? extends HardwareDriver>> result = instance.getPeripheralsByTransportType(Transport.SubSystem.SERIAL, "");
		assertThat(result.stream().allMatch(peripheral -> peripheral.getSubSystem().equals(Transport.SubSystem.SERIAL)), is(true));
	}

	/**
	 * Test of getHardware method, of class HardwareApiController.
	 */
	@Test
	@DisabledIfSystemProperty(named = "org.pidome.skiptests.hardware", matches = "true")
	public void testGetHardwareComponents() throws Exception {
		Map<HardwareComponent.Interface, HardwareComponent> result = instance.getHardware();
		assertThat("libudev-dev expected on linux, and reachable registry on windows", result, hasKey(HardwareComponent.Interface.USB));
		List<Peripheral<? extends HardwareDriver>> peripherals = result.get(HardwareComponent.Interface.USB).getConnectedPeripherals();
		assertThat(peripherals.isEmpty(), is(false));
		/// LPT is not implemented, make sure no implementation is for whatever reason created.
		assertThat(result, not(hasKey(HardwareComponent.Interface.LPT)));
	}

	/**
	 * Test of getHardwareComponent method, of class HardwareApiController.
	 */
	@Test
	@DisabledIfSystemProperty(named = "org.pidome.skiptests.hardware", matches = "true")
	public void testGetHardwareComponent() throws Exception {
		HardwareComponent.Interface interfaceKey = HardwareComponent.Interface.USB;
		assertThat(instance.getHardwareComponent(interfaceKey), is(instanceOf(USBDevices.class)));
	}

	/**
	 * Test of getPeripheral method, of class HardwareApiController.
	 */
	@Test
	@DisabledIfSystemProperty(named = "org.pidome.skiptests.hardware", matches = "true")
	public void testGetPeripheral() throws Exception {
		HardwareComponent.Interface interfaceKey = HardwareComponent.Interface.USB;
		HardwareComponent component = instance.getHardwareComponent(interfaceKey);
		Peripheral peripheral = component.getPeripheralByKey(getHardwareDeviceTestKey())
				.orElseThrow(() -> new Exception("Peripheral missing"));
		assertThat(getHardwareDeviceTestKey(), is(equalTo(peripheral.getKey())));
	}

	/**
	 * Test of getPeripheralDriverCandidates method, of class
	 * HardwareApiController.
	 */
	@Test
	@DisabledIfSystemProperty(named = "org.pidome.skiptests.hardware", matches = "true")
	public void testGetPeripheralDriverCandidates() throws Exception {
		HardwareComponent.Interface interfaceKey = HardwareComponent.Interface.USB;
		HardwareComponent component = instance.getHardwareComponent(interfaceKey);
		Peripheral peripheral = component.getPeripheralByKey(getHardwareDeviceTestKey())
				.orElseThrow(() -> new Exception("Peripheral missing"));
		List<DriverDefinition> candidates = instance.getPeripheralDriverCandidates(peripheral.getVolatileId());
		assertThat(candidates.isEmpty(), is(false));
		assertThat("org.pidome.platform.hardware.driver.serial.PiDomeSerialDriver", is(equalTo(candidates.iterator().next().getDriver())));
	}

	/**
	 * Test of getPeripheralSettings method, of class HardwareApiController.
	 */
	@Test
	@DisabledIfSystemProperty(named = "org.pidome.skiptests.hardware", matches = "true")
	public void testGetPeripheralSettings() throws Exception {
		InputForm form = getinputformForTests(getPeripheralDriverCandidateforTests());
		assertThat(form, is(notNullValue()));
	}

	/**
	 * Test setting a device setting.
	 *
	 * @throws Exception
	 */
	@Test
	@SuppressWarnings("unchecked")
	@DisabledIfSystemProperty(named = "org.pidome.skiptests.hardware", matches = "true")
	public void testStartStopDriver(Vertx vertx, VertxTestContext testContext) throws Exception {
		DriverDefinition candidate = getPeripheralDriverCandidateforTests();
		InputForm form = getinputformForTests(candidate);

		final JsonNode configuration = Serialization.getDefaultObjectMapper().readTree(
				Serialization.getDefaultObjectMapper().writeValueAsString(form)
		);

		UUID peripheralId = handler.getService(HardwareService.class).get()
				.getAllHardware().values().stream()
				.flatMap(component -> component.getConnectedPeripherals().stream())
				.filter(peripheral -> peripheral.getKey().equals(getHardwareDeviceTestKey()))
				.findFirst().get().getVolatileId();

		instance.startPeripheralWithSettings(peripheralId,
				candidate.getId(),
				configuration)
				.setHandler(
						testContext.succeeding(startResult -> {
							try {
								instance.stopPeripheralDriver(peripheralId)
										.setHandler(
												testContext.succeeding(stopResult -> {
													testContext.completeNow();
												})
										);
							} catch (HttpStatusCodeException ex) {
								testContext.failNow(ex);
							}
						})
				);
	}

	/**
	 * Returns a testable input form.
	 *
	 * @param candidate
	 * @return
	 * @throws HttpStatusCodeException
	 */
	private InputForm getinputformForTests(DriverDefinition candidate) throws HttpStatusCodeException {

		UUID peripheralId = handler.getService(HardwareService.class).get()
				.getAllHardware().values().stream()
				.flatMap(component -> component.getConnectedPeripherals().stream())
				.filter(peripheral -> peripheral.getKey().equals(getHardwareDeviceTestKey()))
				.findFirst().get().getVolatileId();

		return instance.getPeripheralSettings(peripheralId, candidate.getId());
	}

	/**
	 * Returns a driver candidate for tests.
	 *
	 * @return
	 * @throws HttpStatusCodeException
	 */
	private DriverDefinition getPeripheralDriverCandidateforTests() throws HttpStatusCodeException {
		UUID peripheralId = handler.getService(HardwareService.class).get()
				.getAllHardware().values().stream()
				.flatMap(component -> component.getConnectedPeripherals().stream())
				.filter(peripheral -> peripheral.getKey().equals(getHardwareDeviceTestKey()))
				.findFirst().get().getVolatileId();
		List<DriverDefinition> candidates
				= instance.getPeripheralDriverCandidates(peripheralId);
		assertThat(candidates.isEmpty(), is(false));
		return candidates.iterator().next();
	}

	/**
	 * Returns the key based on the os.
	 *
	 * @return The key for the specific os.
	 */
	private String getHardwareDeviceTestKey() {
		if (PlatformOs.isOs(PlatformOs.OS.WINDOWS)) {
			return PERIPHERAL_TEST_KEY_WINDOWS;
		} else {
			return PERIPHERAL_TEST_KEY_NIX;
		}
	}

}
