/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.installer.packages;

import java.util.List;
import java.util.Map;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.typeCompatibleWith;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.pidome.platform.hardware.driver.HardwareDriver;
import org.pidome.platform.hardware.driver.HardwareDriverDiscovery;
import org.pidome.server.services.PiDomeService;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.system.config.SystemConfig;

/**
 *
 * @author johns
 */
@SuppressWarnings("CPD-START")
@TestMethodOrder(OrderAnnotation.class)
@TestInstance(Lifecycle.PER_CLASS)
public class PackageLoaderTest implements PackageMutationListener {

    private static final String CLASS_OUTSIDE_CLASS_LOADER = "org.pidome.platform.hardware.driver.serial.PiDomeSerialDriver";

    /**
     * The service handler.
     */
    private static ServiceHandler handler;

    @BeforeAll
    public static void setUpClass() throws Exception {
        SystemConfig.initialize();
    }

    @AfterAll
    public static void tearDownClass() throws Exception {
        handler.stopServicesBlocking();
    }

    @Test
    @Order(0)
    public void testStartServices() throws Exception {
        handler = ServiceHandler.getInstance();
        handler.startServicesBlocking(
                PiDomeService.INSTALLERSERVICE
        );
    }

    /**
     * Test of loadPackage method, of class PackageLoader.
     */
    @Test
    @SuppressWarnings("unchecked")
    public void testPackageClassLoader() throws Exception {
        ServerPackage pack = getOffBoundGenericOutsideServerPackage();
        PackageStore.PackageClassLoader loader = new PackageStore.PackageClassLoader(pack.getPackagePath());
        Class.forName("org.pidome.platform.hardware.driver.serial.PiDomeSerialDriver",
                false,
                loader);
    }

    /**
     * Test of loadPackage method, of class PackageLoader.
     *
     * The following steps are taken: - Load package definition from database. -
     */
    @Test
    public void testLoadInstanceFromPackage() throws Exception {
        HardwareDriver driver = loadHardwareDriverFromExternalClassLoader();
        assertThat(CLASS_OUTSIDE_CLASS_LOADER, is(equalTo(driver.getClass().getCanonicalName())));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testUnboundedPackageLoadByClass() throws Exception {

        final ServerPackage pack = getOffBoundGenericOutsideServerPackage();
        final HardwareDriver driver = loadHardwareDriverFromExternalClassLoader();
        final Class driverClass = driver.getClass();

        try (PackageStore.UnboundPackageLoad<HardwareDriver> loader
                = new PackageStore.UnboundPackageLoad<>(pack, driverClass)) {
            assertThat(CLASS_OUTSIDE_CLASS_LOADER, is(equalTo(loader.get().getClass().getCanonicalName())));
        }

        try (PackageStore.UnboundPackageLoad<HardwareDriver> loader
                = new PackageStore.UnboundPackageLoad<>(pack, CLASS_OUTSIDE_CLASS_LOADER)) {
            assertThat(CLASS_OUTSIDE_CLASS_LOADER, is(equalTo(loader.get().getClass().getCanonicalName())));
        }

    }

    @Test
    @SuppressWarnings("unchecked")
    public void testUnboundedPackageLoadByString() throws Exception {

        final ServerPackage pack = getOffBoundGenericOutsideServerPackage();

        try (PackageStore.UnboundPackageLoad<HardwareDriver> loader
                = new PackageStore.UnboundPackageLoad<>(pack, CLASS_OUTSIDE_CLASS_LOADER)) {
            assertThat(CLASS_OUTSIDE_CLASS_LOADER, is(equalTo(loader.get().getClass().getCanonicalName())));
        }

    }

    @Test
    public void testRegisterDeregisterCaller() throws Exception {

        ServerPackage pack = getOffBoundGenericOutsideServerPackage();
        HardwareDriver driver = loadHardwareDriverFromExternalClassLoader();
        Class driverClass = driver.getClass();

        assertThat(PackageStore.registerCaller(this, pack, driverClass), is(equalTo(true)));
        // Second request.
        assertThat(PackageStore.registerCaller(this, pack, driverClass), is(equalTo(false)));
        // Remove so a new request will be true again.
        PackageStore.releaseInstanceFromPackage(this, pack, driver);
        driver = null;
        System.gc();

    }

    @Test
    public void testProvides() throws Exception {
        Map<ServerPackage, List<Class<? extends HardwareDriver>>> result = PackageStore.provides(HardwareDriverDiscovery.class, HardwareDriver.class);
        assertThat(result.isEmpty(), is(equalTo(false)));
        assertThat(result.keySet().iterator().next(), is(instanceOf(ServerPackage.class)));
        assertThat(result.values().isEmpty(), is(equalTo(false)));
        assertThat(result.values().iterator().next().get(0), is(typeCompatibleWith(HardwareDriver.class)));
    }

    @Override
    public void packageRemoved(ServerPackage serverPackage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void packageAdded(ServerPackage serverPackage) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private ServerPackage getOffBoundGenericOutsideServerPackage() {
        ServerPackage pack = new ServerPackage();
        pack.setPackageGroup("org.pidome.platform.hardware.drivers");
        pack.setPackageName("pidome-driver-serial");
        pack.setPackageVersion("1.0-SNAPSHOT");
        return pack;
    }

    @SuppressWarnings("unchecked")
    private HardwareDriver loadHardwareDriverFromExternalClassLoader() throws Exception {

        ServerPackage pack = new ServerPackage();
        pack.setPackageGroup("org.pidome.platform.hardware.drivers");
        pack.setPackageName("pidome-driver-serial");
        pack.setPackageVersion("1.0-SNAPSHOT");

        assertThat(pack, is(notNullValue()));
        HardwareDriver driver = PackageStore.loadInstanceFromPackage(
                this,
                pack,
                (Class<? extends HardwareDriver>) Class.forName(CLASS_OUTSIDE_CLASS_LOADER,
                        false,
                        new PackageStore.PackageClassLoader(pack.getPackagePath())
                )
        );
        return driver;
    }

}
