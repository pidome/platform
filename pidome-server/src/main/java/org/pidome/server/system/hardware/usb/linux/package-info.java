/**
 * Linux USB peripherals.
 * <p>
 * Provides means and methods to connect PiDome to USB devices on Linux OS'es
 * Makes use of the linux generic udevlib.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.system.hardware.usb.linux;
