/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.database.driver;

import java.nio.file.FileSystems;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.h2.tools.Server;
import org.pidome.server.system.config.SystemConfig;
import org.pidome.server.system.database.DatabaseConfiguration;
import org.pidome.server.system.database.DatabaseConfigurationException;
import org.pidome.tools.enums.ResourceType;

/**
 * H2 database loader.
 *
 * @author John Sirach
 */
@SuppressWarnings("CPD-START")
public final class H2 implements DatabaseDriverInterface {

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(H2.class);

    /**
     * Embedded database tcp server for database console.
     *
     * Only available when ran in development mode.
     */
    private Server tcpServer;

    /**
     * Embedded database console.
     *
     * Only available when ran in development mode.
     */
    private Server webServer;

    /**
     * The H2 driver class path.
     */
    private static final String DRIVER_CLASS_PATH = "org.h2.Driver";

    /**
     * H2 dialect.
     */
    private static final String DRIVER_DIALECT = "org.hibernate.dialect.H2Dialect";

    /**
     * Identifier for in the JDBC string.
     */
    private static final String JDBC_IDENTIFIER = "h2";

    /**
     * Loads the specific driver class.
     *
     * @throws DatabaseConfigurationException When the driver class can not be
     * loaded.
     */
    @Override
    public void loadDriver() throws DatabaseConfigurationException {
        try {
            Class.forName(DRIVER_CLASS_PATH);
        } catch (ClassNotFoundException ex) {
            throw new DatabaseConfigurationException(ex);
        }
    }

    /**
     * Returns the dialect used.
     *
     * @return dialect.
     */
    @Override
    public String getDialect() {
        return DRIVER_DIALECT;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJDBCUrl(final DatabaseConfiguration configuration) {
        ResourceType resourceType = configuration.getDriverType().getResourceType();
        StringBuilder jdbcStringBuilder = new StringBuilder("jdbc:")
                .append(JDBC_IDENTIFIER).append(":")
                .append(resourceType.equals(ResourceType.FILESYSTEM) ? "file:" : "mem:")
                .append(configuration.getLocation().orElse(""))
                .append(configuration.getDatabase());
        if (configuration.getJdbcAppend().isPresent()) {
            jdbcStringBuilder.append(";").append(configuration.getJdbcAppend().get());
        }
        return jdbcStringBuilder.toString();
    }

    /**
     * Starts the embedded database console when in development mode.
     *
     * @param configuration The configuration.
     */
    @Override
    public void afterInit(final DatabaseConfiguration configuration) {
        if (SystemConfig.isDevMode()) {
            LOG.warn("Server in development mode, starting embedded database console");
            try {
                tcpServer = Server.createTcpServer().start();
                webServer = Server.createWebServer().start();
                String url;
                if (tcpServer.getURL().contains("\\./")) {
                    url = FileSystems.getDefault().getPath(".").toAbsolutePath().toString() + tcpServer.getURL().replace("\\./", "/").replace(":", "/");
                } else {
                    url = tcpServer.getURL();
                }
                final String dbStartedOnDebug = "Database console server started on [{}]. use [jdbc:h2:{}/{}:{}] to connect in the console";
                if (configuration.getDriverType().equals(DatabaseDriverType.H2)) {
                    LOG.warn(dbStartedOnDebug, webServer.getURL(), url, configuration.getLocation().orElse("[INVALID]"), configuration.getDatabase());
                } else {
                    LOG.warn(dbStartedOnDebug, webServer.getURL(), url, "mem", configuration.getDatabase());
                }
            } catch (Exception ex) {
                LOG.error("Could not start embedded database console server [{}], console not available", ex.getMessage(), ex);
            }
        }
    }

    /**
     * Stops the embedded database console.
     */
    @Override
    public void onShutdown() {
        if (webServer != null) {
            webServer.stop();
        }
        if (tcpServer != null) {
            tcpServer.stop();
        }
    }

}
