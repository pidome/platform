/*
 * Copyright 2013 John Sirach <john.sirach@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pidome.server.system.hardware.usb;

import org.pidome.server.system.hardware.HardwareRoot;
import org.pidome.platform.hardware.driver.Transport.SubSystem;

/**
 * Listener interface for mutations in the USB sub system.
 *
 * The usb key provided by both methods is unique per USB device and is promised
 * to be the same for an event of the same device.
 *
 * @author John Sirach
 * @since 1.0
 */
public interface UsbMutationListener {

    /**
     * MEthod called when there is a change within the USB device connections.
     *
     * @param mutationType The mutation type fired.
     * @param subSystem The subsystem type.
     * @param serial The device serial id.
     * @param deviceName The device name.
     * @param vendorId The vendor id.
     * @param deviceId The device id.
     * @param usbKey The usb key uniquely identifying the device.
     * @param devicePath The path this device is available on.
     * @param devicePort The port this device is available on.
     * @throws UnsupportedUsbTypeException When the given subsystem is
     * unsupported.
     */
    void deviceMutation(HardwareRoot.Mutation mutationType, SubSystem subSystem, String serial, String deviceName, String vendorId, String deviceId, String usbKey, String devicePath, String devicePort) throws UnsupportedUsbTypeException;

}
