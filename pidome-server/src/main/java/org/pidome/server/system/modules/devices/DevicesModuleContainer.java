/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.modules.devices;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.vertx.core.Promise;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.modules.devices.Device;
import org.pidome.platform.modules.devices.DeviceBuilderType;
import org.pidome.platform.modules.devices.DeviceCommandRequest;
import org.pidome.platform.modules.devices.DeviceControl;
import org.pidome.platform.modules.devices.DeviceControlGroup;
import org.pidome.platform.modules.devices.DeviceDataNotification;
import org.pidome.platform.modules.devices.DevicesModule;
import org.pidome.platform.modules.devices.GenericDevice;
import org.pidome.platform.modules.devices.discovery.DiscoveredDevice;
import org.pidome.platform.modules.items.Item.ItemType;
import org.pidome.platform.modules.items.ItemAddress;
import org.pidome.server.entities.items.DiscoveredItemDefinition;
import org.pidome.server.entities.items.ItemCommand;
import org.pidome.server.entities.items.ItemDefinition;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.events.EventAddress;
import org.pidome.server.services.hardware.DriverDefinition;
import org.pidome.server.services.items.ItemService;
import org.pidome.server.services.items.ItemsDiscoveryMutationEvent;
import org.pidome.server.services.items.ItemsMutationEvent;
import org.pidome.server.services.items.ItemsMutationEvent.MutationType;
import org.pidome.server.services.modules.ModuleDefinition;
import org.pidome.server.system.database.Database;
import org.pidome.server.system.database.Database.AutoClosableEntityManager;
import org.pidome.server.system.modules.ActiveModuleContainer;
import org.pidome.server.system.modules.devices.DeviceCommand.DeviceCommandAction;

/**
 * A module container for device based modules.
 *
 * @author johns
 */
@SuppressWarnings("CPD-START")
public class DevicesModuleContainer extends ActiveModuleContainer<DeviceMeta, DevicesModule> {

    /**
     * Class root logger.
     */
    private static final Logger LOG = LogManager.getLogger(DevicesModuleContainer.class);

    /**
     * The devices module.
     */
    private DevicesModule devicesModule;

    /**
     * Array with capabilities.
     */
    private final ProviderCapabilities[] capabilities = {ProviderCapabilities.DISCOVERY};

    /**
     * The devices proxy.
     */
    private DevicesProxy proxy = new DevicesProxy();

    /**
     * Constructor for device modules.
     *
     * @param containerId The container id.
     * @param module The module.
     * @param driver The driver.
     * @param hardwareName The name of the hardware.
     * @param hardwareKey The hardware key.
     */
    public DevicesModuleContainer(final String containerId,
            final ModuleDefinition module,
            final DriverDefinition driver,
            final String hardwareName,
            final String hardwareKey) {
        super(containerId, module, driver, hardwareName, hardwareKey);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ItemType provides() {
        return ItemType.DEVICE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void startRoutines() {
        proxy.setContainer(this);
        this.devicesModule = this.getModule();
        this.devicesModule.setServerProxy(proxy);
        this.registerModuleCapabilities(Arrays.asList(this.devicesModule.capabilities()));
        ServiceHandler.service(ItemService.class).ifPresent(service -> {
            service.linkProviderMutator(this);
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings({"PMD.NullAssignment"}) /// Required for cross references between different class loaders.
    protected void stopRoutines() {
        Set<DeviceMeta> definitionSet = new HashSet<>();
        this.proxy.getDevices().stream().forEach(deviceMeta -> {
            deviceMeta.getItem().shutdownDevice();
            definitionSet.add(deviceMeta);
        });
        if (!definitionSet.isEmpty()) {
            ItemsMutationEvent removeEvent = new ItemsMutationEvent(MutationType.REMOVED, definitionSet);
            this.itemMutation(removeEvent);
        }
        ServiceHandler.service(ItemService.class).ifPresent(service -> {
            service.unlinkProviderMutator(this);
        });
        this.devicesModule.setServerProxy(null);
        if (this.proxy != null) {
            this.proxy.close();
        }
    }

    /**
     * Handles the notification of a newly discovered device.
     *
     * The device type and address is checked against the already known list of
     * devices.
     *
     * @param device The device discovered.
     */
    protected void handleDiscoveredDevice(final DiscoveredDevice device) {
        ServiceHandler.service(ItemService.class).ifPresent(service -> {
            if (!service.getDiscoveredItems().stream()
                    .filter(item -> item.getType().equals(ItemType.DEVICE))
                    .map(item -> (DiscoveredDevice) item.getDiscoveredItem())
                    .anyMatch(discovered
                            -> discovered.getClass().getCanonicalName().equals(device.getClass().getCanonicalName())
                    && discovered.getAddress().getAddress().equals(device.getAddress().getAddress()))) {
                DiscoveredItemDefinition definition = new DiscoveredItemDefinition(
                        UUID.randomUUID(),
                        this,
                        ItemType.DEVICE,
                        device,
                        this.getModuleDefinition().getProvidedBy());
                ItemsDiscoveryMutationEvent event = new ItemsDiscoveryMutationEvent(MutationType.ADDED, Set.of(definition));
                this.discoveryMutation(event);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ProviderCapabilities> capablities() {
        return Arrays.asList(capabilities);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public void addItem(final DiscoveredItemDefinition definition, final DeviceMeta itemMeta) {
        if (definition.getDiscoveredItem() instanceof DiscoveredDevice) {
            try {
                final DeviceMeta deviceMeta = createDefinitionAndLoadDeviceFromDiscovered(definition, itemMeta);
                storeDevice(deviceMeta);
                registerItems(Set.of(deviceMeta));
                ItemsMutationEvent addEvent = new ItemsMutationEvent(MutationType.ADDED, Set.of(deviceMeta));
                this.itemMutation(addEvent);
                ItemsDiscoveryMutationEvent discoveryEvent = new ItemsDiscoveryMutationEvent(MutationType.REMOVED, Set.of(definition));
                this.discoveryMutation(discoveryEvent);
            } catch (Exception ex) {
                LOG.error("Could not create device from given information: {}, {}", definition, itemMeta, ex);
            }
        } else {
            LOG.error("Providing new item to wrong container, item: [{}] container: [{}]", definition, this);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addItem(final DiscoveredItemDefinition discoveredDefinition, final UUID deviceDefinitionId, final DeviceMeta itemMeta) {
        if (discoveredDefinition.getDiscoveredItem() instanceof DiscoveredDevice) {
            try {
                final DeviceMeta deviceMeta = loadDeviceFromDiscoveryAndDefinition(discoveredDefinition, deviceDefinitionId, itemMeta);
                storeDevice(deviceMeta);
                registerItems(Set.of(deviceMeta));
                ItemsMutationEvent addEvent = new ItemsMutationEvent(MutationType.ADDED, Set.of(deviceMeta));
                this.itemMutation(addEvent);
                ItemsDiscoveryMutationEvent discoveryEvent = new ItemsDiscoveryMutationEvent(MutationType.REMOVED, Set.of(discoveredDefinition));
                this.discoveryMutation(discoveryEvent);
            } catch (Exception ex) {
                LOG.error("Could not create device from given information: {}, {}, {}", discoveredDefinition, deviceDefinitionId, itemMeta, ex);
            }
        } else {
            LOG.error("Incorrect mapping used to add item [{}]", discoveredDefinition);
            throw new RuntimeException("Incorrect mapping used to add item " + discoveredDefinition.getDiscoveredItem().getName());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addItem(final ItemDefinition itemDefinition, final DeviceMeta itemMeta) {
        if (itemDefinition instanceof DeviceDefinition) {
            try {
                final DeviceMeta deviceMeta = loadNewDeviceFromDefinitionId(itemDefinition.getId(), itemMeta);
                storeDevice(deviceMeta);
                registerItems(Set.of(deviceMeta));
                ItemsMutationEvent addEvent = new ItemsMutationEvent(MutationType.ADDED, Set.of(deviceMeta));
                this.itemMutation(addEvent);
            } catch (Exception ex) {
                LOG.error("Could not create device from given information: {}, {}", itemDefinition, itemMeta, ex);
            }
        } else {
            LOG.error("Incorrect mapping used to add item [{}]", itemDefinition);
            throw new RuntimeException("Incorrect mapping used to add item " + itemDefinition.getName());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateItem(final DeviceMeta deviceMeta) {
        this.getItems().stream()
                .filter(activeDeviceMeta -> activeDeviceMeta.getId().equals(deviceMeta.getId()))
                .findFirst()
                .ifPresent(activeDeviceMeta -> {
                    activeDeviceMeta.updateGenericMeta(deviceMeta);
                    ItemAddress currentAddress = activeDeviceMeta.getItem().getAddress();
                    currentAddress.setAddressFromString(deviceMeta.getAddress());
                });
    }

    /**
     * Stores devices in the database.
     *
     * @param deviceMeta The device to store.
     * @throws Exception on failure of storing a device with unknown definition.
     */
    @SuppressWarnings("unchecked")
    private void storeDevice(final DeviceMeta deviceMeta) throws Exception {
        try (Database.AutoClosableEntityManager closableManager = Database.getInstance().getNewAutoClosableManager()) {
            EntityManager manager = closableManager.getManager();
            manager.getTransaction().begin();
            manager.persist(deviceMeta);
            manager.getTransaction().commit();
        }
    }

    /**
     * Builds the device structure.
     *
     * @param device The device to build the structure for.
     * @param deviceDefinition The definition of a device.
     */
    @SuppressWarnings("unchecked")
    private void buildDevice(final Device device, final DeviceDefinition deviceDefinition) {
        if (deviceDefinition.getDeviceParameters() != null) {
            device.setParameters(deviceDefinition.getDeviceParameters());
        } else {
            LOG.warn("An instance of device [{}] being loaded without parameters", device.getName());
        }
        switch (device.getDeviceBuilderType()) {
            case BUILDER:
                Device.DeviceStructureBuilder builder = new Device.DeviceStructureBuilder();
                ((GenericDevice) device).build(builder);
                device.setGroups(builder.build());
                break;
            case PROVIDED:
            case USER:
                device.setGroups(deviceDefinition.getDeviceGroups());
                break;
            default:
                throw new UnsupportedOperationException("Build implementation should be one of " + Arrays.toString(DeviceBuilderType.values()));
        }
    }

    /**
     * Returns the devices in the module.
     *
     * @return The devices.
     */
    @Override
    public Set<DeviceMeta> getItems() {
        return this.proxy.getDevices();
    }

    /**
     * Loads all the items from the database which this container supports.
     */
    @Override
    public final void loadItems() {
        Set<DeviceMeta> loadedMetas = new HashSet<>();
        Set<DeviceMeta> metaDefinitions = new HashSet<>();
        try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {

            CriteriaBuilder cb = autoManager.getManager().getCriteriaBuilder();
            CriteriaQuery<DeviceMeta> cq = cb.createQuery(DeviceMeta.class);
            Root<DeviceMeta> rootEntry = cq.from(DeviceMeta.class);

            Join<DeviceMeta, DeviceDefinition> definitionJoin = rootEntry.join("definition", JoinType.INNER);
            definitionJoin.on(
                    cb.equal(rootEntry.get("definition"), definitionJoin.get("id"))
            );

            CriteriaQuery<DeviceMeta> select = cq.select(rootEntry);
            select.where(
                    cb.equal(definitionJoin.get("moduleDefinition"), this.getModuleDefinition())
            );

            try {
                metaDefinitions.addAll(autoManager.getManager().createQuery(select).getResultList()
                        .stream().collect(Collectors.toSet()));
            } catch (NoResultException nr) {
                LOG.warn("Module [{}] has no devices registered, this is no problem if this is true", this.getModule());
            }
        }
        if (!metaDefinitions.isEmpty()) {
            metaDefinitions.stream().forEach(meta -> {
                try {
                    this.loadDevice(meta);
                    loadedMetas.add(meta);
                    LOG.info("Loaded device [{}]", meta.getName());
                } catch (Exception ex) {
                    LOG.error("Device [{}] not loaded because of error", meta, ex);
                }
            });
            if (!loadedMetas.isEmpty()) {
                registerItems(loadedMetas);
            }
            ServiceHandler.service(ItemService.class).ifPresent(service -> {
                service.notifyItemsLoaded();
            });
        }
    }

    /**
     * Unloads an active item.
     *
     * @param deviceMeta The item to unload.
     */
    @Override
    public void unloadItem(final DeviceMeta deviceMeta) {
        unloadDevice(deviceMeta);
    }

    /**
     * Returns a list of definitions known.
     *
     * @return Item definiction list for devices.
     */
    @Override
    @JsonIgnore
    @SuppressWarnings({"unchecked"})
    public <ID extends ItemDefinition> Collection<ID> getItemDefinitions() {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            Collection<DeviceDefinition> items = autoManager.getManager().createQuery("SELECT did FROM DeviceDefinition did", DeviceDefinition.class).getResultList();
            items.stream()
                    .forEach(itemDefinition -> {
                        itemDefinition.setContainerId(this.getId());
                    });
            return (Collection<ID>) items;
        }
    }

    /**
     * Reloads the given item if this is a device item.
     *
     * @param deviceMeta The device item to be reloaded.
     * @return Promise with future indicating reloading succeeded or failed.
     */
    @Override
    public final Promise<Void> reloadItem(final DeviceMeta deviceMeta) {
        Promise<Void> reloadPromise = Promise.promise();
        this.unloadDevice(deviceMeta);
        try {
            this.loadDevice(deviceMeta);
        } catch (Exception ex) {
            LOG.error("Failed to load device", ex);
            reloadPromise.fail(ex);
        }
        return reloadPromise;
    }

    /**
     * Unloads a device.
     *
     * @param deviceMeta The device to unload.
     */
    private void unloadDevice(final DeviceMeta deviceMeta) {
        Set<DeviceMeta> definitionSet = new HashSet<>();
        this.proxy.getDevices().stream()
                .filter(deviceItem -> deviceItem.getId().equals(deviceMeta.getId()))
                .findFirst()
                .ifPresent(deviceItem -> {
                    definitionSet.add(deviceItem);
                });
        if (!definitionSet.isEmpty()) {
            DeviceMeta shutdownDevice = definitionSet.iterator().next();
            this.proxy.getDevices().remove(shutdownDevice);
            shutdownDevice.getItem().shutdownDevice();
            ItemsMutationEvent removeEvent = new ItemsMutationEvent(MutationType.REMOVED, definitionSet);
            this.itemMutation(removeEvent);
        }
    }

    /**
     * Loads a device by it's stored meta data.
     *
     * This is the preferred method to load a device.
     *
     * @param meta The device's meta data.
     * @throws Exception on load failure.
     */
    @SuppressWarnings({"unchecked"})
    private void loadDevice(final DeviceMeta meta) throws Exception {
        DeviceDefinition definition = meta.getDefinition();
        Device device = loadDevice(definition);
        buildDevice(device, definition);
        ItemAddress address = definition.getAddress();
        address.setAddressFromString(meta.getAddress());
        device.setAddress(address);
        meta.setItem(device);
    }

    /**
     * Loads a device from the generic definition.
     *
     * @param definition The definition to load the device from.
     * @return The loaded device.
     * @throws Exception On failure of device load.
     */
    @SuppressWarnings({"unchecked", "PMD.UseProperClassLoader"}) /// This is not J2EE, we want the class loader from the module.
    private Device loadDevice(final DeviceDefinition definition) throws Exception {
        Device device = loadDeviceInstance((Class<Device>) this.devicesModule.getClass().getClassLoader().loadClass(definition.getItemClass()));
        return device;
    }

    /**
     * Loads a device from a discovery definition.
     *
     * This method should be called when a device is added via discovery and no
     * definition exists yet, or has not been chosen.
     *
     * @param discoveredDefinition The discovery definition.
     * @param itemMeta The item info to supplement the definition.
     * @return A loaded device.
     * @throws Exception On failure of device load.
     */
    @SuppressWarnings("unchecked")
    private DeviceMeta createDefinitionAndLoadDeviceFromDiscovered(final DiscoveredItemDefinition discoveredDefinition, final DeviceMeta itemMeta) throws Exception {
        final DiscoveredDevice discoveredDevice = (DiscoveredDevice) discoveredDefinition.getDiscoveredItem();
        final Device device = loadDeviceInstance(discoveredDevice.getDevice());

        final DeviceDefinition definition = new DeviceDefinition();
        definition.setDeviceParameters(discoveredDevice.getParameters());
        definition.setItemClass(device.getClass().getCanonicalName());
        definition.setModuleDefinition(this.getModuleDefinition());
        definition.setName(itemMeta.getName());
        definition.setDescription(itemMeta.getDescription());
        definition.setBuilderType(device.getDeviceBuilderType());
        definition.setAddress(discoveredDevice.getAddress());

        itemMeta.setAddress(discoveredDevice.getAddressAsString());
        itemMeta.setDefinition(definition);
        itemMeta.setItem(device);

        buildDevice(device, definition);
        definition.setDeviceGroups(device.getGroups());
        ItemAddress address = definition.getAddress();
        address.setAddressFromString(itemMeta.getAddress());
        device.setAddress(address);

        return itemMeta;
    }

    /**
     * Creates and loads a device into meta from a discovered device with given
     * definition and item information.
     *
     * @param discoveredDefinition The discovery definition.
     * @param deviceDefinitionId The item definition to bind the discovered
     * device to.
     * @param itemMeta The item information to assign to the device.
     * @throws Exception on failure of loading from discovery with a device
     * definition id.
     *
     * @return The device meta including the device.
     */
    @SuppressWarnings({"unchecked"})
    private DeviceMeta loadDeviceFromDiscoveryAndDefinition(
            final DiscoveredItemDefinition discoveredDefinition, final UUID deviceDefinitionId, final DeviceMeta itemMeta
    ) throws Exception {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            final DiscoveredDevice discoveredDevice = (DiscoveredDevice) discoveredDefinition.getDiscoveredItem();
            final DeviceDefinition definition = autoManager.getManager().find(DeviceDefinition.class, deviceDefinitionId);
            final Device device = loadDevice(definition);

            itemMeta.setAddress(discoveredDevice.getAddressAsString());
            itemMeta.setDefinition(definition);
            itemMeta.setItem(device);

            buildDevice(device, definition);
            definition.setDeviceGroups(device.getGroups());
            ItemAddress address = definition.getAddress();
            address.setAddressFromString(itemMeta.getAddress());
            device.setAddress(address);

            return itemMeta;

        }
    }

    /**
     * Creates device meta from the supplied device definition and information.
     *
     * @param deviceDefinition The uuid of the device definition of the device
     * to add.
     * @param itemMeta The information about the device to add.
     * @return Device meta to store in the database.
     * @throws Exception When loading from definition id fails with the given
     * item info.
     */
    @SuppressWarnings({"unchecked"})
    private DeviceMeta loadNewDeviceFromDefinitionId(final UUID deviceDefinition, final DeviceMeta itemMeta) throws Exception {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            final DeviceDefinition definition = autoManager.getManager().find(DeviceDefinition.class, deviceDefinition);
            final Device device = loadDevice(definition);

            itemMeta.setDefinition(definition);
            itemMeta.setItem(device);

            buildDevice(device, definition);
            ItemAddress address = definition.getAddress();
            address.setAddressFromString(itemMeta.getAddress());
            device.setAddress(address);

            return itemMeta;

        }
    }

    /**
     * Loads a device instance from class reference.
     *
     * @param deviceClass The device class.
     * @return The instantiated device.
     * @throws Exception On instantiation error.
     */
    private Device loadDeviceInstance(final Class<Device> deviceClass) throws Exception {
        Device device = deviceClass.getConstructor().newInstance();
        return device;
    }

    /**
     * Registers items in the proxy.
     *
     * @param devices The devices to add.
     */
    public void registerItems(final Set<DeviceMeta> devices) {
        proxy.addDevices(devices);
    }

    /**
     * Removes items from the proxy.
     *
     * @param devices the devices to remove.
     */
    public void deregisterItems(final Set<DeviceMeta> devices) {
        proxy.removeDevices(devices);
    }

    /**
     * Sends a data notification.
     *
     * @param notification The notification to be send.
     */
    @SuppressWarnings("unchecked")
    protected void dispatchDataNotification(final DeviceDataNotification notification) {
        ServiceHandler.getInstance().getService(ItemService.class).ifPresent(service -> {
            this.proxy.getDevices().stream()
                    .filter(deviceMeta -> deviceMeta.getItem().getAddress().getAddress().equals(notification.getAddress().getAddress()))
                    .findFirst().ifPresent(deviceMeta -> {

                        /// Update the device controls from the notification.
                        notification.getGroupData().stream().forEach(groupData -> {
                            groupData.getControlData().forEach(controlData -> {
                                deviceMeta.getItem().getGroup(groupData.getId()).ifPresent(deviceGroup -> {
                                    ((DeviceControlGroup) deviceGroup).getControl(controlData.getId()).ifPresent(control -> {
                                        ((DeviceControl) control).setValue(controlData.getValue());
                                    });
                                });
                            });
                        });

                        notification.setDeviceId(deviceMeta.getId());
                        /// Do not send groups which are hidden.
                        notification.getGroupData().removeIf(group -> deviceMeta.getItem().getGroups().stream()
                                .filter(searchGroup -> {
                                    return ((DeviceControlGroup) searchGroup).getId().equals(((DeviceDataNotification.GroupData) group).getId())
                                            && ((DeviceControlGroup) searchGroup).isHidden();
                                }).findFirst().isPresent());
                        /// Do not send to controls which are hidden.
                        notification.getGroupData().stream().forEach(groupData -> {
                            groupData.getControlData().removeIf(controlData -> {
                                return deviceMeta.getItem().getGroups().stream().flatMap(controlGroup -> ((DeviceControlGroup) controlGroup).getControls().stream())
                                        .filter(deviceControl -> {
                                            return ((DeviceControl) deviceControl).getId().equals(controlData.getId())
                                                    && ((DeviceControl) deviceControl).isHidden();
                                        }).findFirst().isPresent();
                            });

                        });
                        service.sendItemNotification(ItemType.DEVICE, EventAddress.DEVICES, notification);
                    });
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public void executeItemCommand(final UUID itemId, final ItemCommand itemCommand, final Promise<Void> executePromise) {
        this.getItems().stream()
                .filter(device -> device.getId().equals(itemId))
                .findFirst().ifPresentOrElse(deviceMeta -> {
                    final Device device = deviceMeta.getItem();
                    final DeviceCommandAction command = ((DeviceCommand) itemCommand).getAction();
                    device.getGroup(command.getGroup())
                            .ifPresentOrElse(group -> {
                                DeviceControlGroup controlGroup = (DeviceControlGroup) group;
                                controlGroup.getControl(command.getControl())
                                        .ifPresentOrElse(control -> {
                                            DeviceControl deviceControl = (DeviceControl) control;

                                            DeviceCommandRequest request = new DeviceCommandRequest(
                                                    device,
                                                    deviceControl,
                                                    command.getValue());

                                            /// Cheating, normally only one future is possible.
                                            executePromise.future().compose(result -> {
                                                return Promise.succeededPromise().future();
                                            }).setHandler(notificationHandler -> {
                                                if (notificationHandler.succeeded()) {
                                                    DeviceDataNotification notification = new DeviceDataNotification(deviceMeta.getItem().getAddress());
                                                    notification.setDeviceId(itemId);
                                                    notification.addData(controlGroup.getId(), deviceControl.getId(), command.getValue());
                                                    dispatchDataNotification(notification);
                                                }
                                            });

                                            device.handleDeviceCommand(devicesModule, request, executePromise);

                                        }, () -> executePromise.fail(new Exception("Device control not found to perform action.")));
                            }, () -> executePromise.fail(new Exception("Device control group not found to perform action.")));
                }, () -> executePromise.fail(new Exception("No device present to handle the action")));
    }

}
