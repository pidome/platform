/**
 * Implementation for device module container.
 * <p>
 * Includes the module container and all related classes for a module container
 * to integrate within pidome.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.system.modules.devices;
