/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.meta.manufacturer;

import java.util.UUID;
import org.pidome.server.system.database.Database;

/**
 * Provides manufacturer info.
 *
 * @author johns
 */
public class ManufacturerProvider {

    /**
     * Returns a manufacturer by it's id.
     *
     * @param id The id of the manufacturer.
     * @return The manufacturer based on the id.
     */
    public Manufacturer getManufacturer(final UUID id) {
        try (Database.AutoClosableEntityManager manager = Database.getInstance().getNewAutoClosableManager()) {
            return manager.getManager().find(Manufacturer.class, id);
        }
    }

}
