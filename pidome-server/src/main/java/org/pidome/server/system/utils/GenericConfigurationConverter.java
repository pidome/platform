/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.NullNode;
import java.io.IOException;
import javax.persistence.AttributeConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.tools.utilities.Serialization;

/**
 * Encapsulating class used for JsonNode to provide a Serializable for raw json
 * nodes.
 *
 * @author johns
 */
public class GenericConfigurationConverter implements AttributeConverter<JsonNode, String> {

    /**
     * Usb device logger.
     */
    private static final Logger LOG = LogManager.getLogger(GenericConfigurationConverter.class);

    /**
     * convert to string.
     *
     * @param configuration The configuration to convert.
     * @return The configuration string.
     */
    @Override
    public String convertToDatabaseColumn(final JsonNode configuration) {
        String customerInfoJson = null;
        try {
            customerInfoJson = Serialization.getDefaultObjectMapper().writeValueAsString(configuration);
        } catch (final JsonProcessingException e) {
            LOG.error("JSON configuration writing error", e);
        }
        return customerInfoJson;
    }

    /**
     * convert to Json Node.
     *
     * @param configurationInfoJson The configuration string to convert.
     * @return The configuration object.
     */
    @Override
    public JsonNode convertToEntityAttribute(final String configurationInfoJson) {
        JsonNode configuration = null;
        try {
            configuration = Serialization.getDefaultObjectMapper().readValue(configurationInfoJson, JsonNode.class);
            if (configuration instanceof NullNode) {
                return null;
            }
        } catch (final IOException e) {
            LOG.error("JSON configuration reading error", e);
        }
        return configuration;
    }

}
