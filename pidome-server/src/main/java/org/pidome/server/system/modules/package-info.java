/**
 * System package for modules.
 * <p>
 * Package supplying modules, module store and (un)loading.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.system.modules;
