/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.modules.devices;

import org.pidome.server.entities.items.ItemCommand;
import org.pidome.server.system.modules.devices.DeviceCommand.DeviceCommandAction;

/**
 * A command to be executed by a device module.
 *
 * @author johns
 */
public class DeviceCommand extends ItemCommand<DeviceCommandAction> {

    /**
     * The command action of the device command.
     */
    public static class DeviceCommandAction {

        /**
         * The group of the action.
         */
        private String group;

        /**
         * The control of the action.
         */
        private String control;

        /**
         * The value of the action.
         */
        private Object value;

        /**
         * The group of the action.
         *
         * @return the group
         */
        public String getGroup() {
            return group;
        }

        /**
         * The group of the action.
         *
         * @param group the group to set
         */
        public void setGroup(final String group) {
            this.group = group;
        }

        /**
         * The control of the action.
         *
         * @return the control
         */
        public String getControl() {
            return control;
        }

        /**
         * The control of the action.
         *
         * @param control the control to set
         */
        public void setControl(final String control) {
            this.control = control;
        }

        /**
         * The value of the action.
         *
         * @return the value
         */
        public Object getValue() {
            return value;
        }

        /**
         * The value of the action.
         *
         * @param value the value to set
         */
        public void setValue(final Object value) {
            this.value = value;
        }

    }

}
