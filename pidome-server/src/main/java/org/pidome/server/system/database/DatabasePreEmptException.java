/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.database;

/**
 * Exception used when the database service is preparing access to the DAO's.
 *
 * @author John
 */
public class DatabasePreEmptException extends Exception {

    /**
     * Class version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>DatabasePreEmptException</code> without
     * detail message.
     */
    public DatabasePreEmptException() {
        //Default constructor.
    }

    /**
     * Constructs an instance of <code>DatabasePreEmptException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public DatabasePreEmptException(final String msg) {
        super(msg);
    }
}
