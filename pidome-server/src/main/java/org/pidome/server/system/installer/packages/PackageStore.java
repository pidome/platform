/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.installer.packages;

import io.github.classgraph.ClassGraph;
import io.github.classgraph.ScanResult;
import io.vertx.core.Promise;
import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.system.config.SystemConfig;
import org.pidome.server.system.database.Database;
import org.pidome.server.system.database.DatabaseUtils;

/**
 * Class responsible to load packages for the server.
 *
 * @author johns
 * @todo Move from systemclassloader to platformclassloader with only provided
 * dependencies.
 */
public final class PackageStore {

    /**
     * Private constructor, utility class.
     */
    private PackageStore() {
        /// utility class.
    }

    /**
     * Map keeping track of package users.
     */
    private static final Map<ServerPackage, Map<PackageMutationListener, Set<Class<?>>>> PACKAGE_USERS = Collections.synchronizedMap(new HashMap<>());

    /**
     * Parser to get the maven name and version.
     */
    private static final Pattern MAVEN_NAME_VERSION = Pattern.compile("^(.+?)-(\\d.*?)\\.jar$");

    /**
     * Usb device logger.
     */
    private static final Logger LOG = LogManager.getLogger(PackageStore.class);

    /**
     * Loads a class from a package and registers the caller as user.
     *
     * <p>
     * The caller is internally registered as an user of the package for two
     * reasons:
     *
     * <p>
     * <li>When a package is removed or updated the caller must release any
     * reference to a package because the class loader of the package needs to
     * be unloaded. The caller gets notified of package unloading intent.
     * <li>The store keeps track how many users are using a package and will
     * unload/release it after the last class has called unload.
     * <p>
     * The caller releases a reference by calling
     * <code>unloadInstanceFromPackage</code>
     *
     * <p>
     * If you need an unbounded one shot package load, please use the class
     * <code>PackageStore.UnboundPackageLoad</code> which provides a non
     * referenced load of a driver and supports <code>AutoClosable</code>.
     *
     * @param <T> The expected class to load into object.
     * @param caller The caller for this method.
     * @param serverPackage The package to load from.
     * @param classToLoad The class to load from the package.
     * @return The object of type T.
     * @throws PackageClassLoaderException When the package is unable to be
     * loaded.
     */
    @SuppressWarnings({"unchecked", "PMD.CloseResource"})
    public static <T> T loadInstanceFromPackage(final PackageMutationListener caller, final ServerPackage serverPackage, final Class<T> classToLoad) throws PackageClassLoaderException {
        PrivilegedExceptionAction<T> priviledged = () -> {
            try {
                T returnObject = loadClassInstance(serverPackage, classToLoad);
                registerCaller(caller, serverPackage, classToLoad);
                return returnObject;
            } catch (PackageClassLoaderException ex) {
                throw new PrivilegedActionException(ex);
            }
        };
        try {
            return AccessController.doPrivileged(priviledged);
        } catch (PrivilegedActionException ex) {
            throw new PackageClassLoaderException("Unable to load class [" + classToLoad.getCanonicalName() + "] from package [" + serverPackage + "]", ex);
        }
    }

    /**
     * Closes the package class loader from the given class.
     *
     * It's important the same class reference used in
     * <code>loadInstanceFromPackage</code> is
     *
     * @param klass the class from which the package class loader should be
     * closed.
     */
    @SuppressWarnings({"PMD.UseProperClassLoader"})
    public static void unloadClass(final Class<?> klass) {
        ClassLoader loader = klass.getClassLoader();
        try {
            if (loader instanceof PackageClassLoader) {
                ((PackageStore.PackageClassLoader) loader).close();
            } else if (loader instanceof URLClassLoader) {
                ((URLClassLoader) loader).close();
                LOG.warn("Package has not been loaded with the PackageClassLoader, but still able to close the class loader", loader.getName());
            } else {
                LOG.error("Package has not been loaded with the PackageClassLoader, but with [{}], this could cause leakage", loader.getName());
            }
        } catch (IOException ex) {
            LOG.error("Error during unloading of package class loader", ex);
        }
    }

    /**
     * Class intended to load a package without holding references to it.
     *
     * <p>
     * Instantiate this class with the same parameters as you would use
     * <code>loadInstanceFromPackage</code>. Use this class in a
     * <code>try (withresources) {}</code> block so it applies the
     * <code>AutoClosable</code> feature to close the package.
     *
     * <p>
     * The class is put in an cleanup queue and the class loader used to supply
     * the package will definitely be closed after a certain amount of time.
     * When <code>Close()</code> is called the class loader will be closed and
     * <code>get()</code> will return null.
     *
     * <p>
     * Usage example:
     * <p>
     * <blockquote><pre>{@code
     * try(PackageStore.UnboundPackageLoad<T> loader = new PackageStore.UnboundPackageLoad<>()){
     *     T object = loader.get();
     * } catch (PackageClassLoaderException ex){
     *     //Handle the exception.
     * }
     * }</pre></blockquote>
     *
     * @param <T> The type that is extended to be loaded unbounded.
     */
    @SuppressWarnings({"unchecked", "PMD.CloseResource"})
    public static final class UnboundPackageLoad<T> implements AutoCloseable {

        /**
         * The object instantiated of the given T type.
         */
        private T returnObject;

        /**
         * the class loaded in the unbound loader.
         */
        private Class<T> loadedClass;

        /**
         * Constructor loading the package.
         *
         * @param serverPackage The package to load from.
         * @param classToLoad The class to load from the package.
         * @throws PackageClassLoaderException When package loading fails.
         */
        public UnboundPackageLoad(final ServerPackage serverPackage, final Class<? extends T> classToLoad) throws PackageClassLoaderException {
            this(serverPackage, classToLoad.getCanonicalName());
        }

        /**
         * Constructor loading the package.
         *
         * @param serverPackage The package to load from.
         * @param classToLoad The canonical name of the class to load from the
         * package.
         * @throws PackageClassLoaderException When package loading fails.
         */
        public UnboundPackageLoad(final ServerPackage serverPackage, final String classToLoad) throws PackageClassLoaderException {
            this(serverPackage, classToLoad, false);
        }

        /**
         * Constructor loading the package.
         *
         * when the onlyClass is set to true, use getClass to return the class.
         *
         * @param serverPackage The package to load from.
         * @param classToLoad The canonical name of the class to load from the
         * package.
         * @param onlyClass To only load the class, and not instantiate it.
         * @throws PackageClassLoaderException When loading fails.
         */
        public UnboundPackageLoad(final ServerPackage serverPackage, final String classToLoad, final boolean onlyClass) throws PackageClassLoaderException {
            if (serverPackage == null) {
                throw new PackageClassLoaderException("Server package is required");
            }
            if (classToLoad == null) {
                throw new PackageClassLoaderException("Class to load is required");
            }
            try {
                loadedClass = (Class<T>) loadWithPackageClassLoader(serverPackage)
                        .loadClass(classToLoad);
                if (!onlyClass) {
                    returnObject = (T) loadedClass
                            .getConstructors()[0]
                            .newInstance();
                }
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                throw new PackageClassLoaderException(ex);
            }
        }

        /**
         * @param serverPackage The package to load from.
         * @return The Package class loader with all correct package paths.
         * @throws PackageClassLoaderException When package loading fails.
         */
        @SuppressWarnings("PMD.PreserveStackTrace")
        private PackageClassLoader loadWithPackageClassLoader(final ServerPackage serverPackage) throws PackageClassLoaderException {
            PrivilegedExceptionAction<PackageClassLoader> priviledged = () -> {
                try {
                    return new PackageClassLoader(serverPackage.getPackagePath());
                } catch (PackageClassLoaderException ex) {
                    throw new PrivilegedActionException(ex);
                }
            };
            try {
                return AccessController.doPrivileged(priviledged);
            } catch (PrivilegedActionException ex) {
                throw new PackageClassLoaderException(ex.getCause().getCause());
            }
        }

        /**
         * The object instantiated from the given package.
         *
         * @return The object instantiated of type T.
         */
        public Class<T> getLoadedClass() {
            return this.loadedClass;
        }

        /**
         * The object instantiated from the given package.
         *
         * @return The object instantiated of type T.
         */
        public T get() {
            return this.returnObject;
        }

        /**
         * Closes the package loaded.
         *
         * If an <code>IOException</code> is thrown, the <code>get()</code>
         * object still returns null.
         *
         * @throws IOException When cloasing of the class loader fails.
         */
        @Override
        @SuppressWarnings({"PMD.NullAssignment", "PMD.UseProperClassLoader"})
        public void close() throws IOException {
            PackageClassLoader loader = (PackageClassLoader) this.loadedClass.getClassLoader();
            this.returnObject = null;
            this.loadedClass = null;
            loader.close();
        }

    }

    /**
     * Loads the class instance from a package using the
     * <code>PackageClassLoader</code>.
     *
     * @param <T> The type to load.
     * @param serverPackage The server package to load from.
     * @param classToLoad The class to load of given T type
     * @return Object loaded of type T from the given class.
     * @throws PackageClassLoaderException When the class instance can not be
     * loaded.
     */
    @SuppressWarnings("unchecked")
    private static <T> T loadClassInstance(final ServerPackage serverPackage, final Class<T> classToLoad) throws PackageClassLoaderException {
        return loadClassInstance(serverPackage, classToLoad.getCanonicalName());
    }

    /**
     * Loads the class instance from a package using the
     * <code>PackageClassLoader</code>.
     *
     * @param <T> The type to load.
     * @param serverPackage The server package to load from.
     * @param classToLoad The class to load of given T type
     * @return Object loaded of type T from the given class.
     * @throws PackageClassLoaderException When the class instance can not be
     * loaded.
     */
    @SuppressWarnings("unchecked")
    public static <T> T loadClassInstance(final ServerPackage serverPackage, final String classToLoad) throws PackageClassLoaderException {
        try {
            return (T) loadClass(serverPackage, classToLoad).getConstructors()[0].newInstance();
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            throw new PackageClassLoaderException(ex);
        }
    }

    /**
     * Loads the class instance from a package using the
     * <code>PackageClassLoader</code>.
     *
     * When this method is called from outside the package store, you are solely
     * responsible for closing the class loader by yourself.
     *
     * @param <T> The type to load.
     * @param serverPackage The server package to load from.
     * @param classToLoad The class to load of given T type
     * @return Object loaded of type T from the given class.
     * @throws PackageClassLoaderException When the class instance can not be
     * loaded.
     */
    @SuppressWarnings("unchecked")
    private static <T> Class<T> loadClass(final ServerPackage serverPackage, final String classToLoad) throws PackageClassLoaderException {
        try {
            return (Class<T>) Class.forName(classToLoad,
                    true,
                    new PackageClassLoader(serverPackage.getPackagePath()));
        } catch (ClassNotFoundException | IllegalArgumentException ex) {
            throw new PackageClassLoaderException(ex);
        }
    }

    /**
     * Registers the caller as user of the package with the given class.
     *
     * This method should only be called when a successful object has been
     * created and is ready to be returned to the caller. The return value
     * should only be interpreted as an hint, check your code if false is
     * returned as your reference then already existed.
     *
     * @param caller The caller object.
     * @param serverPackage The server package requested.
     * @param loadedClass The class loaded from the package.
     * @return Returns true when the reference is added, returns false when the
     * reference already existed.
     */
    protected static boolean registerCaller(final PackageMutationListener caller, final ServerPackage serverPackage, final Class<?> loadedClass) {
        if (!PACKAGE_USERS.containsKey(serverPackage)) {
            PACKAGE_USERS.put(serverPackage, Collections.synchronizedMap(new HashMap<>()));
        }
        if (!PACKAGE_USERS.get(serverPackage).containsKey(caller)) {
            PACKAGE_USERS.get(serverPackage).put(caller, Collections.synchronizedSet(new HashSet<>()));
        }
        return PACKAGE_USERS.get(serverPackage).get(caller).add(loadedClass);
    }

    /**
     * Unloads an instantiated class.
     *
     * @param caller The caller of this method.
     * @param serverPackage The server package the class is retrieved from.
     * @param loadedObjectFromClass The object instantiated from the class used
     * in <code>loadInstanceFromPackage</code> to be loaded.
     */
    @SuppressWarnings("PMD.UseProperClassLoader")
    public static void releaseInstanceFromPackage(final PackageMutationListener caller, final ServerPackage serverPackage, final Object loadedObjectFromClass) {
        if (PACKAGE_USERS.containsKey(serverPackage) && PACKAGE_USERS.get(serverPackage).containsKey(caller)) {
            PACKAGE_USERS.get(serverPackage).get(caller).remove(loadedObjectFromClass.getClass());
            try {
                ((PackageClassLoader) loadedObjectFromClass.getClass().getClassLoader()).close();
            } catch (IOException ex) {
                LOG.warn("An error occured while closing class loader for [{}], possible memory leak", loadedObjectFromClass, ex);
            }
            if (PACKAGE_USERS.get(serverPackage).get(caller).isEmpty()) {
                PACKAGE_USERS.get(serverPackage).remove(caller);
            }
        }
        if (PACKAGE_USERS.containsKey(serverPackage) && PACKAGE_USERS.get(serverPackage).isEmpty()) {
            PACKAGE_USERS.remove(serverPackage);
        }
    }

    /**
     * Scanner for packages installed.
     *
     * When this method is ran for the first time it will scan the modules
     * folder and store the result.
     *
     * @return A list of packages installed.
     * @throws java.io.IOException When scanning fails.
     */
    @SuppressWarnings("checkstyle:MagicNumber")
    public static List<ServerPackage> collect() throws IOException {
        try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            if (DatabaseUtils.getRowCount(autoManager.getManager(), ServerPackage.class) == 0) {
                List<ServerPackage> packages = new ArrayList<>();
                Path walkPath = new File(SystemConfig.getModulesPath()).toPath();
                SystemConfig.UrlPaths paths = new SystemConfig.UrlPaths();
                Files.walkFileTree(walkPath, paths);
                paths.getLibrariesList().stream().forEach(url -> {
                    String path = url.getPath();
                    String[] pathItems = path.split("/");
                    if (pathItems.length > 0 && pathItems[pathItems.length - 1].startsWith(pathItems[pathItems.length - 2])) {
                        Matcher nameMatcher = MAVEN_NAME_VERSION.matcher(pathItems[pathItems.length - 1]);
                        if (nameMatcher.matches()) {
                            ServerPackage serverPackage = new ServerPackage();
                            serverPackage.setPackageName(nameMatcher.group(1));
                            serverPackage.setPackageVersion(nameMatcher.group(2));
                            serverPackage.setPackageGroup(pathItems[pathItems.length - 3]);
                            packages.add(serverPackage);
                        }
                    }
                });
                autoManager.getManager().getTransaction().begin();
                packages.stream().forEach(serverPackage -> {
                    LOG.info("Registering package [{}]", serverPackage);
                    autoManager.getManager().persist(serverPackage);
                });
                autoManager.getManager().getTransaction().commit();
                return packages;
            } else {
                CriteriaBuilder cb = autoManager.getManager().getCriteriaBuilder();
                CriteriaQuery<ServerPackage> cq = cb.createQuery(ServerPackage.class);
                Root<ServerPackage> rootEntry = cq.from(ServerPackage.class);
                CriteriaQuery<ServerPackage> all = cq.select(rootEntry);

                TypedQuery<ServerPackage> allQuery = autoManager.getManager().createQuery(all);
                return allQuery.getResultList();
            }
        }
    }

    /**
     * Provides packages with classes containing the given annotation of the
     * given type.
     *
     * This method returns a map with server packages as key with the list of
     * classes directly callable from that package using
     * <code>loadFromPackage</code>
     *
     * @param <T> The type being expected in the return.
     * @param annotation The annotation to search for.
     * @param expectedType The expected type to return in the resulting
     * annotation search.
     * @return Classes of the expected type with the given annotation.
     */
    public static <T> Map<ServerPackage, List<Class<? extends T>>> provides(final Class<? extends Annotation> annotation, final Class<? extends T> expectedType) {
        Map<ServerPackage, List<Class<? extends T>>> returnMap = new HashMap<>();
        try {
            List<ServerPackage> packages = collect();
            packages.stream().forEach(serverPackage -> {
                List<Class<? extends T>> foundClasses = getClassesWithAnnotationFromPackage(serverPackage, annotation, expectedType, null);
                if (!foundClasses.isEmpty()) {
                    returnMap.put(serverPackage, foundClasses);
                }
            });
        } catch (IOException ex) {
            LOG.error("Unable to gather packages to search in", ex);
        }
        return returnMap;
    }

    /**
     * Scans a server package for it's classes.
     *
     * @param <T> The to be expected to return, this filters the list.
     * @param serverPackage The server package to scan.
     * @param annotation The annotation to look for.
     * @param expectedType The Expected type to return.
     * @param promise The promise to determine if the scan was successful and
     * done
     * @return List of classes from the given package with the given annotation
     * and of type expected.
     */
    @SuppressWarnings("unchecked")
    public static <T> List<Class<? extends T>> getClassesWithAnnotationFromPackage(final ServerPackage serverPackage, final Class<? extends Annotation> annotation, final Class<? extends T> expectedType, final Promise promise) {
        final List<Class<? extends T>> foundClasses = new ArrayList<>();
        AccessController.doPrivileged((PrivilegedAction<Void>) () -> {
            final SingleJarClassLoader classLoader = new SingleJarClassLoader(serverPackage.getPackagePath());
            try (ScanResult scanResult
                    = new ClassGraph()
                            .addClassLoader(classLoader)
                            .whitelistJars(new File(serverPackage.getPackagePath()).getName())
                            .ignoreParentClassLoaders()
                            .enableAnnotationInfo()
                            .disableRuntimeInvisibleAnnotations()
                            .scan()) {
                        scanResult.getClassesWithAnnotation(annotation.getCanonicalName()).stream().forEach(matchedClass -> {
                            Class<? extends T> typedClass = (Class<? extends T>) matchedClass.loadClass();
                            if (expectedType.isAssignableFrom(typedClass)) {
                                /// Is it a bug? Need to load annoations explicitly otherwise the annotation info get's lost.
                                typedClass.getAnnotations();
                                if (LOG.isDebugEnabled()) {
                                    LOG.debug("Found annotations on loaded class [{}]: [{}]", typedClass, Arrays.asList(typedClass.getAnnotations()));
                                }
                                foundClasses.add(typedClass);
                            }
                        });
                        if (promise != null) {
                            promise.complete();
                        }
                    } catch (Exception ex) {
                        if (promise == null) {
                            LOG.error("An error occured during package [{}] search", serverPackage, ex);
                        } else {
                            promise.fail(ex);
                        }
                    }
                    return null;
        });
        return foundClasses;
    }

    /**
     * Special class loader for a single jar.
     */
    protected static class SingleJarClassLoader extends URLClassLoader {

        /**
         * Construct the class loader.
         *
         * @param jarPath The path to the jar file.
         * @throws MalformedURLException When the jar path is illegal.
         */
        SingleJarClassLoader(final String jarPath) {
            super(pathToUrl(SystemConfig.getModulesPath() + jarPath), ClassLoader.getSystemClassLoader());
        }

        /**
         * Constructs a one dimension URL array for the jar file.
         *
         * @param path The path to the jar file.
         * @return The URL with the path to the jar file.
         * @throws MalformedURLException When the url of the jar file is
         * malformed.
         */
        private static URL[] pathToUrl(final String path) {
            try {
                URL[] urls = new URL[1];
                urls[0] = new URL("file://" + new File(path).getAbsolutePath());
                return urls;
            } catch (MalformedURLException ex) {
                LOG.error("Unable to load [{}]", path, ex);
                return new URL[0];
            }
        }

    }

    /**
     * Class loader used to load packages with their dependencies.
     */
    protected static class PackageClassLoader extends URLClassLoader {

        /**
         * Constructor.
         *
         * @param basePath The base path from where the class loader should
         * traverse to include jars for the module to be loaded.
         * @throws PackageClassLoaderException Thrown when walking the possible
         * module paths fail.
         */
        PackageClassLoader(final String basePath) throws PackageClassLoaderException {
            super(new URL[0], ClassLoader.getSystemClassLoader());
            try {
                SystemConfig.UrlPaths paths = new SystemConfig.UrlPaths();
                Files.walkFileTree(new File(SystemConfig.getModulesPath() + basePath).toPath().getParent(), paths);
                paths.getLibrariesList().stream().forEach(url -> {
                    addURL(url);
                });
            } catch (IOException ex) {
                throw new PackageClassLoaderException("Unable to load packes from base path [" + basePath + "]", ex);
            }
        }

        /**
         * Method to add urls to the class loader paths list.
         *
         * @param url The url to add.
         */
        @Override
        protected void addURL(final URL url) {
            super.addURL(url);
        }
    }

}
