/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.modules;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.modules.ModuleBase;
import org.pidome.platform.modules.ModuleCapabilities;
import static org.pidome.platform.modules.ModuleCapabilities.DISCOVERY;
import org.pidome.platform.modules.discovery.ItemDiscovery;
import org.pidome.server.entities.items.DiscoveredItemDefinition;
import org.pidome.server.entities.items.ItemDefinition;
import org.pidome.server.entities.items.ItemMeta;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.events.EventService;
import org.pidome.server.services.events.EventSeverity;
import org.pidome.server.services.events.EventType;
import org.pidome.server.services.hardware.DriverDefinition;
import org.pidome.server.services.hardware.HardwareService;
import org.pidome.server.services.items.ItemService;
import org.pidome.server.services.items.ItemsDiscoveryMutationEvent;
import org.pidome.server.services.items.ItemsDiscoveryMutationListener;
import org.pidome.server.services.items.ItemsMutationEvent;
import org.pidome.server.services.items.ItemsMutationEvent.MutationType;
import org.pidome.server.services.items.ItemsMutationListener;
import org.pidome.server.services.modules.DiscoveryException;
import org.pidome.server.services.modules.DiscoveryPeriod;
import org.pidome.server.services.modules.ModuleDefinition;
import org.pidome.server.services.modules.ModuleEventProducer;
import org.pidome.server.system.items.ItemProvider;
import org.pidome.tools.datetime.DateTimeUtil;
import org.pidome.tools.properties.BooleanPropertyBinding;

/**
 * A container for an active module.
 *
 * An active module container acts as a proxy between the actual module and the
 * server. The implementation is responsible for providing correct
 * <code>Items</code> to the server's Item handler and a proxy to provide to
 * modules.
 *
 * @author johns
 * @param <I> The item type being handled by the module container.
 * @param <T> The type of module in the container.
 */
@SuppressWarnings("CPD-START")
public abstract class ActiveModuleContainer<I extends ItemMeta, T extends ModuleBase> implements ItemProvider {

    /**
     * Class root logger.
     */
    private final Logger instanceLogger = LogManager.getLogger(this.getClass());

    /**
     * One second.
     */
    private static final int SECOND = 1000;

    /**
     * The id of the container.
     */
    private final String id;

    /**
     * Definition of the active module.
     */
    private ModuleDefinition moduleDefinition;

    /**
     * The module capabilities.
     */
    private List<ModuleCapabilities> moduleCapabilities;

    /**
     * The driver definition for this module.
     */
    private DriverDefinition driverDefinition;

    /**
     * Friendly name of the peripheral this module is active on.
     */
    private final String peripheralName;

    /**
     * Friendly name of the peripheral this module is active on.
     */
    @JsonIgnore
    private String peripheralKey;

    /**
     * The moment the container was created, counted as start time for a module.
     */
    private final String started;

    /**
     * The module in the container.
     */
    @JsonIgnore
    private T module;

    /**
     * The data link in the container binding the module to prosumers.
     */
    @JsonIgnore
    private DataLink dataLink;

    /**
     * Listener for mutations.
     */
    @JsonIgnore
    private ItemsMutationListener listener;

    /**
     * Listener for discovered items.
     */
    @JsonIgnore
    private ItemsDiscoveryMutationListener discoveredListener;

    /**
     * The discovery timer for when discovery should end.
     */
    @JsonIgnore
    private Timer discoveryTimer;

    /**
     * The current discovery task, when a discovery is enabled.
     */
    @JsonIgnore
    private DiscoveryDisableTask discoveryTask;

    /**
     * If currently discovering.
     */
    private boolean discovering = false;

    /**
     * Property for tracking discovery enabling.
     */
    @JsonIgnore
    private final BooleanPropertyBinding discoveryEnabled = new BooleanPropertyBinding(Boolean.FALSE);

    /**
     * If a container is in process of stopping.
     *
     * <p>
     * Container stopping occurs in a loop with other services. This to ensure
     * all dependencies are always handled. Two solutions can be applied:
     * <ul>
     * <li>Introduce a new method which tells the handlers to ignore
     * recursion.</li>
     * <li>Have an atomic boolean keeping track of currently being stopped.</li>
     * </ul>
     * <p>
     * The second option has been chosen as multiple services will be able to
     * stop depending modules. Otherwise all services would have a recursive
     * boolean applied which can not be guaranteed and extra checks are needed
     * and possibly extra code written in drivers and all module types
     * increasing complexity.
     * <p>
     * This atomic boolean supports unloading in 1:1, 1:N and N:1 relations
     * depending on other module implementations.
     */
    @JsonIgnore
    private final AtomicBoolean stopping = new AtomicBoolean(Boolean.FALSE);

    /**
     * Constructor for a container.
     *
     * @param containerId The otf id for the container.
     * @param module The definition of the module in the container.
     * @param driver The definition of the driver in the container.
     * @param hardwareName The name of the peripheral used.
     * @param hardwareKey The key of the peripheral used.
     */
    protected ActiveModuleContainer(final String containerId,
            final ModuleDefinition module,
            final DriverDefinition driver,
            final String hardwareName,
            final String hardwareKey) {
        this.id = containerId;
        this.moduleDefinition = module;
        this.driverDefinition = driver;
        this.started = DateTimeUtil.getLongDateTime();
        this.peripheralName = hardwareName;
        this.peripheralKey = hardwareKey;
    }

    /**
     * Stops the containing module.
     *
     * @return Future containing the result.
     */
    protected final Future<Void> stop() {
        stopping.compareAndSet(Boolean.FALSE, Boolean.TRUE);
        Promise<Void> returnPromise = Promise.promise();
        Promise<Void> stopPromise = Promise.promise();
        instanceLogger.info("Stopping module [{}]", this.getModule().getModuleInfo().getTitle());
        stopPromise.future().setHandler(stopFinished -> {
            if (stopFinished.succeeded()) {
                instanceLogger.info("Module [{}] stopped, cancelling running discovery if active", this.getModule().getModuleInfo().getTitle());
                cancelDiscovery();
                this.discoveryEnabled.removeListener(this::handleDiscoveryNotification);
                try {
                    this.dataLink.close();
                } catch (IOException ex) {
                    instanceLogger.warn("Unable to correctly close the datalink between [{}] and [{}], this will cause a memory leak", this.moduleDefinition.getName(), this.driverDefinition.getName(), ex);
                }
                ServiceHandler.service(ItemService.class).ifPresent(service -> {
                    service.clearDiscoveredItems(this);
                });
                ServiceHandler.service(HardwareService.class).ifPresentOrElse(service -> {
                    service.getPeripheralByKey(peripheralKey).ifPresent(peripheral -> {
                        if (peripheral.getDriver() != null) {
                            service.stopDriver(peripheralKey).setHandler(whenFinished -> {
                                returnPromise.handle(whenFinished);
                            });
                        } else {
                            instanceLogger.info("Module [{}] stopped without active driver", this.getModule().getModuleInfo().getTitle());
                            returnPromise.complete();
                        }
                    });
                }, () -> {
                    instanceLogger.warn("Module stopped, but not sure about the hardware, check hardware page.");
                    returnPromise.complete();
                });
            } else {
                returnPromise.fail(stopFinished.cause());
            }
        });
        this.module.stopModule(stopPromise);
        return returnPromise.future();
    }

    /**
     * Returns true if the container is currently stopping.
     *
     * @return True when stopping.
     */
    protected final boolean isStopping() {
        return this.stopping.get();
    }

    /**
     * Removes references to ling living objects able to be garbage collected.
     */
    @SuppressWarnings({"PMD.NullAssignment"})
    protected final void clearReferences() {
        this.module = null;
        this.dataLink = null;
        this.driverDefinition = null;
        this.moduleDefinition = null;
    }

    /**
     * Set the mutation handler.
     *
     * @param mutationListener The listener for item mutations.
     */
    @Override
    public void mutationHandler(final ItemsMutationListener mutationListener) {
        this.listener = mutationListener;
    }

    /**
     * Publish an items mutation event.
     *
     * @param event The event to publish.
     */
    protected final void itemMutation(final ItemsMutationEvent event) {
        if (this.listener != null) {
            this.listener.mutated(event);
        }
    }

    /**
     * Sets a listener for discovered items.
     *
     * @param discoveryListener The discovered items listener.
     */
    @Override
    public void discoveryMutationHandler(final ItemsDiscoveryMutationListener discoveryListener) {
        this.discoveredListener = discoveryListener;
    }

    /**
     * Called when a mutation is done in discovery.
     *
     * @param event The mutation event.
     */
    protected final void discoveryMutation(final ItemsDiscoveryMutationEvent event) {
        instanceLogger.info("Got discovery event: [{}]", event);
        if (this.discoveredListener != null && discoveryEnabled.getValue()) {
            this.discoveredListener.discoveryMutated(event);
            if (this.discoveryTask != null && this.discoveryTask.getPeriodType().equals(DiscoveryPeriod.Type.ONE_SHOT)) {
                this.cancelDiscovery();
            }
        } else if (this.discoveredListener != null && event.getType().equals(MutationType.REMOVED)) {
            this.discoveredListener.discoveryMutated(event);
        }
    }

    /**
     * The id of the container.
     *
     * @return the id
     */
    public final String getId() {
        return id;
    }

    /**
     * Definition of the active module.
     *
     * @return the definition
     */
    public final ModuleDefinition getModuleDefinition() {
        return moduleDefinition;
    }

    /**
     * Definition of the active module.
     *
     * @return the definition
     */
    public final DriverDefinition getDriverDefinition() {
        return driverDefinition;
    }

    /**
     * The moment the container was created, counted as start time for a module.
     *
     * @return the started
     */
    public final String getStarted() {
        return started;
    }

    /**
     * The module in the container.
     *
     * @return the module
     */
    public final T getModule() {
        return module;
    }

    /**
     * The module in the container.
     *
     * @param module the module to set
     */
    public final void setModule(final T module) {
        this.module = module;
        if (this.module instanceof ItemDiscovery) {
            this.discoveryTimer = new Timer();
            this.discoveryEnabled.addListener(this::handleDiscoveryNotification);
        }
    }

    /**
     * Handles discovery notifications.
     *
     * @param newValue The new value.
     * @param oldValue The previous value.
     */
    private void handleDiscoveryNotification(final boolean newValue, final boolean oldValue) {
        if (newValue != oldValue) {
            this.discovering = newValue;
            instanceLogger.info("Publishing discovery [{}], to module [{}], track{} as type [{}]",
                    newValue ? "enabled" : "disabled",
                    this.getModuleDefinition().getName(),
                    newValue ? "ing" : "ed",
                    this.discoveryTask != null ? this.discoveryTask.getPeriodType() : "Unknown");
            ((ItemDiscovery) this.module).setDiscoveryEnabled(newValue);
        } else {
            instanceLogger.warn("Discovery publish not send as there is no state change");
        }
    }

    /**
     * The data link in the container binding the module to prosumers.
     *
     * @return the dataLink
     */
    public final DataLink getDataLink() {
        return dataLink;
    }

    /**
     * The data link in the container binding the module to prosumers.
     *
     * @param dataLink the dataLink to set
     */
    public final void setDataLink(final DataLink dataLink) {
        this.dataLink = dataLink;
    }

    /**
     * Friendly name of the peripheral this module is active on.
     *
     * @return the peripheralName
     */
    public final String getPeripheralName() {
        return peripheralName;
    }

    /**
     * Friendly name of the peripheral this module is active on.
     *
     * @return the peripheralKey
     */
    public final String getPeripheralKey() {
        return peripheralKey;
    }

    /**
     * Friendly name of the peripheral this module is active on.
     *
     * @param peripheralKey the peripheralKey to set
     */
    public final void setPeripheralKey(final String peripheralKey) {
        this.peripheralKey = peripheralKey;
    }

    /**
     * Registration of module capabilities.
     *
     * @param capabilities The capabilities to register.
     */
    protected void registerModuleCapabilities(final List<ModuleCapabilities> capabilities) {
        this.moduleCapabilities = capabilities;
    }

    /**
     * Returns the capabilities of the module.
     *
     * @return The list of available capabilities.
     */
    public List<ModuleCapabilities> getModuleCapabilities() {
        return moduleCapabilities;
    }

    /**
     * Starts discovery when supported by this module.
     *
     * @param discoveryOptions The options for discovery.
     * @throws DiscoveryException When discovery is unsupported or already
     * started.
     */
    public void startDiscovery(final DiscoveryPeriod discoveryOptions) throws DiscoveryException {
        if (this.discoveryTimer != null && !this.discoveryEnabled.getValue()
                && this.moduleCapabilities.stream()
                        .filter(capability
                                -> capability.equals(DISCOVERY)
                        ).findFirst().isPresent()) {
            final ItemDiscovery discoveryModule = (ItemDiscovery) this.module;
            Promise<Void> promise = Promise.promise();
            this.discoveryTask = new DiscoveryDisableTask(discoveryOptions, promise);
            this.discoveryTimer.schedule(this.discoveryTask, discoveryOptions.getPeriodLength() * SECOND);
            this.discoveryEnabled.setValue(Boolean.TRUE);
            discoveryModule.discover(this.discoveryTask.getPromise());
            final String discoveryMessage = new StringBuilder("Discovery started on ")
                    .append(this.getModuleDefinition().getName())
                    .append(" for ")
                    .append(discoveryOptions.getLength().getValue()).append(" ")
                    .append(discoveryOptions.getUnit().getValue())
                    .toString();
            sendNotification(EventSeverity.TRIVIAL, EventType.NOTIFY, discoveryMessage);
            instanceLogger.info(discoveryMessage);
        } else {
            if (this.discoveryTimer == null) {
                throw new DiscoveryException("discovery not supported");
            } else if (this.discoveryEnabled.getValue()) {
                throw new DiscoveryException("Discovery already running");
            }
        }
    }

    /**
     * Cancels a discovery run.
     */
    public void cancelDiscovery() {
        if (this.discoveryTask != null && !this.discoveryTask.isRunning()) {
            this.discoveryTask.setRunning();
            this.discoveryTask.cancel();
            this.discoveryEnabled.setValue(Boolean.FALSE);
            if (!discoveryTask.getPromise().future().isComplete()) {
                discoveryTask.getPromise().complete();
            }
            sendNotification(EventSeverity.TRIVIAL, EventType.NOTIFY, "Discovery ended on " + this.getModuleDefinition().getName());
        }
    }

    /**
     * Returns if discovering.
     *
     * @return If discovery is currently active.
     */
    public boolean isDiscovering() {
        return this.discovering;
    }

    /**
     * Private class for discovery cancellation.
     */
    private final class DiscoveryDisableTask extends TimerTask {

        /**
         * the discovery type.
         */
        private final Promise<Void> promise;

        /**
         * If cancellation is currently running.
         */
        private final AtomicBoolean discoveryCancelIsRunning = new AtomicBoolean(Boolean.FALSE);

        /**
         * The period type.
         */
        private final DiscoveryPeriod.Type periodType;

        /**
         * constructor.
         *
         * @discoveryType The type of discovery set.
         * @discoveryPromise The promise if set.
         *
         * @param period The period.
         * @param discoveryPromise promise for success or failure.
         */
        DiscoveryDisableTask(final DiscoveryPeriod period, final Promise<Void> discoveryPromise) {
            this.promise = discoveryPromise;
            this.periodType = period.getType();
        }

        /**
         * Marks the cancel task running.
         */
        private void setRunning() {
            this.discoveryCancelIsRunning.compareAndSet(false, true);
        }

        /**
         * Returns the period type.
         *
         * @return The type of period.
         */
        private DiscoveryPeriod.Type getPeriodType() {
            return this.periodType;
        }

        /**
         * run the disabling.
         */
        @Override
        public void run() {
            if (!isRunning()) {
                ActiveModuleContainer.this.cancelDiscovery();
            }
        }

        /**
         * Returns false when cancellation is currently not running.
         *
         * @return true when running, otherwise false.
         */
        private boolean isRunning() {
            return discoveryCancelIsRunning.getAcquire();
        }

        /**
         * Returns the promise set.
         *
         * @return The promise.
         */
        public Promise<Void> getPromise() {
            return this.promise;
        }

    }

    /**
     * Sends a module notification.
     *
     * @param severity The severity of the notification.
     * @param type The type of notification.
     * @param message The message for the notification
     */
    private void sendNotification(final EventSeverity severity, final EventType type, final String message) {
        ServiceHandler.service(EventService.class).ifPresent(service -> {
            ModuleEventProducer producer = new ModuleEventProducer();
            producer.setEvent(severity, type, message, null);
            service.publishEvent(producer);
        });
    }

    /**
     * Starts the routines in a purposed module.
     */
    protected abstract void startRoutines();

    /**
     * Stops the routines in a purposed module.
     */
    protected abstract void stopRoutines();

    /**
     * Load the module items.
     *
     * Called when the routines are successfully started.
     */
    protected abstract void loadItems();

    /**
     * Returns a list of item definitions of the module.
     *
     * @param <ID> The type being returned.
     * @return The list of definitions in the module.
     */
    public abstract <ID extends ItemDefinition> Collection<ID> getItemDefinitions();

    /**
     * Adds an item using discover data.
     *
     * @param itemDefinition The discovery definition of the item to add.
     * @param itemMeta The additional info to add the item.
     */
    public abstract void addItem(DiscoveredItemDefinition itemDefinition, I itemMeta);

    /**
     * Add an item by it's known definition.
     *
     * @param discoveredDefinition The discovered item definition.
     * @param itemDefinition The id of the item definition.
     * @param itemMeta The additional information to add the item.
     */
    public abstract void addItem(DiscoveredItemDefinition discoveredDefinition, UUID itemDefinition, I itemMeta);

    /**
     * Adds an item using definition data.
     *
     * @param itemDefinition The definition of the item to add.
     * @param itemMeta The additional info to add the item.
     */
    public abstract void addItem(ItemDefinition itemDefinition, I itemMeta);

    /**
     * Returns the live items available in the module.
     *
     * @return The module items.
     */
    public abstract Set<I> getItems();

    /**
     * Updates a running item.
     *
     * Meta updating does not update the definition.
     *
     * @param itemMeta The meta information to update the item with.
     */
    public abstract void updateItem(I itemMeta);

    /**
     * Reloads an item with the given item meta.
     *
     * @param itemMeta The item to be reloaded.
     * @return Promise with a future indicating the item has been reloaded.
     */
    public abstract Promise<Void> reloadItem(I itemMeta);

    /**
     * Unloads an item active.
     *
     * @param itemMeta The item to unload.
     */
    public abstract void unloadItem(I itemMeta);

}
