/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.hardware.usb.windows.providers;

import java.lang.reflect.InvocationTargetException;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Collection of registry information providers.
 *
 * @author johns
 */
public enum WindowsUsbProvider {

    /**
     * Basic modem provider, used for SMS gateways on mobile or SMS modems.
     */
    USBSER(GenericSerialProvider.class),
    /**
     * FTDI based serial devices.
     */
    FTDIBUS(FtdiProvider.class);

    /**
     * The logger for this class.
     */
    private final Logger log = LogManager.getLogger(WindowsUsbProvider.class);

    /**
     * The provider class to support identifying a connected device.
     */
    private Optional<WindowsUsbRegistryProvider> registryProvider;

    /**
     * Constructor for the enum.
     *
     * @param registryProvider The provider which the enum must supply.
     */
    WindowsUsbProvider(final Class<? extends WindowsUsbRegistryProvider> registryProvider) {
        try {
            this.registryProvider = Optional.of((WindowsUsbRegistryProvider) registryProvider.getConstructors()[0].newInstance());
        } catch (ArrayIndexOutOfBoundsException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            log.error("Unable to provide registry provider for service type [{}]", this, ex);
            this.registryProvider = Optional.empty();
        }
    }

    /**
     * Returns the registry provider.
     *
     * @return The registry provider.
     */
    public final Optional<WindowsUsbRegistryProvider> getRegistryProvider() {
        return this.registryProvider;
    }

}
