/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.modules;

import com.fasterxml.jackson.databind.JsonNode;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Promise;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.hardware.driver.HardwareDriver;
import org.pidome.platform.modules.CommunicationAwareModule;
import org.pidome.platform.modules.ModuleBase;
import org.pidome.platform.modules.ModuleType;
import org.pidome.platform.presentation.input.InputForm;
import org.pidome.server.entities.items.ItemMeta;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.events.EventService;
import org.pidome.server.services.events.EventSeverity;
import org.pidome.server.services.events.EventType;
import org.pidome.server.services.hardware.DriverDefinition;
import org.pidome.server.services.hardware.HardwareService;
import org.pidome.server.services.http.presentation.PresentationCompiler;
import org.pidome.server.services.installer.InstallerService;
import org.pidome.server.services.modules.ModuleDefinition;
import org.pidome.server.services.modules.ModuleEventProducer;
import org.pidome.server.services.modules.ModuleException;
import org.pidome.server.services.modules.ModuleLoaderConfiguration;
import org.pidome.server.system.database.Database;
import org.pidome.server.system.database.Database.AutoClosableEntityManager;
import org.pidome.server.system.hardware.Peripheral;
import org.pidome.server.system.installer.packages.ServerPackage;
import org.pidome.server.system.modules.devices.DevicesModuleContainer;

/**
 * Base class to load modules.
 *
 * Different module types have different loaders based on the module types.
 *
 * @author johns
 */
public final class ModuleLoader {

    /**
     * Class root logger.
     */
    private static final Logger LOG = LogManager.getLogger(ModuleLoader.class);

    /**
     * The module store containing the known modules.
     */
    private ModuleStore moduleStore;

    /**
     * Map of current active modules.
     */
    private final List<ActiveModuleContainer> runningModules = Collections.synchronizedList(new ArrayList<>());

    /**
     * Initializes the module loader.
     *
     * @return Future indicating the loading result.
     */
    public Future<Void> init() {
        moduleStore = new ModuleStore();
        return moduleStore.scan();
    }

    /**
     * Returns a list of modules filtered by given type.
     *
     * @param <T> The type to filter on.
     * @param devicesModule The class of type to filter on.
     * @return List of modules filtered by type.
     */
    public <T extends ModuleBase> List<ModuleDefinition> getModulesFilterByType(final Class<T> devicesModule) {
        return moduleStore.getModuleCollectionByImplementation(devicesModule);
    }

    /**
     * Returns all modules.
     *
     * @return All modules.
     */
    public Map<ModuleType, List<ModuleDefinition>> getAllModules() {
        return moduleStore.getModuleCollection().stream().collect(
                Collectors.groupingBy(module -> module.getModuleType())
        );
    }

    /**
     * Returns a list of all active modules.
     *
     * @return active modules list.
     */
    public List<ActiveModuleContainer> getActiveModules() {
        return this.runningModules;
    }

    /**
     * Returns true if a module definition is valid.
     *
     * @param definition The definition.
     * @return true when valid.
     */
    public boolean moduleExists(final ModuleDefinition definition) {
        return moduleStore.moduleValid(definition);
    }

    /**
     * Returns an optional with a module definition.
     *
     * @param moduleId The module id.
     * @return Optional with optionally a module definition.
     */
    public Optional<ModuleDefinition> getModuleDefinitionById(final UUID moduleId) {
        return getAllModules().values().stream()
                .flatMap(list -> list.stream())
                .filter(module -> module.getId().equals(moduleId))
                .findFirst();
    }

    /**
     * Loads persisted configurations.
     */
    @SuppressFBWarnings(value = {"DLS_DEAD_LOCAL_STORE"}, justification = "configuration list is prefilled and used outside the entitymanager")
    public void loadPersistedConfigurations() {
        List<ModuleLoaderConfiguration> configurationList = new ArrayList<>();
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            CriteriaBuilder builder = autoManager.getManager().getCriteriaBuilder();
            CriteriaQuery<ModuleLoaderConfiguration> criteria = builder.createQuery(ModuleLoaderConfiguration.class);
            criteria.from(ModuleLoaderConfiguration.class);

            configurationList = autoManager.getManager().createQuery(criteria).getResultList();
        }
        if (!configurationList.isEmpty()) {
            configurationList.forEach(configuration -> {
                startModule(configuration);
            });
        }
    }

    /**
     * Starts a module.
     *
     * When a module is started it persists it's configuration to survive
     * restarts. When a module fails to start, and was persisted it will be
     * removed from the database.
     *
     * @param configuration The configuration to start a module.
     * @return The result of starting a module.
     */
    public Future<ActiveModuleContainer> startModule(final ModuleLoaderConfiguration configuration) {
        Promise<ActiveModuleContainer> startPromise = Promise.promise();
        try {
            final ModuleDefinition moduleDefinition = getModuleDefinitionById(configuration.getModuleId()).orElseThrow(() -> new ModuleException("Module with given id [" + configuration.getModuleId() + "] does not exist"));
            if (configuration.getPeripheralKey() != null && configuration.getDriverId() != null) {
                final HardwareService hardwareService = ServiceHandler.service(HardwareService.class)
                        .orElseThrow(() -> new ModuleException("Hardware service not available"));
                final Peripheral peripheral = hardwareService.getPeripheralByKey(configuration.getPeripheralKey())
                        .orElseThrow(() -> new ModuleException("Peripheral with given key [" + configuration.getPeripheralKey() + "] is not connected or is not turned on"));
                final DriverDefinition driverDefinition = hardwareService.getDriverDefinitionById(configuration.getDriverId())
                        .orElseThrow(() -> new ModuleException("Driver definition with given id [" + configuration.getPeripheralKey() + "] does not exist"));
                startPeripheralConfiguredModule(
                        moduleDefinition,
                        configuration.getModuleConfiguration(),
                        peripheral,
                        driverDefinition,
                        configuration.getDriverConfiguration()
                ).setHandler(ifStarted -> {
                    startPromise.handle(ifStarted);
                    if (!ifStarted.succeeded()) {
                        moduleStore.removePersistedModuleConfiguration(configuration);
                    }
                });
            } else {
                throw new ModuleException("Module configuration given is not supported");
            }
        } catch (ModuleException ex) {
            startPromise.fail(ex);
        }
        return startPromise.future();
    }

    /**
     * Starts a module which should attach to a peripheral with a configuration.
     *
     * @param moduleDefinition The module definition.
     * @param moduleConfiguration The module configuration.
     * @param peripheral The peripheral to bind to.
     * @param driverDefinition The driver for the peripheral.
     * @param driverConfiguration The configuration for the peripheral driver.
     * @return Future if starting succeeded.
     * @throws ModuleException On module load failure.
     */
    @SuppressWarnings("unchecked")
    private Future<ActiveModuleContainer> startPeripheralConfiguredModule(
            final ModuleDefinition moduleDefinition,
            final JsonNode moduleConfiguration,
            final Peripheral peripheral,
            final DriverDefinition driverDefinition,
            final JsonNode driverConfiguration) throws ModuleException {

        Promise<ActiveModuleContainer> completedPromise = Promise.promise();

        HardwareService hardwareService = ServiceHandler.service(HardwareService.class)
                .orElseThrow(() -> new ModuleException("No hardware service available for hardware based module"));

        hardwareService.startDriver(
                peripheral,
                driverDefinition,
                driverConfiguration
        ).setHandler(hardwareStarted -> {
            if (hardwareStarted.succeeded()) {
                loadModule(moduleDefinition, moduleConfiguration).setHandler(moduleLoadResult -> {
                    if (moduleLoadResult.succeeded()) {
                        try {
                            final ActiveModuleContainer moduleContainer = getNewPurposedContainer(
                                    UUID.randomUUID().toString(),
                                    moduleDefinition,
                                    driverDefinition,
                                    peripheral.getFriendlyName(),
                                    peripheral.getKey()
                            );
                            ModuleBase module = moduleLoadResult.result();
                            if (module instanceof CommunicationAwareModule) {
                                createModulePeripheralDataLink(moduleContainer,
                                        (CommunicationAwareModule) module,
                                        hardwareStarted.result()
                                );
                            }
                            Promise<Void> startPromise = Promise.promise();
                            startPromise.future().setHandler(startPromiseResult -> {
                                if (startPromiseResult.succeeded()) {
                                    moduleContainer.setModule(module);
                                    moduleContainer.startRoutines();
                                    this.runningModules.add(moduleContainer);
                                    sendNotification(EventSeverity.TRIVIAL, EventType.NOTIFY, "Module " + moduleContainer.getModuleDefinition().getName() + " started");
                                    completedPromise.complete(moduleContainer);
                                    LOG.info("Module [{}] has started on [{}] with [{}]", moduleDefinition.getName(), peripheral.getFriendlyName(), driverDefinition.getName());
                                    new Thread(() -> {
                                        try {
                                            moduleContainer.loadItems();
                                        } catch (Exception ex) {
                                            LOG.error("An unhandled exception occurred during the loading of the items of module [{}]", moduleContainer.getModuleDefinition(), ex);
                                        }
                                    }).start();
                                } else {
                                    completedPromise.fail(startPromiseResult.cause());
                                    LOG.warn("Seems like the module [{}] did not start correctly, stopping", moduleDefinition.getName());
                                    moduleContainer.stop().setHandler((Handler<AsyncResult<Void>>) stopResult -> {
                                        if (stopResult.failed()) {
                                            LOG.warn("Stopped container with errors", stopResult.cause());
                                        } else {
                                            LOG.warn("Container stopped after failed start");
                                        }
                                    });
                                    LOG.error("Module [{}] failed to started on [{}] with [{}]", moduleDefinition.getName(), peripheral.getFriendlyName(), driverDefinition.getName());
                                }
                            });
                            module.startModule(startPromise);
                        } catch (Exception ex) {
                            LOG.error("Unable to load/link module [{}] and driver [{}]. Stopping implementations", moduleLoadResult.result(), hardwareStarted.result(), ex);
                            hardwareService.stopDriver(peripheral);
                            completedPromise.fail(ex);
                        }
                    } else {
                        LOG.error("Module [{}] failed to load, stopping hardware driver", moduleDefinition, moduleLoadResult.cause());
                        hardwareService.stopDriver(peripheral);
                        completedPromise.fail(moduleLoadResult.cause());
                    }
                });
            } else {
                LOG.error("Driver [{}] for module [{}] failed to load", driverDefinition.getDriver(), moduleDefinition, hardwareStarted.cause());
                completedPromise.fail(hardwareStarted.cause());
            }
        });
        return completedPromise.future();
    }

    /**
     * Loads a module container specific for the module container type.
     *
     * Modules are generic, no need to explicitly specify the module type as of
     * this moment.
     *
     * @param containerId The container id.
     * @param module The definition of the module to load.
     * @param driver The definition of the driver.
     * @param hardwareName The name of the hardware on which the driver relies
     * and module uses.
     * @param hardwareKey The key of the hardware the module relies on.
     * @return A module container specific for the module type.
     * @throws ModuleLoaderException When a specific module type specified by
     * the module definition an not be found.
     */
    private ActiveModuleContainer<? extends ItemMeta, ? extends ModuleBase> getNewPurposedContainer(final String containerId,
            final ModuleDefinition module,
            final DriverDefinition driver,
            final String hardwareName,
            final String hardwareKey) throws ModuleLoaderException {
        switch (module.getModuleType()) {
            case DEVICES:
                return new DevicesModuleContainer(containerId, module, driver, hardwareName, hardwareKey);
            default:
                throw new ModuleLoaderException("Module of type [" + module.getModuleType() + "] not supported");
        }
    }

    /**
     * Starts a module which should be binded to a peripheral.
     *
     * This method is mostly with network based modules which are capable to
     * bind just to one specific network address.
     *
     * @param definition The module definition.
     * @param moduleConfiguration The module configuration.
     * @param peripheral The network peripheral to bind to.
     * @param driverDefinition The definition of the driver.
     * @param driverConfiguration The configuration of a driver.
     * @return Future indicating failure or success.
     */
    @SuppressWarnings({"PMD.UnusedFormalParameter", "PMD.UnusedPrivateMethod"}) /// This is a preparation for future implementation.
    private Future<Void> startPeripheralBoundModule(final ModuleDefinition definition,
            final JsonNode moduleConfiguration,
            final Peripheral peripheral,
            final DriverDefinition driverDefinition,
            final JsonNode driverConfiguration) {
        Promise<Void> startPromise = Promise.promise();
        startPromise.complete();
        return startPromise.future();
    }

    /**
     * Loads a module and sets it's configuration.
     *
     * @param definition The module definition.
     * @param configuration The module configuration.
     * @return Returns the module loaded with configuration applied.
     */
    @SuppressWarnings("unchecked")
    public Future<ModuleBase> loadModule(final ModuleDefinition definition, final JsonNode configuration) {
        Promise<ModuleBase> configuredPromise = Promise.promise();
        if (definition != null && moduleExists(definition)) {
            final InstallerService installerService = ServiceHandler.getInstance().getService(InstallerService.class).get();
            final ServerPackage serverPackage = installerService.getPackageById(definition.getProvidedBy());
            if (serverPackage != null) {
                new Thread(() -> {
                    try {
                        final ModuleBase moduleBase = this.moduleStore.loadModule(definition.getModuleType(), serverPackage, definition.getModuleClass());
                        Promise<Void> configurationPromise = Promise.promise();
                        configurationPromise.future().setHandler(confPromiseResult -> {
                            if (confPromiseResult.failed()) {
                                LOG.info("Module [{}] configuration failed", definition.getName(), confPromiseResult.cause());
                                configuredPromise.fail(confPromiseResult.cause());
                            } else {
                                LOG.info("Module [{}] has been configured", definition.getName());
                                configuredPromise.complete(moduleBase);
                            }
                        });
                        if (configuration != null) {
                            InputForm moduleConfiguration = PresentationCompiler.mergeForm(
                                    this.moduleStore.getModuleConfiguration(moduleBase),
                                    configuration.toString()
                            );
                            moduleConfiguration.checkRequiredFields();
                            moduleBase.setConfiguration(moduleConfiguration);
                            moduleBase.configure(moduleConfiguration, configurationPromise);
                        } else {
                            configurationPromise.complete();
                        }
                    } catch (Exception ex) {
                        LOG.error("An error occurred during configuration of [{}]", definition.getName(), ex);
                        configuredPromise.fail(ex);
                    }
                }).start();
            } else {
                configuredPromise.fail(new ModuleException("Server package is absent, unable to load"));
            }
        } else {
            configuredPromise.fail(new ModuleException("Module not known, or empty definition given (definition = null: " + (definition == null) + ")"));
        }
        return configuredPromise.future();
    }

    /**
     * Stops modules with a specific driver bound.
     *
     * @param <D> The driver type.
     * @param driver The driver to look for used by modules.
     */
    public <D extends HardwareDriver> void stopModulesWithDriver(final D driver) {
        LOG.info("Driver [{}] being stopped, looking for running modules to stop", driver);
        List<String> ids = this.runningModules.stream()
                .filter(container -> container.getDataLink().equalsLink(driver))
                .map(container -> container.getId())
                .collect(Collectors.toList());
        ids.stream().forEach(id -> {
            stopModule(id);
        });
    }

    /**
     * Stops a running module.
     *
     * @param activeContainerId The id of the module to stop.
     * @return Future indicating when the module is stopped.
     */
    @SuppressWarnings("unchecked")
    public Future<Void> stopModule(final String activeContainerId) {
        final Promise<Void> stopPromise = Promise.promise();
        Predicate<ActiveModuleContainer> containerPredicate = container -> container.getId().equals(activeContainerId);
        this.runningModules.stream()
                .filter(containerPredicate.and(container -> !container.isStopping()))
                .findFirst()
                .ifPresentOrElse(container -> {
                    container.stopRoutines();
                    container.stop().setHandler((Handler<AsyncResult<Void>>) stopResult -> {
                        if (stopResult.failed()) {
                            LOG.warn("Module [{}] stopped with errors", container.getModuleDefinition().getName(), stopResult.cause());
                        } else {
                            sendNotification(EventSeverity.TRIVIAL, EventType.NOTIFY, "Module " + container.getModuleDefinition().getName() + " stopped");
                            LOG.info("Module [{}] stopped", container.getModuleDefinition().getName());
                        }
                        container.clearReferences();
                        stopPromise.handle(stopResult);
                    });
                }, () -> {
                    stopPromise.fail(new ModuleException("Module container with id [" + activeContainerId + "] not found"));
                });
        this.runningModules.removeIf(containerPredicate);
        return stopPromise.future();
    }

    /**
     * Creates a data link between a peripheral and a module.
     *
     * @param <D> The driver type.
     * @param <M> The module type.
     * @param container The module container containing the module to create the
     * link to.
     * @param module The module to create the link for.
     * @param driver The driver to create the link from.
     * @throws InvalidDataLinkTypeException When the module is unable to be
     * linked to the driver.
     */
    private <D extends HardwareDriver, M extends CommunicationAwareModule> void createModulePeripheralDataLink(
            final ActiveModuleContainer container, final M module, final D driver) throws InvalidDataLinkTypeException {
        container.setDataLink(DataLink.create(module, driver));
    }

    /**
     * Sends a module notification.
     *
     * @param severity The severity of the notification.
     * @param type The type of notification.
     * @param message The message for the notification
     */
    private void sendNotification(final EventSeverity severity, final EventType type, final String message) {
        ServiceHandler.service(EventService.class).ifPresent(service -> {
            ModuleEventProducer producer = new ModuleEventProducer();
            producer.setEvent(severity, type, message, null);
            service.publishEvent(producer);
        });
    }

}
