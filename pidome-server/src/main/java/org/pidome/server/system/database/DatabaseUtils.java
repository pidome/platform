/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.database;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Some usefull tools for database actions.
 *
 * @author John Sirach
 */
public final class DatabaseUtils {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(DatabaseUtils.class);

    /**
     * Private constructor.
     *
     * Utility classes do not instantiate.
     */
    private DatabaseUtils() {
        //Default constructor.
    }

    /**
     * Get's the row count for the given entity.
     *
     * @param manager The entity manager to use.
     * @param clazz The entity to get the row count for.
     * @return The amount of rows of the given entity.
     */
    @SuppressWarnings("unchecked")
    public static long getRowCount(final EntityManager manager, final Class clazz) {
        try {
            CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
            CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
            countQuery.select(criteriaBuilder.count(countQuery.from(clazz)));
            return manager.createQuery(countQuery).getSingleResult();
        } catch (Exception ex) {
            LOG.error("Unabnle to retrieve row count for [{}]", clazz.getClass().getName(), ex);
            return 0;
        }
    }

}
