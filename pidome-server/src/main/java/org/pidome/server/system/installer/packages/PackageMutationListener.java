/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.installer.packages;

/**
 * Listener interface to be implemented by any class who wants to listen to
 * package mutations.
 *
 * @author johns
 */
public interface PackageMutationListener {

    /**
     * Called when a package is removed.
     *
     * @param serverPackage The page removed.
     */
    void packageRemoved(ServerPackage serverPackage);

    /**
     * When a package has been added.
     *
     * @param serverPackage The server package added.
     */
    void packageAdded(ServerPackage serverPackage);

}
