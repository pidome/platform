/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.config;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.FileSystemLoopException;
import java.nio.file.FileVisitResult;
import static java.nio.file.FileVisitResult.CONTINUE;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.pidome.server.env.PlatformException;
import org.pidome.server.env.PlatformOs;
import org.pidome.server.system.logging.ColoredConsoleLogger;
import org.pidome.server.system.logging.LoggerStream;

/**
 * Entry for configuration usages.
 *
 * @author John Sirach
 * @since 1.0
 */
public final class SystemConfig {

    /**
     * Private constructor, utility class.
     */
    private SystemConfig() {
        /// Default private constructor.
    }

    /**
     * String literal to identify a local/fabricated run of the application from
     * an editor which causes different paths than when running live.
     */
    private static final String DEV_RUN = "pidome.dev.run";

    /**
     * Indicator the server is running in development mode thus providing
     * development capabilities.
     */
    private static final String DEV_MODE = "pidome.dev.mode";

    /**
     * Configuration type.
     */
    public enum Type {
        /**
         * The system configuration.
         */
        SYSTEM("system");

        /**
         * The file name prefix to access based on the type.
         */
        private final String fileName;

        /**
         * Configure the enum.
         *
         * @param file The file name prefix to access based on the type.
         */
        Type(final String file) {
            this.fileName = file;
        }

        /**
         * Returns the filename prefix of the type to return.
         *
         * @return The filename prefix.
         */
        public String file() {
            return this.fileName;
        }

        /**
         * String representation of the prefix.
         *
         * @return The representation of the prefix
         */
        @Override
        public String toString() {
            return this.file();
        }

    }

    /**
     * The current log level.
     */
    private static Level logLevel = Level.INFO;

    /**
     * Keep track of loaded configuration.
     *
     * @since 1.0
     */
    private static boolean loaded = false;

    /**
     * Logger for config.
     *
     * @since 1.0
     */
    private static final Logger LOG = LogManager.getLogger(SystemConfig.class);

    /**
     * Properties map.
     *
     * @since 1.0
     */
    private static final Map<SystemConfig.Type, SystemProperties> PROP_LIST
            = new HashMap<>();

    /**
     * List of urls where shared libraries are stored.
     *
     * @since 1.0
     */
    private static final List<URL> LIBRARY_URLS = new ArrayList<>();

    /**
     * The resource path for the allocation of all the file based resources.
     */
    private static String resourcePath = "./";

    /**
     * Location of the modules.
     */
    private static String modulesPath = "./modules/";

    /**
     * If this is the first run or not.
     */
    private static boolean firstRun = false;

    /**
     * Initializes the configuration.
     *
     * @throws SystemConfigException When initialization fails.
     * @since 1.0
     */
    public static void initialize() throws SystemConfigException {
        if (!loaded) {
            try {
                PlatformOs.setPlatform();
            } catch (PlatformException ex) {
                throw new SystemConfigException("Platform identification failed", ex);
            }
            LOG.debug("Reading configuration.");
            try {
                setResourcePaths(false);
                redirectOutputToLog();
                setInterfaceLibs();
                setSharedLibs();
                loaded = true;
                PROP_LIST.put(SystemConfig.Type.SYSTEM,
                        new SystemProperties(getResourcePath() + "config/", SystemConfig.Type.SYSTEM.fileName,
                                new SystemProperties("", "META-INF/" + SystemConfig.Type.SYSTEM.fileName + ".default", true))
                );
                LOG.debug("Done reading configuration.");
                firstRun = SystemConfig.getSetting(Type.SYSTEM, "server.veryfirstrun", false);
            } catch (IOException ex) {
                throw new SystemConfigException("Problem initializing configuration", ex);
            }
        }
    }

    /**
     * If the run is the first one or not.
     *
     * @return true when it's the first run.
     */
    public boolean isFirstRun() {
        return firstRun;
    }

    /**
     * Sets the resource path.
     *
     * This method keeps development (i know), into consideration so resources
     * can correctly be gathered from the build path.
     *
     * @param fromRoot if the resource should be accessed from the system root
     * dir.
     */
    @SuppressWarnings("PMD.SystemPrintln")
    private static void setResourcePaths(final boolean fromRoot) {
        if (fromRoot) {
            resourcePath = System.getProperty("user.dir") + "/";
            modulesPath = System.getProperty("user.dir") + "/modules/";
        }
        if (isDevRun()) {
            // This system out is on purpose to remind a developer that this run
            // is prefabricated. This way it appears in output consoles in
            // most editors. refer to the build system how this fabrication is
            // done.
            System.out.println("Fabricated run, this runtime is only trusted when started from a development tool.");
            resourcePath += "build/resources/main/";
            modulesPath = "./build/modules/";
        }
        if (isDevMode()) {
            System.out.println("PiDome is running in development mode, make sure this is what you want and you "
                    + "understand the implications of running with less security.\nExpect differences in the "
                    + "environment as some routines differ from a non development mode run.\n\n");
        }
    }

    /**
     * If development mode is active.
     *
     * @return true when in dev mode.
     */
    public static boolean isDevRun() {
        return System.getProperty(DEV_RUN) != null && System.getProperty(DEV_RUN)
                .toLowerCase(Locale.US).equals("true");
    }

    /**
     * If the server is running in development mode or not.
     *
     * @return true when in development mode.
     */
    public static boolean isDevMode() {
        return System.getProperty(DEV_MODE) != null && System.getProperty(DEV_MODE)
                .toLowerCase(Locale.US).equals("true");
    }

    /**
     * Returns the resource path. When app is being ran from gradle it returns
     * the correct path.
     *
     * @return The resource path string always ending in "/" character.
     */
    public static String getResourcePath() {
        return resourcePath;
    }

    /**
     * Returns the resource path with the given path included.
     *
     * @param path The path without trailing slash ("/")
     * @return The resource path with the given path.
     */
    public static String getResourcePath(final String path) {
        return resourcePath + path;
    }

    /**
     * Returns the path to the modules.
     *
     * @return The modules path.
     */
    public static String getModulesPath() {
        return modulesPath;
    }

    /**
     * Sets a list of paths to be used by plugins and drivers.
     *
     * @since 1.0
     */
    private static void setSharedLibs() {
        Path connectorPath = new File("libs/shared/").toPath();
        try {
            Files.walkFileTree(connectorPath, new UrlPaths());
            LOG.debug("Shared libraries file paths: {}", LIBRARY_URLS);
        } catch (IOException ex) {
            LOG.error("Could not collect shared libary paths: {}", ex.getMessage());
        }
    }

    /**
     * Sets the interfaces libraries.
     *
     * @since 1.0
     */
    private static void setInterfaceLibs() {
        Path connectorPath = new File("libs/interfaces/").toPath();
        try {
            Files.walkFileTree(connectorPath, new UrlPaths());
            LOG.debug("Interface libraries file paths: {}", LIBRARY_URLS);
        } catch (IOException ex) {
            LOG.error("Could not collect interface libary paths: {}", ex.getMessage());
        }
    }

    /**
     * Returns the library paths.
     *
     * @since 1.0
     * @return List of urls.
     */
    public static List<URL> getLibraryPaths() {
        return LIBRARY_URLS;
    }

    /**
     * Class to be used to set the paths with a walkFileTree. All the visiting
     * is explicitely continued.
     *
     * @since 1.0
     */
    public static class UrlPaths extends SimpleFileVisitor<Path> {

        /**
         * List of urls to set.
         */
        private final List<URL> librariesList = new ArrayList<>();

        /**
         * Executed when a file is found. It gets the url from a file to be
         * added to the library paths.
         *
         * @param file File as Path.
         * @param attr Basic file attributes.
         * @return A fileVisitResult result explicit continue.
         */
        @Override
        public FileVisitResult visitFile(final Path file, final BasicFileAttributes attr) {
            try {
                librariesList.add(file.toUri().toURL());
            } catch (MalformedURLException ex) {
                LOG.error("Could not add {} to libraries path", file.getFileName());
            }
            return CONTINUE;
        }

        /**
         * Executed when a file failed.
         *
         * @param file The file to failed to visit.
         * @param exc The exception thrown when a visit fails
         * @return Implict continue constant.
         */
        @Override
        @SuppressWarnings("PMD.GuardLogStatement")
        public FileVisitResult visitFileFailed(final Path file, final IOException exc) {
            if (exc instanceof FileSystemLoopException) {
                LOG.error("cycle detected while adding libraries: " + file);
            }
            return CONTINUE;
        }

        /**
         * Executed when file visit done.
         *
         * @param file The file visited.
         * @param exc An exception when applicable.
         * @return Explicit continue after post handling.
         */
        @Override
        public FileVisitResult postVisitDirectory(final Path file, final IOException exc) {
            //for (int i = 0; i < librariesList.size(); i++) {
            //    LIBRARY_URLS.add(librariesList.get(i));
            //}
            return CONTINUE;
        }

        public final List<URL> getLibrariesList() {
            return librariesList;
        }

    }

    /**
     * Returns a String value.
     *
     * @param type Where the configuration is gathered from
     * @param name The key to use to get.
     * @param string1 The default String value if not present.
     * @return the requested String value belonging to key.
     */
    public static String getSetting(final SystemConfig.Type type, final String name, final String string1) {
        return get(type, name, String.class, string1);
    }

    /**
     * Returns a boolean value.
     *
     * @param type Where the configuration is gathered from
     * @param name The key to use to get.
     * @param bln The default boolean value if not present.
     * @return the requested boolean value belonging to key.
     */
    public static boolean getSetting(final SystemConfig.Type type, final String name, final boolean bln) {
        return get(type, name, Boolean.class, bln);
    }

    /**
     * Returns an integer value.
     *
     * @param type Where the configuration is gathered from
     * @param name The key to use to get.
     * @param i The default integer value if not present.
     * @return the requested integer value belonging to key.
     */
    public static int getSetting(final SystemConfig.Type type, final String name, final int i) {
        return get(type, name, Integer.class, i);
    }

    /**
     * Returns a long value.
     *
     * @param type Where the configuration is gathered from
     * @param name The key to use to get.
     * @param l The default long value if not present.
     * @return the requested long value belonging to key.
     */
    public static long getSetting(final SystemConfig.Type type, final String name, final long l) {
        return get(type, name, Long.class, l);
    }

    /**
     * Returns a float value.
     *
     * @param type Where the configuration is gathered from
     * @param name The key to use to get.
     * @param f The default float value if not present.
     * @return the requested float value belonging to key.
     */
    public static float getSetting(final SystemConfig.Type type, final String name, final float f) {
        return get(type, name, Float.class, f);
    }

    /**
     * Stores a string value.
     *
     * @param type Where the configuration is set.
     * @param name The key to store it.
     * @param value The string value to store.
     */
    public static void setSetting(final SystemConfig.Type type, final String name, final String value) {
        set(type, name, value);
    }

    /**
     * Stores a boolean value.
     *
     * @param type Where the configuration is set
     * @param name The key to store this under.
     * @param bln The boolean value.
     */
    public static void setSetting(final SystemConfig.Type type, final String name, final boolean bln) {
        set(type, name, bln);
    }

    /**
     * Stores an integer.
     *
     * @param type Where the configuration is set
     * @param name The key to store with.
     * @param i the integer value.
     */
    public static void setSetting(final SystemConfig.Type type, final String name, final int i) {
        set(type, name, i);
    }

    /**
     * Stores a long.
     *
     * @param type Where the configuration is set
     * @param name The key to store with.
     * @param l The long value to store.
     */
    public static void setSetting(final SystemConfig.Type type, final String name, final long l) {
        set(type, name, l);
    }

    /**
     * Sets a float value.
     *
     * @param type Where the configuration is set
     * @param name the key to use.
     * @param f The float value.
     */
    public static void setSetting(final SystemConfig.Type type, final String name, final float f) {
        set(type, name, f);
    }

    /**
     * Type casted return type.
     *
     * @param <T> The object type to be expected to be returned
     * @param key The key to get the value from.
     * @param primitive The primitive type to be returned.
     * @param defaultReturn The default return in case anything goes wrong.
     * @param type The systemconfig type.
     * @return The typecasted requested object. If casting fails or the value
     * does not exist the default value is returned.
     */
    @SuppressWarnings({"unchecked", "PMD.GuardLogStatement"})
    private static <T extends Object> T get(final SystemConfig.Type type, final String key, final Class<T> primitive, final T defaultReturn) {
        if (loaded) {
            try {
                String value = getProperty(type, key);
                if (value != null) {
                    switch (primitive.getSimpleName()) {
                        case "Integer":
                            return (T) Integer.valueOf(value);
                        case "Float":
                            return (T) Float.valueOf(value);
                        case "Long":
                            return (T) Long.valueOf(value);
                        case "String":
                            return (T) value;
                        case "Boolean":
                            return (T) Boolean.valueOf(value);
                        default:
                            return defaultReturn;
                    }
                } else {
                    return defaultReturn;
                }
            } catch (ClassCastException ex) {
                LOG.error("Given key [{0}] was not of expected given, or expected value for type [{1}]: " + ex.getMessage(), new Object[]{key, primitive.getSimpleName()});
                return defaultReturn;
            } catch (ConfigPropertiesException ex) {
                throw new RuntimeException("Initialization of configuration failed", ex);
            }
        } else {
            return defaultReturn;
        }
    }

    /**
     * Stores the given value with key as name.
     *
     * @param type Where the configuration is set
     * @param name The key to store.
     * @param value The value assigned to the key.
     */
    private static void set(final SystemConfig.Type type, final String name, final Object value) {
        if (loaded) {
            PROP_LIST.get(type).put(name, String.valueOf(value));
        }
    }

    /**
     * Stores a preference file with the given message.
     *
     * @param type The settings file type.
     * @param string The message to store with the file
     * @throws IOException When storing does not work.
     */
    public static void storeSettings(final SystemConfig.Type type, final String string) throws IOException {
        PROP_LIST.get(type).store(string);
    }

    /**
     * Stores preferences.
     *
     * Does nothing anymore
     *
     * @param type The settings file type.
     * @throws IOException When the settings can not be stored.
     */
    public static void storeSettings(final SystemConfig.Type type) throws IOException {
        PROP_LIST.get(type).store("");
    }

    /**
     * Returns a configuration property.
     *
     * @param type The configuration type.
     * @param propName The property to retrieve from the type.
     * @return String value of the property type.
     * @throws ConfigPropertiesException When initialization during first read
     * fails.
     */
    private static String getProperty(final SystemConfig.Type type, final String propName) throws ConfigPropertiesException {
        if (!loaded) {
            try {
                initialize();
            } catch (SystemConfigException ex) {
                throw new ConfigPropertiesException(ex.getMessage(), ex);
            }
        }
        if (PROP_LIST.containsKey(type)) {
            String propValue = PROP_LIST.get(type).getProperty(propName);
            return propValue;
        } else {
            return null;
        }
    }

    /**
     * Returns a complete configuration type set.
     *
     * @param type The type to get the full properties set.
     * @return The properties set.
     * @throws ConfigPropertiesException When the given type does not exist.
     */
    public static Set<Map.Entry<Object, Object>> getPropertiesNVP(final SystemConfig.Type type) throws ConfigPropertiesException {
        if (PROP_LIST.containsKey(type)) {
            return PROP_LIST.get(type).entrySet();
        } else {
            throw new ConfigPropertiesException("Property type '" + type + "' does not exist");
        }
    }

    /**
     * Changes the log level used.
     *
     * @param level The log level to set the new level to.
     */
    @SuppressWarnings("PMD.CloseResource")
    public static void setLogLevel(final Level level) {
        logLevel = level;
        LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
        Configuration logConfig = ctx.getConfiguration();
        LoggerConfig loggerConfig = logConfig.getLoggerConfig(LogManager.ROOT_LOGGER_NAME);
        loggerConfig.setLevel(logLevel);
        ctx.updateLoggers();
    }

    /**
     * Returns the current log level.
     *
     * @return The log level.
     */
    public static Level getLogLevel() {
        return logLevel;
    }

    /**
     * Redirects system.out and system.err to the log file.
     */
    private static void redirectOutputToLog() {
        if (isDevRun()) {
            enableLocalLogging();
        } else {
            setOutToLog();
            setErrToLog();
        }
    }

    /**
     * Used when logging in local environment.
     */
    @SuppressWarnings("PMD.CloseResource")
    private static void enableLocalLogging() {
        LoggerContext lc = (LoggerContext) LogManager.getContext(false);
        ColoredConsoleLogger coloredConsoleLogger = ColoredConsoleLogger.createAppender("Console",
                PatternLayout.newBuilder().withPattern("%d [%t] %-5p %c - %m%n").build(),
                null,
                null);
        coloredConsoleLogger.start();
        lc.getConfiguration().addAppender(coloredConsoleLogger);
        lc.getRootLogger().addAppender(lc.getConfiguration().getAppender(coloredConsoleLogger.getName()));

        lc.updateLoggers();
    }

    /**
     * Redirects system.out to the log file.
     */
    @SuppressWarnings("PMD.SystemPrintln")
    private static void setOutToLog() {
        /// This system out is on purpose to inform the developer that any
        /// System.out is being redirected to the log file.
        System.out.println("Setting system out to log file appLog.txt. logging continues there unless the log file could not be accessed where this file will then be the fallback");
        try {
            System.setOut(new PrintStream(new LoggerStream(LogManager.getLogger("out"), Level.DEBUG), true, "US-ASCII"));
        } catch (UnsupportedEncodingException ex) {
            LOG.warn("System out not set to log file as the character set US-ASCII is unsupported", ex);
        }
    }

    /**
     * Redirects system.err to the log file.
     */
    private static void setErrToLog() {
        try {
            System.setErr(new PrintStream(new LoggerStream(LogManager.getLogger("err"), Level.ERROR), true, "US-ASCII"));
        } catch (UnsupportedEncodingException ex) {
            LOG.warn("System err not set to log file as the character set US-ASCII is unsupported", ex);
        }
    }

}
