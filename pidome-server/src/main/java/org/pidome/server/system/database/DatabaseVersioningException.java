/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.database;

/**
 * Exception thrown from versioning.
 *
 * @author johns
 */
public class DatabaseVersioningException extends Exception {

    /**
     * Class version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>DatabaseVersioningException</code>
     * without detail message.
     */
    public DatabaseVersioningException() {
    }

    /**
     * Constructs an instance of <code>DatabaseVersioningException</code> with
     * the specified detail message.
     *
     * @param msg the detail message.
     */
    public DatabaseVersioningException(final String msg) {
        super(msg);
    }

    /**
     * Constructs an instance of <code>DatabaseVersioningException</code> with
     * the specified detail message and cause.
     *
     * @param msg the detail message.
     * @param cause The cause of this exception.
     */
    public DatabaseVersioningException(final String msg, final Throwable cause) {
        super(msg, cause);
    }
}
