/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.modules.devices;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import org.pidome.platform.modules.devices.Device;
import org.pidome.server.entities.items.ItemMeta;

/**
 * A container for linking a device to it's definition.
 *
 * @author johns
 * @todo Add device options here.
 */
@Entity
@DiscriminatorValue("DEVICE")
public class DeviceMeta extends ItemMeta<Device, DeviceDefinition> {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The hash code.
     *
     * @return The hash code.
     */
    @Override
    public int hashCode() {
        return super.hashCode();
    }

    /**
     * Equality check.
     *
     * @param obj The object to check.
     * @return if equal.
     */
    @Override
    public boolean equals(final Object obj) {
        return super.equals(obj);
    }

}
