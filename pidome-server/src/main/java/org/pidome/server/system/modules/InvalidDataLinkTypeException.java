/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.modules;

/**
 * When an invalid data link is to be created.
 *
 * @author johns
 */
public class InvalidDataLinkTypeException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>InvalidDataLinkTypeException</code>
     * without detail message.
     */
    public InvalidDataLinkTypeException() {
    }

    /**
     * Constructs an instance of <code>InvalidDataLinkTypeException</code> with
     * the specified detail message.
     *
     * @param msg the detail message.
     */
    public InvalidDataLinkTypeException(final String msg) {
        super(msg);
    }
}
