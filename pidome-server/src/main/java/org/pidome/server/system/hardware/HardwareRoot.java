/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.hardware;

import com.fasterxml.jackson.databind.JsonNode;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.hardware.driver.HardwareDriver;
import org.pidome.platform.hardware.driver.Transport;
import org.pidome.platform.hardware.driver.config.SerialDeviceConfiguration;
import org.pidome.platform.presentation.input.InputForm;
import org.pidome.platform.presentation.input.InputFormException;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.events.EventService;
import org.pidome.server.services.events.EventSeverity;
import org.pidome.server.services.events.EventType;
import org.pidome.server.services.hardware.DriverDefinition;
import org.pidome.server.services.hardware.HardwareEventProducer;
import org.pidome.server.services.hardware.HardwareServiceException;
import org.pidome.server.services.http.presentation.PresentationCompiler;
import org.pidome.server.services.installer.InstallerService;
import org.pidome.server.services.modules.ModuleService;
import org.pidome.server.system.hardware.serial.SerialDevices;
import org.pidome.server.system.hardware.usb.USBDevices;
import org.pidome.server.system.installer.packages.PackageClassLoaderException;
import org.pidome.server.system.installer.packages.PackageStore;
import org.pidome.server.system.installer.packages.ServerPackage;

/**
 * Hardware interface to the collection of peripherals connected to the server.
 *
 * The hardware root is the most parent hub to all the connected peripherals and
 * hardware available systems and subsystems on the running host.
 *
 * This peripheral maintenance class provides the possibility to perform
 * mutations on peripherals and moves peripherals between waiting and active
 * state.
 *
 * @author John Sirach
 * @since 1.0
 */
public final class HardwareRoot implements HardwareMutationListener {

    /**
     * Hardware root logger.
     */
    private static final Logger LOG = LogManager.getLogger(HardwareRoot.class);

    /**
     * A mutation within a peripheral.
     */
    public enum Mutation {
        /**
         * A peripheral has been connected to the hardware.
         */
        ADD,
        /**
         * A peripheral has been removed from the hardware.
         */
        REMOVE,
        /**
         * A peripheral has been updated.
         */
        UPDATE

    }

    /**
     * Collection of available hardware subsystems.
     */
    private final Map<HardwareComponent.Interface, HardwareComponent> availableHardware = Collections.synchronizedMap(new HashMap<>());

    /**
     * The driver store containing the available drivers for hardware.
     */
    private HardwareDriverStore driverStore;

    /**
     * Empty hardware root constructor.
     */
    public HardwareRoot() {
        try {
            USBDevices usb = new USBDevices(this);
            synchronized (availableHardware) {
                availableHardware.put(usb.getIdentifyingInterface(), usb);
            }
        } catch (UnsupportedOperationException ex) {
            LOG.error("USB devices are unsupported on this system [{}]", ex.getMessage());
        }
        try {
            SerialDevices serial = new SerialDevices(this);
            synchronized (availableHardware) {
                availableHardware.put(serial.getIdentifyingInterface(), serial);
            }
        } catch (UnsupportedOperationException ex) {
            LOG.error("Serial devices are unsupported on this system [{}]", ex.getMessage());
        }
    }

    /**
     * Starts the peripheral service.
     *
     * @return Returns the future.
     */
    public Future<Void> start() {
        final Promise<Void> startPromise = Promise.promise();
        driverStore = new HardwareDriverStore();
        driverStore.scan().setHandler(result -> {
            if (result.succeeded()) {
                this.availableHardware.values().forEach((component) -> {
                    LOG.info("Starting hardware component [{}]", component);
                    try {
                        component.start();
                        LOG.info("Starting discovery of compatible devices on [{}]", component);
                        component.discover();
                    } catch (UnsupportedOperationException ex) {
                        LOG.warn("An unsupported feature was requested [{}]", ex.getMessage());
                    }
                });
            }
            startPromise.handle(result);
        });
        return startPromise.future();
    }

    /**
     * Stops the peripheral service.
     *
     * @todo stop hardware
     */
    public void stop() {

    }

    /**
     * Returns a list of available hardware.
     *
     * @return Hardware available within the system.
     */
    public Map<HardwareComponent.Interface, HardwareComponent> getHardware() {
        return Collections.unmodifiableMap(availableHardware);
    }

    /**
     * Returns the known drivers categorized by transport in an unmodifiable
     * map.
     *
     * @return Unmodifiable map with transport and associated drivers.
     */
    public List<DriverDefinition> getDriverCollection() {
        return driverStore.getDriverCollection();
    }

    /**
     * Returns the hardware component of the given interface.
     *
     * @param interfaceKey The interface key of the hardware to retrieve.
     * @return The hardware component of the interface.
     */
    public Optional<HardwareComponent> getHardware(final HardwareComponent.Interface interfaceKey) {
        return Optional.ofNullable(this.availableHardware.get(interfaceKey));
    }

    /**
     * Returns a list of available hardware of the given transport type.
     *
     * @param transport The transport type to fetch for.
     * @return List of hardware components for the given transport type.
     */
    public List<Peripheral<? extends HardwareDriver>> getHardware(final Transport.SubSystem transport) {
        return this.availableHardware.values().stream()
                .flatMap(component -> component.getConnectedPeripherals().stream())
                .filter(peripheral -> peripheral.getSubSystem().equals(transport))
                .collect(Collectors.toList());
    }

    /**
     * Returns a set of driver for the given transport subsystem.
     *
     * @param subSystem The transport subsystem.
     * @return Unmodifiable set of drivers.
     */
    public List<DriverDefinition> getDriverCollection(final Transport.SubSystem subSystem) {
        return driverStore.getDriverCollectionForSubSystem(subSystem);
    }

    /**
     * Loads a configuration for the given device and driver.
     *
     * @param peripheral The peripheral to construct the configuration for.
     * @param packageId The id of the package to load.
     * @param classToLoad The path of the class to load.
     * @return An input form with a driver configuration.
     */
    @SuppressWarnings("unchecked")
    public InputForm constructConfigurationFor(final Peripheral peripheral, final UUID packageId, final String classToLoad) {
        InstallerService installerService = ServiceHandler.getInstance().getService(InstallerService.class).get();
        ServerPackage serverPackage = installerService.getPackageById(packageId);
        if (serverPackage != null) {
            try (PackageStore.UnboundPackageLoad<HardwareDriver> loader
                    = new PackageStore.UnboundPackageLoad<>(serverPackage, classToLoad)) {
                HardwareDriver driver = loader.get();
                Transport.SubSystem driverSubSystem = Transport.SubSystem.getByImplementationClass(driver.getClass().getSuperclass().getSimpleName());
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Subsystems peripheral [{}], driver [{}] from package [{}]", peripheral, classToLoad, serverPackage);
                }
                if (peripheral.getSubSystem().equals(driverSubSystem)) {
                    Promise configurationPromise = Promise.promise();
                    switch (driverSubSystem) {
                        case SERIAL:
                            SerialDeviceConfiguration serialConfig = new SerialDeviceConfiguration();
                            serialConfig.setName(peripheral.getName());
                            serialConfig.setPath(peripheral.getPath());
                            serialConfig.setPort(peripheral.getPort());
                            driver.composeConfiguration(serialConfig, configurationPromise);
                            break;
                        default:
                            LOG.warn("Driver of type [{}] is currently not supported.", driverSubSystem);
                            break;
                    }
                    if (configurationPromise.future().succeeded()) {
                        return driver.getConfiguration();
                    } else if (configurationPromise.future().failed()) {
                        if (configurationPromise.future().cause() != null) {
                            LOG.error("configuration of [{}] failed", driver, configurationPromise.future().cause());
                        } else {
                            LOG.error("configuration of [{}] failed for an unknown reason", driver);
                        }
                    } else {
                        LOG.error("Unknown if configuration composition in the driver has succeeded, contact author of [{}] (result: [{}])", driver, configurationPromise);
                    }
                } else {
                    LOG.error("Peripheral [{}] of type [{}] is not compatible with driver [{}] of type [{}]", peripheral, peripheral.getSubSystem(), driver, driverSubSystem);
                }
            } catch (IOException ex) {
                LOG.error("Unable to properly close the loader from package [{}], Unknown if driver [{}] is stable, we do not continue", serverPackage, classToLoad, ex);
            } catch (PackageClassLoaderException ex) {
                LOG.error("Unable to load configuration for peripheral [{}] using package [{}] width driver [{}]", peripheral, serverPackage, classToLoad, ex);
            }
        }
        return null;
    }

    /**
     * Starts a driver on the given peripheral.
     *
     * @param peripheral The peripheral to apply the configuration to start.
     * @param packageId The package to load the driver from.
     * @param driver The driver to load from the package.
     * @param configuration The configuration to apply to the driver to put the
     * peripheral in use.
     * @return A future indicating success or failure.
     */
    @SuppressWarnings("unchecked")
    public Future<HardwareDriver> startDriver(
            final Peripheral peripheral,
            final UUID packageId,
            final String driver,
            final JsonNode configuration) {
        final InstallerService installerService = ServiceHandler.getInstance().getService(InstallerService.class).get();
        final ServerPackage serverPackage = installerService.getPackageById(packageId);
        final Promise finishPromise = Promise.promise();
        new Thread(() -> {
            if (serverPackage != null) {
                try {
                    HardwareDriver peripheralDriver = this.driverStore.loadDriver(peripheral.getSubSystem(), serverPackage, driver);
                    InputForm driverConfiguration = PresentationCompiler.mergeForm(
                            this.driverStore.getDriverBaseConfiguration(peripheralDriver),
                            configuration.toString()
                    );

                    driverConfiguration.checkRequiredFields();
                    Promise<Void> configurationPromise = Promise.promise();
                    peripheralDriver.setConfiguration(driverConfiguration);
                    peripheralDriver.configure(driverConfiguration, configurationPromise);

                    if (configurationPromise.future().failed()) {
                        if (configurationPromise.future().cause() != null) {
                            finishPromise.fail(new HardwareServiceException("Unable to configure driver", configurationPromise.future().cause()));
                        } else {
                            finishPromise.fail(new HardwareServiceException("Unable to configure driver, check log files"));
                        }
                    } else {
                        peripheral.setDriver(peripheralDriver);
                        Promise<Void> startPromise = Promise.promise();
                        peripheralDriver.startDriver(startPromise);
                        if (startPromise.future().failed()) {
                            peripheralDriver.setStopping();
                            peripheralDriver.stopDriver(Promise.promise());
                            finishPromise.fail(new HardwareServiceException("Unable to start driver", startPromise.future().cause()));
                        } else {
                            peripheral.setActive(true);
                            sendNotification(EventSeverity.TRIVIAL, EventType.NOTIFY, "Driver " + peripheral.getName() + " started");
                            finishPromise.complete(peripheral.getDriver());
                        }
                    }

                } catch (IOException | HardwareDriverStoreException ex) {
                    finishPromise.fail(new HardwareServiceException(ex));
                } catch (InputFormException ex) {
                    finishPromise.fail(ex);
                }
            } else {
                finishPromise.fail(new HardwareServiceException("Server package [" + packageId + "] not found in the system"));
            }
        }).start();
        return finishPromise.future();
    }

    /**
     * Stops a driver on a peripheral identified by the peripheral key.
     *
     * @param peripheralKey The key of the peripheral.
     * @return Future indicating success or failure.
     */
    public Future<Void> stopDriver(final String peripheralKey) {
        Promise<Void> stopByKey = Promise.promise();
        this.availableHardware.values().stream()
                .flatMap(component -> component.getConnectedPeripherals().stream())
                .filter(peripheral -> peripheral.getKey().equals(peripheralKey))
                .findFirst()
                .ifPresentOrElse(peripheral -> {
                    stopDriver(peripheral).setHandler(result -> {
                        stopByKey.handle(result);
                    });
                }, () -> {
                    stopByKey.fail(new HardwareDriverStoreException("No driver found on [" + peripheralKey + "]"));
                });
        return stopByKey.future();
    }

    /**
     * Stops a driver on a peripheral.
     *
     * @param peripheral The peripheral to stop the driver on.
     * @return Future indicating stop success or failure.
     */
    @SuppressWarnings({"PMD.NullAssignment", "unchecked"})
    public Future<Void> stopDriver(final Peripheral peripheral) {
        final Promise<Void> finishPromise = Promise.promise();
        new Thread(() -> {
            if (peripheral.getDriver() != null && !peripheral.getDriver().isStopping()) {
                peripheral.getDriver().setStopping();
                Promise<Void> driverStop = Promise.promise();
                signalServicesDriverStop(peripheral);
                driverStop.future().setHandler(driverStopResult -> {
                    if (driverStopResult.succeeded()) {
                        detachDriverFromPeripheral(peripheral);
                        sendNotification(EventSeverity.TRIVIAL, EventType.MODIFY, peripheral.getFriendlyName() + " stopped, safe to remove peripheral.");
                    } else {
                        sendNotification(EventSeverity.MINOR, EventType.MODIFY, peripheral.getFriendlyName() + " possibly stopped.");
                    }
                    finishPromise.handle(driverStopResult);
                });
                peripheral.getDriver().stopDriver(driverStop);
            } else {
                finishPromise.fail("No driver available on the peripheral to stop");
            }
        }).start();
        return finishPromise.future();
    }

    /**
     * Called when a peripheral is detached from the system.
     *
     * This method does check if there is a driver available and when it's there
     * tries to stop it. It does not check if a driver is successfully stopped
     * to be compatible with hot detach while a driver is still running and has
     * not been stopped with <code>stopDriver</code> before detaching
     *
     * @param peripheral The peripheral being detached.
     */
    @SuppressWarnings({"PMD.NullAssignment", "unchecked"})
    private void peripheralDetached(final Peripheral<? extends HardwareDriver> peripheral) {
        new Thread(() -> {
            if (peripheral.getDriver() != null && !peripheral.getDriver().isStopping()) {
                peripheral.getDriver().setStopping();
                signalServicesDriverStop(peripheral);
                Promise<Void> driverStop = Promise.promise();
                driverStop.future().setHandler(driverStopResult -> {
                    if (driverStopResult.succeeded()) {
                        LOG.info("Driver was succesfully stopped during hot detach.");
                        sendNotification(EventSeverity.MINOR, EventType.DELETE, peripheral.getFriendlyName() + " removed while running, please stop first next time.");
                    } else {
                        LOG.info("Undetermined if a driver was succesfully stopped. You can discard this message if the driver has been stopped before detaching. If not, continuation can be unpredictable.");
                        sendNotification(EventSeverity.MAJOR, EventType.DELETE, peripheral.getFriendlyName() + " possibly incorrectly stopped, please stop first next time.");
                    }
                    detachDriverFromPeripheral(peripheral);
                });
                peripheral.getDriver().stopDriver(driverStop);
            } else {
                sendNotification(EventSeverity.TRIVIAL, EventType.DELETE, peripheral.getFriendlyName() + " removed.");
            }
        }).start();
    }

    /**
     * Method to signal services a driver is going to be unloaded.
     *
     * @param peripheral The peripheral on which the driver is attached.
     */
    private void signalServicesDriverStop(final Peripheral peripheral) {
        ServiceHandler.service(ModuleService.class).ifPresent(service -> {
            service.stoppingDriver(peripheral.getDriver());
        });
    }

    /**
     * Generic stop actions to perform on a peripheral to remove and unset a
     * driver.
     *
     * @param peripheral The peripheral to perform the actions on.
     */
    @SuppressWarnings("unchecked")
    private void detachDriverFromPeripheral(final Peripheral peripheral) {
        this.driverStore.unloadDriver(peripheral.getDriver());
        peripheral.setDriver(null);
        peripheral.setActive(false);
        peripheral.setInUse(false);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void hardwareMutation(final Peripheral<? extends HardwareDriver> peripheral, final Mutation mutation) {
        switch (mutation) {
            case ADD:
                LOG.info("Should load driver for registered peripheral [{}] by notifying the service", peripheral);
                sendNotification(EventSeverity.TRIVIAL, EventType.ADD, "Added " + peripheral.getFriendlyName());
                ServiceHandler.service(ModuleService.class).ifPresent(service -> {
                    service.loadModulesForPeripheral(peripheral.getVendorId(), peripheral.getProductId(), peripheral.getSerial());
                });
                break;
            case REMOVE:
                LOG.info("Peripheral [{}] detached, unloading driver if present.", peripheral);
                peripheralDetached(peripheral);
                break;
            default:
                LOG.error("Unsupported mutation [{}] used.", mutation);
                break;
        }
    }

    /**
     * Sends an hardware notification.
     *
     * @param severity The severity of the notification.
     * @param type The type of notification.
     * @param message The message for the notification
     */
    private void sendNotification(final EventSeverity severity, final EventType type, final String message) {
        ServiceHandler.service(EventService.class).ifPresent(service -> {
            HardwareEventProducer producer = new HardwareEventProducer();
            producer.setEvent(severity, type, message, null);
            service.publishEvent(producer);
        });
    }

}
