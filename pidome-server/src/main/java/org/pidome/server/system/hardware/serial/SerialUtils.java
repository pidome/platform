/*
 * Copyright 2013 John Sirach <john.sirach@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pidome.server.system.hardware.serial;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.system.config.SystemConfig;
import org.pidome.server.system.config.SystemConfig.Type;

/**
 * Utility class for Serial device access.
 *
 * @author John Sirach
 */
public final class SerialUtils {

    /**
     * Private constructor as is utility class.
     */
    private SerialUtils() {
        /// Default constructor.
    }

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(SerialUtils.class);

    /**
     * Librxtx sux sometimes.
     *
     * When we have ttyACM[0-9]+ ports they are not recognized, now every time
     * when there is a new device we create a new known ports environment
     * setting. We automatic include ttyAMA0 because of the serial GPIO usage
     *
     * @return List with Linux port names
     */
    public static List<String> discoverPorts() {
        List<String> ports = new ArrayList<>();
        String path = SystemConfig.getSetting(Type.SYSTEM, "server.linuxdevlocation", "/dev/");
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();
        String newPortsList = "";
        String fileName;
        if (listOfFiles != null) {
            for (File listOfFile : listOfFiles) {
                fileName = listOfFile.getName();
                if (fileName.contains("ttyACM") || fileName.contains("ttyUSB") || fileName.contains("ttyAMA0") || fileName.contains("ttyS")) {
                    newPortsList += path + fileName + ":";
                    ports.add(path + fileName);
                }
            }
        } else {
            LOG.warn("Could not find any serial ports compatible with PiDome");
        }
        if (newPortsList.length() > 0) {
            String portsList = newPortsList.substring(0, newPortsList.length() - 1);
            System.setProperty("gnu.io.rxtx.SerialPorts", portsList);
            LOG.debug("Setting new known ports list: {}", portsList);
        }
        return ports;
    }
}
