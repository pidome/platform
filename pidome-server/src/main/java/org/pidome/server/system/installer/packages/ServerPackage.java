/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.installer.packages;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import org.hibernate.annotations.GenericGenerator;
import org.pidome.server.services.hardware.DriverDefinition;
import org.pidome.server.services.modules.ModuleDefinition;

/**
 * A server package.
 *
 * @author John
 */
@Entity
public class ServerPackage implements Serializable {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Entity id.
     */
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    /**
     * Presentation name visible to the end user.
     */
    private String presentationName;

    /**
     * Name of the package.
     */
    @Column(nullable = false)
    private String packageName;

    /**
     * Version of the package.
     */
    @Column(nullable = false)
    private String packageVersion;

    /**
     * Group of the package.
     */
    @Column(nullable = false)
    private String packageGroup;

    /**
     * Set of modules provided by the package.
     */
    @OneToMany(fetch = FetchType.LAZY)
    private Set<ModuleDefinition> modules;

    /**
     * The drivers provided by the package.
     */
    @OneToMany(fetch = FetchType.LAZY)
    private Set<DriverDefinition> drivers;

    /**
     * Full path to the package.
     */
    private transient String packagePath;

    /**
     * @return the id
     */
    public UUID getId() {
        return id;
    }

    /**
     * @return the presentationName
     */
    public String getPresentationName() {
        return presentationName;
    }

    /**
     * @param presentationName the presentationName to set
     */
    public void setPresentationName(final String presentationName) {
        this.presentationName = presentationName;
    }

    /**
     * @return the packageName
     */
    public String getPackageName() {
        return packageName;
    }

    /**
     * @param packageName the packageName to set
     */
    public void setPackageName(@NotBlank final String packageName) {
        this.packageName = packageName;
    }

    /**
     * @return the packageVersion
     */
    public String getPackageVersion() {
        return packageVersion;
    }

    /**
     * @param packageVersion the packageVersion to set
     */
    public void setPackageVersion(@NotBlank final String packageVersion) {
        this.packageVersion = packageVersion;
    }

    /**
     * @return the packageGroup
     */
    public String getPackageGroup() {
        return packageGroup;
    }

    /**
     * @param packageGroup the packageGroup to set
     */
    public void setPackageGroup(@NotBlank final String packageGroup) {
        this.packageGroup = packageGroup;
    }

    /**
     * Set of modules provided by the package.
     *
     * @return the modules
     */
    public Set<ModuleDefinition> getModules() {
        return modules;
    }

    /**
     * Set of modules provided by the package.
     *
     * @param modules the modules to set
     */
    public void setModules(final Set<ModuleDefinition> modules) {
        this.modules = modules;
    }

    /**
     * The drivers provided by the package.
     *
     * @return the drivers
     */
    public Set<DriverDefinition> getDrivers() {
        return drivers;
    }

    /**
     * The drivers provided by the package.
     *
     * @param drivers the drivers to set
     */
    public void setDrivers(final Set<DriverDefinition> drivers) {
        this.drivers = drivers;
    }

    /**
     * @return the packagePath
     */
    public String getPackagePath() {
        if (packagePath == null) {
            packagePath = new StringBuilder(this.packageGroup).append("/")
                    .append(this.packageName).append("/")
                    .append(this.packageName).append("-").append(this.packageVersion)
                    .append(".jar")
                    .toString();
        }
        return packagePath;
    }

    /**
     * Class to string.
     *
     * @return The string representation.
     */
    @Override
    public String toString() {
        return new StringBuilder("Package:[")
                .append("packageGroup: ").append(this.packageGroup).append(", ")
                .append("packageName: ").append(this.packageName).append(", ")
                .append("packageVersion: ").append(this.packageVersion).append(", ")
                .append("packagePath: ").append(getPackagePath())
                .append("]").toString();
    }

    /**
     * Calculate hash.
     *
     * @return The hash.
     */
    @Override
    @SuppressWarnings("CPD-START")
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.id);
        return hash;
    }

    /**
     * Object equality test.
     *
     * @param obj The object to test.
     * @return If equal or not.
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ServerPackage other = (ServerPackage) obj;
        return Objects.equals(this.id, other.id);
    }

}
