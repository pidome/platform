/**
 * System logging.
 * <p>
 * All supporting classes to provide the system with a logging mechanism.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.system.logging;
