/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.installer;

/**
 * Installation exception.
 *
 * @author John
 */
public class ServerPackageInstallationException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>ServerPackageInstallationException</code>
     * without detail message.
     */
    public ServerPackageInstallationException() {
    }

    /**
     * Constructs an instance of <code>ServerPackageInstallationException</code>
     * with the specified detail message.
     *
     * @param msg the detail message.
     */
    public ServerPackageInstallationException(final String msg) {
        super(msg);
    }
}
