/**
 * Hibernate utilities.
 * <p>
 * Provides utilities and converters for hibernate.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.system.utils.hibernate;
