/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.modules.devices.builder;

/**
 * A control configuration input type.
 *
 * @author johns
 */
public enum ControlConfigurationInputType {

    /**
     * Just a string input.
     */
    STRING,
    /**
     * A boolean selection.
     */
    BOOLEAN,
    /**
     * A list of items.
     */
    LIST,
    /**
     * A numeric input.
     */
    INTEGER,
    /**
     * A decimal input.
     */
    DOUBLE;

}
