/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.modules.devices;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import org.hibernate.annotations.Type;
import org.pidome.platform.modules.devices.DeviceBuilderType;
import org.pidome.platform.modules.devices.DeviceControlGroup;
import org.pidome.platform.modules.devices.DeviceParametersList;
import org.pidome.platform.modules.items.ItemPropertiesListInterface;
import org.pidome.platform.modules.items.ItemPropertyInterface;
import org.pidome.server.entities.items.ItemDefinition;

/**
 * A definition of a device to be stored in the database and is re-creatable.
 *
 * @author johns
 */
@Entity
@DiscriminatorValue("DEVICE")
public class DeviceDefinition extends ItemDefinition {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Default constructor.
     */
    public DeviceDefinition() {
        super();
        this.setDefinitionType(DefinitionType.DEVICE);
    }

    /**
     * The builder type used for this definition.
     */
    @Enumerated(EnumType.STRING)
    private DeviceBuilderType builderType;

    /**
     * The device structure.
     */
    @Type(type = "json")
    @Column(columnDefinition = "text")
    @SuppressFBWarnings(value = {"SE_BAD_FIELD"}, justification = "Handled by json type")
    private List<DeviceControlGroup> deviceGroups;

    /**
     * The device parameters set when configuring.
     */
    @Type(type = "json")
    @Column(columnDefinition = "text")
    @SuppressFBWarnings(value = {"SE_BAD_FIELD"}, justification = "Handled by json type")
    private DeviceParametersList deviceParameters;

    /**
     * The device structure.
     *
     * @return the deviceGroups
     */
    public List<DeviceControlGroup> getDeviceGroups() {
        return deviceGroups;
    }

    /**
     * The device structure.
     *
     * @param deviceGroups the deviceGroup to set
     */
    public void setDeviceGroups(final List<DeviceControlGroup> deviceGroups) {
        this.deviceGroups = deviceGroups;
    }

    /**
     * Sets the device parameters.
     *
     * @param parametersList The list of parameters to set.
     */
    public void setDeviceParameters(final DeviceParametersList parametersList) {
        this.deviceParameters = parametersList;
    }

    /**
     * Gets the device parameters.
     *
     * @return The parameters set.
     */
    public ItemPropertiesListInterface<? extends ItemPropertyInterface> getDeviceParameters() {
        return this.deviceParameters;
    }

    /**
     * The builder type used for this definition.
     *
     * @return the builderType
     */
    public DeviceBuilderType getBuilderType() {
        return builderType;
    }

    /**
     * The builder type used for this definition.
     *
     * @param builderType the builderType to set
     */
    public void setBuilderType(final DeviceBuilderType builderType) {
        this.builderType = builderType;
    }

    /**
     * Calculates hash.
     *
     * @return The hash.
     */
    @Override
    @SuppressWarnings("CPD-START")
    public int hashCode() {
        return super.hashCode();
    }

    /**
     * Equality check.
     *
     * @param obj the object to check.
     * @return If equals.
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return super.equals(obj);
    }
}
