/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.database.driver;

import org.pidome.server.system.database.DatabaseConfiguration;
import org.pidome.server.system.database.DatabaseConfigurationException;

/**
 * Mysql driver proxy.
 *
 * @author John
 */
@SuppressWarnings("CPD-START")
public final class Mysql implements DatabaseDriverInterface {

    /**
     * Loads the mysql driver.
     *
     * @throws DatabaseConfigurationException When configuring the mysql driver
     * fails.
     */
    @Override
    public void loadDriver() throws DatabaseConfigurationException {
        throw new UnsupportedOperationException("MySQL not yet active");
    }

    /**
     * Returns the dialect used.
     *
     * @return dialect.
     */
    @Override
    public String getDialect() {
        throw new UnsupportedOperationException("MySQL not yet active");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getJDBCUrl(final DatabaseConfiguration configuration) {
        return "";
    }

}
