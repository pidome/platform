/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.modules.devices.builder;

import java.util.List;

/**
 * Structure for custom composed devices when posted to the server.
 *
 * @author johns
 */
public final class ControlConfiguration {

    /**
     * The data type used in the parameter.
     */
    private ControlConfigurationInputType dataType;

    /**
     * the parameter in the control this is for.
     */
    private String parameter;

    /**
     * the control label.
     */
    private String label;

    /**
     * The description of the control.
     */
    private String description;

    /**
     * Any kind of predefined value(s).
     */
    private List<ControlListConfigurationItem> listValues;

    /**
     * The parameter this is for.
     *
     * @param parameterName The parameter to set.
     * @return this for fluent.
     */
    public ControlConfiguration setParameter(final String parameterName) {
        this.parameter = parameterName;
        return this;
    }

    /**
     * Set the data type.
     *
     * @param controlDataType The datatype to set.
     * @return this for fluent.
     */
    public ControlConfiguration setDataType(final ControlConfigurationInputType controlDataType) {
        this.dataType = controlDataType;
        return this;
    }

    /**
     * Sets a description for a control.
     *
     * @param controlDescription The description to set.
     * @return this for fluent.
     */
    public ControlConfiguration setDescription(final String controlDescription) {
        this.description = controlDescription;
        return this;
    }

    /**
     * Set the list values.
     *
     * @param configListValues The list values to set.
     * @return this for fluent.
     */
    public ControlConfiguration setListValues(final List<ControlListConfigurationItem> configListValues) {
        this.listValues = configListValues;
        return this;
    }

    /**
     * The data type used in the parameter.
     *
     * @return the datatype
     */
    public ControlConfigurationInputType getDataType() {
        return dataType;
    }

    /**
     * the parameter in the control this is for.
     *
     * @return the parameter
     */
    public String getParameter() {
        return parameter;
    }

    /**
     * the control label.
     *
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * the control label.
     *
     * @param configLabel the label to set
     * @return this for fluent.
     */
    public ControlConfiguration setLabel(final String configLabel) {
        this.label = configLabel;
        return this;
    }

    /**
     * The description of the control.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Any kind of predefined value(s).
     *
     * @return the listValues
     */
    public List<ControlListConfigurationItem> getListValues() {
        return listValues;
    }

}
