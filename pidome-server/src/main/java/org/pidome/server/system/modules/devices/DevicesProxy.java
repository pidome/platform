/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.modules.devices;

import io.vertx.core.Promise;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.pidome.platform.modules.devices.Device;
import org.pidome.platform.modules.devices.DeviceDataNotification;
import org.pidome.platform.modules.devices.discovery.DevicesServerProxy;
import org.pidome.platform.modules.devices.discovery.DiscoveredDevice;
import org.pidome.platform.modules.items.ItemAddress;

/**
 * Proxy for device related methods and handling.
 *
 * @author johns
 */
public class DevicesProxy implements DevicesServerProxy {

    /**
     * The devices module container.
     */
    private DevicesModuleContainer container;

    /**
     * List of running devices in this proxy.
     */
    private final Set<DeviceMeta> devices = Collections.synchronizedSet(new HashSet<>());

    /**
     * Registers devices in the proxy.
     *
     * @param devicesToAdd The devices to register.
     */
    protected void addDevices(final Set<DeviceMeta> devicesToAdd) {
        devices.addAll(devicesToAdd);
    }

    /**
     * Removes devices from the proxy.
     *
     * @param devicesToRemove The devices to remove.
     */
    protected void removeDevices(final Set<DeviceMeta> devicesToRemove) {
        devices.removeAll(devicesToRemove);
    }

    /**
     * Returns the devices known in the proxy.
     *
     * @return The devices set.
     */
    protected Set<DeviceMeta> getDevices() {
        return this.devices;
    }

    /**
     * Sets the devices module container.
     *
     * @param devicesContainer The devices container to set.
     */
    protected final void setContainer(final DevicesModuleContainer devicesContainer) {
        this.container = devicesContainer;
    }

    /**
     * Closes a proxy.
     */
    @SuppressWarnings({"PMD.NullAssignment"}) /// Required for cross references between different class loaders.
    public void close() {
        this.container = null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void handleNewDiscoveredDevice(final DiscoveredDevice device) {
        if (this.container != null) {
            this.container.handleDiscoveredDevice(device);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<Class<? extends Device>, List<ItemAddress>> getRunningAddresses() {
        Map<Class<? extends Device>, List<ItemAddress>> returnMap = new HashMap<>();
        this.devices.stream().forEach(deviceMeta -> {
            Class<? extends Device> deviceClass = deviceMeta.getItem().getClass();
            returnMap.putIfAbsent(deviceClass, new ArrayList<>());
            returnMap.get(deviceMeta.getItem().getClass()).add(deviceMeta.getItem().getAddress());
        });
        return returnMap;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void handleDeviceDataNotification(final DeviceDataNotification notification, final Promise sendResult) {
        this.container.dispatchDataNotification(notification);
        sendResult.complete();
    }

}
