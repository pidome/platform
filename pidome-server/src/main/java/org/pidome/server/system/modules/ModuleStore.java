/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.modules;

import io.vertx.core.Future;
import io.vertx.core.Promise;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.modules.ModuleBase;
import org.pidome.platform.modules.ModuleTargets;
import org.pidome.platform.modules.ModuleType;
import org.pidome.platform.modules.PidomeModule;
import org.pidome.platform.presentation.input.InputForm;
import org.pidome.server.services.modules.ModuleDefinition;
import org.pidome.server.services.modules.ModuleLoaderConfiguration;
import org.pidome.server.system.database.Database;
import org.pidome.server.system.installer.packages.PackageClassLoaderException;
import org.pidome.server.system.installer.packages.PackageMutationListener;
import org.pidome.server.system.installer.packages.PackageStore;
import org.pidome.server.system.installer.packages.ServerPackage;

/**
 * Store for scanning, caching and supplying modules.
 *
 * @author johns
 */
@SuppressWarnings("CPD-START")
public final class ModuleStore implements PackageMutationListener {

    /**
     * Hardware root logger.
     */
    private static final Logger LOG = LogManager.getLogger(ModuleStore.class);

    /**
     * Constructor.
     */
    protected ModuleStore() {
    }

    /**
     * Returns the known modules categorized by module type.
     *
     * @return Map with module types and associated modules.
     */
    public List<ModuleDefinition> getModuleCollection() {
        try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {

            CriteriaBuilder cb = autoManager.getManager().getCriteriaBuilder();
            CriteriaQuery<ModuleDefinition> cq = cb.createQuery(ModuleDefinition.class);
            Root<ModuleDefinition> rootEntry = cq.from(ModuleDefinition.class);
            CriteriaQuery<ModuleDefinition> select = cq.select(rootEntry);

            return autoManager.getManager().createQuery(select).getResultList();
        }
    }

    /**
     * Returns a set of modules for the given module type.
     *
     * @param moduleType The module type.
     * @return Unmodifiable set of modules.
     */
    public List<ModuleDefinition> getModuleCollectionForType(final ModuleType moduleType) {
        try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {

            CriteriaBuilder cb = autoManager.getManager().getCriteriaBuilder();
            CriteriaQuery<ModuleDefinition> cq = cb.createQuery(ModuleDefinition.class);
            Root<ModuleDefinition> rootEntry = cq.from(ModuleDefinition.class);
            CriteriaQuery<ModuleDefinition> select = cq.select(rootEntry);

            select.where(cb.equal(rootEntry.get("moduleType"), moduleType));
            return autoManager.getManager().createQuery(select).getResultList();
        }
    }

    /**
     * Removes a configuration from the database.
     *
     * @param configuration The configuration to remove.
     */
    public void removePersistedModuleConfiguration(final ModuleLoaderConfiguration configuration) {
        if (configuration != null) {
            try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
                autoManager.getManager().getTransaction().begin();
                if (autoManager.getManager().contains(configuration)) {
                    autoManager.getManager().remove(configuration);
                } else if (configuration.getId() != null) {
                    autoManager.getManager().remove(autoManager.getManager().getReference(ModuleLoaderConfiguration.class, configuration.getId()));
                }
                autoManager.getManager().getTransaction().commit();
            } catch (EntityNotFoundException ex) {
                LOG.warn("Unable to remove a configuration [{}], [{}]", configuration, ex.getMessage());
            }
        }
    }

    /**
     * Returns a list of persisted configurations to reload.
     *
     * @return Module configurations list.
     */
    public List<ModuleLoaderConfiguration> getPersistedConfigurations() {
        try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {

            CriteriaBuilder cb = autoManager.getManager().getCriteriaBuilder();
            CriteriaQuery<ModuleLoaderConfiguration> cq = cb.createQuery(ModuleLoaderConfiguration.class);
            Root<ModuleLoaderConfiguration> rootEntry = cq.from(ModuleLoaderConfiguration.class);
            CriteriaQuery<ModuleLoaderConfiguration> select = cq.select(rootEntry);

            return autoManager.getManager().createQuery(select).getResultList();
        }
    }

    /**
     * Returns a set of modules for the given module implementation class.
     *
     * @param <T> The base implementation class type to fetch for.
     * @param implementation The base implementation to fetch for.
     * @return Unmodifiable set of modules.
     */
    public <T extends ModuleBase> List<ModuleDefinition> getModuleCollectionByImplementation(final Class<T> implementation) {
        return getModuleCollectionForType(ModuleType.getByImplementationClass(implementation));
    }

    /**
     * Checks if there are modules available of the given type.
     *
     * @param moduleType The type to get the modules for.
     * @return true if a module set is found.
     */
    protected boolean hasModulesAvailable(final ModuleType moduleType) {
        return !getModuleCollectionForType(moduleType).isEmpty();
    }

    /**
     * Scans for drivers annotated with HardwareDriverDiscovery.
     *
     * @return The future indicating scanning is done.
     */
    @SuppressWarnings("unchecked")
    public Future<Void> scan() {
        final Promise<Void> promise = Promise.promise();
        LOG.info("Requesting packages for available modules");
        EntityManager manager = Database.getInstance().getNewManager();
        Map<ServerPackage, List<Class<? extends ModuleBase>>> collection = PackageStore.provides(PidomeModule.class, ModuleBase.class);
        collection.forEach((serverPackage, classList) -> {
            Set<ModuleDefinition> definitionsSet = new HashSet<>();
            classList.stream().forEach(matchedClass -> {
                LOG.info("Found module: [{}], [{}]", serverPackage, matchedClass);
                for (ModuleType moduleType : ModuleType.values()) {
                    if (matchedClass.getSuperclass().equals(moduleType.getImplementation())) {
                        PidomeModule moduleInfo = matchedClass.getAnnotation(PidomeModule.class);
                        ModuleTargets moduleTargets = matchedClass.getAnnotation(ModuleTargets.class);
                        ModuleDefinition moduleDefinition = new ModuleDefinition();
                        if (moduleTargets != null) {
                            moduleDefinition.setTargets(Arrays.asList(moduleTargets.targets()));
                            moduleDefinition.setTargetExclusive(moduleTargets.exclusive());
                        }
                        moduleDefinition.setModuleType(moduleType);
                        moduleDefinition.setTransportTarget(moduleInfo.transport());
                        moduleDefinition.setProvidedBy(serverPackage.getId());
                        moduleDefinition.setName(moduleInfo.name());
                        moduleDefinition.setDescription(moduleInfo.description());
                        moduleDefinition.setModuleClass(matchedClass.getCanonicalName());
                        manager.persist(moduleDefinition);
                        definitionsSet.add(moduleDefinition);
                        break;
                    }
                }
            });
            if (!definitionsSet.isEmpty()) {
                if (!manager.getTransaction().isActive()) {
                    manager.getTransaction().begin();
                }
            }
            serverPackage.setModules(definitionsSet);
        });
        if (manager.getTransaction().isActive()) {
            manager.getTransaction().commit();
        }
        if (manager.isOpen()) {
            manager.close();
        }
        promise.complete();
        return promise.future();
    }

    /**
     * Returns the generic implementation of a module.
     *
     * @param moduleType The module type to load the module from.
     * @param serverPackage The server package to load.
     * @param moduleName The name of the module to load.
     * @return Instantiated module.
     * @throws ModuleStoreException When loading a package fails, cause is
     * included.
     */
    public ModuleBase loadModule(final ModuleType moduleType, final ServerPackage serverPackage, final String moduleName) throws ModuleStoreException {
        Class<? extends ModuleBase> classToLoad = getModuleClassFor(moduleType, serverPackage, moduleName);
        try {
            return PackageStore.loadInstanceFromPackage(this, serverPackage, classToLoad);
        } catch (PackageClassLoaderException ex) {
            throw new ModuleStoreException("Unable to load module package from store", ex);
        }
    }

    /**
     * Checks if the given module is a valid module.
     *
     * A valid module is a module that exists within the store.
     *
     * @param module The module to check.
     * @return true when valid.
     */
    @SuppressWarnings({"PMD.AvoidCatchingNPE"})
    public boolean moduleValid(final ModuleDefinition module) {
        try {
            return this.getModuleCollectionForType(module.getModuleType())
                    .stream()
                    .anyMatch(moduleInList -> moduleInList.equals(module));
        } catch (NullPointerException ex) {
            LOG.warn("Module does not exist in store", ex);
        }
        return false;
    }

    /**
     * Unloads a module.
     *
     * This method does not remove the module from the store registration. For
     * this a package needs to be removed.
     *
     * @param module The module to unload.
     */
    public void unloadModule(final ModuleBase module) {
        PackageStore.unloadClass(module.getClass());
    }

    /**
     * When there is no known registration of the class requested.
     *
     * @param moduleType The module type the class is meant for.
     * @param serverPackage The server package where the class is available in.
     * @param moduleClass the name of the class to load.
     * @return The class able to be instantiated.
     * @throws ModuleStoreException When there is no registration of the class
     * known.
     */
    @SuppressWarnings("unchecked")
    private Class<? extends ModuleBase> getModuleClassFor(final ModuleType moduleType, final ServerPackage serverPackage, final String moduleClass) throws ModuleStoreException {
        if (hasModulesAvailable(moduleType)) {
            try (PackageStore.UnboundPackageLoad loader = new PackageStore.UnboundPackageLoad(serverPackage,
                    getModuleCollectionForType(moduleType).stream()
                            .filter(mod -> mod.getProvidedBy().equals(serverPackage.getId()) && mod.getModuleClass().equals(moduleClass))
                            .findFirst()
                            .map(def -> def.getModuleClass())
                            .orElseThrow(() -> new ModuleStoreException("Module " + moduleClass + " is unknown on the system, make sure the package is installed")),
                    true)) {
                return loader.getLoadedClass();
            } catch (Exception ex) {
                throw new ModuleStoreException("Unable to load class [" + moduleClass + "] from [" + serverPackage.getPresentationName() + "]", ex);
            }
        } else {
            throw new ModuleStoreException("Module type " + moduleType + " has no modules available");
        }
    }

    /**
     * Returns the configuration class for the given driver.
     *
     * This method searches in the class loader of an instantiated
     * <code>HardwareDriver</code>.
     *
     * @param module The instantiated hardware driver.
     * @return The configuration class configured on the
     * <code>ModuleBase</code>.
     * @throws ModuleStoreException When the base configuration is unable to be
     * retrieved.
     */
    @SuppressWarnings({"unchecked", "CPD-START"})
    public InputForm getModuleConfiguration(final ModuleBase module) throws ModuleStoreException {
        try {
            Type type = module.getClass().getGenericSuperclass();
            Class<? extends InputForm> configurationFormClass = (Class<? extends InputForm>) ((ParameterizedType) type).getActualTypeArguments()[0];
            return (InputForm) configurationFormClass.getConstructors()[0].newInstance();
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            LOG.error("Unable to get the default configuration for driver.");
            throw new ModuleStoreException("Unable to get driver base configuration", ex);
        }
    }

    @Override
    public void packageRemoved(final ServerPackage serverPackage) {
        /// When a package is removed.
    }

    @Override
    public void packageAdded(final ServerPackage serverPackage) {
        /// When a package is added.
    }

}
