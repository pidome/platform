/*
 * Copyright 2013 John Sirach <john.sirach@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pidome.server.system.hardware.serial;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.env.PlatformOs;
import org.pidome.server.system.hardware.HardwareComponent;
import org.pidome.server.system.hardware.HardwareMutationListener;
import org.pidome.server.system.hardware.linux.LinuxUdevLib;
import org.pidome.server.system.hardware.windows.WindowsSerialUtils;

/**
 * Root class for serial based devices.
 *
 * @author John Sirach
 * @todo finish implementation
 */
public class SerialDevices extends HardwareComponent {

    /**
     * Serial devices logger.
     */
    private static final Logger LOG = LogManager.getLogger(SerialDevices.class);

    /**
     * Port literal for the raspberry pi GPIO serial port.
     */
    private static final String RASP_SERIAL_PORT = "ttyAMA0";

    /**
     * Constructor.
     *
     * @param root The hardware root.
     * @throws UnsupportedOperationException When the platform is unsupported.
     */
    public SerialDevices(final HardwareMutationListener root) throws UnsupportedOperationException {
        super(Interface.SERIAL, root);
        if (!PlatformOs.isOs(PlatformOs.OS.LINUX) && !PlatformOs.isOs(PlatformOs.OS.WINDOWS)) {
            LOG.error("Platform [{}] is not (yet) supported", PlatformOs.getOs());
        }
    }

    /**
     * Discovery of the serial devices.
     *
     * @throws UnsupportedOperationException When the discovery is unsupported.
     */
    @Override
    public void discover() throws UnsupportedOperationException {
        List<SerialDevice> serialCollection = null;
        switch (PlatformOs.getOs()) {
            case WINDOWS:
                serialCollection = discoverWindowsSerialDevices();
                break;
            case LINUX:
                serialCollection = discoverLinuxSerialDevices();
                break;
            default:
                throw new UnsupportedOperationException("Platform [" + PlatformOs.getOs() + "] is currently unsupported for serial discovery.");

        }
        serialCollection.stream().forEach(serialDevice -> {
            this.registerPeripheral(serialDevice);
        });
    }

    /**
     * discovery list on windows.
     *
     * @return List of found com ports on windows.
     */
    private List<SerialDevice> discoverWindowsSerialDevices() {
        WindowsSerialUtils.enumerateComPorts(false);
        return Collections.emptyList();
    }

    /**
     * Performs discoery of fixed serial devices on linux.
     *
     * @return Collection of linux serial devices.
     */
    @SuppressWarnings({"PMD.AvoidCatchingNPE"})
    private List<SerialDevice> discoverLinuxSerialDevices() {
        List<SerialDevice> serialCollection = new ArrayList<>();
        try {
            LinuxUdevLib udevLib = LinuxUdevLib.INSTANCE;
            LinuxUdevLib.udev udevPointer = udevLib.udev_new();

            if (udevPointer == null) {
                LOG.error("Can't create udev link, unable to discover physical serial ports");
            } else {
                LinuxUdevLib.udev_enumerate enumerate = udevLib.udev_enumerate_new(udevPointer);
                udevLib.udev_enumerate_scan_devices(enumerate);

                LinuxUdevLib.udev_list_entry devices = udevLib.udev_enumerate_get_list_entry(enumerate);

                while (devices != null) {
                    String devicePath = udevLib.udev_list_entry_get_name(devices);
                    LinuxUdevLib.udev_device enumDev = udevLib.udev_device_new_from_syspath(udevPointer, devicePath);
                    try {
                        LinuxUdevLib.udev_device parentDevice = udevLib.udev_device_get_parent_with_subsystem_devtype(
                                enumDev,
                                "tty",
                                "platform");
                        if (parentDevice != null) {
                            if (devicePath != null) {
                                String devicePort = devicePath.substring(devicePath.lastIndexOf("/") + 1);
                                SerialDevice serialDevice = new SerialDevice();
                                serialDevice.setKey(devicePath);
                                serialDevice.setPath(devicePath);
                                serialDevice.setPort(devicePort);
                                if (RASP_SERIAL_PORT.equals(devicePort) && PlatformOs.isRaspberryPi()) {
                                    serialDevice.setVendorId("Raspberry Pi Foundation");
                                    serialDevice.setProductId("GPIOSerial");
                                    serialDevice.setFriendlyName("Raspberry Pi GPIO Serial");
                                } else {
                                    serialDevice.setVendorId("Host platform");
                                    serialDevice.setProductId("Unknown");
                                    serialDevice.setFriendlyName(devicePort);
                                }
                                serialCollection.add(serialDevice);
                            }
                        }
                    } catch (NullPointerException ex) {
                        if (LOG.isDebugEnabled()) {
                            LOG.error("No subsystem present to check if the serial device is on platform", ex);
                        }
                    }
                    if (enumDev != null) {
                        udevLib.udev_device_unref(enumDev);
                    }
                    devices = udevLib.udev_list_entry_get_next(devices);
                }
                udevLib.udev_enumerate_unref(enumerate);
            }
            if (udevPointer != null) {
                udevLib.udev_unref(udevPointer);
            }
        } catch (UnsatisfiedLinkError e) {
            LOG.error("Udev library not found, no automatic attach/detach and discovery functions possible");
        }
        return serialCollection;
    }

    /**
     * Starts the Serial Devices component.
     *
     * @throws UnsupportedOperationException When serial devices are not
     * supported.
     */
    @Override
    public void start() throws UnsupportedOperationException {
        /// Not used, no watchdog available for this interface.
    }

    /**
     * Stops the serial devices component.
     *
     * @throws UnsupportedOperationException When serial devices are not
     * supported.
     */
    @Override
    public void stop() throws UnsupportedOperationException {
        /// Not used, no watchdog available for this interface.
    }

}
