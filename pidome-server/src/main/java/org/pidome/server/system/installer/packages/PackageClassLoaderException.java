/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.installer.packages;

/**
 * Exception used when the package class loader fails to initialize.
 *
 * @author johns
 */
public class PackageClassLoaderException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructs an instance of <code>PackageClassLoaderException</code> with
     * the specified detail message.
     *
     * @param msg The message of exception.
     */
    public PackageClassLoaderException(final String msg) {
        super(msg);
    }

    /**
     * Constructs an instance of <code>PackageClassLoaderException</code> with
     * the specified detail message and cause.
     *
     * @param cause The original exception.
     */
    public PackageClassLoaderException(final Throwable cause) {
        super(cause);
    }

    /**
     * Constructs an instance of <code>PackageClassLoaderException</code> with
     * the specified detail message and cause.
     *
     * @param msg the detail message.
     * @param cause The original exception.
     */
    public PackageClassLoaderException(final String msg, final Throwable cause) {
        super(msg, cause);
    }

}
