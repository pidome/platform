/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.database;

import java.util.Locale;
import java.util.Optional;
import org.pidome.server.system.database.driver.DatabaseDriverType;
import org.pidome.tools.enums.Protocol;

/**
 * The database configuration class.
 *
 * @author johns
 */
public class DatabaseConfiguration {

    /**
     * the resolved database driver type.
     */
    private final DatabaseDriverType driverType;

    /**
     * Database location.
     */
    private Optional<String> location;

    /**
     * the protocol used.
     */
    private Optional<Protocol> protocol;

    /**
     * The remote/local port.
     */
    private Optional<Integer> port;

    /**
     * Additional information applicable to an JDBC string.
     */
    private Optional<String> jdbcAppend;

    /**
     * The database to connect to.
     */
    private String database;

    /**
     * The catalog.
     */
    private Optional<String> catalog;

    /**
     * The schema.
     */
    private Optional<String> schema;

    /**
     * Configuration constructor.
     *
     * @param driverType The driver type.
     */
    protected DatabaseConfiguration(final DatabaseDriverType driverType) {
        this.driverType = driverType;
    }

    /**
     * the resolved database driver type.
     *
     * @return the driverType
     */
    public DatabaseDriverType getDriverType() {
        return driverType;
    }

    /**
     * Set the protocol.
     *
     * @param protocol The protocol to set.
     */
    protected void setProtocol(final String protocol) {
        if (protocol.isBlank()) {
            this.protocol = Optional.empty();
        } else {
            this.protocol = Optional.of(Enum.valueOf(Protocol.class, protocol.toUpperCase(Locale.US)));
        }
    }

    /**
     * Returns the protocol.
     *
     * @return The protocol.
     */
    public Optional<Protocol> getProtocol() {
        return this.protocol;
    }

    /**
     * Sets the port.
     *
     * @param port the port to set.
     */
    protected void setPort(final String port) {
        if (port == null || port.isBlank()) {
            this.port = Optional.empty();
        } else {
            this.port = Optional.of(Integer.parseInt(port));
        }
    }

    /**
     * Returns the port.
     *
     * @return The port to return.
     */
    public Optional<Integer> getPort() {
        return this.port;
    }

    /**
     * Database location.
     *
     * @return the location
     */
    public Optional<String> getLocation() {
        return location;
    }

    /**
     * Database location.
     *
     * @param location the location to set
     */
    protected void setLocation(final String location) {
        if (location.isBlank()) {
            this.location = Optional.empty();
        } else {
            this.location = Optional.of(location);
        }
    }

    /**
     * Additional information applicable to an JDBC string.
     *
     * @return the jdbcAppend
     */
    public Optional<String> getJdbcAppend() {
        return jdbcAppend;
    }

    /**
     * Additional information applicable to an JDBC string.
     *
     * @param jdbcAppend the jdbcAppend to set
     */
    protected void setJdbcAppend(final String jdbcAppend) {
        this.jdbcAppend = Optional.ofNullable(jdbcAppend);
    }

    /**
     * The database to connect to.
     *
     * @return the database
     */
    public String getDatabase() {
        return database;
    }

    /**
     * The database to connect to.
     *
     * @param database the database to set
     */
    protected void setDatabase(final String database) {
        this.database = database;
    }

    /**
     * The catalog.
     *
     * @return the catalog
     */
    public Optional<String> getCatalog() {
        return catalog;
    }

    /**
     * The catalog.
     *
     * @param catalog the catalog to set
     */
    protected void setCatalog(final String catalog) {
        if (catalog.isBlank()) {
            this.catalog = Optional.empty();
        } else {
            this.catalog = Optional.of(catalog);
        }
    }

    /**
     * The schema.
     *
     * @return the schema
     */
    public Optional<String> getSchema() {
        return schema;
    }

    /**
     * The schema.
     *
     * @param schema the schema to set
     */
    protected void setSchema(final String schema) {
        if (schema.isBlank()) {
            this.schema = Optional.empty();
        } else {
            this.schema = Optional.of(schema);
        }
    }

}
