/**
 * Private utilities.
 * <p>
 * This package contains some private utilities, when considered useful they
 * will move to the generic utilities package.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.system.utils;
