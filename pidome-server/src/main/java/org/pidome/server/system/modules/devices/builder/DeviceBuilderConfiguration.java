/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.modules.devices.builder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import org.pidome.platform.modules.devices.ColorpickerMode;
import org.pidome.platform.modules.devices.ControlGraphType;
import org.pidome.platform.modules.devices.ControlVisualType;
import org.pidome.platform.modules.devices.DeviceBooleanDataControl.BooleanVisualType;
import org.pidome.platform.modules.devices.DeviceBuilderType;
import org.pidome.platform.modules.devices.DeviceControlGroup;
import org.pidome.platform.modules.devices.DeviceControlType;
import org.pidome.platform.modules.devices.DeviceParametersList;
import org.pidome.platform.modules.items.ItemAddress;
import org.pidome.tools.utilities.NumberUtils;

/**
 * A configuration for the device builder.
 *
 * @author johns
 */
public class DeviceBuilderConfiguration {

    /**
     * Parameter true text reusable.
     */
    private static final String PARAMETER_TRUETEXT = "trueText";

    /**
     * Parameter false text reusable.
     */
    private static final String PARAMETER_FALSETEXT = "falseText";

    /**
     * Id of the package of the device.
     */
    private UUID packageId;

    /**
     * Which container handles this device configuration.
     */
    private String deviceContainerId;

    /**
     * Class path of the device.
     */
    private String deviceClass;

    /**
     * The builder type.
     */
    private DeviceBuilderType builderType;

    /**
     * Group configuration of the device.
     */
    private List<DeviceControlGroup> groups;

    /**
     * The device parameters.
     */
    private DeviceParametersList deviceParameters;

    /**
     * The device address configuration.
     */
    private ItemAddress deviceAddress;

    /**
     * The options available for the controls included in the builder
     * configuration.
     */
    @SuppressWarnings({"PMD.LooseCoupling"})
    private LinkedHashMap<DeviceControlType, Map<String, ControlConfiguration>> controlOptions;

    /**
     * Id of the package of the device.
     *
     * @return the packageId
     */
    public UUID getPackageId() {
        return packageId;
    }

    /**
     * Id of the package of the device.
     *
     * @param packageId the packageId to set
     */
    public void setPackageId(final UUID packageId) {
        this.packageId = packageId;
    }

    /**
     * Which container handles this device configuration.
     *
     * @return the deviceContainer
     */
    public String getDeviceContainerId() {
        return deviceContainerId;
    }

    /**
     * Which container handles this device configuration.
     *
     * @param deviceContainerId the deviceContainer to set
     */
    public void setDeviceContainerId(final String deviceContainerId) {
        this.deviceContainerId = deviceContainerId;
    }

    /**
     * Class path of the device.
     *
     * @return the deviceClass
     */
    public String getDeviceClass() {
        return deviceClass;
    }

    /**
     * The builder type.
     *
     * @return the builderType
     */
    public DeviceBuilderType getBuilderType() {
        return builderType;
    }

    /**
     * The builder type.
     *
     * @param builderType the builderType to set
     */
    public void setBuilderType(final DeviceBuilderType builderType) {
        this.builderType = builderType;
    }

    /**
     * Class path of the device.
     *
     * @param deviceClass the deviceClass to set
     */
    public void setDeviceClass(final String deviceClass) {
        this.deviceClass = deviceClass;
    }

    /**
     * Group configuration of the device.
     *
     * @return the groups
     */
    public List<DeviceControlGroup> getGroups() {
        return groups;
    }

    /**
     * Group configuration of the device.
     *
     * @param groups the groups to set
     */
    public void setGroups(final List<DeviceControlGroup> groups) {
        this.groups = groups;
        this.setControlOptions(groups);
    }

    /**
     * Set the device parameters.
     *
     * @param deviceParameters The device parameters to set.
     */
    public void setDeviceParameters(final DeviceParametersList deviceParameters) {
        this.deviceParameters = deviceParameters;
    }

    /**
     * The device parameters.
     *
     * @return The device parameters.
     */
    public DeviceParametersList getDeviceParameters() {
        return this.deviceParameters;
    }

    /**
     * The device address configuration.
     *
     * @return the deviceAddress
     */
    public ItemAddress getDeviceAddress() {
        return deviceAddress;
    }

    /**
     * The device address configuration.
     *
     * @param deviceAddress the deviceAddress to set
     */
    public void setDeviceAddress(final ItemAddress deviceAddress) {
        this.deviceAddress = deviceAddress;
    }

    /**
     * Returns the control options.
     *
     * @return Map with control options.
     */
    @JsonProperty("controlOptions")
    public Map<DeviceControlType, Map<String, ControlConfiguration>> getControlOptions() {
        return this.controlOptions;
    }

    /**
     * Creates the options available for the controls included.
     *
     * Control types are not duplicated.
     *
     * @param controlGroups The control groups containing the controls.
     */
    @JsonIgnore
    @SuppressWarnings({"checkstyle:methodlength"})
    private void setControlOptions(final List<DeviceControlGroup> controlGroups) {
        this.controlOptions = new LinkedHashMap<>();
        controlGroups.stream()
                .flatMap(group -> group.getControls().stream())
                .map(control -> control.getControlType())
                .distinct()
                .forEach(controlType -> {
                    /// All type options.
                    this.controlOptions.putIfAbsent(controlType, new LinkedHashMap<>());
                    this.controlOptions.get(controlType).put(
                            "retention",
                            new ControlConfiguration()
                                    .setParameter("retention")
                                    .setLabel("Retention")
                                    .setDescription("Save state between restarts")
                                    .setDataType(ControlConfigurationInputType.BOOLEAN));
                    this.controlOptions.get(controlType).put(
                            "shortcutSlot",
                            new ControlConfiguration()
                                    .setParameter("shortcutSlot")
                                    .setLabel("Shortcut slot")
                                    .setDescription("Set location in lists")
                                    .setDataType(ControlConfigurationInputType.LIST)
                                    .setListValues(List.of(
                                            new ControlListConfigurationItem().setName("None").setValue(NumberUtils.INTEGER_ZERO),
                                            new ControlListConfigurationItem().setName("First").setValue(NumberUtils.INTEGER_ONE),
                                            new ControlListConfigurationItem().setName("Second").setValue(NumberUtils.INTEGER_TWO),
                                            new ControlListConfigurationItem().setName("Third").setValue(NumberUtils.INTEGER_THREE)
                                    ))
                    );
                    /// Combined data options.
                    if (controlType.equals(controlType.DATA_BOOLEAN)
                            || controlType.equals(controlType.DATA_STRING)
                            || controlType.equals(controlType.DATA_NUMBER)) {
                        this.controlOptions.get(controlType).put(
                                "visualType",
                                new ControlConfiguration()
                                        .setParameter("visualType")
                                        .setLabel("Visual type")
                                        .setDescription("Data interpretation")
                                        .setDataType(ControlConfigurationInputType.LIST)
                                        .setListValues(
                                                Arrays.asList(ControlVisualType.class.getEnumConstants()).stream()
                                                        .map(visualType -> new ControlListConfigurationItem().setName(visualType.getDescription()).setValue(visualType))
                                                        .collect(Collectors.toList())
                                        ));
                        this.controlOptions.get(controlType).put(
                                "prefix",
                                new ControlConfiguration()
                                        .setParameter("prefix")
                                        .setLabel("Prefix")
                                        .setDescription("Text before the data")
                                        .setDataType(ControlConfigurationInputType.STRING));
                        this.controlOptions.get(controlType).put(
                                "suffix",
                                new ControlConfiguration()
                                        .setParameter("suffix")
                                        .setLabel("Suffix")
                                        .setDescription("Text after the data")
                                        .setDataType(ControlConfigurationInputType.STRING)
                        );
                    }
                    switch (controlType) {
                        case DATA_STRING:
                        case DATA_BOOLEAN:
                        case DATA_NUMBER:
                            this.controlOptions.get(controlType).put(
                                    "graphType",
                                    new ControlConfiguration()
                                            .setParameter("graphType")
                                            .setLabel("Graph type")
                                            .setDescription("The graph type to apply")
                                            .setDataType(ControlConfigurationInputType.LIST)
                                            .setListValues(
                                                    Arrays.asList(ControlGraphType.class.getEnumConstants()).stream()
                                                            .map(graphType -> new ControlListConfigurationItem().setName(graphType.getDescription()).setValue(graphType))
                                                            .collect(Collectors.toList())
                                            )
                            );
                            break;
                        default:
                            break;
                    }
                    /// Type specific options.
                    switch (controlType) {
                        case TOGGLE:
                            this.controlOptions.get(controlType).put(
                                    PARAMETER_TRUETEXT,
                                    new ControlConfiguration()
                                            .setParameter(PARAMETER_TRUETEXT)
                                            .setLabel("True/On text")
                                            .setDescription("Text shown when true")
                                            .setDataType(ControlConfigurationInputType.STRING));
                            this.controlOptions.get(controlType).put(
                                    PARAMETER_FALSETEXT,
                                    new ControlConfiguration()
                                            .setParameter(PARAMETER_FALSETEXT)
                                            .setLabel("False/Off text")
                                            .setDescription("Text shown when false")
                                            .setDataType(ControlConfigurationInputType.STRING)
                            );
                            break;
                        case SLIDER:
                            this.controlOptions.get(controlType).put(
                                    "min",
                                    new ControlConfiguration()
                                            .setParameter("min")
                                            .setLabel("Minimal value")
                                            .setDescription("The minimum value")
                                            .setDataType(ControlConfigurationInputType.DOUBLE));
                            this.controlOptions.get(controlType).put(
                                    "max",
                                    new ControlConfiguration()
                                            .setParameter("max")
                                            .setLabel("Maximum value")
                                            .setDescription("The maximum value")
                                            .setDataType(ControlConfigurationInputType.DOUBLE)
                            );
                            break;
                        case SELECT:
                            /// No additional options.
                            break;
                        case DATA_BOOLEAN:
                            this.controlOptions.get(controlType).put(
                                    "booleanVisualType",
                                    new ControlConfiguration()
                                            .setParameter("booleanVisualType")
                                            .setLabel("Visualization")
                                            .setDescription("True / False presentation when shown")
                                            .setDataType(ControlConfigurationInputType.LIST)
                                            .setListValues(
                                                    Arrays.asList(BooleanVisualType.class.getEnumConstants()).stream()
                                                            .map(boolVisualType -> new ControlListConfigurationItem().setName(boolVisualType.getDescription()).setValue(boolVisualType))
                                                            .collect(Collectors.toList())
                                            ));
                            this.controlOptions.get(controlType).put(
                                    PARAMETER_TRUETEXT,
                                    new ControlConfiguration()
                                            .setParameter(PARAMETER_TRUETEXT)
                                            .setLabel("True text")
                                            .setDescription("Text to show when true")
                                            .setDataType(ControlConfigurationInputType.STRING));
                            this.controlOptions.get(controlType).put(
                                    PARAMETER_FALSETEXT,
                                    new ControlConfiguration()
                                            .setParameter(PARAMETER_FALSETEXT)
                                            .setLabel("False text")
                                            .setDescription("Text to show when false")
                                            .setDataType(ControlConfigurationInputType.STRING));
                            break;
                        case DATA_NUMBER:
                            this.controlOptions.get(controlType).put(
                                    "minValue",
                                    new ControlConfiguration()
                                            .setParameter("minValue")
                                            .setLabel("Minimum value")
                                            .setDescription("The minimal value, leave empty for no minimum")
                                            .setDataType(ControlConfigurationInputType.DOUBLE));
                            this.controlOptions.get(controlType).put(
                                    "lowCriticalValue",
                                    new ControlConfiguration()
                                            .setParameter("lowCriticalValue")
                                            .setLabel("Low critical")
                                            .setDescription("A low critical threshold, leave empty for no threshold")
                                            .setDataType(ControlConfigurationInputType.DOUBLE));
                            this.controlOptions.get(controlType).put(
                                    "lowWarningValue",
                                    new ControlConfiguration()
                                            .setParameter("lowWarningValue")
                                            .setLabel("Low warning")
                                            .setDescription("A low warning threshold, leave empty for no threshold")
                                            .setDataType(ControlConfigurationInputType.DOUBLE));
                            this.controlOptions.get(controlType).put(
                                    "highWarningValue",
                                    new ControlConfiguration()
                                            .setParameter("highWarningValue")
                                            .setLabel("High warning")
                                            .setDescription("A high warning threshold, leave empty for no threshold")
                                            .setDataType(ControlConfigurationInputType.DOUBLE));
                            this.controlOptions.get(controlType).put(
                                    "highCriticalValue",
                                    new ControlConfiguration()
                                            .setParameter("highCriticalValue")
                                            .setLabel("High critical")
                                            .setDescription("A high critical threshold, leave empty for no threshold")
                                            .setDataType(ControlConfigurationInputType.DOUBLE));
                            this.controlOptions.get(controlType).put(
                                    "maxValue",
                                    new ControlConfiguration()
                                            .setParameter("maxValue")
                                            .setLabel("Maximum value")
                                            .setDescription("The maximum value, leave empty for no maximum")
                                            .setDataType(ControlConfigurationInputType.DOUBLE));
                            break;
                        case DATA_STRING:
                            //// No additional options.
                            break;
                        case COLORPICKER:
                            this.controlOptions.get(controlType).putAll(Map.of(
                                    "mode",
                                    new ControlConfiguration()
                                            .setParameter("mode")
                                            .setLabel("Colorpicker mode")
                                            .setDescription("Color picker mode to display and apply")
                                            .setDataType(ControlConfigurationInputType.LIST)
                                            .setListValues(
                                                    Arrays.asList(ColorpickerMode.class.getEnumConstants()).stream()
                                                            .map(colorMode -> new ControlListConfigurationItem().setName(colorMode.getDescription()).setValue(colorMode))
                                                            .collect(Collectors.toList())
                                            )
                            ));
                            break;
                        case BUTTON:
                            this.controlOptions.get(controlType).putAll(Map.of(
                                    "label",
                                    new ControlConfiguration()
                                            .setParameter("label")
                                            .setLabel("Button text")
                                            .setDescription("The text on the button")
                                            .setDataType(ControlConfigurationInputType.STRING))
                            );
                            break;
                        default:
                            break;
                    }
                });
    }

}
