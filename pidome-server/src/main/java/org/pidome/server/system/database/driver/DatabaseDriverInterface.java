/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.database.driver;

import com.zaxxer.hikari.HikariConfig;
import org.pidome.server.system.database.DatabaseConfiguration;
import org.pidome.server.system.database.DatabaseConfigurationException;

/**
 * Interface for database connections.
 *
 * @author John
 */
public interface DatabaseDriverInterface {

    /**
     * Returns the data source of the driver.
     *
     * @throws DatabaseConfigurationException When a database driver can not be
     * loaded.
     */
    void loadDriver() throws DatabaseConfigurationException;

    /**
     * Returns the dialect used.
     *
     * @return The dialect as string.
     */
    String getDialect();

    /**
     * Modifier for a given HikariCP configuration.
     *
     * @param hikariConfig The HikariCP configuration to modify.
     */
    default void appendConfiguration(HikariConfig hikariConfig) {
        /// emtpty method, only required when changes are required.
    }

    /**
     * Returns the JDBC url as required by the implementation.
     *
     * @param configuration The database configuration.
     * @return The JDBC connect url.
     */
    String getJDBCUrl(DatabaseConfiguration configuration);

    /**
     * Optionally executed after initialization.
     *
     * @param configuration The configuration.
     */
    default void afterInit(final DatabaseConfiguration configuration) {
        /// Empty method.
    }

    /**
     * Optionally ran during server shutdown.
     */
    default void onShutdown() {
        /// Empty method.
    }

}
