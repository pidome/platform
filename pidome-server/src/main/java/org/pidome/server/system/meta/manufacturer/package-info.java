/**
 * Package to supply manufacturer information.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.system.meta.manufacturer;
