/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.installer.repositories.maven;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.maven.repository.internal.MavenRepositorySystemUtils;
import org.eclipse.aether.DefaultRepositorySystemSession;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.connector.basic.BasicRepositoryConnectorFactory;
import org.eclipse.aether.impl.DefaultServiceLocator;
import org.eclipse.aether.repository.LocalRepository;
import org.eclipse.aether.repository.RemoteRepository;
import org.eclipse.aether.resolution.VersionRangeRequest;
import org.eclipse.aether.resolution.VersionRangeResolutionException;
import org.eclipse.aether.resolution.VersionRangeResult;
import org.eclipse.aether.spi.connector.RepositoryConnectorFactory;
import org.eclipse.aether.spi.connector.transport.TransporterFactory;
import org.eclipse.aether.transport.file.FileTransporterFactory;
import org.eclipse.aether.transport.http.HttpTransporterFactory;
import org.eclipse.aether.version.Version;
import org.pidome.server.system.installer.UpdateSearchResult;
import org.pidome.server.system.installer.packages.GenericVersion;
import org.pidome.server.system.installer.packages.ServerPackage;
import org.pidome.server.system.installer.repositories.Repository;
import org.pidome.server.system.installer.repositories.RepositoryType;

/**
 * A maven repository type.
 *
 * This type provides searching and gethering packages from maven repositories.
 * It supports both normal and snapshot repositories. There is a naming
 * convention to follow which can be identified at:
 *
 * @author John
 */
@Entity
@DiscriminatorValue("MAVEN2")
public class Maven2Repository extends Repository {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The logger for this class.
     */
    private static final Logger LOG = LogManager.getLogger(Maven2Repository.class);

    /**
     * The current repository session.
     */
    @JsonIgnore
    @Transient
    private transient DefaultRepositorySystemSession repoSession;
    /**
     * The repository system.
     */
    @JsonIgnore
    @Transient
    private transient RepositorySystem system;

    /**
     * The remote repository.
     */
    @JsonIgnore
    @Transient
    private transient RemoteRepository remoteRepository;

    /**
     * Repository type.
     */
    public Maven2Repository() {
        super(RepositoryType.MAVEN2);
    }

    /**
     * @inheritDoc
     */
    @Override
    public final UpdateSearchResult getUpdates(final String repoId, final ServerPackage checkPackage) {
        prepareEnv(repoId);
        UpdateSearchResult result = new UpdateSearchResult();
        result.setCheckPackage(checkPackage);

        try {
            List<Version> versions = getVersions(checkPackage);
            GenericVersion current = new GenericVersion(checkPackage.getPackageVersion());

            List<String> older = new ArrayList<>();
            List<String> newer = new ArrayList<>();
            result.setOlder(older);
            result.setNewer(newer);

            for (Version version : versions) {
                GenericVersion gatheredVersion = new GenericVersion(version.toString());
                int compareResult = gatheredVersion.compareTo(current);
                switch (compareResult) {
                    case -1:
                        result.getOlder().add(gatheredVersion.toString());
                        break;
                    case 1:
                        result.getNewer().add(gatheredVersion.toString());
                        break;
                    default:
                        break;
                }
            }

            /*
            Artifact artifact = new DefaultArtifact(component.getPackageGroup(), component.getPackageName(), null, "jar", "[0,)");

            Dependency dependency = new Dependency(artifact, JavaScopes.COMPILE);

            CollectRequest collectRequest = new CollectRequest();
            collectRequest.setRoot(dependency);
            collectRequest.addRepository(repo);

            DependencyRequest dependencyRequest = new DependencyRequest();
            dependencyRequest.setCollectRequest(collectRequest);

            DependencyNode rootNode = system.resolveDependencies(session, dependencyRequest).getRoot();

            PreorderNodeListGenerator nlg = new PreorderNodeListGenerator();
            rootNode.accept(nlg);
            //return new ResolverResult(rootNode, nlg.getFiles(), nlg.getClassPath());
            return new ArrayList<>();
             */
        } catch (Exception ex) {
            LOG.error("Could not update search result for server package [{}] with repo id [{}]", checkPackage, repoId, ex);
        }

        return result;
    }

    /**
     * Retrieves the version list of the given component.
     *
     * @param component The component for which versions should be retrieved.
     * @return List of available versions.
     * @throws VersionRangeResolutionException When version retrieval fails.
     */
    private List<Version> getVersions(final ServerPackage component) throws VersionRangeResolutionException {
        DefaultArtifact artifact = new DefaultArtifact(component.getPackageGroup(), component.getPackageName(), "jar", "[,)");
        VersionRangeRequest rangeRequest = new VersionRangeRequest();
        rangeRequest.setArtifact(artifact);
        rangeRequest.addRepository(this.remoteRepository);
        VersionRangeResult rangeResult = system.resolveVersionRange(this.repoSession, rangeRequest);
        return rangeResult.getVersions();
    }

    /**
     * Prepares the environment to be used.
     *
     * @param repoId The id of the repository to prepare the environment for.
     */
    private void prepareEnv(final String repoId) {
        if (this.system == null || this.repoSession == null || this.remoteRepository == null) {
            DefaultServiceLocator locator = MavenRepositorySystemUtils.newServiceLocator();
            locator.addService(RepositoryConnectorFactory.class, BasicRepositoryConnectorFactory.class);
            locator.addService(TransporterFactory.class, FileTransporterFactory.class);
            locator.addService(TransporterFactory.class, HttpTransporterFactory.class);
            system = locator.getService(RepositorySystem.class);
            repoSession = MavenRepositorySystemUtils.newSession();
            repoSession.setLocalRepositoryManager(system.newLocalRepositoryManager(repoSession, new LocalRepository(this.constructLocalRepositoryLocation(repoId))));
            remoteRepository = new RemoteRepository.Builder(repoId, "default", this.getRepositoryLocation()).build();
        }
    }

}
