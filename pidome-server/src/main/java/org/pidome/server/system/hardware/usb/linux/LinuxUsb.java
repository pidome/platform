/*
 * Copyright 2019 John Sirach <john.sirach@pidome.org>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pidome.server.system.hardware.usb.linux;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.hardware.driver.Transport.SubSystem;
import org.pidome.server.system.hardware.HardwareRoot;
import org.pidome.server.system.hardware.linux.LinuxUdevLib;
import org.pidome.server.system.hardware.usb.UnsupportedUsbTypeException;
import org.pidome.server.system.hardware.usb.UsbInterface;
import org.pidome.server.system.hardware.usb.UsbMutationListener;
import org.pidome.server.system.hardware.usb.linux.LinuxUsbCFuncsImpl.LinuxCLib.FDSet;
import org.pidome.server.system.hardware.usb.linux.LinuxUsbCFuncsImpl.TimeVal;

/**
 * Linux implementation of USB devices.
 *
 * @author John Sirach
 */
public final class LinuxUsb implements UsbInterface {

    /**
     * Logger for the class.
     */
    private static final Logger LOG = LogManager.getLogger(LinuxUsb.class);

    /**
     * List of listeners listening to the linux USB events.
     */
    private final CopyOnWriteArrayList<UsbMutationListener> usbEventListeners = new CopyOnWriteArrayList<>();

    /**
     * Executor service for the USB polling task.
     */
    private ScheduledExecutorService pollService;

    /**
     * Link to the java udev library interface.
     */
    private LinuxUdevLib udevLib;
    /**
     * The udev pointer to the new_udev instance.
     */
    private LinuxUdevLib.udev udevPointer;
    /**
     * The udev_monitor pointer to udev_monitor_new_from_netlink.
     */
    private LinuxUdevLib.udev_monitor mon;

    /**
     * Wait time in miliseconds before the first device monitor iteration
     * starts.
     */
    private static final int FIRST_ITERATION_WAIT = 100;

    /**
     * Time between monitor scans.
     */
    private static final int CONTINUOUS_ITERATION_WAIT = 450;

    /**
     * Will hold the file descriptor int.
     */
    private int fd;

    /**
     * Adds an event listener for USB device changes.
     *
     * @param l The mutation listener to add.
     */
    @Override
    public synchronized void addEventListener(final UsbMutationListener l) {
        usbEventListeners.add(l);
        LOG.debug("Added listener: {}", l.getClass().getName());
    }

    /**
     * Removes a listener for USB device changes.
     *
     * @param l The mutation listener to remove.
     */
    @Override
    public synchronized void removeEventListener(final UsbMutationListener l) {
        usbEventListeners.remove(l);
        LOG.debug("Removed listener: {}", l.getClass().getName());
    }

    /**
     * Prediscover usb ports.
     */
    @Override
    public void discover() {
        LOG.info("Started Linux USB devices discovery");
        try {
            udevLib = LinuxUdevLib.INSTANCE;
            udevPointer = udevLib.udev_new();

            if (udevPointer == null) {
                LOG.error("Can't create udev link, unable to discover attached and real time USB events");
            } else {
                mon = udevLib.udev_monitor_new_from_netlink(udevPointer, "udev");

                LinuxUdevLib.udev_enumerate enumerate = udevLib.udev_enumerate_new(udevPointer);
                udevLib.udev_enumerate_scan_devices(enumerate);

                LinuxUdevLib.udev_list_entry devices = udevLib.udev_enumerate_get_list_entry(enumerate);

                String devicePath;

                while (devices != null) {
                    devicePath = udevLib.udev_list_entry_get_name(devices);
                    LinuxUdevLib.udev_device enumDev = udevLib.udev_device_new_from_syspath(udevPointer, devicePath);
                    if (enumDev != null) {
                        handleUsbDeviceAction(enumDev, true);
                        udevLib.udev_device_unref(enumDev);
                    }
                    devices = udevLib.udev_list_entry_get_next(devices);
                }
                udevLib.udev_enumerate_unref(enumerate);
            }
        } catch (UnsatisfiedLinkError e) {
            LOG.error("Udev library not found, no automatic attach/detach and discovery functions possible");
        }
        LOG.info("Discovery of USB devices finished");
    }

    /**
     * Enumerates device information and adds or removes a device.
     *
     * @param dev The usb device we are talking about.
     * @param action set to true to add a device, false to remove.
     */
    private void handleUsbDeviceAction(final LinuxUdevLib.udev_device dev, final boolean action) {
        LinuxUdevLib.udev_device parentDevice = udevLib.udev_device_get_parent_with_subsystem_devtype(
                dev,
                "usb",
                "usb_device");
        if (parentDevice != null) {
            String deviceName = udevLib.udev_device_get_sysattr_value(parentDevice, "product");
            String vendorId = udevLib.udev_device_get_sysattr_value(parentDevice, "idVendor");
            String deviceId = udevLib.udev_device_get_sysattr_value(parentDevice, "idProduct");
            String devicePath = udevLib.udev_device_get_devnode(dev);
            String devicePort = null;
            if (devicePath != null) {
                devicePort = devicePath.substring(devicePath.lastIndexOf("/") + 1);
            }
            String vendorName = udevLib.udev_device_get_sysattr_value(parentDevice, "manufacturer");
            String subClass = udevLib.udev_device_get_subsystem(dev);
            String path = udevLib.udev_device_get_devpath(dev);
            String serial = udevLib.udev_device_get_sysattr_value(parentDevice, "serial");

            if (serial == null) {
                serial = "unknown";
            }
            if (vendorName == null) {
                vendorName = "Unknown";
            }

            switch (subClass) {
                case "tty":
                case "hidraw":
                case "bluetooth":
                    LOG.info("USB device {}, please wait", (action ? "added" : "removed"));
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Path: {}", path);
                        LOG.debug("Device sub class: {}", subClass);
                        LOG.debug("Device serial: {}", serial);
                        LOG.debug("Device name: {}", deviceName);
                        LOG.debug("Device id: {}", deviceId);
                        LOG.debug("Device path: {}", devicePath);
                        LOG.debug("Device port: {}", devicePort);
                        LOG.debug("Device vendor id: {}", vendorId);
                        LOG.debug("Device vendor name: {}", vendorName);
                    }
                    if (action) {
                        SubSystem subSys;
                        switch (subClass) {
                            case "tty":
                                subSys = SubSystem.SERIAL;
                                break;
                            case "hidraw":
                                subSys = SubSystem.HID;
                                break;
                            case "bluetooth":
                                subSys = SubSystem.BLUETOOTH;
                                break;
                            default:
                                subSys = SubSystem.UNKNOWN;
                                break;
                        }
                        fireDeviceEvent(HardwareRoot.Mutation.ADD, subSys, serial, vendorName + ", " + deviceName, vendorId, deviceId, udevLib.udev_device_get_devpath(dev), devicePath, devicePort);
                    } else {
                        fireDeviceEvent(HardwareRoot.Mutation.REMOVE, udevLib.udev_device_get_devpath(dev));
                    }
                    break;
                default:
                    LOG.debug("Subclass {} of usb device {} is unsupported.", subClass, deviceName);
                    break;
            }
        }
    }

    /**
     * The internal usb listener for real time connects.
     *
     * @todo Switch to poll mode.
     */
    @Override
    public void startWatchdog() {
        if (udevLib != null) {

            pollService = Executors.newSingleThreadScheduledExecutor();

            udevLib.udev_monitor_enable_receiving(mon);
            fd = udevLib.udev_monitor_get_fd(mon);

            FDSet fds = LinuxUsbCFuncsImpl.newFDSet();
            TimeVal tv = new TimeVal();

            pollService.scheduleAtFixedRate(() -> {

                LinuxUsbCFuncsImpl.FD_ZERO(fds);
                LinuxUsbCFuncsImpl.FD_SET(fd, fds);
                tv.tv_sec = 0;
                tv.tv_usec = 0;

                int ret = LinuxUsbCFuncsImpl.select(fd + 1, fds, null, null, tv);

                if (ret > 0 && LinuxUsbCFuncsImpl.FD_ISSET(fd, fds)) {
                    LinuxUdevLib.udev_device dev = udevLib.udev_monitor_receive_device(mon);
                    if (dev != null) {
                        LOG.trace("USB event subsystem: {}", udevLib.udev_device_get_subsystem(dev));
                        handleUsbDeviceAction(dev, udevLib.udev_device_get_action(dev).equals("add"));
                        udevLib.udev_device_unref(dev);
                    } else {
                        LOG.error("No Device from receive_device(). An error occured.\n");
                    }
                }

            }, FIRST_ITERATION_WAIT, CONTINUOUS_ITERATION_WAIT, TimeUnit.MILLISECONDS);
            LOG.info("Usb listener started");
        }
    }

    /**
     * Stops the watchdog for real time connects.
     */
    @Override
    public void stopWatchdog() {
        if (pollService != null) {
            pollService.shutdownNow();
        }
        if (mon != null) {
            udevLib.udev_monitor_unref(mon);
        }
        udevLib.udev_unref(udevPointer);
        LOG.error("Watchog stopped.");
    }

    /**
     * Returns if the service is accepting usb connections.
     *
     * @return true when running.
     */
    @Override
    public boolean watchDogRunning() {
        return mon != null;
    }

    /**
     * This usb device event is used when a device disconnects.
     *
     * @param eventType The eventType
     * @param usbKey The USB device key
     */
    private synchronized void fireDeviceEvent(final HardwareRoot.Mutation eventType, final String usbKey) {
        fireDeviceEvent(eventType, SubSystem.UNKNOWN, null, null, null, null, usbKey, null, null);
    }

    /**
     * Used for when a device is added.
     *
     * @param eventType The event type.
     * @param subSystem The subsystem
     * @param serial The device serial.
     * @param deviceName Name of the device.
     * @param vendorId The device vendor id.
     * @param deviceId The id of the device.
     * @param usbKey The usb key of the device.
     * @param devicePath The path to the device.
     * @param devicePort The port of the device.
     */
    private synchronized void fireDeviceEvent(final HardwareRoot.Mutation eventType, final SubSystem subSystem, final String serial, final String deviceName, final String vendorId, final String deviceId, final String usbKey, final String devicePath, final String devicePort) {
        Runnable run = () -> {
            LOG.debug("Event: {}, {}", eventType, usbKey);
            Iterator<UsbMutationListener> listeners = usbEventListeners.iterator();
            while (listeners.hasNext()) {
                try {
                    listeners.next().deviceMutation(eventType, subSystem, serial, deviceName, vendorId, deviceId, usbKey, devicePath, devicePort);
                } catch (UnsupportedUsbTypeException ex) {
                    if (LOG.isDebugEnabled()) {
                        LOG.error("Unsupported USB device [{}]", ex.getMessage(), ex);
                    } else {
                        LOG.error("Unsupported USB device [{}]", ex.getMessage());
                    }
                }
            }
        };
        run.run();
    }

}
