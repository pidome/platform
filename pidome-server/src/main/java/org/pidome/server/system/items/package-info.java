/**
 * System logging.
 * <p>
 * All supporting classes to provide the system with syatem based item
 * management.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.system.items;
