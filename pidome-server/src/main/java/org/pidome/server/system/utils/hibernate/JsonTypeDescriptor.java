/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.utils.hibernate;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.type.descriptor.WrapperOptions;
import org.hibernate.type.descriptor.java.AbstractTypeDescriptor;
import org.hibernate.type.descriptor.java.MutableMutabilityPlan;
import org.hibernate.usertype.DynamicParameterizedType;

/**
 * A json type descriptor.
 *
 * This is a simple descriptor.
 *
 * @author johns
 */
public class JsonTypeDescriptor extends AbstractTypeDescriptor<Object> implements DynamicParameterizedType {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Descriptor logger.
     */
    private static final Logger LOG = LogManager.getLogger(JsonTypeDescriptor.class);

    /**
     * Default mapper without any additional options.
     */
    private static final ObjectMapper OBJECT_MAPPER;

    /**
     * Static initialization for the object mapper.
     */
    static {
        OBJECT_MAPPER = new ObjectMapper();
        OBJECT_MAPPER.setSerializationInclusion(Include.NON_NULL);
    }

    /**
     * Descriptor instance.
     */
    public static final JsonTypeDescriptor INSTANCE = new JsonTypeDescriptor();

    /**
     * The class.
     */
    private Class<?> jsonObjectClass;

    /**
     * The generics applied on an object class.
     *
     * This is required to support generics on for example lists.
     */
    private Class<?> appliedGeneric;

    /**
     * Descriptor constructor.
     */
    public JsonTypeDescriptor() {
        super(Object.class, new MutableMutabilityPlan<Object>() {
            @Override
            protected Object deepCopyNotNull(final Object value) {
                return objectify(stringify(value), value.getClass());
            }
        });
    }

    /**
     * To string.
     *
     * @param value The value to string.
     * @return The string value.
     */
    @Override
    public String toString(final Object value) {
        return stringify(value);
    }

    /**
     * To object.
     *
     * @param string The string to object.
     * @return The object.
     */
    @Override
    @SuppressWarnings({"unchecked"})
    public Object fromString(final String string) {
        try {
            if (string == null) {
                return null;
            }
            if (this.appliedGeneric != null) {
                if (Collection.class.isAssignableFrom(jsonObjectClass)) {
                    return OBJECT_MAPPER.readValue(string, OBJECT_MAPPER.getTypeFactory()
                            .constructCollectionType((Class<Collection>) jsonObjectClass, this.appliedGeneric));
                } else {
                    return null;
                }
            } else {
                return OBJECT_MAPPER.readValue(string, jsonObjectClass);
            }
        } catch (IOException ex) {
            LOG.error("Unabled to de-serialize device control group from string", ex);
            return null;
        }
    }

    /**
     * Unwrap the object.
     *
     * @param <X> The type.
     * @param value The control group.
     * @param type The type.
     * @param options The options.
     * @return Unwrapped to the type.
     */
    @Override
    @SuppressWarnings("unchecked")
    public <X> X unwrap(final Object value, final Class<X> type, final WrapperOptions options) {
        if (value == null) {
            return null;
        }
        if (String.class.isAssignableFrom(type)) {
            return (X) toString(value);
        }
        throw unknownUnwrap(type);
    }

    /**
     * Wrap.
     *
     * @param <X> The type.
     * @param value The value.
     * @param options The options.
     * @return the object wrapped.
     */
    @Override
    public <X> Object wrap(final X value, final WrapperOptions options) {
        if (value == null) {
            return null;
        }
        if (String.class.isInstance(value)) {
            return fromString((String) value);
        }
        throw unknownWrap(value.getClass());
    }

    /**
     * Sets the parameter values.
     *
     * @param parameters The parameters to set.
     */
    @Override
    public void setParameterValues(final Properties parameters) {
        jsonObjectClass = ((ParameterType) parameters.get(PARAMETER_TYPE))
                .getReturnedClass();
        String entity = null;
        try {
            if (parameters.get("typeGeneric") != null) {
                entity = (String) parameters.get("typeGeneric");
                this.appliedGeneric = Class.forName(entity);
            } else {
                entity = (String) parameters.get(ENTITY);
                if (Collection.class.isAssignableFrom(jsonObjectClass)) {
                    String propertyName = parameters.getProperty(DynamicParameterizedType.PROPERTY);
                    Class entityClass = Class.forName(entity);
                    List<Field> fields = Arrays.asList(entityClass.getDeclaredFields());
                    fields.stream()
                            .filter(field -> field.getName().equals(propertyName))
                            .findFirst()
                            .ifPresent(field -> {
                                try {
                                    ParameterizedType stringListType = (ParameterizedType) field.getGenericType();
                                    this.appliedGeneric = (Class<?>) stringListType.getActualTypeArguments()[0];
                                    if (LOG.isTraceEnabled()) {
                                        LOG.trace("Generics [{}] applied on collection [{}]: ", this.appliedGeneric, this.jsonObjectClass);
                                    }
                                } catch (java.lang.ClassCastException ex) {
                                    if (LOG.isTraceEnabled()) {
                                        LOG.trace("Possibly safe to ignore: [{}]", ex.getMessage());
                                    }
                                } catch (Exception ex) {
                                    LOG.error("Unable to extract parameterized type for [{}]", field, ex);
                                }
                            });
                }
            }
        } catch (Exception ex) {
            LOG.error("Unable to extract properties for/from class [{}]", entity, ex);
        }
    }

    /**
     * To String.
     *
     * @param value The value to stringify.
     * @return Stringed value.
     */
    private static String stringify(final Object value) {
        try {
            return OBJECT_MAPPER.writeValueAsString(value);
        } catch (JsonProcessingException ex) {
            LOG.error("Unabled to serialize device control group from object", ex);
            return null;
        }
    }

    /**
     * From string to object.
     *
     * @param <T> The type.
     * @param value The string.
     * @param klass The class type.
     * @return Object from class type.
     */
    private static <T> T objectify(final String value, final Class<T> klass) {
        try {
            return OBJECT_MAPPER.readValue(value, klass);
        } catch (IOException ex) {
            LOG.error("Unabled to de-serialize device control group from string", ex);
            return null;
        }
    }

}
