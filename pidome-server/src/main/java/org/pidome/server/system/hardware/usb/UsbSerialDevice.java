/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.hardware.usb;

import org.pidome.platform.hardware.driver.HardwareDriver;
import org.pidome.platform.hardware.driver.Transport.SubSystem;

/**
 * An USB device supporting a serial interface.
 *
 * @author John Sirach
 */
public class UsbSerialDevice extends USBDevice<HardwareDriver> {

    /**
     * USB serial device constructor.
     */
    public UsbSerialDevice() {
        super(SubSystem.SERIAL);
    }

}
