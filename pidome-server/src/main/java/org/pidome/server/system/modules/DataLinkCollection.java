/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.modules;

import java.util.ArrayList;

/**
 * Collection of data links.
 *
 * Used between modules and hardware drivers.
 *
 * @author johns
 */
public class DataLinkCollection extends ArrayList<DataLink> {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

}
