/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.modules;

import java.io.IOException;
import org.pidome.platform.hardware.driver.HardwareDriver;
import org.pidome.platform.hardware.driver.types.consumers.SerialDataProsumer;
import org.pidome.platform.modules.CommunicationAwareModule;

/**
 * A data link between serial consumers and producers.
 *
 * @author johns
 */
public final class SerialDataLink implements DataLink {

    /**
     * The module.
     */
    private SerialDataProsumer module;

    /**
     * The hardware driver.
     */
    private SerialDataProsumer driver;

    /**
     * Constructor creating the link.
     *
     * @param module The module.
     * @param driver The driver.
     */
    protected SerialDataLink(final SerialDataProsumer module, final SerialDataProsumer driver) {
        this.module = module;
        this.driver = driver;
        this.driver.setDriverConsumer(this::passToModule);
        this.module.setModuleConsumer(this::passToDriver);
    }

    /**
     * Method to pass to driver.
     *
     * @param data The data to pass.
     */
    private void passToDriver(final byte[] data) {
        driver.produceByModule(data);
    }

    /**
     * Passes data to a module.
     *
     * @param data The data to pass.
     */
    private void passToModule(final byte[] data) {
        module.produceByDriver(data);
    }

    /**
     * Closes the data connection bewteen a module and an hardware driver.
     *
     * @throws IOException When closing fails.
     */
    @Override
    @SuppressWarnings({"PMD.NullAssignment"}) /// Required for cross references between different class loaders.
    public void close() throws IOException {
        this.driver.setDriverConsumer(null);
        this.driver.setModuleConsumer(null);
        this.module.setDriverConsumer(null);
        this.module.setModuleConsumer(null);
        this.module = null;
        this.driver = null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <M extends CommunicationAwareModule> boolean equalsLink(final M checkModule) {
        return checkModule.getProsumer().equals(this.module);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <D extends HardwareDriver> boolean equalsLink(final D checkDriver) {
        return checkDriver.getProsumer().equals(this.driver);
    }

}
