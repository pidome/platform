/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.utils;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * To be implemented by enums.
 *
 * @author johns
 */
public interface LabeledEnum {

    /**
     * The label of the enum.
     *
     * @return The label.
     */
    String getLabel();

    /**
     * The value of the enum.
     *
     * @return The enum value.
     */
    Object getValue();

    /**
     * the enum value.
     *
     * @return The name
     */
    default String getName() {
        return this.name();
    }

    /**
     * Enum name.
     *
     * @return The enum name.
     */
    @JsonIgnore
    String name();

}
