/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.hardware;

import io.vertx.core.Future;
import io.vertx.core.Promise;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.hardware.driver.HardwareDriver;
import org.pidome.platform.hardware.driver.HardwareDriverDiscovery;
import org.pidome.platform.hardware.driver.Transport;
import org.pidome.platform.presentation.input.InputForm;
import org.pidome.server.services.hardware.DriverDefinition;
import org.pidome.server.system.database.Database;
import org.pidome.server.system.installer.packages.PackageClassLoaderException;
import org.pidome.server.system.installer.packages.PackageMutationListener;
import org.pidome.server.system.installer.packages.PackageStore;
import org.pidome.server.system.installer.packages.ServerPackage;

/**
 * Collection of server hardware drivers currently available.
 *
 * @author johns
 */
@SuppressWarnings("CPD-START")
public final class HardwareDriverStore implements PackageMutationListener {

    /**
     * Hardware root logger.
     */
    private static final Logger LOG = LogManager.getLogger(HardwareDriverStore.class);

    /**
     * Constructor.
     */
    public HardwareDriverStore() {
    }

    /**
     * Returns the known drivers categorized by transport.
     *
     * @return Map with transport and associated drivers.
     */
    public List<DriverDefinition> getDriverCollection() {
        try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {

            CriteriaBuilder cb = autoManager.getManager().getCriteriaBuilder();
            CriteriaQuery<DriverDefinition> cq = cb.createQuery(DriverDefinition.class);
            Root<DriverDefinition> rootEntry = cq.from(DriverDefinition.class);
            CriteriaQuery<DriverDefinition> select = cq.select(rootEntry);

            return autoManager.getManager().createQuery(select).getResultList();
        }
    }

    /**
     * Returns a set of driver for the given <code>Transport.SubSystem</code>.
     *
     * @param subSystem The transport subsystem.
     * @return Unmodifiable set of drivers.
     */
    public List<DriverDefinition> getDriverCollectionForSubSystem(final Transport.SubSystem subSystem) {
        try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {

            CriteriaBuilder cb = autoManager.getManager().getCriteriaBuilder();
            CriteriaQuery<DriverDefinition> cq = cb.createQuery(DriverDefinition.class);
            Root<DriverDefinition> rootEntry = cq.from(DriverDefinition.class);
            CriteriaQuery<DriverDefinition> select = cq.select(rootEntry);

            select.where(cb.equal(rootEntry.get("transport"), subSystem));
            return autoManager.getManager().createQuery(select).getResultList();
        }
    }

    /**
     * Checks if there are drivers available of the given transport.
     *
     * @param transport The transport subsystem to get the driver for.
     * @return true if a driver set is found.
     */
    protected boolean hasDriversAvailable(final Transport.SubSystem transport) {
        return !this.getDriverCollectionForSubSystem(transport).isEmpty();
    }

    /**
     * Scans for drivers annotated with HardwareDriverDiscovery.
     *
     * @return The future indicating scanning is done.
     */
    @SuppressWarnings("unchecked")
    public Future<Void> scan() {
        final Promise<Void> promise = Promise.promise();
        LOG.info("Requesting packages for hardware");
        Map<ServerPackage, List<Class<? extends HardwareDriver>>> collection = PackageStore.provides(HardwareDriverDiscovery.class, HardwareDriver.class);
        final EntityManager manager = Database.getInstance().getNewManager();
        collection.forEach((serverPackage, classList) -> {
            Set<DriverDefinition> definitionsSet = new HashSet<>();
            classList.stream().forEach(matchedClass -> {
                for (Transport.SubSystem subSystem : Transport.SubSystem.values()) {
                    if (matchedClass.getSuperclass().equals(subSystem.getImplementation())) {
                        if (!manager.getTransaction().isActive()) {
                            manager.getTransaction().begin();
                        }
                        DriverDefinition definition = new DriverDefinition();
                        HardwareDriverDiscovery annotation = matchedClass.getAnnotation(HardwareDriverDiscovery.class);
                        definition.setProvidedBy(serverPackage.getId());
                        definition.setName(annotation.name());
                        definition.setDescription(annotation.description());
                        definition.setDriver(matchedClass.getCanonicalName());
                        definition.setTransport(subSystem);
                        manager.persist(definition);
                        definitionsSet.add(definition);
                        break;
                    }
                }
            });
            serverPackage.setDrivers(definitionsSet);
        });
        if (manager.getTransaction().isActive()) {
            manager.getTransaction().commit();
        }
        if (manager.isOpen()) {
            manager.close();
        }
        promise.complete();
        return promise.future();
    }

    /**
     * Returns the generic implementation of an hardware driver suitable to be
     * attached to a peripheral.
     *
     * @param subSystem The subsystem the peripheral is attached to.
     * @param serverPackage The server package to load.
     * @param driverName The driver to load from the package.
     * @return Instantiated hardware driver to be combined with a peripheral.
     * @throws HardwareDriverStoreException When loading a package fails, cause
     * is included.
     */
    public HardwareDriver loadDriver(final Transport.SubSystem subSystem, final ServerPackage serverPackage, final String driverName) throws HardwareDriverStoreException {
        Class<? extends HardwareDriver> classToLoad = getDriverClassFor(subSystem, serverPackage, driverName);
        try {
            return PackageStore.loadInstanceFromPackage(this, serverPackage, classToLoad);
        } catch (PackageClassLoaderException ex) {
            throw new HardwareDriverStoreException("Unable to load package from store", ex);
        }
    }

    /**
     * Unloads a driver.
     *
     * This method does not remove the driver from the store registration. For
     * this a package needs to be removed.
     *
     * @param driver The driver to unload.
     */
    public void unloadDriver(final HardwareDriver driver) {
        PackageStore.unloadClass(driver.getClass());
    }

    /**
     * When there is no known registration of the class requested.
     *
     * @param subSystem The subsystem the class is meant for.
     * @param serverPackage The server package where the class is available in.
     * @param driverClass The class to load.
     * @return The class able to be instantiated.
     * @throws HardwareDriverStoreException When there is no registration of the
     * class known.
     */
    @SuppressWarnings("unchecked")
    private Class<? extends HardwareDriver> getDriverClassFor(final Transport.SubSystem subSystem, final ServerPackage serverPackage, final String driverClass) throws HardwareDriverStoreException {
        if (hasDriversAvailable(subSystem)) {
            try (PackageStore.UnboundPackageLoad loader = new PackageStore.UnboundPackageLoad(serverPackage,
                    getDriverCollectionForSubSystem(subSystem).stream()
                            .filter(def -> def.getDriver().equals(driverClass))
                            .findFirst()
                            .map(def -> def.getDriver())
                            .orElseThrow(() -> new HardwareDriverStoreException("Driver " + driverClass + " is unknown on the system, make sure the package is installed")),
                    true)) {
                return loader.getLoadedClass();
            } catch (Exception ex) {
                throw new HardwareDriverStoreException("Unable to load class [" + driverClass + "] from [" + serverPackage.getPresentationName() + "]", ex);
            }
        } else {
            throw new HardwareDriverStoreException("Subsystem " + subSystem + " has no drivers available");
        }
    }

    /**
     * Returns the configuration class for the given driver.
     *
     * This method searches in the class loader of an instantiated
     * <code>HardwareDriver</code>.
     *
     * @param driver The instantiated hardware driver.
     * @return The configuration class configured on the
     * <code>HardwareDriver</code>.
     * @throws HardwareDriverStoreException When the base configuration is
     * unable to be retrieved.
     */
    @SuppressWarnings({"unchecked", "CPD-START"})
    public InputForm getDriverBaseConfiguration(final HardwareDriver driver) throws HardwareDriverStoreException {
        try {
            Type type = driver.getClass().getGenericSuperclass();
            Class<? extends InputForm> configurationFormClass = (Class<? extends InputForm>) ((ParameterizedType) type).getActualTypeArguments()[0];
            return (InputForm) configurationFormClass.getConstructors()[0].newInstance();
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            LOG.error("Unable to get the default configuration for driver.");
            throw new HardwareDriverStoreException("Unable to get driver base configuration", ex);
        }
    }

    @Override
    public void packageRemoved(final ServerPackage serverPackage
    ) {
        /// When a package is removed.
    }

    @Override
    public void packageAdded(final ServerPackage serverPackage
    ) {
        /// When a package is added.
    }

}
