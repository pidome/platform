/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.database;

import io.vertx.core.Future;
import io.vertx.core.Promise;
import javax.persistence.Query;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.services.AbstractService;
import org.pidome.server.services.ServiceException;

/**
 * The database service class.
 *
 * Configures and provides access to dao objects.
 *
 * @author John Sirach
 */
public class DatabaseService extends AbstractService {

    /**
     * The database service.
     */
    private Database database;

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(DatabaseService.class);

    /**
     * Starts the database service.
     *
     * @param startPromise The promise used to detect success or failure.
     */
    @Override
    public final void start(final Promise<Void> startPromise) {
        LOG.info("Calling start database service");
        try {
            database = Database.getInstance();
            database.init();
            try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
                /// Check if it opens.
                Query query = autoManager.getManager().createNativeQuery("SELECT 1");
                query.getSingleResult();
            }
            startPromise.complete();
        } catch (DatabaseConfigurationException ex) {
            startPromise.fail(new ServiceException("Failed to start database service because of a configuration error", ex));
        } catch (DatabaseConnectionException ex) {
            startPromise.fail(new ServiceException("Failed to start database service because of a connection error", ex));
        } catch (DatabasePreEmptException ex) {
            startPromise.fail(new ServiceException("Failed to start database service because of a database population error", ex));
        }
    }

    /**
     * Runs a full export of the current database.
     *
     * This process can not be interrupted and runs in a separate thread. It is
     * wise to do not any database changes during the export as the result can
     * not be guaranteed.
     *
     * @return Future indicating when ready or failure.
     */
    public final Future<Void> runFullExport() {
        Promise<Void> promise = Promise.promise();
        new Thread(() -> {
            try {
                this.database.exportCurrentDatabaseSchema();
                promise.complete();
            } catch (Exception ex) {
                promise.fail(ex);
            }
        }).start();
        return promise.future();
    }

    /**
     * Stops the database service.
     *
     * @param stopPromise The promise used to detect success or failure.
     */
    @Override
    public final void stop(final Promise<Void> stopPromise) {
        database.disconnect();
        stopPromise.complete();
    }

}
