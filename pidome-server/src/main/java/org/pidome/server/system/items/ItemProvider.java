/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.items;

import io.vertx.core.Promise;
import java.util.List;
import java.util.UUID;
import org.pidome.platform.modules.items.Item.ItemType;
import org.pidome.server.entities.items.ItemCommand;
import org.pidome.server.services.items.ItemsDiscoveryMutationListener;
import org.pidome.server.services.items.ItemsMutationListener;

/**
 * A service capable of providing items.
 *
 * @author johns
 */
public interface ItemProvider {

    /**
     * Capabilities of an item provider.
     */
    enum ProviderCapabilities {
        /**
         * If discovery is supported.
         */
        DISCOVERY;
    }

    /**
     * Returns the capabilities.
     *
     * @return The capabilities of an item provider.
     */
    List<ProviderCapabilities> capablities();

    /**
     * Identifies which ItemType is being provided by the item provider.
     *
     * @return The supported ItemType.
     */
    ItemType provides();

    /**
     * Execute an item command.
     *
     * @param itemId The id of the item.
     * @param itemCommand The item command to execute.
     * @param executePromise The promise to fulfil or fail.
     */
    void executeItemCommand(UUID itemId, ItemCommand itemCommand, Promise<Void> executePromise);

    /**
     * Called when items are mutated.
     *
     * @param listener The listener for handling the mutation event.
     */
    void mutationHandler(ItemsMutationListener listener);

    /**
     * Called when items are being discovered.
     *
     * @param listener The listener for discovered items.
     */
    default void discoveryMutationHandler(ItemsDiscoveryMutationListener listener) {
        // Do nothing, but method is required.
        // Implementer should decide if available
    }

}
