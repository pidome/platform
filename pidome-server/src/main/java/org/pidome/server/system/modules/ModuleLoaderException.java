/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.modules;

/**
 * Exception thrown from the modules store.
 *
 * @author johns
 */
public class ModuleLoaderException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>ModuleStoreException</code> without
     * detail message.
     */
    public ModuleLoaderException() {
    }

    /**
     * Constructs an instance of <code>ModuleStoreException</code> with the
     * specified detail message.
     *
     * @param msg The detail message.
     */
    public ModuleLoaderException(final String msg) {
        super(msg);
    }

    /**
     * Constructs an instance of <code>ModuleStoreException</code> with the
     * specified detail message and cause.
     *
     * @param msg The detail message.
     * @param thr The cause of the thrown exception.
     */
    public ModuleLoaderException(final String msg, final Throwable thr) {
        super(msg, thr);
    }
}
