/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.hardware.serial;

/**
 * Class used to set and get custom serial device parameters.
 *
 * @author John Sirach
 */
public class CustomSerialDeviceParameters {

    /**
     * Custom identified serial key.
     */
    private String serialKey;

    /**
     * The port the device resides.
     */
    private String path;

    /**
     * The port the device resides.
     */
    private String port;

    /**
     * The friendlyname of the device to identify this easier.
     */
    private String friendlyName;

    /**
     * @return the userCustomSerialKey
     */
    public String getSerialKey() {
        return serialKey;
    }

    /**
     * @param serialKey the userCustomSerialKey to set
     */
    public void setSerialKey(final String serialKey) {
        this.serialKey = serialKey;
    }

    /**
     * @return the port
     */
    public String getPort() {
        return port;
    }

    /**
     * @param port the port to set
     */
    public void setPort(final String port) {
        this.port = port;
    }

    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(final String path) {
        this.path = path;
    }

    /**
     * @return the friendlyName
     */
    public String getFriendlyName() {
        return friendlyName;
    }

    /**
     * @param friendlyName the friendlyName to set
     */
    public void setFriendlyName(final String friendlyName) {
        this.friendlyName = friendlyName;
    }

}
