/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.meta.manufacturer;

import javax.persistence.Entity;
import org.pidome.server.entities.base.BaseEntity;

/**
 * A manufacturer of an item or other component used.
 *
 * @author johns
 */
@Entity
public class Manufacturer extends BaseEntity {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Manufacturer name.
     */
    private String name;

    /**
     * Manufacturer name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Manufacturer name.
     *
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Generate hash code.
     *
     * @return The object hash.
     */
    @Override
    @SuppressWarnings("CPD-START")
    public int hashCode() {
        return super.hashCode();
    }

    /**
     * Equality check.
     *
     * @param obj the object to check.
     * @return If equals.
     */
    @Override
    public boolean equals(final Object obj) {
        return super.equals(obj);
    }
}
