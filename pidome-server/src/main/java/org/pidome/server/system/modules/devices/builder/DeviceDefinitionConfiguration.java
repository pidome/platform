/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.modules.devices.builder;

import java.util.List;
import java.util.UUID;
import org.pidome.platform.modules.devices.DeviceControlGroup;
import org.pidome.platform.modules.devices.DeviceParametersList;
import org.pidome.platform.modules.items.ItemAddress;

/**
 * The configuration to be posted to create or update a device definition.
 *
 * @author johns
 */
public class DeviceDefinitionConfiguration {

    /**
     * Id of the package of the device.
     */
    private UUID packageId;

    /**
     * Device definition name.
     */
    private String name;

    /**
     * Device definition description.
     */
    private String description;

    /**
     * Class path of the device.
     */
    private String deviceClass;

    /**
     * Which container will handle this configuration.
     */
    private String containerId;

    /**
     * Group configuration of the device.
     */
    private List<DeviceControlGroup> groups;

    /**
     * The base device parameters.
     */
    private DeviceParametersList deviceParameters;

    /**
     * The device address configuration.
     */
    private ItemAddress deviceAddress;

    /**
     * Id of the package of the device.
     *
     * @return the packageId
     */
    public UUID getPackageId() {
        return packageId;
    }

    /**
     * Id of the package of the device.
     *
     * @param packageId the packageId to set
     */
    public void setPackageId(final UUID packageId) {
        this.packageId = packageId;
    }

    /**
     * Device definition name.
     *
     * @return the name
     */
    @SuppressWarnings("CPD-START")
    public String getName() {
        return name;
    }

    /**
     * Device definition name.
     *
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Device definition description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Device definition description.
     *
     * @param description the description to set
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Class path of the device.
     *
     * @return the deviceClass
     */
    @SuppressWarnings("CPD-END")
    public String getDeviceClass() {
        return deviceClass;
    }

    /**
     * Class path of the device.
     *
     * @param deviceClass the deviceClass to set
     */
    public void setDeviceClass(final String deviceClass) {
        this.deviceClass = deviceClass;
    }

    /**
     * Group configuration of the device.
     *
     * @return the groups
     */
    public List<DeviceControlGroup> getGroups() {
        return groups;
    }

    /**
     * Group configuration of the device.
     *
     * @param groups the groups to set
     */
    public void setGroups(final List<DeviceControlGroup> groups) {
        this.groups = groups;
    }

    /**
     * The base device parameters.
     *
     * @return the deviceParameters
     */
    public DeviceParametersList getDeviceParameters() {
        return deviceParameters;
    }

    /**
     * The base device parameters.
     *
     * @param deviceParameters the deviceParameters to set
     */
    public void setDeviceParameters(final DeviceParametersList deviceParameters) {
        this.deviceParameters = deviceParameters;
    }

    /**
     * The device address configuration.
     *
     * @return the deviceAddress
     */
    public ItemAddress getDeviceAddress() {
        return deviceAddress;
    }

    /**
     * The device address configuration.
     *
     * @param deviceAddress the deviceAddress to set
     */
    public void setDeviceAddress(final ItemAddress deviceAddress) {
        this.deviceAddress = deviceAddress;
    }

    /**
     * Which container will handle this configuration.
     *
     * @return the containerId
     */
    public String getContainerId() {
        return containerId;
    }

    /**
     * Which container will handle this configuration.
     *
     * @param containerId the containerId to set
     */
    public void setContainerId(final String containerId) {
        this.containerId = containerId;
    }

}
