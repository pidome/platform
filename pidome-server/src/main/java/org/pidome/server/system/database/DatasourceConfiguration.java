/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.database;

import com.zaxxer.hikari.HikariConfig;
import java.util.Locale;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.system.config.SystemConfig;
import org.pidome.server.system.database.driver.DatabaseDriverType;

/**
 * Gathers and supplies a data source configuration.
 *
 * @author John
 */
public final class DatasourceConfiguration {

    /**
     * Threshold used for leak detection.
     */
    private static final int LEAK_THRESHOLD = 5000;

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(DatasourceConfiguration.class);

    /**
     * The database configuration.
     */
    private final DatabaseConfiguration databaseConfiguration;

    /**
     * The generated jdbcPath.
     */
    private String jdbcPath;

    /**
     * The spoken dialect.
     */
    private String dialect;

    /**
     * The created hikariConfig.
     */
    private HikariConfig hikariConfig;

    /**
     * Constructor.
     *
     * Gathers the information.
     *
     * @throws DatabaseConfigurationException When the configuration can not be
     * applied.
     */
    public DatasourceConfiguration() throws DatabaseConfigurationException {
        this.databaseConfiguration = parseConfiguration();
        applyConfig();
    }

    /**
     * Returns the database configuration.
     *
     * @return The configuration.
     */
    public DatabaseConfiguration getDatabaseConfiguration() {
        return this.databaseConfiguration;
    }

    /**
     * The created hikariConfig.
     *
     * @return the hikariConfig
     */
    public HikariConfig getHikariConfig() {
        return hikariConfig;
    }

    /**
     * The generated jdbcPath.
     *
     * @return the jdbcPath
     */
    public String getJdbcPath() {
        return jdbcPath;
    }

    /**
     * Sets the dialect of the chosen driver.
     *
     * @param dialect The dialect.
     */
    public void setDialect(final String dialect) {
        this.dialect = dialect;
    }

    /**
     * Returns the dialect of the chosen driver.
     *
     * @return The dialect.
     */
    public String getDialect() {
        return this.dialect;
    }

    /**
     * Applies the configuration read by <code>readConfig</code>.
     */
    private void applyConfig() {
        hikariConfig = new HikariConfig();
        hikariConfig.setUsername(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "database.user", ""));
        hikariConfig.setPassword(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "database.pass", ""));
        hikariConfig.setLeakDetectionThreshold(LEAK_THRESHOLD);
        hikariConfig.setPoolName("DBConnectionPool_PiDomeServer");
        hikariConfig.addDataSourceProperty("characterEncoding", "utf8");
        hikariConfig.addDataSourceProperty("useUnicode", "true");
        hikariConfig.setAutoCommit(false);
    }

    /**
     * Parses the configuration from the system properties.
     *
     * @return The parsed database configuration.
     * @throws DatabaseConfigurationException when required items are not met.
     */
    private DatabaseConfiguration parseConfiguration() throws DatabaseConfigurationException {
        final DatabaseConfiguration configuration = new DatabaseConfiguration(this.resolveDriverType());
        configuration.setProtocol(
                SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "database.protocol", "")
        );
        configuration.setPort(
                SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "database.port", "")
        );
        configuration.setDatabase(
                SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "database.database", "")
        );
        configuration.setLocation(
                SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "database.location", "")
        );
        configuration.setCatalog(
                SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "database.catalog", "")
        );
        configuration.setSchema(
                SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "database.schema", "")
        );
        configuration.setJdbcAppend(
                SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "database.JDBC_append", "")
        );
        if (LOG.isDebugEnabled()) {
            LOG.debug("Created database configuration: ", configuration);
        }
        return configuration;
    }

    /**
     * Resolves the driver type.
     *
     * @return The driver type configured.
     * @throws DatabaseConfigurationException when the driver type can not be
     * gathered.
     */
    private DatabaseDriverType resolveDriverType() throws DatabaseConfigurationException {
        String configuredDriver = SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "database.driver", "");
        if (configuredDriver.isEmpty()) {
            throw createInvalidDriverException(configuredDriver, new Exception("No driver given, check database.driver configuration"));
        }
        try {
            return Enum.valueOf(DatabaseDriverType.class, configuredDriver.toUpperCase(Locale.US));
        } catch (IllegalArgumentException ex) {
            throw createInvalidDriverException(configuredDriver, ex);
        }
    }

    /**
     * Invalid driver exception builder.
     *
     * @param configuredDriver The given invalid driver.
     * @param cause The cause of throwing this.
     * @return Returns a <code>DatabaseConfigurationException</code> on purpose
     * meant to be thrown.
     */
    private DatabaseConfigurationException createInvalidDriverException(final String configuredDriver, final Throwable cause) {
        StringBuilder valid = new StringBuilder("Invalid database configured [database.driver]: [")
                .append(configuredDriver)
                .append("]. Should be one of [");
        for (DatabaseDriverType driver : DatabaseDriverType.values()) {
            if (driver.isActive()) {
                valid.append(driver.name().toLowerCase(Locale.US)).append(",");
            }
        }
        return new DatabaseConfigurationException(valid.deleteCharAt(valid.length() - 1).append("]").toString(), cause);
    }

}
