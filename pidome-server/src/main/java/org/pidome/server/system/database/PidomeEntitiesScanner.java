/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.database;

import io.github.classgraph.ClassGraph;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.boot.archive.internal.UrlInputStreamAccess;
import org.hibernate.boot.archive.scan.internal.ClassDescriptorImpl;
import org.hibernate.boot.archive.scan.internal.ScanResultCollector;
import org.hibernate.boot.archive.scan.spi.ClassDescriptor;
import org.hibernate.boot.archive.scan.spi.ClassDescriptor.Categorization;
import org.hibernate.boot.archive.scan.spi.ScanEnvironment;
import org.hibernate.boot.archive.scan.spi.ScanOptions;
import org.hibernate.boot.archive.scan.spi.ScanParameters;
import org.hibernate.boot.archive.scan.spi.ScanResult;
import org.hibernate.boot.archive.scan.spi.Scanner;

/**
 * Custom scanner for pidome entities.
 *
 * @author johns
 */
public final class PidomeEntitiesScanner implements Scanner {

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(PidomeEntitiesScanner.class);

    /**
     * Scans for the entities.
     *
     * @param environment The environment to scan.
     * @param options The options for scanning.
     * @param params The parameters for scanning.
     * @return The scan result.
     */
    @Override
    public ScanResult scan(final ScanEnvironment environment, final ScanOptions options, final ScanParameters params) {
        final ScanResultCollector collector = new ScanResultCollector(environment, options, params);

        List<Class> scanResultList = entityScanner();

        scanResultList.stream().forEach(klass -> {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Registering entity [{}]", klass.getCanonicalName());
            }
            ClassDescriptor descriptor = new ClassDescriptorImpl(klass.getCanonicalName(),
                    Categorization.MODEL,
                    new UrlInputStreamAccess(klass.getResource(klass.getCanonicalName())));
            collector.handleClass(descriptor, true);
        });
        return collector.toScanResult();
    }

    /**
     * Scans for classes with the entity annotation.
     *
     * @return List of classes found by the scanner.
     */
    @SuppressWarnings("unchecked")
    private List<Class> entityScanner() {
        final List<Class> entityClasses = new ArrayList<>();
        try (io.github.classgraph.ScanResult scanResult
                = new ClassGraph()
                        .whitelistPackages("org.pidome.server")
                        .enableAnnotationInfo()
                        .scan()) {
                    scanResult.getClassesWithAnnotation(Entity.class.getCanonicalName()).stream().forEach(matchedClass -> {
                        entityClasses.add(matchedClass.loadClass());
                    });
                }
                return entityClasses;
    }

}
