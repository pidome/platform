/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.utils.hibernate;

import java.util.Properties;
import org.hibernate.type.AbstractSingleColumnStandardBasicType;
import org.hibernate.type.descriptor.sql.LongVarcharTypeDescriptor;
import org.hibernate.usertype.DynamicParameterizedType;

/**
 * Custom for storing device structure.
 *
 * @author johns
 */
public final class JsonType extends AbstractSingleColumnStandardBasicType<Object> implements DynamicParameterizedType {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Use this constant to define the generic type class being used if not able
     * to determine automatically.
     */
    public static final String GENERIC_TYPE = "typeGeneric";

    /**
     * Constructor.
     */
    public JsonType() {
        super(LongVarcharTypeDescriptor.INSTANCE, new JsonTypeDescriptor());
    }

    /**
     * Type name.
     *
     * @return The type name.
     */
    @Override
    public String getName() {
        return "json";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean registerUnderJavaType() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setParameterValues(final Properties parameters) {
        ((JsonTypeDescriptor) getJavaTypeDescriptor())
                .setParameterValues(parameters);
    }

}
