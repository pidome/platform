/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.hardware.windows;

import com.sun.jna.platform.win32.Advapi32Util;
import com.sun.jna.platform.win32.WinReg;
import java.util.Collections;
import java.util.Locale;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Helper for windows serial.
 *
 * @author johns
 */
public final class WindowsSerialUtils {

    /**
     * Private constructor in utility class.
     */
    private WindowsSerialUtils() {
        /// provate default constructor.
    }

    /**
     * The logger for this class.
     */
    private static final Logger LOG = LogManager.getLogger(WindowsSerialUtils.class);

    /**
     * Location of active com ports.
     */
    private static final String COM_REG_LOCATION = "HARDWARE\\DEVICEMAP\\SERIALCOMM\\";

    /**
     * Prefix for virtual COM/Serial ports.
     *
     * Virtual ports are for example ports created when attaching USB devices.
     */
    private static final String VIRTUAL_COM_PORT_PREFIX = "\\device\\vcp";

    /**
     * Gathers current active com ports.
     *
     * @param virtialPort When enabled the list returns virtual com ports (USB
     * for example), otherwise the fixed serials will be retutned.
     * @return Set of com ports found in the registry.
     */
    public static Set<String> enumerateComPorts(final boolean virtialPort) {
        try {
            final TreeMap<String, Object> valuesSet = Advapi32Util.registryGetValues(WinReg.HKEY_LOCAL_MACHINE, COM_REG_LOCATION);
            if (LOG.isDebugEnabled()) {
                LOG.info("List of items in COM_REG_LOCATION: [{}]", valuesSet);
            }
            return valuesSet.entrySet().stream()
                    .filter(valueItem -> (virtialPort && valueItem.getKey().toLowerCase(Locale.US).startsWith(VIRTUAL_COM_PORT_PREFIX))
                    || (!virtialPort && !valueItem.getKey().toLowerCase(Locale.US).startsWith(VIRTUAL_COM_PORT_PREFIX)))
                    .map(valueSetItem -> valueSetItem.getValue().toString()).collect(Collectors.toSet());
        } catch (Exception ex) {
            LOG.error("Unable to gather available com ports", ex);
            return Collections.emptySet();
        }
    }

}
