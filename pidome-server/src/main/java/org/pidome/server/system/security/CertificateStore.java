/*
 * Copyright 2014 John.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pidome.server.system.security;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.util.Date;
import java.util.UUID;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.network.NetworkService;
import org.pidome.server.services.network.NoInterfaceAvailableException;
import org.pidome.server.system.config.SystemConfig;

/**
 * Class for certificate generation. This class is capable of auto generating a
 * keystore with password.
 *
 * @author John
 * @todo reference system config in constants.
 */
public final class CertificateStore {

    /**
     * The keystore type.
     */
    public static final String KEYSTORE_TYPE = "pkcs12";

    /**
     * The primary alias used.
     */
    public static final String PRIMARY_ALIAS = "rs256";

    /**
     * The Radix for strings.
     */
    private static final int RADIX = 16;

    /**
     * Literal 255.
     */
    private static final int LITERAL_INT_255 = 0xff;

    /**
     * 256.
     */
    private static final int DECIMAL_256 = 0x00;

    /**
     * Kind of ten years.
     */
    private static final long TEN_YERS_ISH = (1000L * 60 * 60 * 24 * 365 * 10);

    /**
     * Kind of a month.
     */
    private static final long ONE_MONTH_ISH = (1000L * 60 * 60 * 24 * 30);

    /**
     * 1024 bits.
     */
    private static final int BITS_2048 = 2048;

    /**
     * configuration literal to server.certexists.
     */
    private static final String SYSTEM_CERT_EXISTS = "server.certexists";

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(CertificateStore.class);

    /**
     * Boolean indicating a store is available.
     */
    private boolean available = false;

    /**
     * If already tried to generate the store.
     */
    private boolean done = false;

    /**
     * The keypair.
     */
    private KeyPair kPair;

    /**
     * The key store.
     */
    private KeyStore privateKS;

    /**
     * Current host name.
     */
    private String curHost;

    /**
     * The certificate store instance.
     */
    private static CertificateStore certStore;

    /**
     * Returns the certificate store.
     *
     * When this method is first called it creates a store. Always check if the
     * store is available by calling CertificateStore#isAvailable()
     *
     * @return a certificate store.
     */
    public static synchronized CertificateStore getInstance() {
        if (certStore == null) {
            certStore = new CertificateStore();
        }
        return certStore;
    }

    /**
     * Private constructor.
     */
    private CertificateStore() {
        if (!done) {
            try {
                createStore();
            } catch (CertificateStoreException ex) {
                LOG.error("Unable to create store [{}], TLS not available", ex.getMessage(), ex);
            }
            done = true;
        }
    }

    /**
     * Generates and registers a keystore if none currently exists.
     *
     * A random keystore is generated when in the configuration file one of the
     * following options are set: server.certificateregen=true,
     * server.veryfirstrun=true server.certexists=false, server.certpass has no
     * value
     *
     * @throws CertificateStoreException When the store creation fails.
     */
    private void createStore() throws CertificateStoreException {
        if (SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "server.certificateregen", false)
                || SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "server.veryfirstrun", false)
                || !SystemConfig.getSetting(SystemConfig.Type.SYSTEM, SYSTEM_CERT_EXISTS, true)
                || SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "server.certpass", "").equals("empty")) {
            LOG.info("Generating (new) certificate(s), please wait......");
            try {
                curHost = ServiceHandler.getInstance()
                        .getService(NetworkService.class).get().getInterfaceInUse().getIpAddressAsString();
                genKeyPair();
                gen();
                SystemConfig.setSetting(SystemConfig.Type.SYSTEM, "server.certificateregen", false);
                SystemConfig.setSetting(SystemConfig.Type.SYSTEM, SYSTEM_CERT_EXISTS, true);
                try {
                    SystemConfig.storeSettings(SystemConfig.Type.SYSTEM);
                } catch (IOException ex) {
                    LOG.error("Could not save keystore configuration. If a future save succeeds settings will be saved, otherwise current settings are temporary [{}]", ex.getMessage());
                }
            } catch (NoInterfaceAvailableException ex) {
                throw new CertificateStoreException("Could not determine host, no certificate creation done", ex);
            } catch (NoSuchAlgorithmException ex) {
                throw new CertificateStoreException("Could not create key pair for certificate creation, wrong algorithm: " + ex.getMessage(), ex);
            } catch (NoSuchProviderException ex) {
                throw new CertificateStoreException("Could not create key pair for certificate creation, wrong provider: " + ex.getMessage(), ex);
            }
        } else if (SystemConfig.getSetting(SystemConfig.Type.SYSTEM, SYSTEM_CERT_EXISTS, false)) {
            LOG.info("Using certificate store [{}]", SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "server.keystore", ""));
            System.setProperty("javax.net.ssl.keyStore", SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "server.keystore", ""));
            System.setProperty("javax.net.ssl.keyStorePassword", SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "server.certpass", ""));
            available = true;
        }
    }

    /**
     * Generate a key pair.
     *
     * @throws NoSuchAlgorithmException When the required algorithm is not
     * available.
     * @throws NoSuchProviderException When the provider is not available.
     */
    private void genKeyPair() throws NoSuchAlgorithmException, NoSuchProviderException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(BITS_2048);
        kPair = keyPairGenerator.generateKeyPair();
    }

    /**
     * Generates the certificate and store.
     *
     * @throws CertificateStoreException When generation fails.
     */
    private void gen() throws CertificateStoreException {
        FileOutputStream outputStream = null;
        try {
            File store = new File(SystemConfig.getResourcePath() + SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "server.keystore", ""));
            if (!store.getParentFile().exists() && !store.getParentFile().mkdirs()) {
                throw new IOException("Unable to create the directories to be able to store the certificate file, please create the folder/directory [" + store.getParentFile().getAbsolutePath() + "] manually");
            }
            if (store.exists() && !store.delete()) {
                throw new IOException("Unable to delete the current certificate file. Delete manually and restart the server");
            }

            GeneralName altName = new GeneralName(GeneralName.iPAddress, ServiceHandler.getInstance()
                    .getService(NetworkService.class).get().getInterfaceInUse().getIpAddressAsString());
            GeneralNames subjectAltName = new GeneralNames(altName);

            ContentSigner signer = new JcaContentSignerBuilder("SHA256WITHRSA").setProvider(new BouncyCastleProvider()).build(kPair.getPrivate());

            X509CertificateHolder holder = new X509v3CertificateBuilder(
                    new X500Name("CN=PiDome Server at " + curHost),
                    BigInteger.valueOf(System.currentTimeMillis()),
                    new Date(System.currentTimeMillis() - ONE_MONTH_ISH),
                    new Date(System.currentTimeMillis() + TEN_YERS_ISH),
                    new X500Name("CN=" + curHost),
                    SubjectPublicKeyInfo.getInstance(ASN1Sequence.getInstance(kPair.getPublic().getEncoded())))
                    .addExtension(new Extension(Extension.subjectAlternativeName, false, subjectAltName.getEncoded()))
                    .build(signer);
            Certificate cert = new JcaX509CertificateConverter().setProvider(new BouncyCastleProvider()).getCertificate(holder);

            privateKS = KeyStore.getInstance(KEYSTORE_TYPE);
            privateKS.getProvider().setProperty("storetype", "CaseExactJKS");
            privateKS.load(null, null);

            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(UUID.randomUUID().toString().getBytes("US-ASCII"));

            byte[] byteData = md.digest();

            StringBuilder storePass = new StringBuilder();
            for (byte singleByte : byteData) {
                storePass.append(Integer.toString((singleByte & LITERAL_INT_255) + DECIMAL_256, RADIX).substring(1));
            }

            String ps = storePass.toString();

            privateKS.setKeyEntry(PRIMARY_ALIAS, kPair.getPrivate(),
                    ps.toCharArray(),
                    new java.security.cert.Certificate[]{cert});

            if (!store.createNewFile()) {
                throw new IOException("Unable to create new certificate store file");
            }

            outputStream = new FileOutputStream(store);
            privateKS.store(outputStream, ps.toCharArray());

            System.setProperty("javax.net.ssl.keyStore", store.getAbsolutePath());
            System.setProperty("javax.net.ssl.keyStorePassword", ps);

            SystemConfig.setSetting(SystemConfig.Type.SYSTEM, "server.certpass", ps);

            setAvailable(true);

            LOG.info("Certificate(s) generated and stored at [{}]", store.getAbsoluteFile());

            try {
                SystemConfig.setSetting(SystemConfig.Type.SYSTEM, SYSTEM_CERT_EXISTS, true);
                SystemConfig.storeSettings(SystemConfig.Type.SYSTEM, "Saved by cert generation.");
            } catch (IOException ex) {
                LOG.warn("Could not save generation setting, certificate will be regenerated at next reboot.", ex);
            }
        } catch (NoInterfaceAvailableException ex) {
            throw new CertificateStoreException("No network interface available, can not continue: " + ex.getMessage(), ex);
        } catch (CertificateEncodingException ex) {
            throw new CertificateStoreException("Could not encode certificate, can not continue: " + ex.getMessage(), ex);
        } catch (IllegalStateException ex) {
            throw new CertificateStoreException("Illegal state, can not continue: " + ex.getMessage(), ex);
        } catch (NoSuchAlgorithmException ex) {
            throw new CertificateStoreException("No such algorithm, can not continue: " + ex.getMessage(), ex);
        } catch (KeyStoreException ex) {
            throw new CertificateStoreException("KeyStore problem, can not continue: " + ex.getMessage(), ex);
        } catch (IOException ex) {
            throw new CertificateStoreException("KeyStore can not be opened, can not continue: " + ex.getMessage(), ex);
        } catch (CertificateException ex) {
            throw new CertificateStoreException("KeyStore certificate problem, can not continue: " + ex.getMessage(), ex);
        } catch (OperatorCreationException ex) {
            throw new CertificateStoreException("Certificate content signer operation problem: " + ex.getMessage(), ex);
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException ex) {
                    LOG.error("Store outputstream close error [{}]", ex.getMessage());
                }
            }
        }
    }

    /**
     * @return the available
     */
    public boolean isAvailable() {
        return available;
    }

    /**
     * @param aAvailable the available to set
     */
    private void setAvailable(final boolean aAvailable) {
        available = aAvailable;
    }
}
