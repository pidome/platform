/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.modules;

import java.io.Closeable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.hardware.driver.HardwareDriver;
import org.pidome.platform.hardware.driver.types.consumers.SerialDataProsumer;
import org.pidome.platform.modules.CommunicationAwareModule;

/**
 * Data link for interchanging data between components.
 *
 * @author johns
 */
public interface DataLink extends Closeable {

    /**
     * Hardware root logger.
     */
    Logger LOG = LogManager.getLogger(DataLink.class);

    /**
     * If the module in the link compares to the given link.
     *
     * @param <M> The module type.
     * @param checkModule The module to check for.
     * @return true when equal.
     */
    <M extends CommunicationAwareModule> boolean equalsLink(M checkModule);

    /**
     * If the driver in the link compares to the given link.
     *
     * @param <D> The driver type.
     * @param checkDriver The driver to check for.
     * @return true when equal.
     */
    <D extends HardwareDriver> boolean equalsLink(D checkDriver);

    /**
     * Static caller to internally created data links.
     *
     * @param <D> The type extending the hardware driver interface.
     * @param module The module to use for the link.
     * @param driver The driver used for the link.
     * @return The link type of R.
     * @throws InvalidDataLinkTypeException When an unknown link is to be
     * created.
     */
    static <D extends HardwareDriver> DataLink create(final CommunicationAwareModule module, final D driver) throws InvalidDataLinkTypeException {
        if (driver.getProsumer() instanceof SerialDataProsumer && module.getProsumer() instanceof SerialDataProsumer) {
            return new SerialDataLink((SerialDataProsumer) module.getProsumer(), (SerialDataProsumer) driver.getProsumer());
        }
        LOG.error("Missing or incompatible link in driver and/or module, driver link: [{}], module link: [{}]", driver.getProsumer(), module.getProsumer());
        throw new InvalidDataLinkTypeException("Unable to link driver and module");
    }

}
