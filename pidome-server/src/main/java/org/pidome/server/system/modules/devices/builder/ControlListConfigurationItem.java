/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.modules.devices.builder;

/**
 * A configuration for list items.
 *
 * @author johns
 */
public class ControlListConfigurationItem {

    /**
     * List item name.
     */
    private String name;

    /**
     * List item value.
     */
    private Object value;

    /**
     * List item name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * List item name.
     *
     * @param itemName the name to set
     * @return this for fluent.
     */
    public ControlListConfigurationItem setName(final String itemName) {
        this.name = itemName;
        return this;
    }

    /**
     * List item value.
     *
     * @return the value
     */
    public Object getValue() {
        return value;
    }

    /**
     * List item value.
     *
     * @param itemValue the value to set
     * @return this for fluent.
     */
    public ControlListConfigurationItem setValue(final Object itemValue) {
        this.value = itemValue;
        return this;
    }

}
