/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;
import javax.xml.parsers.ParserConfigurationException;
import liquibase.Liquibase;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.diff.output.DiffOutputControl;
import liquibase.diff.output.StandardObjectChangeFilter;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.integration.commandline.CommandLineUtils;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Helper to upgrade databases.
 *
 * @author johns
 */
public class DatabaseVersioning {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(DatabaseVersioning.class);

    /**
     * When taking snapshots these are the types.
     */
    protected enum SnapshotType {
        /**
         * Schema type.
         */
        SCHEMA(null),
        /**
         * Data type.
         */
        DATA("data");

        /**
         * The type as <code>String</code> to be passed.
         */
        private final String type;

        /**
         * Enum constructor.
         *
         * @param typeNaming The textual naming of the type.
         */
        SnapshotType(final String typeNaming) {
            this.type = typeNaming;
        }

        /**
         * Returns the type as to be passed.
         *
         * @return The type.
         */
        private String getType() {
            return this.type;
        }

    }

    /**
     * The database instance.
     */
    private final liquibase.database.Database database;

    /**
     * The catalog name.
     */
    private final String catalogName;

    /**
     * The schema name.
     */
    private final String schemaName;

    /**
     * The root file to read when running database upgrades.
     */
    private static final String VERSIONING_ROOT_FILE = "org/pidome/server/system/database/version.xml";

    /**
     * The context name used for the initial import in the database.
     */
    private static final String INITIAL_IMPORT_CONTEXT = "initial";

    /**
     * The root file to read when running database upgrades.
     */
    private static final String EXPORT_FILE = "backup/database/%tF_%tT_%s.xml";

    /**
     * Constructor setting the database instance.
     *
     * @param databaseConnection The database connection to set.
     * @throws DatabaseVersioningException When no instance can be created to
     * interact.
     */
    protected DatabaseVersioning(final Connection databaseConnection) throws DatabaseVersioningException {
        try {
            this.catalogName = databaseConnection.getCatalog();
            this.schemaName = databaseConnection.getSchema();
            this.database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(databaseConnection));
        } catch (SQLException | DatabaseException cause) {
            throw new DatabaseVersioningException("Unable to instantiate versioning", cause);
        }
    }

    /**
     * Run any updates if required.
     */
    protected void runUpdates() {
        try {
            Liquibase liquibase = new Liquibase(VERSIONING_ROOT_FILE, new ClassLoaderResourceAccessor(), database);
            liquibase.update(INITIAL_IMPORT_CONTEXT);
        } catch (LiquibaseException ex) {
            LOG.error("Update failed", ex);
        }
    }

    /**
     * Exports the current schema.
     *
     * @param snapshotType The type of snapshot to take.
     * @throws DatabaseVersioningException On export failure, see cause for
     * details.
     */
    protected void exportCurrent(final SnapshotType snapshotType) throws DatabaseVersioningException {
        LOG.info("Trying export of catalog: [{}], schema: [{}] of type: [{}]", this.catalogName, this.schemaName, snapshotType.getType());
        try {
            LocalDateTime now = LocalDateTime.now();
            CommandLineUtils.doGenerateChangeLog(
                    String.format(EXPORT_FILE, now, now, "schema"),
                    this.database,
                    this.catalogName,
                    this.schemaName,
                    snapshotType.getType(),
                    "pidome",
                    "backup",
                    null,
                    createDiffOutputAll()
            );
        } catch (IOException | ParserConfigurationException | LiquibaseException cause) {
            throw new DatabaseVersioningException("Unable to perform export", cause);
        }
    }

    /**
     * Creates a diff output to include everything found.
     *
     * @return All inclusive diff output object.
     */
    private DiffOutputControl createDiffOutputAll() {
        DiffOutputControl diffOutputControl = new DiffOutputControl(false, false, false, null);
        StandardObjectChangeFilter filter = new StandardObjectChangeFilter(StandardObjectChangeFilter.FilterType.INCLUDE, null);
        diffOutputControl.setObjectChangeFilter(filter);
        return diffOutputControl;
    }

}
