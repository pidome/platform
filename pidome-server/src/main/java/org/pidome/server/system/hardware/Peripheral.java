/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.hardware;

import java.util.UUID;
import org.pidome.platform.hardware.driver.HardwareDriver;
import org.pidome.platform.hardware.driver.Transport;

/**
 * A to PiDome attached device.
 *
 * @author John Sirach
 * @param <T> The expected driver type.
 * @since 1.0
 */
public abstract class Peripheral<T extends HardwareDriver> implements Transport {

    /**
     * A volatile id which is dynamically assigned when a peripheral is
     * connected.
     */
    private final UUID volatileId;
    /**
     * The internally used device key to identify an unique device.
     */
    private String key;
    /**
     * The path on which the device is known on.
     */
    private String path;
    /**
     * Name of the hardware device as reported by the device if available.
     */
    private String name;
    /**
     * The name communicated with an end user.
     */
    private String friendlyName;
    /**
     * The id of the vendor of the device if available.
     */
    private String vendorId;
    /**
     * The id of the product if available.
     */
    private String productId;
    /**
     * The port this device works on.
     */
    private String port;
    /**
     * The serial number of the device if available.
     */
    private String serial = "NONE";
    /**
     * The last known error of the device.
     */
    private String lastError;
    /**
     * If the current hardware component is active or not.
     */
    private boolean active;

    /**
     * If the current component is in use or not.
     */
    private boolean inUse;

    /**
     * The peripheral subsystem used.
     */
    private final Transport.SubSystem subSystem;

    /**
     * The hardware driver for this peripheral.
     */
    private T driver;

    /**
     * Constructor.
     *
     * @param usedSubSystem The subsystem used by the peripheral.
     */
    public Peripheral(final Transport.SubSystem usedSubSystem) {
        this.subSystem = usedSubSystem;
        this.volatileId = UUID.randomUUID();
    }

    /**
     * Returns the volatile id of the peripheral.
     *
     * The id of the peripheral is volatile, it's not wise to store it as a
     * reference as at any time between disconnects and connects it can change.
     *
     * @return The peripheral id.
     */
    public UUID getVolatileId() {
        return this.volatileId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the vendorId
     */
    public String getVendorId() {
        return vendorId;
    }

    /**
     * @param vendorId the vendorId to set
     */
    public void setVendorId(final String vendorId) {
        this.vendorId = vendorId;
    }

    /**
     * @return the deviceId
     */
    public String getProductId() {
        return productId;
    }

    /**
     * @param productId the deviceId to set
     */
    public void setProductId(final String productId) {
        this.productId = productId;
    }

    /**
     * @return the devicePath
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(final String path) {
        this.path = path;
    }

    /**
     * @return the serial
     */
    public String getSerial() {
        return serial;
    }

    /**
     * @param serial the serial to set
     */
    public void setSerial(final String serial) {
        this.serial = serial;
    }

    /**
     * @return the lastKnownError
     */
    public String getLastError() {
        return lastError;
    }

    /**
     * @param lastError the lastKnownError to set
     */
    public void setLastError(final String lastError) {
        this.lastError = lastError;
    }

    /**
     * @return the subSystem
     */
    public Transport.SubSystem getSubSystem() {
        return subSystem;
    }

    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key the key to set
     */
    public void setKey(final String key) {
        this.key = key;
    }

    /**
     * @return the port
     */
    public String getPort() {
        return port;
    }

    /**
     * @param port the devicePort to set
     */
    public void setPort(final String port) {
        this.port = port;
    }

    /**
     * @return the friendlyName
     */
    public String getFriendlyName() {
        return friendlyName;
    }

    /**
     * @param friendlyName the friendlyName to set
     */
    public void setFriendlyName(final String friendlyName) {
        this.friendlyName = friendlyName;
    }

    /**
     * Sets the driver for the hardware interaction.
     *
     * @param peripheralDriver The peripheral hardware driver.
     */
    public void setDriver(final T peripheralDriver) {
        this.driver = peripheralDriver;
    }

    /**
     * Gets the driver for the hardware interaction.
     *
     * @return Returns the driver registered for this peripheral.
     */
    public T getDriver() {
        return this.driver;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(final boolean active) {
        this.active = active;
    }

    /**
     * @return the inUse
     */
    public boolean isInUse() {
        return inUse;
    }

    /**
     * @param inUse the inUse to set
     */
    public void setInUse(final boolean inUse) {
        this.inUse = inUse;
    }

    /**
     * Returns the hardware interface where the device originates from.
     *
     * @return The hardware interface.
     */
    public abstract HardwareComponent.Interface getHardwareInterface();

    /**
     * String representation of the peripheral.
     *
     * @return This peripheral with the most important identifying data.
     */
    @Override
    public String toString() {
        return new StringBuilder("Type:").append("[").append(this.getClass().getSimpleName()).append("],")
                .append("Subsystem:").append("[").append(this.subSystem).append("],")
                .append("Name:").append("[").append(this.friendlyName).append("],")
                .append("Key:").append("[").append(this.key).append("],")
                .append("Path:").append("[").append(this.path).append("],")
                .append("Port:").append("[").append(this.port).append("],")
                .append("Serial:").append("[").append(this.serial).append("]")
                .append("VendorId:").append("[").append(this.vendorId).append("],")
                .append("ProductId:").append("[").append(this.productId).append("],")
                .append("Driver:").append("[").append(this.driver).append("]")
                .toString();
    }

}
