/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.system.hardware;

/**
 * Exception thrown from the Hardware driver store.
 *
 * @author johns
 */
public class HardwareDriverStoreException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>HardwareDriverStoreException</code>
     * without detail message.
     */
    public HardwareDriverStoreException() {
    }

    /**
     * Constructs an instance of <code>HardwareDriverStoreException</code> with
     * the specified detail message.
     *
     * @param msg The detail message.
     */
    public HardwareDriverStoreException(final String msg) {
        super(msg);
    }

    /**
     * Constructs an instance of <code>HardwareDriverStoreException</code> with
     * the specified detail message and cause.
     *
     * @param msg The detail message.
     * @param thr The cause of the thrown exception.
     */
    public HardwareDriverStoreException(final String msg, final Throwable thr) {
        super(msg, thr);
    }
}
