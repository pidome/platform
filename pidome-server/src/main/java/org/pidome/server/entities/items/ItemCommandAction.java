/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.items;

/**
 * Interface for the command action.
 *
 * @author johns
 * @param <A> the action type.
 */
public interface ItemCommandAction<A> {

    /**
     * Sets ther action.
     *
     * @param action The action to set.
     */
    void setAction(A action);

    /**
     * Returns the action to be performed.
     *
     * @return The command action.
     */
    A getAction();

}
