/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.premises;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * A region on a premises level.
 *
 * From rooms, attics, toilets to a region in a garden, driveway or parking lot
 * etc.
 *
 * @author John Sirach
 */
@Entity
@DiscriminatorValue("PROPERTY")
@SuppressWarnings("CPD-START")
public class PropertySection extends RegionSection {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

}
