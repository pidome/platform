/**
 * Modules entity classes.
 * <p>
 * Provides entities (not always a database entity) for database and service
 * communications.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.entities.modules;
