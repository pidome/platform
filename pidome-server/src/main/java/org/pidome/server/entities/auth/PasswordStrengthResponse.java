/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.auth;

import java.util.List;

/**
 * Password strength response object.
 *
 * @author johns
 */
public class PasswordStrengthResponse {

    /**
     * Strength score.
     */
    private int score;

    /**
     * Any warning message.
     */
    private String warning;

    /**
     * Feedback.
     */
    private String feedback;

    /**
     * Possible suggestions.
     */
    private List<String> suggestions;

    /**
     * Strength score.
     *
     * @return the score
     */
    public int getScore() {
        return score;
    }

    /**
     * Strength score.
     *
     * @param score the score to set
     */
    public void setScore(final int score) {
        this.score = score;
    }

    /**
     * Any warning message.
     *
     * @return the warning
     */
    public String getWarning() {
        return warning;
    }

    /**
     * Any warning message.
     *
     * @param warning the warning to set
     */
    public void setWarning(final String warning) {
        this.warning = warning;
    }

    /**
     * Feedback.
     *
     * @return the feedback
     */
    public String getFeedback() {
        return feedback;
    }

    /**
     * Feedback.
     *
     * @param feedback the feedback to set
     */
    public void setFeedback(final String feedback) {
        this.feedback = feedback;
    }

    /**
     * Possible suggestions.
     *
     * @return the suggestions
     */
    public List<String> getSuggestions() {
        return suggestions;
    }

    /**
     * Possible suggestions.
     *
     * @param suggestions the suggestions to set
     */
    public void setSuggestions(final List<String> suggestions) {
        this.suggestions = suggestions;
    }

}
