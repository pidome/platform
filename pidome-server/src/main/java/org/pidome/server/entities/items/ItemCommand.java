/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.items;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.pidome.platform.modules.items.Item.ItemType;
import org.pidome.server.system.modules.devices.DeviceCommand;

/**
 * Base class for an item command.
 *
 * @author johns
 * @param <A> The command action type.
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.EXISTING_PROPERTY,
        property = "type",
        visible = true)
@JsonSubTypes({
    @JsonSubTypes.Type(value = DeviceCommand.class, name = "DEVICE")
})
@SuppressWarnings({"PMD.AbstractClassWithoutAbstractMethod"}) /// Only generic required methods, extended by json subtypes with domain specific methods.
public abstract class ItemCommand<A> implements ItemCommandAction<A> {

    /**
     * The action to be handled by the appropriate module.
     */
    private A action;

    /**
     * The item type.
     */
    private ItemType type;

    /**
     * Returns the item type.
     *
     * @return The item type.
     */
    public final ItemType getType() {
        return this.type;
    }

    /**
     * Sets the item type.
     *
     * @param type The type to set.
     */
    public void setType(final ItemType type) {
        this.type = type;
    }

    /**
     * The command to be handled.
     *
     * @param action The content of the command.
     */
    @Override
    public final void setAction(final A action) {
        this.action = action;
    }

    /**
     * Returns the action.
     *
     * @return The action to exec by the appropriate module.
     */
    @Override
    public final A getAction() {
        return this.action;
    }

}
