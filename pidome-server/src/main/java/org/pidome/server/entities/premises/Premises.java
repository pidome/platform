/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.premises;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.Type;
import org.pidome.server.entities.base.BaseEntity;
import org.pidome.server.entities.users.person.GeoLocation;

/**
 * The premises.
 *
 * @author John Sirach
 */
@Entity
@SuppressWarnings("CPD-START")
public class Premises extends BaseEntity {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The premises name.
     */
    @NotNull(message = "Name is required")
    @Column(nullable = false)
    private String name;

    /**
     * The properties on the premises.
     */
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "premises_id")
    private Set<Property> properties;

    /**
     * property sections on the premises.
     */
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "premises_id")
    private Set<PremisesSection> sections;

    /**
     * Icon used to represent a premises.
     */
    @Type(type = "json")
    @Column(columnDefinition = "text")
    @SuppressFBWarnings(value = {"SE_BAD_FIELD"}, justification = "Handled by json type")
    private PremisesIcon icon;

    /**
     * The location of the premises.
     */
    private GeoLocation location;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the properties
     */
    public Set<Property> getProperties() {
        return properties;
    }

    /**
     * @param properties the properties to set
     */
    public void setProperties(final Set<Property> properties) {
        this.properties = properties;
    }

    /**
     * @return the sections
     */
    public Set<PremisesSection> getSections() {
        return sections;
    }

    /**
     * @param sections the sections to set
     */
    public void setSections(final Set<PremisesSection> sections) {
        this.sections = sections;
    }

    /**
     * @return the location
     */
    public GeoLocation getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(final GeoLocation location) {
        this.location = location;
    }

    /**
     * @return the icon
     */
    public PremisesIcon getIcon() {
        return icon;
    }

    /**
     * @param icon the icon to set
     */
    public void setIcon(final PremisesIcon icon) {
        this.icon = icon;
    }

    /**
     * The hash code.
     *
     * @return The hash code.
     */
    @Override
    public int hashCode() {
        return super.hashCode();
    }

    /**
     * Equality check.
     *
     * @param obj The object to check.
     * @return if equal.
     */
    @Override
    public boolean equals(final Object obj) {
        return super.equals(obj);
    }
}
