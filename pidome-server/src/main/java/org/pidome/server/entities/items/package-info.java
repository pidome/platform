/**
 * Provides item definitions.
 * <p>
 * PAckage for supplying item definitions.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.entities.items;
