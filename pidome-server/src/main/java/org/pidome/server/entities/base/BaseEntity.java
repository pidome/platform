/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import javassist.Modifier;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.cluster.ClusterService;
import org.pidome.server.services.cluster.HostIdentification;
import org.pidome.server.system.database.Database;
import org.pidome.server.system.utils.hibernate.JsonType;
import org.pidome.tools.utilities.StringUtils;

/**
 * The base entity for all entities.
 *
 * @author johns
 */
@TypeDefs({
    @TypeDef(name = "json", typeClass = JsonType.class)
})
@MappedSuperclass
public class BaseEntity implements Serializable {

    /**
     * Device logger.
     */
    private static final Logger LOG = LogManager.getLogger(BaseEntity.class);

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * List of fields not allowed to be modified externally.
     */
    private static final List<String> PROTECTED_FIELDS = List.of(
            "version",
            "id",
            "created",
            "updated"
    );

    /**
     * Reference to the set host method if this entity is inheriting from
     * <code>HostBoundEntity.class</code>.
     */
    private static final Method HOST_METHOD;

    /**
     * Static initializer.
     */
    static {
        try {
            HOST_METHOD = HostBoundEntity.class.getMethod("setHostIdentification", HostIdentification.class);
        } catch (NoSuchMethodException | SecurityException ex) {
            throw new RuntimeException("Unable to reference set host method", ex);
        }
    }

    /**
     * Entity id.
     */
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    /**
     * Entity version.
     */
    @Version
    private Integer version;

    /**
     * Creation date.
     */
    private LocalDateTime created;

    /**
     * Last modified date.
     */
    private LocalDateTime updated;

    /**
     * Called before persisting.
     */
    @PrePersist
    public void prePersist() {
        attachHost(this);
        this.setCreated(LocalDateTime.now());
    }

    /**
     * Called before updating.
     */
    @PreUpdate
    public void preUpdate() {
        this.setUpdated(LocalDateTime.now());
    }

    /**
     * @return the id.
     */
    public UUID getId() {
        return id;
    }

    /**
     * @param id the id to set.
     */
    public void setId(final UUID id) {
        this.id = id;
    }

    /**
     * Entity version.
     *
     * @return the version.
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * Entity version.
     *
     * @param version the version to set.
     */
    public void setVersion(final Integer version) {
        this.version = version;
    }

    /**
     * Creation date.
     *
     * @return the created.
     */
    public LocalDateTime getCreated() {
        return created;
    }

    /**
     * Creation date.
     *
     * @param created the created to set.
     */
    @JsonIgnore
    public void setCreated(final LocalDateTime created) {
        this.created = created;
    }

    /**
     * Last modified date.
     *
     * @return the updated.
     */
    public LocalDateTime getUpdated() {
        return updated;
    }

    /**
     * Last modified date.
     *
     * @param updated the updated to set.
     */
    @JsonIgnore
    public void setUpdated(final LocalDateTime updated) {
        this.updated = updated;
    }

    /**
     * Returns if the entity is a new entity or not.
     *
     * @return boolean true if new.
     */
    @JsonIgnore
    public boolean isNew() {
        return this.id == null;
    }

    /**
     * Saves the entity to the database.
     *
     * This method does both persist a new object to the database as updating
     * the object.
     */
    public void save() {
        try (Database.AutoClosableEntityManager closableManager = Database.getInstance().getNewAutoClosableManager()) {
            EntityManager manager = closableManager.getManager();
            EntityTransaction transaction = manager.getTransaction();
            boolean attachedTransaction = false;
            if (transaction.isActive()) {
                LOG.debug("Join");
                manager.joinTransaction();
                attachedTransaction = true;
            } else {
                LOG.debug("Begin");
                transaction.begin();
            }
            if (this.isNew()) {
                LOG.debug("persist");
                BaseEntity toSave = this;
                manager.persist(toSave);
                this.setId(toSave.getId());
            } else {
                LOG.debug("merge");
                manager.merge(this);
            }
            if (!attachedTransaction) {
                LOG.debug("commit");
                transaction.commit();
            }
        }
    }

    /**
     * Updates the current entity with the given entity.
     *
     * This method merges the given object with this object. This means only
     * changed fields are required to be supplied next to the same id. If the
     * object itself is updated, please use save.
     *
     * @param <T> The Object type.
     * @param entity The entity to update this entity with, must match the
     * object type on which update is called.
     * @return The entity updated.
     */
    public final <T extends BaseEntity> T update(final T entity) {
        if (this.isNew()) {
            throw new IllegalStateException("Can not update a new object, use save");
        } else if (entity == null) {
            throw new IllegalArgumentException("Update with a null object");
        } else if (entity.isNew()) {
            throw new IllegalStateException("Can not update a with new object, use save");
        } else if (entity == this) {
            throw new IllegalStateException("Use save to store the object itself.");
        } else if (!this.getClass().equals(entity.getClass())) {
            throw new IllegalArgumentException("Type used to update is not the same type of object");
        } else if (!this.getId().equals(entity.getId())) {
            throw new IllegalArgumentException("Id's of this object and the used object to update with are not the same");
        }
        mergeThatInThis(this, entity);
        try (Database.AutoClosableEntityManager closableManager = Database.getInstance().getNewAutoClosableManager()) {
            EntityManager manager = closableManager.getManager();
            EntityTransaction transaction = manager.getTransaction();
            boolean attachedTransaction = false;
            if (transaction.isActive()) {
                attachedTransaction = true;
                manager.joinTransaction();
            } else {
                transaction.begin();
            }
            T newObject = manager.merge(entity);
            if (!attachedTransaction) {
                transaction.commit();
            }
            return newObject;
        }
    }

    /**
     * Generate hash code.
     *
     * @return The object hash.
     */
    @Override
    @SuppressWarnings("CPD-START")
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    /**
     * Equality check.
     *
     * @param obj the object to check.
     * @return If equals.
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return Objects.equals(this.id, ((BaseEntity) obj).id);
    }

    /**
     * This entity to a string representation.
     *
     * @return The entity as string.
     */
    @Override
    public String toString() {
        final StringBuilder toStringBuilder = new StringBuilder(this.getClass().getName()).append(": {");
        toStringBuilder.append("entityObjectId: ").append(super.toString()).append(", ");
        Class c = this.getClass();
        Field[] fields = c.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                toStringBuilder.append(field.toString()).append(":")
                        .append(field.get(this))
                        .append(", ");
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                java.util.logging.Logger.getLogger(BaseEntity.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return toStringBuilder.delete(toStringBuilder.length() - 2, toStringBuilder.length())
                .append("}").toString();
    }

    /**
     * Returns a single entity.
     *
     * @param <T> The class type.
     * @param klazz The class to create object for.
     * @param primaryId The primary of the entity to retrieve.
     * @return The object when available.
     */
    public static <T extends BaseEntity> T getsingle(final Class<T> klazz, final UUID primaryId) {
        try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            return autoManager.getManager().find(klazz, primaryId);
        }
    }

    /**
     * Returns a single entity.
     *
     * @param <T> The class type.
     * @param entity The entity to delete.
     */
    public static <T extends BaseEntity> void delete(final T entity) {
        try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            EntityManager manager = autoManager.getManager();
            manager.getTransaction().begin();
            manager.remove(
                    manager.merge(entity)
            );
            manager.getTransaction().commit();
        }
    }

    /**
     * For returning a list of type T without any transactions in it's own
     * isolated manager instance.
     *
     * Be aware that when entities are returned by this method they are
     * detached.
     *
     * @param <T> The type to return in the list.
     * @param klazz The class of the entities to be returned.
     * @return The list of entities requested.
     */
    public static <T extends BaseEntity> Collection<T> getList(final Class<T> klazz) {
        try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            return BaseEntity.getList(klazz, autoManager);
        }
    }

    /**
     * Return a list of entities requested by klazz of type T.
     *
     * Using this method allows to perform more actions on the entities.
     *
     * @param <T> The type to return in the list.
     * @param klazz The class of the entities to be returned.
     * @param manager the auto closable manager as given by
     * <code>Database.getInstance().getNewAutoClosableManager()</code>
     * @return The list of entities requested.
     */
    public static <T extends BaseEntity> Collection<T> getList(final Class<T> klazz, final Database.AutoClosableEntityManager manager) {
        CriteriaBuilder cb = manager.getManager().getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(klazz);
        Root<T> rootEntry = cq.from(klazz);
        CriteriaQuery<T> select = cq.select(rootEntry);
        return manager.getManager().createQuery(select).getResultList();
    }

    /**
     * Merge fields from two identical classes.
     *
     * This method merges fields from two identical entity classes. Fields not
     * being merged are managed fields. The to be merged classes field members
     * will only be gathered using proper setter and getter methods.
     *
     * In short: <code>thisClass.setSomething(thatClass.getSomething())</code>.
     * If there are no setters and getters for an operation it will perform a
     * best effort.
     *
     * @param <T> the type.
     * @param thisClass The class to merge to.
     * @param thatClass The class to merge from.
     */
    @SuppressWarnings({"unchecked"})
    private static <T extends BaseEntity> void mergeThatInThis(final T thisClass, final T thatClass) {
        Class<T> clazz = (Class<T>) thisClass.getClass();
        Set<Field> fields = getAllEntityFields(clazz);

        fields.stream().filter((field) -> (mayICopyField(field))).forEachOrdered((field) -> {
            try {
                try {
                    Method setter = thisClass.getClass().getMethod("set" + StringUtils.capitalise(field.getName()), field.getClass());
                    Method getter = thatClass.getClass().getMethod("get" + StringUtils.capitalise(field.getName()));

                    setter.invoke(thisClass, getter.invoke(thatClass));

                } catch (NoSuchMethodException ex) {
                    LOG.warn("One of those methods does not exist for field [{}]", field.getName(), ex);
                } catch (IllegalArgumentException ex) {
                    LOG.error("Illegal setter used on field [{}] in class [{}]", field.getName(), thisClass.getClass().getCanonicalName(), ex);
                } catch (InvocationTargetException ex) {
                    LOG.error("Unable to call getter or setter for field [{}]", field.getName(), ex);
                }
                field.setAccessible(true);
                field.set(thisClass, field.get(thatClass));
            } catch (IllegalAccessException e) {
                LOG.error("Unable to copy field for entity [{}]", thisClass.getClass().getCanonicalName(), e);
            }
        });
    }

    /**
     * Determines if the entity is an <code>HostBoundEntity</code> and is so
     * attaches the current host identification to it.
     *
     * @param entity A base entity.
     */
    private static void attachHost(final BaseEntity entity) {
        ServiceHandler.service(ClusterService.class).ifPresent(clusterService -> {
            if (entity instanceof HostBoundEntity) {
                try {
                    HOST_METHOD.invoke(entity, clusterService.getHostInformation().getHostIdentification());
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                    LOG.error("Unabled to bind [{}] to host", entity.getClass().getCanonicalName());
                }
            }
        });
    }

    /**
     * Returns true or false if a field may be copied.
     *
     * This method checks for:
     * <ul>
     * <li>synthetic methods,</li>
     * <li>Transient modifiers,</li>
     * <li>Transient Javax annotation,</li>
     * <li>JsonIgore annotation (although this one is not sure yet to be
     * overwritten or not.</li>
     * </ul>
     *
     * @param field The field being checked.
     * @return true when it may be copied, otherwise false.
     */
    private static boolean mayICopyField(final Field field) {
        if (!field.isSynthetic()
                && !Modifier.isTransient(field.getModifiers())
                && field.getAnnotation(Transient.class) == null
                && field.getAnnotation(JsonIgnore.class) == null) {
            return !PROTECTED_FIELDS.contains(field.getName());
        } else {
            return false;
        }
    }

    /**
     * Returns all fields in a class including it's superclass.
     *
     * @param klazz The class to fetch from.
     * @return List of fields
     */
    public static Set<Field> getAllEntityFields(final Class klazz) {
        Set<Field> fields = new HashSet<>();
        Class traverseClass = klazz;
        do {
            Collections.addAll(fields, traverseClass.getDeclaredFields());
            traverseClass = traverseClass.getSuperclass();
        } while (traverseClass != null);
        return fields;
    }

}
