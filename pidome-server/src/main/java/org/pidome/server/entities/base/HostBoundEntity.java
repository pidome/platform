/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import org.pidome.server.services.cluster.HostIdentification;

/**
 * An host bound entity is an entity which should only be available on a
 * specific host.
 *
 * @author johns
 */
@MappedSuperclass
public abstract class HostBoundEntity extends BaseEntity {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The host identification.
     */
    @JsonIgnore
    @ManyToOne
    private HostIdentification hostIdentification;

    /**
     * Returns the information about the host on the entity.
     *
     * @return The host identification.
     */
    public HostIdentification getHostIdentification() {
        return this.hostIdentification;
    }

    /**
     * Set the host identification on the entity.
     *
     * @param identification The identification to set.
     */
    public void setHostIdentification(final HostIdentification identification) {
        this.hostIdentification = identification;
    }

}
