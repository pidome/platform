/**
 * Plugin entity classes.
 * <p>
 * Provides helpers, interfaces and other routines supporting the plugin system
 * entities.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.entities.plugins;
