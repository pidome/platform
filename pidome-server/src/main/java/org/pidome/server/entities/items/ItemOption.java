/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.items;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.pidome.platform.modules.items.ItemPropertyInterface;

/**
 * A single item option.
 *
 * @author johns
 */
public class ItemOption implements ItemPropertyInterface {

    /**
     * Name of the parameter.
     */
    private final String name;

    /**
     * Parameter value.
     */
    private final String value;

    /**
     * Constructor for the device parameter.
     *
     * @param name The name of the parameter.
     * @param value The value of the parameter.
     */
    @JsonCreator
    public ItemOption(final @JsonProperty("name") String name, final @JsonProperty("value") String value) {
        this.name = name;
        this.value = value;
    }

    /**
     * Name of the parameter.
     *
     * @return the name
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Parameter value.
     *
     * @return the value
     */
    @Override
    public String getValue() {
        return value;
    }

}
