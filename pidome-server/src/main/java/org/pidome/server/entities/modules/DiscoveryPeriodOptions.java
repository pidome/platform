/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.modules;

import java.util.Arrays;
import java.util.List;
import org.pidome.server.services.modules.DiscoveryPeriod;

/**
 * Entity providing discovery options.
 *
 * @author johns
 */
public class DiscoveryPeriodOptions {

    /**
     * Provides unit lengths.
     */
    private final List<DiscoveryPeriod.Type> types = Arrays.asList(DiscoveryPeriod.Type.values());

    /**
     * Provides unit lengths.
     */
    private final List<DiscoveryPeriod.Length> lengths = Arrays.asList(DiscoveryPeriod.Length.values());

    /**
     * Provides units.
     */
    private final List<DiscoveryPeriod.Unit> units = Arrays.asList(DiscoveryPeriod.Unit.values());

    /**
     * Provides unit lengths.
     *
     * @return the types
     */
    public List<DiscoveryPeriod.Type> getTypes() {
        return types;
    }

    /**
     * Provides unit lengths.
     *
     * @return the lengths
     */
    public List<DiscoveryPeriod.Length> getLengths() {
        return lengths;
    }

    /**
     * Provides units.
     *
     * @return the units
     */
    public List<DiscoveryPeriod.Unit> getUnits() {
        return units;
    }

}
