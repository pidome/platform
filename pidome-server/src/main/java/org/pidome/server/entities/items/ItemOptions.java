/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.items;

import java.util.ArrayList;
import java.util.Optional;
import org.pidome.platform.modules.items.ItemPropertiesListInterface;

/**
 * Object used within item info to supply options.
 *
 * @author johns
 */
public class ItemOptions extends ArrayList<ItemOption> implements ItemPropertiesListInterface<ItemOption> {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Returns a parameter by it's name.
     *
     * @param optionName The name to return for.
     * @return The option requested.
     */
    @Override
    public Optional<ItemOption> getParameter(final String optionName) {
        return this.stream().filter(parameter -> parameter.getName().equals(optionName)).findFirst();
    }

}
