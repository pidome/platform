/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.auth;

import java.io.Serializable;

/**
 * A login resource object.
 *
 * @author johns
 */
public class LoginResource implements Serializable {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The browser type.
     *
     * The type of browser can be a browser, application or other identifyer
     * understandable for an end user a specific type of application is being
     * used. Example: Google chrome is a browser, the PiDome app is mobile-app.
     */
    private String browserType;
    /**
     * The name of the browser.
     *
     * The name of the browser helps an end user the name of the application
     * type mentioned in <code>browserType</code>. For example the name Chrome
     * is from the Chrome browser, PiDome is from the PiDome mobile-app.
     */
    private String browserName;
    /**
     * The version of the browser used.
     */
    private String browserVersion;
    /**
     * The device type being used.
     *
     * This is identifyable for the end user. Chrome can either be on a desktop
     * or mobile device.
     */
    private String deviceType;
    /**
     * The name of the operating system the browser is running on.
     */
    private String osName;
    /**
     * The version of the OS the browser is running on.
     */
    private String osVersion;
    /**
     * The ip address of the login resource.
     */
    private String remoteIp;

    /**
     * @return the browserType
     */
    public String getBrowserType() {
        return browserType;
    }

    /**
     * @param browserType the browserType to set
     */
    public void setBrowserType(final String browserType) {
        this.browserType = browserType;
    }

    /**
     * @return the browserName
     */
    public String getBrowserName() {
        return browserName;
    }

    /**
     * @param browserName the browserName to set
     */
    public void setBrowserName(final String browserName) {
        this.browserName = browserName;
    }

    /**
     * @return the browserVersion
     */
    public String getBrowserVersion() {
        return browserVersion;
    }

    /**
     * @param browserVersion the browserVersion to set
     */
    public void setBrowserVersion(final String browserVersion) {
        this.browserVersion = browserVersion;
    }

    /**
     * @return the deviceType
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * @param deviceType the deviceType to set
     */
    public void setDeviceType(final String deviceType) {
        this.deviceType = deviceType;
    }

    /**
     * @return the osName
     */
    public String getOsName() {
        return osName;
    }

    /**
     * @param osName the osName to set
     */
    public void setOsName(final String osName) {
        this.osName = osName;
    }

    /**
     * @return the osVersion
     */
    public String getOsVersion() {
        return osVersion;
    }

    /**
     * @param osVersion the osVersion to set
     */
    public void setOsVersion(final String osVersion) {
        this.osVersion = osVersion;
    }

    /**
     * @return the remoteIp
     */
    public String getRemoteIp() {
        return remoteIp;
    }

    /**
     * @param remoteIp the remoteIp to set
     */
    public void setRemoteIp(final String remoteIp) {
        this.remoteIp = remoteIp;
    }

}
