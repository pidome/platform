/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.items;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.UUID;
import org.pidome.platform.modules.items.DiscoveredItem;
import org.pidome.platform.modules.items.Item;
import org.pidome.server.system.modules.ActiveModuleContainer;

/**
 * An item defining a discovered item.
 *
 * @author johns
 */
public class DiscoveredItemDefinition {

    /**
     * The id of the item.
     */
    private final UUID id;

    /**
     * The module where the item was discovered.
     */
    @JsonIgnore
    private final String containerId;

    /**
     * The type of item.
     */
    private final Item.ItemType type;

    /**
     * The item it self.
     */
    private final DiscoveredItem item;

    /**
     * The id of the package where the discovered item originates from.
     */
    private final UUID packageId;

    /**
     * Constructor.
     *
     * @param definitionId The definition id.
     * @param providingContainer The module container owning the item
     * definition.
     * @param itemType The item type.
     * @param theItem The item.
     * @param thePackage id of the package where this item originates from.
     */
    public DiscoveredItemDefinition(
            final UUID definitionId,
            final ActiveModuleContainer providingContainer,
            final Item.ItemType itemType,
            final DiscoveredItem theItem,
            final UUID thePackage) {
        this.containerId = providingContainer.getId();
        this.id = definitionId;
        this.type = itemType;
        this.item = theItem;
        this.packageId = thePackage;
    }

    /**
     * The id of the item.
     *
     * @return the id
     */
    public UUID getId() {
        return id;
    }

    /**
     * Provides the id of the package where this item originates from.
     *
     * @return Package id.
     */
    public UUID getPackageId() {
        return this.packageId;
    }

    /**
     * The module owning the item.
     *
     * @return The module owning the item.
     */
    public String getContainerId() {
        return this.containerId;
    }

    /**
     * The type of item.
     *
     * @return the itemType
     */
    public Item.ItemType getType() {
        return type;
    }

    /**
     * The item it self.
     *
     * @return the item
     */
    public DiscoveredItem getDiscoveredItem() {
        return item;
    }
}
