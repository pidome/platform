/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.items;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import org.pidome.platform.modules.items.Item;
import org.pidome.platform.modules.items.Item.ItemType;
import org.pidome.server.services.definitions.GenericMetaDefinition;
import org.pidome.server.system.modules.devices.DeviceMeta;

/**
 * Item Meta information.
 *
 * @author johns
 * @param <I> The type of item in the meta.
 * @param <D> The definition type in the meta.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.EXISTING_PROPERTY,
        property = "itemType"
)
@JsonSubTypes({
    @JsonSubTypes.Type(value = DeviceMeta.class, name = "DEVICE")
})
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "ITEMTYPE", discriminatorType = DiscriminatorType.STRING)
public abstract class ItemMeta<I extends Item, D extends ItemDefinition> extends GenericMetaDefinition {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The type of item.
     */
    @Column(nullable = false, insertable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private ItemType itemType;

    /**
     * The running device.
     */
    @Transient
    private I item;

    /**
     * The item definition.
     */
    @ManyToOne
    @JsonIgnore
    private D definition;

    /**
     * The address value of an item.
     */
    private String address;

    /**
     * To set a live device.
     *
     * @param item The live device to set.
     */
    public void setItem(final I item) {
        this.item = item;
    }

    /**
     * Returns the live device if present.
     *
     * @return The live device.
     */
    public I getItem() {
        return this.item;
    }

    /**
     * The device address.
     *
     * @param address the address to set
     */
    public void setAddress(final String address) {
        this.address = address;
    }

    /**
     * The device address.
     *
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * The item definition.
     *
     * @return the definition
     */
    public D getDefinition() {
        return definition;
    }

    /**
     * The item definition.
     *
     * @param definition the definition to set
     */
    public void setDefinition(final D definition) {
        this.definition = definition;
    }

    /**
     * The type of item.
     *
     * @return the itemType
     */
    public ItemType getItemType() {
        return itemType;
    }

    /**
     * The type of item.
     *
     * @param itemType the itemType to set
     */
    public void setItemType(final ItemType itemType) {
        this.itemType = itemType;
    }

    /**
     * Updates the generic information as name and description with given
     * definition.
     *
     * The given meta definition must have the same id as the one being updated.
     *
     * @param metaDefinition The meta definition to update this definition with.
     */
    public void updateGenericMeta(final ItemMeta metaDefinition) {
        super.updateGenericMeta(metaDefinition);
        this.setAddress(metaDefinition.getAddress());
    }

}
