/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.premises;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * A section on a premises.
 *
 * @author johns
 */
@Entity
@DiscriminatorValue("PREMISES")
@SuppressWarnings("CPD-START")
public class PremisesSection extends RegionSection {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

}
