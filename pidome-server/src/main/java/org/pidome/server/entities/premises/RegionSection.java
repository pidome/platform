/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.premises;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import org.hibernate.annotations.Type;
import org.pidome.server.entities.base.BaseEntity;

/**
 * The base for any section.
 *
 * @author johns
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "SECTIONTYPE", discriminatorType = DiscriminatorType.STRING)
public abstract class RegionSection extends BaseEntity {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The property name.
     */
    private String name;

    /**
     * The region's X position on the map.
     */
    private int posX;

    /**
     * The region's Y position on the map.
     */
    private int posY;

    /**
     * The visual representation of the reqion.
     *
     * An SVG based object for visual representation.
     */
    @Type(type = "text")
    @Column(columnDefinition = "text")
    private String visualRepresentation;

    /**
     * Icon used to represent a section.
     */
    @Type(type = "json")
    @Column(columnDefinition = "text")
    @SuppressFBWarnings(value = {"SE_BAD_FIELD"}, justification = "Handled by json type")
    private PremisesIcon icon;

    /**
     * The type of item.
     */
    @Column(name = "SECTIONTYPE", nullable = false, insertable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private PropertySectionType sectionType;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the posX
     */
    public int getPosX() {
        return posX;
    }

    /**
     * @param posX the posX to set
     */
    public void setPosX(final int posX) {
        this.posX = posX;
    }

    /**
     * @return the posY
     */
    public int getPosY() {
        return posY;
    }

    /**
     * @param posY the posY to set
     */
    public void setPosY(final int posY) {
        this.posY = posY;
    }

    /**
     * Return the property section type.
     *
     * @return The section type.
     */
    public PropertySectionType getSectionType() {
        return this.sectionType;
    }

    /**
     * @return the visualRepresentation
     */
    public String getVisualRepresentation() {
        return visualRepresentation;
    }

    /**
     * @param visualRepresentation the visualRepresentation to set
     */
    public void setVisualRepresentation(final String visualRepresentation) {
        this.visualRepresentation = visualRepresentation;
    }

    /**
     * @return the icon
     */
    public PremisesIcon getIcon() {
        return icon;
    }

    /**
     * @param icon the icon to set
     */
    public void setIcon(final PremisesIcon icon) {
        this.icon = icon;
    }

}
