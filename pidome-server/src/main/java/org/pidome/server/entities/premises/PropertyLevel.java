/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.premises;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import org.hibernate.annotations.Type;
import org.pidome.server.entities.base.BaseEntity;

/**
 * A level in the premises.
 *
 * From basement to top floor/attic location.
 *
 * @author John Sirach
 */
@Entity
public class PropertyLevel extends BaseEntity {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The level name.
     */
    private String name;

    /**
     * The level index.
     */
    private int index;

    /**
     * The level image url if present.
     */
    private String image;

    /**
     * The sections on the property level.
     */
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "propertylevel_id")
    private Set<PropertySection> sections;

    /**
     * Icon used to represent a section.
     */
    @Type(type = "json")
    @Column(columnDefinition = "text")
    @SuppressFBWarnings(value = {"SE_BAD_FIELD"}, justification = "Handled by json type")
    private PremisesIcon icon;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the index
     */
    public int getIndex() {
        return index;
    }

    /**
     * @param index the index to set
     */
    public void setIndex(final int index) {
        this.index = index;
    }

    /**
     * @return the image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image the image to set
     */
    public void setImage(final String image) {
        this.image = image;
    }

    /**
     * @return the sections
     */
    public Set<PropertySection> getSections() {
        return sections;
    }

    /**
     * @param sections the regions to set
     */
    public void setSections(final Set<PropertySection> sections) {
        this.sections = sections;
    }

    /**
     * @return the icon
     */
    public PremisesIcon getIcon() {
        return icon;
    }

    /**
     * @param icon the icon to set
     */
    public void setIcon(final PremisesIcon icon) {
        this.icon = icon;
    }

    /**
     * The hash code.
     *
     * @return The hash code.
     */
    @Override
    public int hashCode() {
        return super.hashCode();
    }

    /**
     * Equality check.
     *
     * @param obj The object to check.
     * @return if equal.
     */
    @Override
    public boolean equals(final Object obj) {
        return super.equals(obj);
    }
}
