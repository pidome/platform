/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.premises;

import java.io.Serializable;
import org.pidome.platform.presentation.icon.IconType;

/**
 * An icon to represent a premises object.
 *
 * @author John Sirach
 */
public class PremisesIcon implements Serializable {

    /**
     * Class version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The icon type.
     */
    private IconType iconType;

    /**
     * The icon.
     */
    private String icon;

    /**
     * The icon type.
     *
     * @return The icon type.
     */
    public IconType getIconType() {
        return iconType;
    }

    /**
     * Sets the icon type.
     *
     * @param iconType The type of icon.
     */
    public void setIconType(final IconType iconType) {
        this.iconType = iconType;
    }

    /**
     * The icon available in type.
     *
     * @return The icon.
     */
    public String getIcon() {
        return this.icon;
    }

    /**
     * Sets the icon available in icon type.
     *
     * @param icon the icon.
     */
    public void setIcon(final String icon) {
        this.icon = icon;
    }

}
