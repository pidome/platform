/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.premises;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import org.hibernate.annotations.Type;
import org.pidome.server.entities.base.BaseEntity;
import org.pidome.server.entities.users.person.GeoLocation;

/**
 * A property (housing / building)
 *
 * The current meaning of property is housing, building on a premises. If
 * looking for system based properties go to
 * {@link org.pidome.server.system.config.SystemConfig}
 *
 * @author John Sirach
 */
@Entity
@SuppressWarnings("CPD-START")
public class Property extends BaseEntity {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The property naming to describe the property.
     */
    private String name;

    /**
     * property sections on the premises.
     */
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "property_id")
    private Set<PropertyLevel> propertyLevels;

    /**
     * The location of the property.
     */
    private GeoLocation location;

    /**
     * Icon used to represent a section.
     */
    @Type(type = "json")
    @Column(columnDefinition = "text")
    @SuppressFBWarnings(value = {"SE_BAD_FIELD"}, justification = "Handled by json type")
    private PremisesIcon icon;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the propertyLevels
     */
    public Set<PropertyLevel> getPropertyLevels() {
        return propertyLevels;
    }

    /**
     * @param propertyLevels the propertyLevels to set
     */
    public void setPropertyLevels(final Set<PropertyLevel> propertyLevels) {
        this.propertyLevels = propertyLevels;
    }

    /**
     * @return the location
     */
    public GeoLocation getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(final GeoLocation location) {
        this.location = location;
    }

    /**
     * @return the icon
     */
    public PremisesIcon getIcon() {
        return icon;
    }

    /**
     * @param icon the icon to set
     */
    public void setIcon(final PremisesIcon icon) {
        this.icon = icon;
    }

    /**
     * The hash code.
     *
     * @return The hash code.
     */
    @Override
    public int hashCode() {
        return super.hashCode();
    }

    /**
     * Equality check.
     *
     * @param obj The object to check.
     * @return if equal.
     */
    @Override
    public boolean equals(final Object obj) {
        return super.equals(obj);
    }
}
