/**
 * Premises objects and services.
 * <p>
 * Provides the ability to apply premises and geolocation methods.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.entities.premises;
