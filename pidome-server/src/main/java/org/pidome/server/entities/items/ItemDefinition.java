/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.entities.items;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.Type;
import org.pidome.platform.modules.items.ItemAddress;
import org.pidome.server.entities.base.BaseEntity;
import org.pidome.server.services.modules.ModuleDefinition;
import org.pidome.server.system.meta.manufacturer.Manufacturer;
import org.pidome.server.system.modules.devices.DeviceDefinition;

/**
 * A definition of a single item.
 *
 * @author johns
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.EXISTING_PROPERTY,
        property = "definitionType"
)
@JsonSubTypes({
    @JsonSubTypes.Type(value = DeviceDefinition.class, name = "DEVICE")
})
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class ItemDefinition extends BaseEntity {

    /**
     * Entity logger.
     */
    private static final Logger LOG = LogManager.getLogger(DeviceDefinition.class);

    /**
     * The definition types.
     */
    public enum DefinitionType {
        /**
         * Definition type for devices.
         */
        DEVICE;
    }

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Name for the item.
     */
    private String name;

    /**
     * Description for the item.
     */
    private String description;

    /**
     * The definition type.
     */
    @Enumerated(EnumType.STRING)
    private DefinitionType definitionType;

    /**
     * The owning container.
     */
    @Transient
    private String containerId;

    /**
     * Which module provides the device.
     */
    @ManyToOne
    private ModuleDefinition moduleDefinition;

    /**
     * The device address.
     */
    @Type(type = "json")
    @Column(columnDefinition = "text")
    @SuppressFBWarnings(value = {"SE_BAD_FIELD"}, justification = "Handled by json type")
    private ItemAddress address;

    /**
     * The device supplier.
     */
    private Manufacturer manufacturer;

    /**
     * The device class.
     */
    private String itemClass;

    /**
     * The package id this definition is for.
     */
    private UUID packageId;

    /**
     * The device manufacturer.
     *
     * @return the manufacturer.
     */
    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    /**
     * The device manufacturer.
     *
     * @param manufacturer the manufacturer to set
     */
    public void setManufacturer(final Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    /**
     * The item class for loading.
     *
     * @return the itemClass
     */
    public String getItemClass() {
        return itemClass;
    }

    /**
     * Set the item class.
     *
     * @param itemClass the deviceClass to set
     */
    public void setItemClass(final String itemClass) {
        this.itemClass = itemClass;
    }

    /**
     * Name for the item.
     *
     * @return the name
     */
    @SuppressWarnings("CPD-START")
    public String getName() {
        return name;
    }

    /**
     * Name for the item.
     *
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Description for the item.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Description for the item.
     *
     * @param description the description to set
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * The definition type.
     *
     * @return the definitionType
     */
    @SuppressWarnings("CPD-END")
    public DefinitionType getDefinitionType() {
        return definitionType;
    }

    /**
     * The definition type.
     *
     * @param definitionType the definitionType to set
     */
    public void setDefinitionType(final DefinitionType definitionType) {
        this.definitionType = definitionType;
    }

    /**
     * The owning container.
     *
     * @return the containerId
     */
    public String getContainerId() {
        return containerId;
    }

    /**
     * The owning container.
     *
     * @param containerId the containerId to set
     */
    public void setContainerId(final String containerId) {
        this.containerId = containerId;
    }

    /**
     * The package id this definition is for.
     *
     * @return the packageId
     */
    public UUID getPackageId() {
        return packageId;
    }

    /**
     * The package id this definition is for.
     *
     * @param packageId the packageId to set
     */
    public void setPackageId(final UUID packageId) {
        this.packageId = packageId;
    }

    /**
     * Which module provides the device.
     *
     * @return the moduleDefinition
     */
    public ModuleDefinition getModuleDefinition() {
        return moduleDefinition;
    }

    /**
     * Which module provides the device.
     *
     * @param moduleDefinition the moduleDefinition to set
     */
    public void setModuleDefinition(final ModuleDefinition moduleDefinition) {
        this.moduleDefinition = moduleDefinition;
    }

    /**
     * The device address.
     *
     * @return the address
     */
    public ItemAddress getAddress() {
        try {
            return address.clone();
        } catch (CloneNotSupportedException ex) {
            LOG.warn("Could not return duplicate of the item address", ex);
            return this.address;
        }
    }

    /**
     * The device address.
     *
     * @param address the address to set
     */
    @SuppressWarnings({"unchecked"})
    public void setAddress(final ItemAddress address) {
        try {
            this.address = address.clone();
            this.address.setAddress(null);
        } catch (CloneNotSupportedException ex) {
            if (LOG.isDebugEnabled()) {
                LOG.warn("Unabled to clone address structure", ex);
            }
            this.address = address;
        }
    }
}
