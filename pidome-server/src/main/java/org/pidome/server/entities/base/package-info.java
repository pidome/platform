/**
 * Provides base methods and classes for entities.
 * <p>
 * Package for supplying entity base definitions, methods en classes.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.entities.base;
