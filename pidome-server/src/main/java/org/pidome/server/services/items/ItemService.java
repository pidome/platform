/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.items;

import io.vertx.core.Future;
import io.vertx.core.Promise;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.modules.items.Item;
import org.pidome.platform.modules.items.Item.ItemType;
import org.pidome.platform.modules.items.ItemDataNotification;
import org.pidome.server.entities.base.BaseEntity;
import org.pidome.server.entities.items.DiscoveredItemDefinition;
import org.pidome.server.entities.items.ItemCommand;
import org.pidome.server.entities.items.ItemDefinition;
import org.pidome.server.entities.items.ItemMeta;
import org.pidome.server.services.AbstractService;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.events.EventAddress;
import org.pidome.server.services.events.EventService;
import org.pidome.server.services.events.EventSeverity;
import org.pidome.server.services.events.EventType;
import org.pidome.server.services.items.ItemsMutationEvent.MutationType;
import org.pidome.server.services.modules.ModuleService;
import org.pidome.server.system.items.ItemProvider;
import org.pidome.server.system.modules.ActiveModuleContainer;

/**
 * Service for handling of user controllable items and mapping of these items to
 * their own respective services.
 *
 * @author johns
 */
public class ItemService extends AbstractService {

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(ItemService.class);

    /**
     * Literal for when module service is not available.
     */
    private static final String UNAVAILABLE_MODULE_SERVICE = "Module service not available";

    /**
     * Collection of available discovered items.
     */
    private final SortedSet<DiscoveredItemDefinition> discoveredItems = Collections.synchronizedSortedSet(
            new TreeSet<>(
                    (DiscoveredItemDefinition o1, DiscoveredItemDefinition o2)
                    -> o1.getDiscoveredItem().getDiscoveryDateTime().compareTo(o2.getDiscoveredItem().getDiscoveryDateTime())
            )
    );

    /**
     * Returns the items set.
     *
     * @return The set of items available.
     */
    @SuppressWarnings("unchecked")
    public final SortedSet<ItemMeta> getItems() {
        SortedSet<ItemMeta> items = new TreeSet<>((ItemMeta o1, ItemMeta o2) -> o1.getName().compareTo(o2.getName()));
        ServiceHandler.getInstance().getService(ModuleService.class).ifPresent(service -> {
            service.getActiveModules().stream().forEach(module -> {
                items.addAll(module.getItems());
            });
        });
        return items;
    }

    /**
     * Returns an item from the system.
     *
     * This method will first run through the live modules to search for the
     * item t be returned, if not found, it will return the offline version of
     * the item.
     *
     * @param itemId The id of the item to be returned.
     * @return The item requested, or empty optional.
     */
    public final Optional<ItemMeta> getItem(final UUID itemId) {
        return getItem(itemId, false);
    }

    /**
     * Returns an item from the system.This method will first run through the
     * live modules to search for the item t be returned, if not found, it will
     * return the offline version of the item.
     *
     *
     * @param itemId The id of the item to be returned.
     * @param offlineFallback If set to true and no live item is present, try to
     * get the offline instance.
     * @return The item requested, or empty optional.
     */
    public final Optional<ItemMeta> getItem(final UUID itemId, final boolean offlineFallback) {
        return getLiveItem(itemId)
                .or(() -> {
                    if (offlineFallback) {
                        return getOfflineItem(itemId);
                    } else {
                        return Optional.empty();
                    }
                });
    }

    /**
     * Return an item to be expected to be live.
     *
     * @param itemId The id of the item to return.
     * @return Optional with item, or empty optional when not running.
     */
    @SuppressWarnings("unchecked")
    protected final Optional<ItemMeta> getLiveItem(final UUID itemId) {
        return Optional.ofNullable(ServiceHandler.getInstance().getService(ModuleService.class).orElseThrow()
                .getActiveModules().stream()
                .flatMap(module -> (Stream<ItemMeta>) module.getItems().stream())
                .filter(item -> item.getId().equals(itemId))
                .findFirst()
                .orElse(null));
    }

    /**
     * Return an item to be expected to be live.
     *
     * @param itemId The id of the item to return.
     * @return Optional with item, or empty optional when not running.
     */
    protected final Optional<ItemMeta> getOfflineItem(final UUID itemId) {
        return Optional.ofNullable(BaseEntity.getsingle(ItemMeta.class, itemId));
    }

    /**
     * Returns all offline items.
     *
     * @return offline items.
     */
    public final SortedSet<ItemMeta> getAllItems() {
        SortedSet<ItemMeta> items = new TreeSet<>((ItemMeta o1, ItemMeta o2) -> o1.getName().compareTo(o2.getName()));
        items.addAll(BaseEntity.getList(ItemMeta.class));
        return items;
    }

    /**
     * Returns the items set.
     *
     * @return The set of items available.
     */
    public final SortedSet<DiscoveredItemDefinition> getDiscoveredItems() {
        return this.discoveredItems;
    }

    /**
     * Returns a discovered item by it's id.
     *
     * @param discoveredItemId The id of the discovered item.
     * @return Discovered item definition.
     * @throws NoSuchElementException When no item with the given id can be
     * found.
     */
    public final DiscoveredItemDefinition getDiscoveredItem(final UUID discoveredItemId) throws NoSuchElementException {
        return getDiscoveredItems().stream()
                .filter(definition -> definition.getId().equals(discoveredItemId))
                .findFirst()
                .get();
    }

    /**
     * Clears items from the discovered list.
     *
     * @param owner the owner of the items to remove.
     */
    public final void clearDiscoveredItems(final ActiveModuleContainer owner) {
        final Predicate<DiscoveredItemDefinition> predicate = definition -> definition.getContainerId().equals(owner.getId());
        final List<DiscoveredItemDefinition> removeList = discoveredItems.stream().filter(predicate).collect(Collectors.toList());
        synchronized (discoveredItems) {
            discoveredItems.removeIf(predicate);
        }
        if (!removeList.isEmpty()) {
            ServiceHandler.service(EventService.class).ifPresent(service -> {
                ItemEventBody eventBody = new ItemEventBody();
                eventBody.setItems(removeList.stream()
                        .map(mapper -> mapper.getDiscoveredItem())
                        .collect(Collectors.toList())
                );
                eventBody.setType(ItemEventBody.ItemEventType.DISCOVERED);
                ItemEventProducer producer = new ItemEventProducer();
                producer.setEvent(EventSeverity.TRIVIAL, EventType.DELETE, "Items removed from discovery", eventBody);
                service.publishEvent(producer);
            });
        }
    }

    /**
     * Links the provider to the item list mutator.
     *
     * @param provider The provider which is capable to provide items.
     */
    public final void linkProviderMutator(final ItemProvider provider) {
        provider.mutationHandler(this::handleMutation);
        if (provider.capablities().contains(ItemProvider.ProviderCapabilities.DISCOVERY)) {
            provider.discoveryMutationHandler(this::handleDiscoveryMutation);
        }
    }

    /**
     * Unlinks the provider to the item list mutator.
     *
     * @param provider The provider which is capable to provide items.
     */
    public final void unlinkProviderMutator(final ItemProvider provider) {
        provider.mutationHandler(null);
        if (provider.capablities().contains(ItemProvider.ProviderCapabilities.DISCOVERY)) {
            provider.discoveryMutationHandler(null);
        }
    }

    /**
     * Method used to mutate on the items list.
     *
     * @param event The event to be handled.
     */
    @SuppressWarnings("unchecked")
    private void handleMutation(final ItemsMutationEvent event) {
        switch (event.getType()) {
            case ADDED:
                LOG.info("Added event");
                break;
            case REMOVED:
                LOG.info("Removed event");
                break;
            default:
                LOG.error("Unknown event type");
                break;
        }
    }

    /**
     * Method used to mutate on the items list.
     *
     * @param event The event to be handled.
     */
    private void handleDiscoveryMutation(final ItemsDiscoveryMutationEvent event) {
        ItemEventBody eventBody = new ItemEventBody();
        if (event.getItems() != null && !event.getItems().isEmpty()) {
            if (event.getType().equals(MutationType.ADDED)) {
                discoveredItems.addAll(event.getItems());
            } else if (event.getType().equals(MutationType.REMOVED)) {
                discoveredItems.removeAll(event.getItems());
            }
            eventBody.setItems(
                    event.getItems().stream().map(definition -> definition.getDiscoveredItem()).collect(Collectors.toList())
            );
            eventBody.setType(ItemEventBody.ItemEventType.DISCOVERED);
            if (event.getType().equals(MutationType.ADDED)) {
                sendNotification(EventSeverity.TRIVIAL, EventType.ADD, "New Item(s) discovered", eventBody);
            } else if (event.getType().equals(MutationType.REMOVED)) {
                sendNotification(EventSeverity.TRIVIAL, EventType.DELETE, "Item(s) removed", eventBody);
            }
        }
    }

    /**
     * Adds a discovered item to the repository.
     *
     * @param discoveredItem The discovered item.
     * @param itemMeta The information about the item.
     * @throws ItemServiceException On failure of adding because of a service
     * failing.
     */
    @SuppressWarnings({"CPD-START", "unchecked"})
    public void addItemFromDiscovery(final DiscoveredItemDefinition discoveredItem, final ItemMeta itemMeta) throws ItemServiceException {
        ServiceHandler.service(ModuleService.class).orElseThrow(() -> new ItemServiceException(UNAVAILABLE_MODULE_SERVICE))
                .getActiveModuleContainer(discoveredItem.getContainerId())
                .orElseThrow(() -> new ItemServiceException("Container for handling item not available"))
                .addItem(discoveredItem, itemMeta);
    }

    /**
     * Adds a discovered item to the repository.
     *
     * @param discoveredItem The discovered item.
     * @param itemDefinitionId The definition id of the item to add from
     * discovery.
     * @param itemMeta The information about the item.
     * @throws ItemServiceException On failure of adding because of a service
     * failing or not present.
     */
    @SuppressWarnings({"unchecked"})
    public void addItemFromDiscovery(final DiscoveredItemDefinition discoveredItem, final UUID itemDefinitionId, final ItemMeta itemMeta) throws ItemServiceException {
        ServiceHandler.service(ModuleService.class).orElseThrow(() -> new ItemServiceException(UNAVAILABLE_MODULE_SERVICE))
                .getActiveModuleContainer(discoveredItem.getContainerId())
                .orElseThrow(() -> new ItemServiceException("Container for handling item not available"))
                .addItem(discoveredItem, itemDefinitionId, itemMeta);
    }

    /**
     * Returns an item definition by it's definition id.
     *
     * @param itemType The type of item to get the definition for.
     * @param itemDefinitionId The definition id of the item definition to
     * retrieve.
     * @return Optionally the definition requested.
     * @throws ItemServiceException When there are no active definitions.
     */
    @SuppressWarnings("CPD-STOP")
    public Optional<ItemDefinition> getItemDefinition(final ItemType itemType, final UUID itemDefinitionId) throws ItemServiceException {
        return getActiveDefinitionsForItemType(itemType)
                .stream()
                .filter(definition -> definition.getId().equals(itemDefinitionId))
                .findFirst();
    }

    /**
     * Returns a list of known definitions.
     *
     * @param itemType The item type to return for.
     * @return List of definition for the given type.
     * @throws ItemServiceException On failure of retrieving because of a
     * service failing or not present.
     */
    @SuppressWarnings({"unchecked"})
    public List<ItemDefinition> getActiveDefinitionsForItemType(final ItemType itemType) throws ItemServiceException {
        List<ItemDefinition> definitions = (List<ItemDefinition>) ServiceHandler.service(ModuleService.class).orElseThrow(() -> new ItemServiceException(UNAVAILABLE_MODULE_SERVICE))
                .getActiveModules().stream()
                .filter(module -> module.provides().equals(itemType))
                .flatMap(module -> module.getItemDefinitions().stream())
                .collect(Collectors.toUnmodifiableList());
        return definitions;
    }

    /**
     * Adds an item using it's definition.
     *
     * @param definition The item definition.
     * @param itemMeta The information about the item.
     * @throws ItemServiceException When the item can not be added.
     */
    @SuppressWarnings({"unchecked"})
    public void addItemFromDefinition(final ItemDefinition definition, final ItemMeta itemMeta) throws ItemServiceException {
        ServiceHandler.service(ModuleService.class).orElseThrow(() -> new ItemServiceException(UNAVAILABLE_MODULE_SERVICE))
                .getActiveModuleContainer(definition.getContainerId())
                .orElseThrow(() -> new ItemServiceException("Container for handling item not available"))
                .addItem(definition, itemMeta);
    }

    /**
     * Updates an item with the given item info.
     *
     * @param itemId The id of the item.
     * @param itemMeta The info to update an item with.
     * @return Future indicating the update status.
     * @throws ItemServiceException When an item fails to be updated.
     */
    @SuppressWarnings({"unchecked"})
    public Future<Void> updateItem(final UUID itemId, final ItemMeta itemMeta) throws ItemServiceException {
        Promise<Void> saveAndRestartPromise = Promise.promise();
        Optional<ItemMeta> offlineMeta = this.getOfflineItem(itemMeta.getId());
        offlineMeta.ifPresentOrElse(toUpdate -> {
            toUpdate.updateGenericMeta(itemMeta);
            toUpdate.save();
            ServiceHandler.service(ModuleService.class)
                    .ifPresent(moduleService -> {
                        moduleService.getActiveModules().stream()
                                .filter(activeModule -> activeModule.getItems().contains(itemMeta))
                                .findFirst()
                                .ifPresent(module -> module.updateItem(itemMeta));
                    });
            saveAndRestartPromise.complete();
        }, () -> saveAndRestartPromise.fail(new ItemServiceException("Item not known")));
        return saveAndRestartPromise.future();
    }

    /**
     * Deletes an item.
     *
     * @param item The item to delete.
     * @throws ItemServiceException When deletion fails.
     */
    @SuppressWarnings({"unchecked"})
    public void deleteItem(final ItemMeta item) throws ItemServiceException {
        BaseEntity.delete(item);
        ServiceHandler.service(ModuleService.class)
                .ifPresent(moduleService -> {
                    moduleService.getActiveModules().stream()
                            .filter(activeModule -> activeModule.getItems().contains(item))
                            .findFirst()
                            .ifPresent(module -> {
                                module.unloadItem(item);
                                notifyItemsUnLoaded();
                            });
                });
    }

    /**
     * Send out a notification items have been loaded.
     *
     * Used when items are loaded in bulk by a module.
     */
    public void notifyItemsUnLoaded() {
        ItemEventBody eventBody = new ItemEventBody();
        eventBody.setType(ItemEventBody.ItemEventType.ADDED);
        sendNotification(EventSeverity.TRIVIAL, EventType.UNLOADED, "Item(s) stopped", eventBody);
    }

    /**
     * Send out a notification items have been loaded.
     *
     * Used when items are loaded in bulk by a module.
     */
    public void notifyItemsLoaded() {
        ItemEventBody eventBody = new ItemEventBody();
        eventBody.setType(ItemEventBody.ItemEventType.ADDED);
        sendNotification(EventSeverity.TRIVIAL, EventType.LOADED, "Item(s) started", eventBody);
    }

    /**
     * Performs an action on an item.
     *
     * An action can be either an instruction for an item to execute, or a
     * method to execute which provides settings, updated interface, etc...
     *
     * @param itemAction The action to perform.
     * @param executePromise Promise to fulfil or fail.
     */
    public void performItemAction(final ItemAction itemAction, final Promise<Void> executePromise) {
        switch (itemAction.getType()) {
            case COMMAND:
                assignCommand(itemAction.getItemId(), ((BaseItemCommand) itemAction).getItemCommand(), executePromise);
                break;
            case METHOD:
                throw new UnsupportedOperationException("Action [METHOD] not yet supported");
            default:
                throw new UnsupportedOperationException("Unsupported action type [" + itemAction.getType() + "]");

        }
    }

    /**
     * Hands over a command to the correct items container.
     *
     * @param itemId The id of the item.
     * @param itemCommand The command.
     * @param executePromise The promise to fulfil or fail.
     */
    @SuppressWarnings("unchecked")
    private void assignCommand(final UUID itemId, final ItemCommand itemCommand, final Promise<Void> executePromise) {
        ServiceHandler.getInstance().getService(ModuleService.class).ifPresent(service -> {
            service.getActiveModules().stream()
                    .filter(
                            module -> module.provides().equals(itemCommand.getType())
                            && module.getItems().stream().filter(item -> ((ItemMeta) item).getId().equals(itemId)).findFirst().isPresent()
                    ).findFirst()
                    .ifPresentOrElse(runModule -> {
                        runModule.executeItemCommand(itemId, itemCommand, executePromise);
                    }, () -> {
                        executePromise.fail(new Exception("Unable to find the device or module to perform the action with."));
                    });
        });
    }

    /**
     * Sends notifications.
     *
     * @param severity The severity.
     * @param type The type.
     * @param description The description.
     * @param eventBody The event body.
     */
    private void sendNotification(final EventSeverity severity, final EventType type, final String description, final ItemEventBody eventBody) {
        ServiceHandler.service(EventService.class).ifPresent(service -> {
            ItemEventProducer producer = new ItemEventProducer();
            producer.setEvent(severity, type, description, eventBody);
            service.publishEvent(producer);
        });
    }

    /**
     * Sends an event targeting an item.
     *
     * @param itemType The item type.
     * @param eventAddress The event address.
     * @param notification The notification generated by or for an item.
     */
    public final void sendItemNotification(final Item.ItemType itemType, final EventAddress eventAddress, final ItemDataNotification notification) {
        ServiceHandler.service(EventService.class).ifPresent(service -> {
            EventSeverity severity = EventSeverity.MAJOR;
            EventType eventType = EventType.NOTIFY;
            ItemEventProducer producer = new ItemEventProducer(eventAddress);
            ItemEventBody body = new ItemEventBody();
            body.setDataNotification(notification);
            body.setType(ItemEventBody.ItemEventType.UPDATED);
            producer.setEvent(severity, eventType, "Item update", body);
            service.publishEvent(producer);
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void start(final Promise<Void> startPromise) {
        ServiceHandler.service(EventService.class).ifPresent(service -> {
            service.registerCodec(ItemEventBody.class, new ItemEventEncoder());
        });
        startPromise.complete();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void stop(final Promise<Void> stopPromise) {
        stopPromise.complete();
        ServiceHandler.service(EventService.class).ifPresent(service -> {
            service.unregisterCodec(ItemEventBody.class);
        });
    }

}
