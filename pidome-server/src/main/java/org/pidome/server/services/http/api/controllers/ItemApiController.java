/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.controllers;

import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;
import com.webcohesion.enunciate.metadata.rs.TypeHint;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import java.util.Collection;
import java.util.Set;
import java.util.UUID;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.modules.items.Item.ItemType;
import org.pidome.server.entities.items.DiscoveredItemDefinition;
import org.pidome.server.entities.items.ItemDefinition;
import org.pidome.server.entities.items.ItemMeta;
import org.pidome.server.services.http.api.ApiControllerResource;
import org.pidome.server.services.http.api.response.ApiResponseCode;
import org.pidome.server.services.http.api.response.HttpStatusCodeException;
import org.pidome.server.services.items.ItemAction;
import org.pidome.server.services.items.ItemService;
import org.pidome.server.services.items.ItemServiceException;

/**
 * Controller for items available for the end user to interact with.
 *
 * @author johns
 */
@Path("items")
@Produces("application/json")
@SuppressWarnings("CPD-START")
public class ItemApiController extends ApiControllerResource {

    /**
     * Controller logger.
     */
    private static final Logger LOG = LogManager.getLogger(ItemApiController.class);

    /**
     * Returns a list of all available items.
     *
     * @return List of items.
     * @throws HttpStatusCodeException On error.
     */
    @GET
    @RolesAllowed("USER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "Successfully returns the available items."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_503, condition = "When the item service is not available.")
    })
    public Set<ItemMeta> getItems() throws HttpStatusCodeException {
        return getItemService().getItems();
    }

    /**
     * Returns a list of all live and offline items.
     *
     * @return List of items.
     * @throws HttpStatusCodeException On error.
     */
    @GET
    @RolesAllowed("POWER")
    @Path("all")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "Successfully returns the available items."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_503, condition = "When the item service is not available.")
    })
    public Set<ItemMeta> getAllItems() throws HttpStatusCodeException {
        return getItemService().getAllItems();
    }

    /**
     * Returns a list of all available items.
     *
     * @return List of items.
     * @throws HttpStatusCodeException On error.
     */
    @GET
    @Path("discovered")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "Successfully returns the available discovered items."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_503, condition = "When the item service is not available.")
    })
    public Set<DiscoveredItemDefinition> getDiscoveredItems() throws HttpStatusCodeException {
        return getItemService().getDiscoveredItems();
    }

    /**
     * Returns the discovered item id.
     *
     * @param discoveredItemId The id of the item to get.
     * @return The item.
     * @throws HttpStatusCodeException When there is a problem with the service
     * or the item can not be found.
     */
    @GET
    @Path("discovered/{discoveredItemId}")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "Successfully returns the available discovered items."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the requested item does not exist."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_503, condition = "When the item service is not available.")
    })
    public DiscoveredItemDefinition getDiscoveredItem(final @PathParam("discoveredItemId") UUID discoveredItemId) throws HttpStatusCodeException {
        return getItemService().getDiscoveredItems().stream()
                .filter(definition -> definition.getId().equals(discoveredItemId))
                .findFirst()
                .orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_404));
    }

    /**
     * Adds an item to the item repository.
     *
     * @param discoveredItemId The discovered item id.
     * @param itemDefinitionId The id of the item definition.
     * @param itemMeta The information to store for the item.
     * @throws HttpStatusCodeException On error adding a new item to the
     * repository.
     */
    @POST
    @Path("discovered/{discoveredItemId}/{itemDefinitionId}")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_204, condition = "Successfully added the item."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_500, condition = "When adding an item causes an unrecoverable error."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_503, condition = "When the item service is not available.")
    })
    @Consumes("application/json")
    public void addItemFromDiscovery(
            final @PathParam("discoveredItemId") UUID discoveredItemId,
            final @PathParam("itemDefinitionId") UUID itemDefinitionId,
            final ItemMeta itemMeta) throws HttpStatusCodeException {
        try {
            getItemService().addItemFromDiscovery(getDiscoveredItem(discoveredItemId), itemDefinitionId, itemMeta);
        } catch (Exception ex) {
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_500, ex);
        }
    }

    /**
     * Returns a list of definitions for the given item type.
     *
     * @param itemType The item type to return for.
     * @return List of item definitions.
     * @throws HttpStatusCodeException On endpoint failure.
     */
    @GET
    @Path("definitions/{itemType}")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "Successfully returns the available item definitions."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_503, condition = "When the item or depending services are not available.")
    })
    public Collection<ItemDefinition> getItemDefinitions(final @PathParam("itemType") ItemType itemType) throws HttpStatusCodeException {
        try {
            return getItemService().getActiveDefinitionsForItemType(itemType);
        } catch (ItemServiceException ex) {
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_503, ex);
        }
    }

    /**
     * Adds an item by it's definition id.
     *
     * @param definitionId The definition on which the item added is based.
     * @param itemMeta The info about the item to add.
     * @throws HttpStatusCodeException Thrown when an item can not be added.
     */
    @POST
    @Path("item/{definitionId}")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "Successfully starts the module."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_409, condition = "When the definition can not be found."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_500, condition = "On server error attempting to start a module.")
    })
    @TypeHint(value = TypeHint.NONE.class)
    @Consumes("application/json")
    public void addItem(final @PathParam("definitionId") UUID definitionId,
            final ItemMeta itemMeta) throws HttpStatusCodeException {
        try {
            ItemDefinition definition = getItemService().getItemDefinition(itemMeta.getItemType(), definitionId)
                    .orElseThrow(
                            () -> new HttpStatusCodeException(ApiResponseCode.HTTP_409, "definition not found")
                    );
            getItemService().addItemFromDefinition(definition, itemMeta);
        } catch (ItemServiceException ex) {
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_500, "Error in item service", ex);
        }
    }

    /**
     * Returns the definition of an item.
     *
     * @param itemId The id of the item.
     * @return The definition of the given item.
     * @throws HttpStatusCodeException On failure.
     */
    @GET
    @Path("item/{itemId}/definition")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_204, condition = "Successfully returns an item's definition."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the item does not exist.")
    })
    public ItemDefinition getItemDefinitionForItem(final @PathParam("itemId") UUID itemId) throws HttpStatusCodeException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Requested item: [{}]", itemId);
        }
        ItemMeta itemMeta = getItemService().getItem(itemId, true).orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_404, "Item not found"));
        return itemMeta.getDefinition();
    }

    /**
     * Deletes the given item.
     *
     * @param itemId The id of the item to be deleted.
     * @throws HttpStatusCodeException When deletion fails.
     */
    @DELETE
    @Path("item/{itemId}")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "Successfully deletes an item."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the item does not exist")
    })
    public void deleteItem(final @PathParam("itemId") UUID itemId) throws HttpStatusCodeException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Deleting item: [{}]", itemId);
        }
        try {
            ItemMeta item = getItemService().getItem(itemId, true).orElseThrow(
                    () -> new HttpStatusCodeException(ApiResponseCode.HTTP_404, "Item not found")
            );
            getItemService().deleteItem(item);
        } catch (ItemServiceException ex) {
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_500, ex);
        }
    }

    /**
     * Returns a single item.
     *
     * @param itemId The id of the item.
     * @return Future with success promise.
     * @throws HttpStatusCodeException On failure.
     */
    @GET
    @Path("item/{itemId}")
    @RolesAllowed("USER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_204, condition = "Successfully returns an item."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the item does not exist, or is not running.")
    })
    @TypeHint(value = TypeHint.NONE.class)
    public ItemMeta getItem(final @PathParam("itemId") UUID itemId) throws HttpStatusCodeException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Requested item: [{}]", itemId);
        }
        return getItemService().getItems().stream()
                .filter(item -> item.getId().equals(itemId))
                .findFirst()
                .orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_404));
    }

    /**
     * Updates a single item.
     *
     * @param itemId The id of the item.
     * @param itemMeta The info of the item to update.
     * @throws HttpStatusCodeException On failure.
     * @return Future when item has been reloaded or not.
     */
    @PUT
    @Path("item/{itemId}")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "Successfully updates an item."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the item does not exist, or is not running.")
    })
    @TypeHint(value = TypeHint.NONE.class)
    @Consumes("application/json")
    public Future<Void> updateItem(final @PathParam("itemId") UUID itemId, final ItemMeta itemMeta) throws HttpStatusCodeException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Requested item: [{}]", itemId);
        }
        try {
            return getItemService().updateItem(itemId, itemMeta);
        } catch (ItemServiceException ex) {
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_500, ex);
        }
    }

    /**
     * Performs an action send to an item.
     *
     * @param itemId The id of the item.
     * @param itemAction The action to perform by an item.
     * @return Future with success promise.
     * @throws HttpStatusCodeException On failure.
     */
    @PATCH
    @Path("item/{itemId}")
    @RolesAllowed("USER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_204, condition = "Successfully performed the item action."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_400, condition = "On bad action data."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the item does not exist, or is not running.")
    })
    @TypeHint(value = TypeHint.NONE.class)
    @Consumes("application/json")
    public Future<Void> performItemAction(final @PathParam("itemId") UUID itemId, final ItemAction itemAction) throws HttpStatusCodeException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Handling ItemId: [{}], ItemAction [{}]", itemId, itemAction);
        }
        ItemService service = getItemService();
        Promise<Void> performedPromise = Promise.promise();
        service.performItemAction(itemAction, performedPromise);
        return performedPromise.future();
    }

    /**
     * Returns the item service.
     *
     * @return The item service.
     * @throws HttpStatusCodeException When the item service is not available.
     */
    private ItemService getItemService() throws HttpStatusCodeException {
        return getService(ItemService.class)
                .orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_503));
    }

}
