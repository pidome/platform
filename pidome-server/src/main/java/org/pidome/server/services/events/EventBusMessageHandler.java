/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.events;

import io.vertx.core.Handler;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.MessageConsumer;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.entities.users.Role;

/**
 * Base class for handling events from and to the message bus.
 *
 * @author John Sirach
 * @param <T> The Object type being transfered over the bus.
 */
public abstract class EventBusMessageHandler<T> implements Handler<Message<T>> {

    /**
     * The logger.
     */
    private static final Logger LOG = LogManager.getLogger(EventBusMessageHandler.class);

    /**
     * The event address.
     */
    private EventAddress address;

    /**
     * The consumer for handling the messages.
     */
    private Optional<MessageConsumer<T>> eventHandler = Optional.empty();

    /**
     * Empty constructor for deserialization.
     *
     * for normal implementation use the
     * <code>EventBusMessageHandler(final EventAddress eventsAddress)</code>
     * constructor. When an eventbus message needs to be send, and it does not
     * contain an address, it will be discarded.
     *
     */
    public EventBusMessageHandler() {

    }

    /**
     * Constructor holding the address to listen for.
     *
     * @param eventsAddress The address to send to.
     */
    protected EventBusMessageHandler(final EventAddress eventsAddress) {
        this.address = eventsAddress;
    }

    /**
     * Creates the event service based on the given message bus.
     *
     * @param bus The bus to register on.
     * @throws ConsumerMissingException When the consumer is not created.
     */
    protected final void createEventConsumerHandler(final EventBus bus) throws ConsumerMissingException {
        eventHandler = Optional.ofNullable(bus.consumer(this.address.getLocalAddress(), this));
        eventHandler.orElseThrow(() -> new ConsumerMissingException("Consumer not created")).completionHandler(res -> {
            if (res.succeeded()) {
                LOG.debug("The handler registration of [{}] has reached all nodes", this);
            } else {
                LOG.error("Registration of [{}] failed!", this);
            }
        });
    }

    /**
     * De-registers the event handler from the bus.
     *
     * @param bus The bus to remove the handler from.
     *
     * @throws ConsumerMissingException When de-registering the event handler is
     * not possible because it does not exist
     */
    protected final void removeEventConsumerHandler(final EventBus bus) throws ConsumerMissingException {
        eventHandler.orElseThrow(() -> new ConsumerMissingException("Consumer not unregistered")).unregister(res -> {
            if (res.succeeded()) {
                LOG.debug("The handler de-registration of [{}] has reached all nodes", this);
            } else {
                LOG.error("Registration of [{}] failed!", this);
            }
        });
    }

    /**
     * Handles an incoming message from the message bus.
     *
     * @param message The message incoming.
     */
    @Override
    public final void handle(final Message<T> message) {
        EventMessageHeaders eventHeaders = new EventMessageHeaders(Role.UNKNOWN);
        eventHeaders.getHeaders().setAll(message.headers());
        handleMessage(eventHeaders, message);
    }

    /**
     * convenience method to handle incoming messages.
     *
     * @param headers The original headers cast to
     * <code>EventMessageHeaders</code> for convenience header retrieval.
     * @param message The original message received.
     */
    public abstract void handleMessage(EventMessageHeaders headers, Message<T> message);

}
