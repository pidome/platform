/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.items;

/**
 * Exception thrown from the item service.
 *
 * @author johns
 */
public class ItemServiceException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>ItemServiceException</code> without
     * detail message.
     */
    public ItemServiceException() {
    }

    /**
     * Constructs an instance of <code>ItemServiceException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public ItemServiceException(final String msg) {
        super(msg);
    }
}
