/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.controllers;

import com.nulabinc.zxcvbn.Strength;
import com.nulabinc.zxcvbn.Zxcvbn;
import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;
import com.webcohesion.enunciate.metadata.rs.TypeHint;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.auth.User;
import is.tagomor.woothee.Classifier;
import is.tagomor.woothee.DataSet;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.entities.auth.LoginResource;
import org.pidome.server.entities.auth.PasswordStrengthResponse;
import org.pidome.server.entities.auth.QrData;
import org.pidome.server.entities.users.Role;
import org.pidome.server.entities.users.UserLogin;
import org.pidome.server.entities.users.person.MobileDevice;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.authentication.AuthenticationService;
import org.pidome.server.services.authentication.PidomeAuthToken;
import org.pidome.server.services.authentication.PidomeAuthTokenReference;
import org.pidome.server.services.authentication.PidomeAuthUser;
import org.pidome.server.services.http.HttpService;
import org.pidome.server.services.http.api.ApiControllerResource;
import org.pidome.server.services.http.api.auth.LoginObject;
import org.pidome.server.services.http.api.auth.QrProxy;
import org.pidome.server.services.http.api.response.ApiResponseCode;
import org.pidome.server.services.http.api.response.HttpStatusCodeException;
import org.pidome.server.services.network.NetworkService;
import org.pidome.server.services.network.NoInterfaceAvailableException;
import org.pidome.server.system.database.Database;
import org.pidome.server.system.database.Database.AutoClosableEntityManager;

/**
 * Controller for authorizations.
 *
 * @author John Sirach
 */
@Path("auth")
@Produces("application/json")
public class AuthApiController extends ApiControllerResource {

    /**
     * Authorization literal.
     */
    private static final String AUTHORIZATION = "authorization";

    /**
     * Authentication not available literal.
     */
    private static final String AUTHENTICATION_NOT_AVAILABLE = "Authentication service not available";

    /**
     * Logger for the auth rest api.
     */
    private static final Logger LOG = LogManager.getLogger(AuthApiController.class);

    /**
     * Password strength checked.
     */
    private static final Zxcvbn ZXCVBN = new Zxcvbn();

    /**
     * Authenticates and authorizes a user.
     *
     * @param loginObject the object containing the login data.
     * @param response the internal repsonse object.
     * @param request The request object to gather request information.
     * @return Promise indicating failure or success.
     */
    @POST
    @Path("/service/login")
    @PermitAll
    @TypeHint(value = PidomeAuthToken.class)
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "When authentication succeeded."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_401, condition = "When authentication fails."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_500, condition = "When authentication fails due to system reasons.")
    })
    @Consumes("application/json")
    public final Future<PidomeAuthToken> authenticate(final LoginObject loginObject, final @Context HttpServerResponse response, final @Context HttpServerRequest request) {
        final Promise<PidomeAuthToken> promise = Promise.promise();
        try {
            loginObject.getLoginSource().setLoginResource(parseUA(request));
            getAuthenticationService().authenticate(loginObject, authResult -> {
                if (authResult.succeeded()) {
                    promise.complete(authResult.result());
                } else {
                    LOG.warn("Authentication failed", authResult.cause());
                    promise.fail(new HttpStatusCodeException(ApiResponseCode.HTTP_401, "Authentication failed"));
                }
            });
        } catch (HttpStatusCodeException ex) {
            promise.fail(new HttpStatusCodeException(ApiResponseCode.HTTP_500, AUTHENTICATION_NOT_AVAILABLE));
        }
        return promise.future();
    }

    /**
     * Password strength check.
     *
     * @param password The password to check.
     * @return The strength.
     */
    @GET
    @Path("/service/password/strength/{password}")
    @PermitAll
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "Generic response after check")
    })
    public PasswordStrengthResponse checkPasswordStrength(@PathParam("password") final String password) {
        Strength strength = ZXCVBN.measure(password);
        PasswordStrengthResponse response = new PasswordStrengthResponse();
        response.setScore(strength.getScore());
        response.setFeedback(strength.getFeedback().getWarning());
        response.setSuggestions(strength.getFeedback().getSuggestions());
        response.setScore(strength.getScore());
        response.setWarning(strength.getFeedback().getWarning());
        strength.wipe();
        return response;
    }

    /**
     * Parses a user agent to hopefully perform some us identification.
     *
     * @param request The request context
     * @return The identified remote agent.
     */
    @GET
    @Path("/service/ua")
    @PermitAll
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "User agent identification result")
    })
    public LoginResource parseUA(final @Context HttpServerRequest request) {
        Map<String, String> uaResult = Classifier.parse(request.getHeader("User-Agent"));
        final LoginResource resource = new LoginResource();
        resource.setBrowserName(uaResult.get(DataSet.ATTRIBUTE_NAME));
        resource.setBrowserType(uaResult.get(DataSet.ATTRIBUTE_CATEGORY));
        resource.setBrowserVersion(uaResult.get(DataSet.ATTRIBUTE_VERSION));
        resource.setOsName(uaResult.get(DataSet.ATTRIBUTE_OS));
        resource.setOsVersion(uaResult.get(DataSet.ATTRIBUTE_OS_VERSION));
        resource.setDeviceType(uaResult.get(DataSet.ATTRIBUTE_CATEGORY));
        try {
            resource.setRemoteIp(
                    request.remoteAddress().host()
            );
        } catch (Exception ex) {
            LOG.warn("Unable to set login source ip", ex.getMessage());
            resource.setRemoteIp(
                    "Unknown"
            );
        }
        return resource;
    }

    /**
     * Logs out the current user.
     *
     * Even though, JWT tokens should expire naturally, we are using a
     * whitelist. This removes from this whitelist causing the token cause
     * invalidation.
     *
     * This method fails silently unless the service responsible for revoking a
     * token is not available.
     *
     * @param response The response to supply back to the requester.
     * @param request The request object.
     * @throws HttpStatusCodeException When the authentication service is not
     * available.
     */
    @GET
    @Path("/service/logout")
    @PermitAll
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "When logged out"),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_401, condition = "When a non authorized user tries to logout."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_500, condition = "When the authentication service is not available")
    })
    public final void logout(final @Context HttpServerResponse response, final @Context HttpServerRequest request) throws HttpStatusCodeException {
        if (request.getHeader(AUTHORIZATION) != null && !request.getHeader(AUTHORIZATION).isBlank()) {
            getAuthenticationService().revokeToken(AuthenticationService.trimWebBearerToken(request.getHeader(AUTHORIZATION)));
        } else {
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_400);
        }
    }

    /**
     * Returns a list of token references for the logged in user.This method
     * only returns a reference to tokens for the logged in end user.
     *
     * The tokens themself are never returned.
     *
     * @param loggedInUser The current logged in user.
     * @param request The server request object.
     * @return List of token references.
     * @throws HttpStatusCodeException When the authentication service is not
     * available.
     */
    @GET
    @Path("tokens")
    @RolesAllowed("USER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "When authentication succeeded."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_500, condition = "When authentication service is not available or a retrieval error occurs.")
    })
    public final List<PidomeAuthTokenReference> getTokenReferences(final @Context HttpServerRequest request, final @Context User loggedInUser) throws HttpStatusCodeException {
        List<PidomeAuthTokenReference> returnList;
        AuthenticationService service = getAuthenticationService();
        try {
            String tokenBearer = "";
            if (request.getHeader(AUTHORIZATION) != null && !request.getHeader(AUTHORIZATION).isBlank()) {
                tokenBearer = AuthenticationService.trimWebBearerToken(request.getHeader(AUTHORIZATION));
            }
            final String currentToken = tokenBearer;
            returnList = service.getTokenReferences((PidomeAuthUser) loggedInUser);
            if (!currentToken.isBlank()) {
                returnList.forEach(token -> {
                    try {
                        token.setCurrentSession(
                                service.getTokenByReferenceId((PidomeAuthUser) loggedInUser, token.getId()).getToken().equals(currentToken)
                        );
                    } catch (Exception ex) {
                        if (LOG.isDebugEnabled()) {
                            LOG.error("Session is not current session", ex);
                        }
                        token.setCurrentSession(false);
                    }
                });
            }
        } catch (Exception ex) {
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_500, "Error during retrieval", ex);
        }
        return returnList;
    }

    /**
     * Returns a referenced token.
     *
     * This method only returns the token reference requested user. A literal
     * token is never returned.
     *
     * @param loggedInUser The current logged in user.
     * @param tokenReferenceId The id of the token reference.
     * @throws HttpStatusCodeException When the authentication service is not
     * available.
     * @return List of token references.
     */
    @GET
    @Path("tokens/{tokenReferenceId}")
    @RolesAllowed("USER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "When token reference is returned."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_401, condition = "When not authorized."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When not found."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_500, condition = "When authentication service is not available or a retrieval error occurs.")
    })
    public final PidomeAuthTokenReference getTokenReference(final @Context User loggedInUser, final @PathParam("tokenReferenceId") UUID tokenReferenceId) throws HttpStatusCodeException {
        if (ServiceHandler.getInstance().getService(AuthenticationService.class).isPresent()) {
            AuthenticationService service = ServiceHandler.getInstance().getService(AuthenticationService.class).get();
            try {
                return service.getTokenReference((PidomeAuthUser) loggedInUser, tokenReferenceId);
            } catch (Exception ex) {
                throw new HttpStatusCodeException(ApiResponseCode.HTTP_500, "Error during retrieval", ex);
            }
        } else {
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_500, AUTHENTICATION_NOT_AVAILABLE);
        }
    }

    /**
     * Invalidates a token by the logged in user.
     *
     * @param loggedInUser The current logged in user.
     * @param referenceId The id of the token reference.
     * @throws HttpStatusCodeException When the authentication service is not
     * available.
     */
    @DELETE
    @RolesAllowed("USER")
    @Path("tokens/{tokenReferenceId}")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "When deletion succeeded."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_401, condition = "When not authorized."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_500, condition = "When authentication service is not available or error during deletion action.")
    })
    public final void revokeToken(final @Context User loggedInUser, final @PathParam("tokenReferenceId") UUID referenceId) throws HttpStatusCodeException {
        if (ServiceHandler.getInstance().getService(AuthenticationService.class).isPresent()) {
            AuthenticationService service = ServiceHandler.getInstance().getService(AuthenticationService.class).get();

            ServiceHandler.service(HttpService.class).ifPresent(httpService -> {
                httpService.revokeSocketMessage((PidomeAuthUser) loggedInUser, referenceId);
            });
            try {
                service.revokeTokenByReference((PidomeAuthUser) loggedInUser, referenceId);
            } catch (Exception ex) {
                throw new HttpStatusCodeException(ApiResponseCode.HTTP_500, "Error during revoking", ex);
            }
        } else {
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_500, AUTHENTICATION_NOT_AVAILABLE);
        }
    }

    /**
     * Generates the QR code for remote auth.This method does an output of a QR
     * image which allows an end user to authorize a mobile device to be linked
     * to his or her account.
     *
     * This method can be used by a user with the role of
     * {@link org.pidome.server.entities.users.Role#POWER Role.POWER} who is
     * allowed to assign a code to a
     * {@link org.pidome.server.entities.users.Role#USER Role.USER} user.
     *
     * A QR image is valid for a short period of time. This information is
     * inside the image at the TTL parameter. When the TTL has passed a new
     * image needs to be requested. It is advised to start to request a new
     * image a couple of seconds before the TTL has passed.
     *
     * @param response The response object to write the QR image to.
     * @param request The request object to get the user information.
     * @param user The user making the request.
     * @param forUser The id of the user for who the QR is generated.
     * @return Returns a QR image in image/png format
     * @throws UnknownHostException This is thrown when the remote host can not
     * be identified which is a requirement.
     * @throws NoInterfaceAvailableException When there is no intrerface to
     * confirm host for QR available.
     * @todo Move QR base to service.
     */
    @GET
    @RolesAllowed("USER")
    @Path("qr")
    @Produces("image/png")
    @TypeHint(Byte[].class)
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "When image is created."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_401, condition = "When creating a qr image for another used but not allowed todo so.")
    })
    public final HttpServerResponse generateQr(final @QueryParam("forUser") Integer forUser, final @Context HttpServerResponse response, final @Context HttpServerRequest request, final @Context User user) throws UnknownHostException, NoInterfaceAvailableException {
        PidomeAuthUser loggedInUser = (PidomeAuthUser) user;
        if (forUser != null && loggedInUser.hasLowerLevelThan(Role.POWER)) {
            response.setStatusCode(ApiResponseCode.HTTP_401.getResponseCode());
        } else {
            boolean isAllowed = true;
            QrData qrData = new QrData();
            if (forUser != null && loggedInUser.hasHigherOrEqualLevelThan(Role.POWER)) {
                qrData.setOnBehalf(forUser);
            } else if (forUser != null && loggedInUser.hasLowerLevelThan(Role.POWER)) {
                response.setStatusCode(ApiResponseCode.HTTP_200.getResponseCode());
            }
            if (isAllowed) {
                qrData.setPhoneUniqueId(UUID.randomUUID().toString());
                qrData.setRemoteIp(request.host());
                qrData.setServerIp(ServiceHandler.getInstance().getService(NetworkService.class).get().getInterfaceInUse().getIpAddressAsString());
                qrData.setUid(loggedInUser.getUid());
                qrData.setUuId(UUID.randomUUID().toString());

                QrProxy proxy = new QrProxy(qrData);
                response.setStatusCode(ApiResponseCode.HTTP_200.getResponseCode());
                Buffer buffer = Buffer.buffer(proxy.generate().toByteArray());
                response.setChunked(true);
                response.write(buffer);
            }
        }
        response.end();
        return response;
    }

    /**
     * Used for responding to a QR request for confirming the correct user.
     *
     * This method can be used by a user with the role of
     * {@link org.pidome.server.entities.users.Role#POWER Role.POWER} who is
     * allowed to assign a code to a
     * {@link org.pidome.server.entities.users.Role#USER Role.USER} user.
     *
     * For a POWER user also 409 can still apply.
     *
     * @param response the response object of the webserver.
     * @param loggedInUser The user performing the confirmation.
     * @param qrData The QrData object used to confirm a previously send QR
     * image.
     * @todo Move to service.
     */
    @POST
    @Path("qr")
    @RolesAllowed("USER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_201, condition = "When binding is created."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_409, condition = "When the device already exists which would result in a conflict, token is expired or session is invalid."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_410, condition = "Gone, when a power user is unable to link to a user the user is considerd gone.")
    })
    @Consumes("application/json")
    public final void confirmQr(final @Context HttpServerResponse response, final @Context User loggedInUser, final QrData qrData) {
        UserLogin forUser = null;
        PidomeAuthUser user = (PidomeAuthUser) loggedInUser;
        if (user.hasHigherOrEqualLevelThan(Role.POWER) && qrData.getOnBehalf() != null) {
            try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
                forUser = autoManager.getManager().find(UserLogin.class,
                        qrData.getOnBehalf());
            } catch (Exception ex) {
                LOG.error("Unable to retrieve the user given in the QR code on behalf parameter [{}].", qrData.getOnBehalf(), ex);
            }
        } else {
            try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
                forUser = autoManager.getManager().find(UserLogin.class,
                        user.getUid());
            } catch (Exception ex) {
                LOG.error("Unable to retrieve the user of the QR code [{}].", user.getUid(), ex);
            }
        }
        if (forUser != null) {
            Collection<MobileDevice> mobiles = forUser.getPerson().getMobiles();
            boolean exists = false;
            for (MobileDevice mobile : mobiles) {
                if (mobile.getUniqueId().equals(qrData.getPhoneUniqueId())) {
                    exists = true;
                }
            }
            if (!exists) {
                MobileDevice device = new MobileDevice();
                forUser.getPerson().addMobile(device);
                response.setStatusCode(ApiResponseCode.HTTP_201.getResponseCode());
            } else {
                response.setStatusCode(Response.Status.CONFLICT.getStatusCode());
                response.setStatusMessage("Device already known, remove device to add again.");
            }
        } else {
            if (user.hasHigherOrEqualLevelThan(Role.POWER) && qrData.getOnBehalf() != null) {
                response.setStatusCode(ApiResponseCode.HTTP_410.getResponseCode());
                response.setStatusMessage("Unable to link to the given user, user unavailable.");
            } else {
                response.setStatusCode(ApiResponseCode.HTTP_409.getResponseCode());
                response.setStatusMessage("Unable to link to your account, token expired or session invalid. please re login and try again.");
            }
        }
        response.end();
    }

    /**
     * Returns the authentication service.
     *
     * @return The authentication service.
     * @throws HttpStatusCodeException When the service is not available.
     */
    private AuthenticationService getAuthenticationService() throws HttpStatusCodeException {
        return ServiceHandler.getInstance().getService(AuthenticationService.class)
                .orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_503));
    }

}
