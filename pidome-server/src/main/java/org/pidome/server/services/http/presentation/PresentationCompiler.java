/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.presentation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.io.IOException;
import java.util.Objects;
import org.pidome.platform.presentation.input.InputField;
import org.pidome.platform.presentation.input.InputForm;
import org.pidome.tools.utilities.Serialization;

/**
 * Base entry to compile various type of presentations to be exposed via REST.
 *
 * @author johns
 */
public final class PresentationCompiler {

    /**
     * Private constructor as PresentationCompiler is an utility class.
     */
    private PresentationCompiler() {
        /// default constructor.
    }

    /**
     * Merges an original input form with values from a json source.
     *
     * The form merge takes the send in form and merges this with the original
     * form. This means the original form is always leading.
     *
     * @param originalForm The original form.
     * @param override The json form send in from external.
     * @return The merged input form.
     * @throws java.io.IOException When merging values fails.
     */
    @SuppressWarnings("PMD.UseProperClassLoader")
    public static InputForm mergeForm(final InputForm originalForm, final String override) throws IOException {
        TypeFactory typeFactory = TypeFactory.defaultInstance()
                .withClassLoader(originalForm.getClass().getClassLoader());
        ObjectMapper copyedMapper = Serialization.getDefaultObjectMapper().copy().setTypeFactory(typeFactory);
        SubmittedForm suppliedForm = copyedMapper.readValue(override, SubmittedForm.class);
        setDefaultValues(originalForm, suppliedForm);
        return originalForm;
    }

    /**
     * Sets the default values on fields which do not have them set and the
     * original fields has them defined.
     *
     * @param originalForm The original form supplied by a supplier.
     * @param suppliedForm The form send in from external.
     */
    private static void setDefaultValues(final InputForm originalForm, final SubmittedForm suppliedForm) {
        if (originalForm.getSections() != null) {
            originalForm.getSections().stream().forEach(originalSection -> {
                if (originalSection.getElements() != null) {
                    originalSection.getElements().stream().forEach(originalInputField -> {
                        mapValue(originalInputField, suppliedForm);
                    });
                }
            });
        }
    }

    /**
     * Maps a value from the supplied form to an input field of the original
     * form.
     *
     * @param field The field of the original form.
     * @param suppliedForm The form supplied.
     */
    @SuppressWarnings("unchecked")
    private static void mapValue(final InputField field, final SubmittedForm suppliedForm) {
        if (suppliedForm.getSections() != null) {
            suppliedForm.getSections().stream().forEach(section -> {
                if (section.getElements() != null) {
                    section.getElements().stream().forEach(suppliedField -> {
                        if (Objects.equals(field, suppliedField)) {
                            if (suppliedField.getValue() == null && field.isRequired() && field.getDefaultValue() != null) {
                                field.setValue(field.getDefaultValue());
                            } else {
                                field.setValue(suppliedField.getValue());
                            }
                        }
                    });
                }
            });
        }
    }

}
