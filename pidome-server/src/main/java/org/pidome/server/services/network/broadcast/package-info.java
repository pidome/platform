/**
 * Broadcast services.
 * <p>
 * Provides services in which the server broadcasts it's availability.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.services.network.broadcast;
