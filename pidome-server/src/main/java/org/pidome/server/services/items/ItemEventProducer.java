/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.items;

import org.pidome.server.entities.users.Role;
import org.pidome.server.services.events.EventAddress;
import org.pidome.server.services.events.EventMessageHeaders;
import org.pidome.server.services.events.EventProducer;
import org.pidome.server.services.events.EventSeverity;
import org.pidome.server.services.events.EventType;

/**
 * Produces an hardware event.
 *
 * @author johns
 */
public class ItemEventProducer extends EventProducer<ItemEventBody> {

    /**
     * Notification constructor.
     */
    public ItemEventProducer() {
        this.addTargetAddress(EventAddress.ITEMS, EventAddress.WEBSOCKET);
    }

    /**
     * Notification constructor.
     *
     * @param address The event address.
     */
    public ItemEventProducer(final EventAddress address) {
        this.addTargetAddress(address, EventAddress.WEBSOCKET);
    }

    /**
     * Set's the event to broadcast.
     *
     * @param severity The severity of the event.
     * @param type The type of event.
     * @param message Optional message, may be null.
     * @param event The event content.
     */
    public void setEvent(final EventSeverity severity, final EventType type, final String message, final ItemEventBody event) {
        EventMessageHeaders headers = new EventMessageHeaders(Role.ADMIN);
        headers.setType(type);
        headers.setSeverity(severity);
        this.setHeaders(headers);
        this.setMessage(message);
        this.setBody(event);
    }

}
