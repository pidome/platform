/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.cluster;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.vertx.core.Promise;
import java.util.UUID;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.entities.base.BaseEntity;
import org.pidome.server.entities.users.Role;
import org.pidome.server.env.PlatformOs;
import org.pidome.server.services.AbstractService;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.events.EventAddress;
import org.pidome.server.services.events.EventMessageHeaders;
import org.pidome.server.services.events.EventService;
import org.pidome.server.services.network.NetInterface;
import org.pidome.server.services.network.NetworkService;
import org.pidome.server.services.network.NoInterfaceAvailableException;
import org.pidome.server.system.config.SystemConfig;
import org.pidome.tools.utilities.Serialization;

/**
 * The service for cluster management.
 *
 * @author John Sirach
 */
public class ClusterService extends AbstractService {

    /**
     * Logger for the auth rest api.
     */
    private static final Logger LOG = LogManager.getLogger(ClusterService.class);

    /**
     * Information about this cluster host.
     */
    private ClusterHost hostInformation;

    /**
     * The run mode of this instance.
     */
    private ClusterMode runMode = ClusterMode.LOCAL;

    /**
     * Period for waiting until announcement is made.
     */
    private static final int ANNOUNCEMENT_WAIT_PERIOD = 1000;

    /**
     * The host id parameter to be used with the config.
     */
    private static final String HOST_ID_PARAMETER = "server.hostid";

    /**
     * Setting used when the host is not known.
     */
    private static final String ANONYMOUS_HOST = "anonymous";

    /**
     * Starts the cluster service.
     *
     * @param startPromise The future to identify server start.
     */
    @Override
    public void start(final Promise<Void> startPromise) {
        try {
            runMode = ClusterMode.valueOf(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "server.runmode", "LOCAL"));
        } catch (Exception ex) {
            LOG.error("Unable to determine in which mode the server should run, should be one of [{}]", ClusterMode.values(), ex);
            runMode = ClusterMode.LOCAL;
        }
        LOG.info("Cluster service starting in [{}] mode", runMode);
        createHostInformation();
        announce();
        startPromise.complete();
    }

    /**
     * Creates the host information to be used on the cluster.
     */
    private void createHostInformation() {
        hostInformation = new ClusterHost();
        ClusterHost.ServerVersion version = new ClusterHost.ServerVersion();
        version.setBuild(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "SERVER_VERSION_BUILD", 0));
        version.setDate(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "SERVER_VERSION_RELEASEDATE", "Unknown"));
        version.setMajor(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "SERVER_VERSION_MAJOR", 0));
        version.setMinor(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "SERVER_VERSION_MINOR", 0));
        version.setName(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "SERVER_VERSION_RELEASENAME", "Unknown"));
        try {
            version.setPatch(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "SERVER_VERSION_PATCH", 0));
            version.setSnapshot(false);
        } catch (NumberFormatException ex) {
            if (!SystemConfig.isDevRun()) {
                LOG.error("Unable to read patch version, cluster services unreliable", ex);
            }
            version.setPatch(0);
            version.setSnapshot(true);
        }
        version.setType(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "SERVER_VERSION_RELEASETYPE", "Local"));

        ClusterHost.PlatformInfo info = new ClusterHost.PlatformInfo();
        info.setArch(PlatformOs.getReportedArch());
        info.setOs(PlatformOs.getReportedOs());
        info.setPi(PlatformOs.isRaspberryPi());
        info.setJavaVersionInfo(System.getProperty("java.version"));
        info.setJavaVendorInfo(System.getProperty("java.vendor"));

        ServiceHandler.getInstance().getService(NetworkService.class).ifPresentOrElse(service -> {
            try {
                hostInformation.setServerIp(service.getInterfaceInUse().getIpAddressAsString());
            } catch (NoInterfaceAvailableException ex) {
                LOG.error("No network interface present to register", ex);
                hostInformation.setServerIp(NetInterface.NULL_IP);
            }
            hostInformation.setServerName(service.getLocalHostName());
        }, () -> {
            hostInformation.setServerIp(NetInterface.NULL_IP);
            hostInformation.setServerName("localhost");
        });

        hostInformation.setPlatform(info);
        hostInformation.setVersion(version);
        hostInformation.setMode(runMode);
        createHostIdentification();
    }

    /**
     * Creates the host identification.
     */
    private void createHostIdentification() {
        final HostIdentification hostIdentification;
        final String hostId = SystemConfig.getSetting(SystemConfig.Type.SYSTEM, HOST_ID_PARAMETER, ANONYMOUS_HOST);
        if (hostId.equals(ANONYMOUS_HOST)) {
            hostIdentification = new HostIdentification();
            hostIdentification.setNodeName(hostInformation.getServerName());
            hostIdentification.setNodeId(UUID.randomUUID());
            hostIdentification.save();
            LOG.info("Created a new host identification: {}", hostIdentification);
            SystemConfig.setSetting(SystemConfig.Type.SYSTEM, HOST_ID_PARAMETER, hostIdentification.getId().toString());
        } else {
            hostIdentification = BaseEntity.getsingle(HostIdentification.class, UUID.fromString(hostId));
        }
        this.getHostInformation().setHostIdentification(hostIdentification);
    }

    /**
     * Sends an announcement on the cluster to announce this node's presence.
     */
    private void announce() {
        if (runMode == ClusterMode.NODE) {
            ServiceHandler.service(EventService.class).ifPresent(service -> {
                vertx.setTimer(ANNOUNCEMENT_WAIT_PERIOD, id -> {
                    EventMessageHeaders headers = new EventMessageHeaders(Role.SERVICE);
                    try {
                        vertx.eventBus().publish(EventAddress.CLUSTER.getBroadcastAddress(), Serialization.getDefaultObjectMapper().writeValueAsString(getHostInformation()), headers);
                    } catch (JsonProcessingException ex) {
                        LOG.error("Could not send announcement", ex);
                    }
                });
            });
        }
    }

    /**
     * Returns the current cluster modus.
     *
     * @return The modus the instance is running in.
     */
    public final ClusterMode getClusterMode() {
        return this.runMode;
    }

    /**
     * Stops the cluster service.
     *
     * @param stopPromise The future to identify server stop.
     */
    @Override
    public void stop(final Promise<Void> stopPromise) {
        stopPromise.complete();
    }

    /**
     * Returns the cluster host information.
     *
     * @return Host information.
     */
    public ClusterHost getHostInformation() {
        return this.hostInformation;
    }

}
