/**
 * Service package for modules.
 * <p>
 * Supplying methods for endpoints for modules.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.services.modules;
