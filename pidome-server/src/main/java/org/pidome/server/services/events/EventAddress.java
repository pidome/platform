/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.events;

import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.cluster.ClusterService;

/**
 * The event addresses are used by the event bus to determine the system paths
 * to send messages between system and service components.
 *
 * There are distinctive broadcast and local addresses. Please use the correct
 * one for explicit purposes.
 *
 * @author John Sirach
 * @since 1.0
 */
public enum EventAddress {

    /**
     * Address for the websockets.
     *
     * The websocket service reads from this address.
     */
    WEBSOCKET("org.pidome.server.service.http.websocket"),
    /**
     * Address for cluster service messaging.
     */
    CLUSTER("org.pidome.servier.service.cluster"),
    /**
     * Address for hardware service messaging.
     */
    HARDWARE("org.pidome.servier.service.hardware"),
    /**
     * Address for module service messaging.
     */
    MODULES("org.pidome.servier.service.modules"),
    /**
     * Address for device service messaging.
     */
    DEVICES("org.pidome.server.service.devices"),
    /**
     * Address for the items service messaging.
     */
    ITEMS("org.oidome.server.service.items");

    /**
     * The address of the events.
     */
    private final String address;

    /**
     * Event address enum constructor.
     *
     * @param eventAddress The textual representation of the address.
     */
    EventAddress(final String eventAddress) {
        this.address = eventAddress;
    }

    /**
     * Returns the receive address for receiving events.
     *
     * @return The events address.
     */
    public final String getBroadcastAddress() {
        return ServiceHandler.service(ClusterService.class).map(service -> {
            StringBuilder builder = new StringBuilder();
            switch (service.getClusterMode()) {
                case NODE:
                    builder.append("master:").append(service.getHostInformation().getHostIdentification().getNodeId().toString()).append(":");
                    break;
                case LOCAL:
                    /// Do nothing, local is always to self.
                    break;
                default:
                    builder.append(":");
                    break;
            }
            return builder.append(this.address).toString();
        }).orElse("LOCAL:NULL");
    }

    /**
     * Returns the local address.
     *
     * @return The local address.
     */
    public final String getLocalAddress() {
        return ServiceHandler.service(ClusterService.class).map(service -> {
            return new StringBuilder("LOCAL:").append(service.getHostInformation().getHostIdentification().getNodeId().toString()).append(":").append(this.address).toString();
        }).orElse("NODE:NULL:");
    }

}
