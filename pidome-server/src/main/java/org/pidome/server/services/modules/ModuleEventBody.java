/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.modules;

/**
 * The module event body.
 *
 * @author johns
 */
public class ModuleEventBody {

    /**
     * Name of the module.
     */
    private String name;

    /**
     * The module definition id in the event.
     */
    private String definitionId;

    /**
     * When it involves a module which is live, the id of the active module
     * container.
     */
    private String containerId;

    /**
     * Name of the module.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Name of the module.
     *
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the definitionId
     */
    public String getDefinitionId() {
        return definitionId;
    }

    /**
     * @param definitionId the definitionId to set
     */
    public void setDefinitionId(final String definitionId) {
        this.definitionId = definitionId;
    }

    /**
     * When it involves a module which is live, the id of the active module
     * container.
     *
     * @return the activeContainerId
     */
    public String getContainerId() {
        return containerId;
    }

    /**
     * When it involves a module which is live, the id of the active module
     * container.
     *
     * @param containerId the activeContainerId to set
     */
    public void setContainerId(final String containerId) {
        this.containerId = containerId;
    }

}
