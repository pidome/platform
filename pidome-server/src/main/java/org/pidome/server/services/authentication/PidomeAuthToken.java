/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.authentication;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import org.hibernate.annotations.Type;

/**
 * The token used in the authentication of a request on behalf of a user.
 *
 * This is more then just the token. This object supplies the end user extra
 * context about an token where applicable. This will support the end user in
 * revoking tokens.
 *
 * Great care should be taken by API development. The return of tokens should
 * only be done by the end user which owns these.
 *
 * @author John Sirach
 */
@Entity
public class PidomeAuthToken implements Serializable {

    /**
     * Class version.
     */
    public static final long serialVersionUID = 1L;

    /**
     * The token to return.
     */
    @Id
    @Column(updatable = false, nullable = false, unique = true)
    @Type(type = "text")
    private String token;

    /**
     * The authentication type.
     */
    private String type;

    /**
     * The token uid.
     */
    private long uid;

    /**
     * Token originator information.
     */
    @OneToOne(orphanRemoval = true)
    @JsonIgnore
    private PidomeAuthTokenReference tokenOriginatingInfo;

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(final String token) {
        this.token = token;
    }

    /**
     * String representation of the token.
     *
     * @return The string representation of the token.
     */
    @Override
    public final String toString() {
        return "PidomeAuthToken:[secure]";
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(final String type) {
        this.type = type;
    }

    /**
     * @return the uid
     */
    public long getUid() {
        return uid;
    }

    /**
     * @param uid the uid to set
     */
    public void setUid(final long uid) {
        this.uid = uid;
    }

    /**
     * @return the tokenOriginatingInfo
     */
    public PidomeAuthTokenReference getTokenOriginatingInfo() {
        return tokenOriginatingInfo;
    }

    /**
     * @param tokenOriginatingInfo the tokenOriginatingInfo to set
     */
    public void setTokenOriginatingInfo(final PidomeAuthTokenReference tokenOriginatingInfo) {
        this.tokenOriginatingInfo = tokenOriginatingInfo;
    }

    /**
     * Calculate hash.
     *
     * @return The hash.
     */
    @Override
    @SuppressWarnings("CPD-START")
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + Objects.hashCode(this.token);
        return hash;
    }

    /**
     * Object equality test.
     *
     * @param obj The object to test.
     * @return If equal or not.
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PidomeAuthToken other = (PidomeAuthToken) obj;
        return Objects.equals(this.token, other.token);
    }

}
