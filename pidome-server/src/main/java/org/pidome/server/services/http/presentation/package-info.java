/**
 * Presentation parsers.
 * <p>
 * Provides classes and methods to implement the pidome-presentation package
 * supplied presentations.
 *
 * All components which supply a presentation are supported.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.services.http.presentation;
