/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.events;

import java.util.HashSet;
import java.util.Set;

/**
 * Base class to produce events to be send over the event bus.
 *
 * @author johns
 * @param <T> The type to send.
 */
@SuppressWarnings({"PMD.AbstractClassWithoutAbstractMethod", "CPD-START"})
public abstract class EventProducer<T> {

    /**
     * The address to send the notification to.
     */
    private final Set<EventAddress> targetAddresses = new HashSet<>();

    /**
     * The event headers.
     */
    private EventMessageHeaders headers;

    /**
     * The event body.
     */
    private T body;

    /**
     * The address to send the notification to.
     *
     * @return the targetAddress
     */
    protected Set<EventAddress> getTargetAddresses() {
        return targetAddresses;
    }

    /**
     * The address to send the notification to.
     *
     * @param targetAddressList the targetAddress to add
     */
    protected void addTargetAddress(final EventAddress targetAddressList) {
        this.targetAddresses.add(targetAddressList);
    }

    /**
     * The address to send the notification to.
     *
     * @param targetAddressesList the targetAddress to add
     */
    protected void addTargetAddress(final EventAddress... targetAddressesList) {
        if (targetAddresses != null) {
            this.targetAddresses.addAll(Set.of(targetAddressesList));
        }
    }

    /**
     * The event headers.
     *
     * @return the headers
     */
    protected EventMessageHeaders getHeaders() {
        return headers;
    }

    /**
     * The event headers.
     *
     * @param headers the headers to set
     */
    protected void setHeaders(final EventMessageHeaders headers) {
        this.headers = headers;
        this.headers.setDomain(this.getClass().getPackageName());
    }

    /**
     * The event body.
     *
     * @return the body
     */
    protected T getBody() {
        return body;
    }

    /**
     * The event body.
     *
     * @param body the body to set
     */
    protected void setBody(final T body) {
        this.body = body;
    }

    /**
     * An optional message to send.
     *
     * @return the message
     */
    public String getMessage() {
        return this.headers.getMessage();
    }

    /**
     * An optional message to send.
     *
     * @param message the message to set
     */
    protected void setMessage(final String message) {
        this.headers.setMessage(message);
    }

}
