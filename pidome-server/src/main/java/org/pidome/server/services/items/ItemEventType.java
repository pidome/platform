/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.items;

/**
 * The item event types.
 *
 * @author johns
 */
public enum ItemEventType {

    /**
     * Item(s) added.
     */
    ADDED,
    /**
     * Item(s) removed.
     */
    REMOVED,
    /**
     * Item(s) updated.
     */
    UPDATED,
    /**
     * Item(s) discovered.
     */
    DISCOVERED

}
