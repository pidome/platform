/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.modules;

/**
 * Thrown when an error occurs with discovery.
 *
 * @author johns
 */
public class DiscoveryException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>DiscoveryException</code> without detail
     * message.
     */
    public DiscoveryException() {
    }

    /**
     * Constructs an instance of <code>DiscoveryException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public DiscoveryException(final String msg) {
        super(msg);
    }
}
