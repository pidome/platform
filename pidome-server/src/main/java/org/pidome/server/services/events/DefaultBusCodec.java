/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.events;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;
import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.tools.utilities.Serialization;

/**
 * The base encoder used on the message bus.
 *
 * @author johns
 * @param <T> The object type expected.
 */
public abstract class DefaultBusCodec<T> implements MessageCodec<T, T> {

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(DefaultBusCodec.class);

    /**
     * The position from which a read should start.
     *
     * Messages are prefixed with length, byte length of an int is 4 which
     * should be skipped.
     */
    private static final int READ_POSITION = 4;

    /**
     * Encode an object to be transferred over the wire.
     *
     * @param buffer The buffer used to transfer the object.
     * @param object The object to be encoded of type &lt;T&gt;
     */
    @Override
    public final void encodeToWire(final Buffer buffer, final T object) {
        try {
            final byte[] data = Serialization.getDefaultObjectMapper().writeValueAsBytes(object);
            buffer.appendInt(data.length);
            buffer.appendBytes(data);
        } catch (JsonProcessingException ex) {
            LOG.error("Unable to encode [{}] to wire", object, ex);
        }
    }

    /**
     * Decodes from the buffer on the wire to the &lt;T&gt; expected.
     *
     * @param position from which position in the buffer read should start.
     * @param buffer The buffer.
     * @return The object from the buffer.
     */
    @Override
    public final T decodeFromWire(final int position, final Buffer buffer) {
        try {
            return Serialization.getDefaultObjectMapper().readValue(
                    buffer.getBytes(position + READ_POSITION, position + buffer.getInt(position)),
                    new TypeReference<T>() {
            });
        } catch (IOException ex) {
            LOG.error("Unable to decode from wire", ex);
            return null;
        }
    }

    /**
     * Transform for local delivery.
     *
     * @param object The object to transform.
     * @return The transformed object.
     */
    @Override
    public final T transform(final T object) {
        return object;
    }

    /**
     * The codec identifying name.
     *
     * @return base class.
     */
    @Override
    public final String name() {
        return this.getClass().getSimpleName();
    }

    /**
     * Used to identify system codecs.
     *
     * Custom codecs are never system codecs.
     *
     * @return -1 as it's not a system codec.
     */
    @Override
    public final byte systemCodecID() {
        // Always -1
        return -1;
    }

}
