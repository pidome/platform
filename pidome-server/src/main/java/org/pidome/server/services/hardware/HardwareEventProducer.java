/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.hardware;

import org.pidome.server.entities.users.Role;
import org.pidome.server.services.events.EventAddress;
import org.pidome.server.services.events.EventMessageHeaders;
import org.pidome.server.services.events.EventProducer;
import org.pidome.server.services.events.EventSeverity;
import org.pidome.server.services.events.EventType;

/**
 * Produces an hardware event.
 *
 * @author johns
 */
@SuppressWarnings({"CPD-START"})
public class HardwareEventProducer extends EventProducer<HardwareEventBody> {

    /**
     * Notification constructor.
     */
    public HardwareEventProducer() {
        this.addTargetAddress(EventAddress.HARDWARE, EventAddress.WEBSOCKET);
    }

    /**
     * Set's the event to broadcast.
     *
     * @param severity The severity of the event.
     * @param type The type of event.
     * @param message Optional message, may be null.
     * @param event The event content.
     */
    public void setEvent(final EventSeverity severity, final EventType type, final String message, final HardwareEventBody event) {
        EventMessageHeaders headers = new EventMessageHeaders(Role.ADMIN);
        headers.setType(type);
        headers.setSeverity(severity);
        this.setHeaders(headers);
        this.setMessage(message);
        this.setBody(event);
    }

}
