/**
 * Service package generic implementation for items.
 * <p>
 * The generic implementation provides end users with a single point of entry
 * for handling of user controllable items. The sole purpose for the item
 * service is to combine all user controllable items into a single service for
 * generic handling.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.services.items;
