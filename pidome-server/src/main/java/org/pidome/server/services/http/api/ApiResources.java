/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api;

import com.zandero.rest.RestRouter;
import static com.zandero.rest.RestRouter.ORDER_CORS_HANDLER;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.CorsHandler;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.http.HttpService;
import org.pidome.server.services.http.api.controllers.AuthApiController;
import org.pidome.server.services.http.api.controllers.ClusterApiController;
import org.pidome.server.services.http.api.controllers.DeviceBuilderController;
import org.pidome.server.services.http.api.controllers.HardwareApiController;
import org.pidome.server.services.http.api.controllers.InstallerApiController;
import org.pidome.server.services.http.api.controllers.ItemApiController;
import org.pidome.server.services.http.api.controllers.ModulesApiController;
import org.pidome.server.services.http.api.controllers.PersonApiController;
import org.pidome.server.services.http.api.controllers.PremisesController;
import org.pidome.server.services.http.api.controllers.SystemApiController;
import org.pidome.server.services.http.api.controllers.UserApiController;
import org.pidome.server.services.http.api.response.GenericExceptionHandler;
import org.pidome.server.services.http.api.response.HttpStatusCodeExceptionHandler;
import org.pidome.server.services.network.NetworkService;
import org.pidome.server.services.network.NoInterfaceAvailableException;
import org.pidome.server.system.VertXHandler;
import org.pidome.server.system.config.SystemConfig;

/**
 * Collection of classes which will provide jax-rs like interfaces.
 *
 * @author John
 * @since 1.0
 */
public final class ApiResources {

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(ApiResources.class);

    /**
     * Maximum cors lifetime in seconds.
     */
    private static final int MAX_CORS_LIFETIME_SECONDS = 1728000;

    /**
     * Statically defined API resources.
     */
    public enum Collection {
        /**
         * The System controller.
         */
        SYSTEM(SystemApiController.class, "System"),
        /**
         * The cluster controller.
         */
        CLUSTER(ClusterApiController.class, "Cluster"),
        /**
         * User controller.
         */
        USERS(UserApiController.class, "Users"),
        /**
         * The auth controller.
         */
        AUTH(AuthApiController.class, "Authentication"),
        /**
         * Controller for interacting with persons.
         */
        PERSONS(PersonApiController.class, "Persons"),
        /**
         * Peripherals controller for interacting with connected hardware.
         */
        HARDWARE(HardwareApiController.class, "Hardware"),
        /**
         * Installation controller for interacting with all installation related
         * actions.
         *
         * This includes the installation source management.
         */
        INSTALLATION(InstallerApiController.class, "Installer"),
        /**
         * Premises controller.
         */
        PREMISES(PremisesController.class, "Premises"),
        /**
         * Controller to interact with modules.
         */
        MODULES(ModulesApiController.class, "Modules"),
        /**
         * Items available for the end user.
         */
        ITEMS(ItemApiController.class, "Items"),
        /**
         * Builder for creating item definitions.
         */
        BUILDER(DeviceBuilderController.class, "Builder");

        /**
         * The Class resource extending <code>ApiResource</code>.
         */
        private final Class<? extends ApiControllerResource> clazz;

        /**
         * The description of this resource.
         */
        private final String description;

        /**
         * Enum constructor.
         *
         * @param apiClass Class resource extending <code>ApiResource</code>.
         * @param desc The description.
         */
        Collection(final Class<? extends ApiControllerResource> apiClass, final String desc) {
            this.clazz = apiClass;
            this.description = desc;
        }

        /**
         * Returns the class resource.
         *
         * @return Class resource extending <code>ApiResource</code>
         */
        public Class<? extends ApiControllerResource> getResource() {
            return this.clazz;
        }

        /**
         * Returns the description of the resource.
         *
         * @return Resource description.
         */
        public String getDescription() {
            return this.description;
        }
    }

    /**
     * The Vertx instance.
     */
    private final Vertx vertx;

    /**
     * The <code>ApiResourceCollection</code> instance.
     */
    private static ApiResources resource;

    /**
     * private constructor used when getting the instance for the first time.
     */
    private ApiResources() {
        this.vertx = VertXHandler.getInstance().getVertX();
    }

    /**
     * Returns the <code>ApiResourceCollection</code>.
     *
     * @return Returns the <code>ApiResourceCollection</code> instance.
     */
    public static ApiResources getInstance() {
        if (resource == null) {
            resource = new ApiResources();
        }
        return resource;
    }

    /**
     * Method to build the rest resources from the resources enum returned as a
     * certx router.
     *
     * This method also applies the token security to the REST service.
     *
     * @return Vertx router with API resources.
     */
    public Router build() {
        LOG.info("Registering [{}] API controllers: {}", Collection.values().length, Collection.values());
        List<Class<? extends ApiControllerResource>> apiList = new ArrayList<>();
        for (Collection api : Collection.values()) {
            apiList.add(api.getResource());
        }
        Router router = Router.router(vertx);
        RestRouter.getExceptionHandlers().register(HttpStatusCodeExceptionHandler.class, GenericExceptionHandler.class);
        LOG.trace("Registered [{}] exception handlers on REST service", RestRouter.getExceptionHandlers());
        HashSet<String> allowedHeaders = new HashSet<>();
        allowedHeaders.addAll(Arrays.asList("X-Requested-With", "Origin", "Content-Type", "Accept", "Authorization"));
        HashSet<String> exposedHeaders = new HashSet<>();
        exposedHeaders.addAll(Arrays.asList("Access-Control-Allow-Headers", "Authorization", "x-xsrf-token", "Access-Control-Allow-Headers", "Origin", "Accept", "X-Requested-With",
                "Content-Type", "Access-Control-Request-Method", "Access-Control-Request-Headers"));

        String corsHostSet = getSelfCorsHost();
        List<String> corsHosts = getConfiguredAdditionalCorsHosts();
        if (!corsHosts.isEmpty()) {
            if (corsHostSet.isBlank()) {
                corsHostSet = String.join("|", corsHosts);
            } else {
                corsHostSet += "|" + String.join("|", corsHosts);
            }
        }

        LOG.info("Applying CORS, check if these hosts are valid [{}]", corsHostSet);

        CorsHandler handler = CorsHandler.create(corsHostSet)
                .allowCredentials(true)
                .allowedHeaders(allowedHeaders)
                .exposedHeaders(exposedHeaders)
                .maxAgeSeconds(MAX_CORS_LIFETIME_SECONDS);
        handler.allowedMethods(Arrays.asList(HttpMethod.values()).stream().collect(Collectors.toSet()));
        router.route().order(ORDER_CORS_HANDLER).handler(handler);
        RestRouter.register(router, apiList.toArray());
        return router;
    }

    /**
     * Return the self host as to be used with cors.
     *
     * @return The self host.
     */
    private String getSelfCorsHost() {
        final StringBuilder builder = new StringBuilder("https://");
        ServiceHandler.getInstance().getService(NetworkService.class).ifPresent(service -> {
            try {
                builder.append(service.getInterfaceInUse().getIpAddressAsString());
                builder.append(":");
                builder.append(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "http.port", HttpService.HTTP_PORT));
            } catch (NoInterfaceAvailableException ex) {
                LOG.error("Unable to gather primary host information for CORS configuration.", ex);
            }
        });
        return builder.toString();
    }

    /**
     * Return a list of hosts configured to approve as hosts to connect to this
     * server with CORS.
     *
     * @return List of allowed hosts.
     */
    private List<String> getConfiguredAdditionalCorsHosts() {
        List<String> arrList = new ArrayList<>();
        String hostsList = SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "cors.allowedhosts", "");
        if (!hostsList.isBlank()) {
            arrList.addAll(
                    Arrays.asList(
                            Arrays.stream(hostsList.split(",")).map(String::trim).toArray(String[]::new)
                    )
            );
        }
        return arrList;
    }

}
