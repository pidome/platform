/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.modules;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.io.Serializable;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import org.hibernate.annotations.GenericGenerator;
import org.pidome.server.system.utils.GenericConfigurationConverter;

/**
 * Main object containing all the required information to start a module.
 *
 * When a module is started successfully a configuration is stored to create a
 * persistence object to survive between restarts.
 *
 * @author johns
 */
@Entity
@SuppressWarnings("CPD-START")
public class ModuleLoaderConfiguration implements Serializable {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Id of the configuration.
     */
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    /**
     * The module definition.
     */
    private UUID moduleId;

    /**
     * The configuration for the module.
     */
    @Convert(converter = GenericConfigurationConverter.class)
    @Column(columnDefinition = "LONGVARCHAR")
    @SuppressFBWarnings(value = {"SE_BAD_FIELD"}, justification = "Using a converter to overcome json node")
    private JsonNode moduleConfiguration;

    /**
     * the peripheral to which the module should attach.
     */
    private String peripheralKey;

    /**
     * The peripheral vendor id.
     */
    @JsonIgnore
    private String vendorId;

    /**
     * The peripheral product id.
     */
    @JsonIgnore
    private String productId;

    /**
     * The product serial.
     */
    @JsonIgnore
    private String productSerial;

    /**
     * The driver definition.
     */
    private UUID driverId;

    /**
     * The configuration for a driver.
     */
    @Convert(converter = GenericConfigurationConverter.class)
    @Column(columnDefinition = "LONGVARCHAR")
    @SuppressFBWarnings(value = {"SE_BAD_FIELD"}, justification = "Using a converter to overcome json node")
    private JsonNode driverConfiguration;

    /**
     * @return The configuration id.
     */
    public UUID getId() {
        return id;
    }

    /**
     * Sets the configuration id.
     *
     * @param id The id to set.
     */
    public void setId(final UUID id) {
        this.id = id;
    }

    /**
     * The module definition id.
     *
     * @return the moduleDefinition id
     */
    public UUID getModuleId() {
        return moduleId;
    }

    /**
     * The module definition.
     *
     * @param moduleId The id of the module definition
     */
    public void setModuleId(final UUID moduleId) {
        this.moduleId = moduleId;
    }

    /**
     * The configuration for the module.
     *
     * @return the moduleConfiguration
     */
    public JsonNode getModuleConfiguration() {
        return moduleConfiguration;
    }

    /**
     * The configuration for the module.
     *
     * @param moduleConfiguration the moduleConfiguration to set
     */
    public void setModuleConfiguration(final JsonNode moduleConfiguration) {
        this.moduleConfiguration = moduleConfiguration;
    }

    /**
     * The driver definition id.
     *
     * @return the driverDefinition id.
     */
    public UUID getDriverId() {
        return driverId;
    }

    /**
     * The driver definition id.
     *
     * @param driverId the driver definition id to set
     */
    public void setDriverId(final UUID driverId) {
        this.driverId = driverId;
    }

    /**
     * The configuration for a driver.
     *
     * @return the driverConfiguration
     */
    public JsonNode getDriverConfiguration() {
        return driverConfiguration;
    }

    /**
     * The configuration for a driver.
     *
     * @param driverConfiguration the driverConfiguration to set
     */
    public void setDriverConfiguration(final JsonNode driverConfiguration) {
        this.driverConfiguration = driverConfiguration;
    }

    /**
     * the peripheral to which the module should attach.
     *
     * @return the peripheral
     */
    public String getPeripheralKey() {
        return peripheralKey;
    }

    /**
     * the peripheral to which the module should attach.
     *
     * @param peripheralKey the peripheral to set
     */
    public void setPeripheralKey(final String peripheralKey) {
        this.peripheralKey = peripheralKey;
    }

    /**
     * The peripheral vendor id.
     *
     * @return the vendorId
     */
    public String getVendorId() {
        return vendorId;
    }

    /**
     * The peripheral vendor id.
     *
     * @param vendorId the vendorId to set
     */
    public void setVendorId(final String vendorId) {
        this.vendorId = vendorId;
    }

    /**
     * The peripheral product id.
     *
     * @return the productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     * The peripheral product id.
     *
     * @param productId the productId to set
     */
    public void setProductId(final String productId) {
        this.productId = productId;
    }

    /**
     * The product serial.
     *
     * @return the productSerial
     */
    public String getProductSerial() {
        return productSerial;
    }

    /**
     * The product serial.
     *
     * @param productSerial the productSerial to set
     */
    public void setProductSerial(final String productSerial) {
        this.productSerial = productSerial;
    }

}
