/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.items;

/**
 * Listener for mutations in discovery.
 *
 * @author johns
 */
public interface ItemsDiscoveryMutationListener {

    /**
     * Called when a items discovery mutation takes place.
     *
     * @param event The event.
     */
    void discoveryMutated(ItemsDiscoveryMutationEvent event);

}
