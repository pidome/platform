/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api;

import io.vertx.core.http.HttpServerResponse;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.services.AbstractService;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.http.api.response.ApiResponseCode;
import org.pidome.server.services.http.api.response.GenericExceptionHandler;
import org.pidome.server.system.config.SystemConfig;

/**
 * Base class for endpoints to implement.
 *
 * @author John
 */
public class ApiControllerResource {

    /**
     * Logger for the auth rest api.
     */
    private static final Logger LOG = LogManager.getLogger(ApiControllerResource.class);

    /**
     * Private constructor as this class needs to be extended.
     */
    protected ApiControllerResource() {
        /// Private constructor.
    }

    /**
     * Returns the service requested.
     *
     * @param <T> The service class to retrieve.
     * @param service The service expected.
     * @return The service encapsulated in an optional.
     */
    public final <T extends AbstractService> Optional<T> getService(final Class<T> service) {
        return ServiceHandler.getInstance().getService(service);
    }

    /**
     * Creates a generic http response.
     *
     * @param response The http response.
     * @param responseCode The response code to set.
     */
    protected final void createHttpResponse(
            final HttpServerResponse response,
            final ApiResponseCode responseCode) {
        response.setStatusCode(responseCode.getResponseCode());
    }

    /**
     * Generic method for modifying the http response.
     *
     * @param response The http response object.
     * @param responseCode The response code to apply.
     * @param responseMessage An optional message to return.
     */
    protected final void createHttpResponse(
            final HttpServerResponse response,
            final ApiResponseCode responseCode,
            final String responseMessage) {
        response.setStatusCode(responseCode.getResponseCode());
        if (responseMessage != null && !responseMessage.isBlank()) {
            response.end(responseMessage);
        }
    }

    /**
     * Generic method for modifying the http response.
     *
     * This method accepts stacktraces, but these are only included in the
     * output when the server runs in development or debug mode.
     *
     * @param response The http response object.
     * @param responseCode The response code to apply.
     * @param throwable Supply an exception to create stacktraces.
     */
    protected final void createHttpResponse(
            final HttpServerResponse response,
            final ApiResponseCode responseCode,
            final Throwable throwable) {
        response.setStatusCode(responseCode.getResponseCode());
        if (!response.ended()) {
            if (LOG.isDebugEnabled() || SystemConfig.isDevMode()) {
                response.end(GenericExceptionHandler.exceptionFormatter(throwable, response.getStatusCode()));
            }
        } else {
            LOG.error("Unable to write an exception response to HTTP. Posting to log:\n", (Exception) throwable);
        }
    }

}
