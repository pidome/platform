/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.items;

import java.util.Set;
import org.pidome.server.entities.items.ItemMeta;

/**
 * An items mutation object.
 *
 * @author johns
 */
public final class ItemsMutationEvent {

    /**
     * The mutation type.
     */
    public enum MutationType {
        /**
         * Items are added.
         */
        ADDED,
        /**
         * Items are removed.
         */
        REMOVED;
    }

    /**
     * The mutation type.
     */
    private final MutationType type;

    /**
     * The items in the event.
     */
    private final Set<? extends ItemMeta> itemDefinitions;

    /**
     * Event constructor.
     *
     * @param mutationType The mutation type in the event.
     * @param mutationItems The items to be included in this event.
     */
    public ItemsMutationEvent(final MutationType mutationType, final Set<? extends ItemMeta> mutationItems) {
        this.type = mutationType;
        this.itemDefinitions = mutationItems;
    }

    /**
     * Returns the mutation type.
     *
     * @return The type of mutation.
     */
    public MutationType getType() {
        return this.type;
    }

    /**
     * Returns the items in the event.
     *
     * @return The items.
     */
    public Set<? extends ItemMeta> getItems() {
        return this.itemDefinitions;
    }

    /**
     * Returns the items extracted from the definitions in this event.
     *
     * @return The items.
     */
    //public Set<Item> getExtractedItems() {
    //    return this.itemDefinitions.stream()
    //            .map(mapper -> mapper.getItem())
    //            .collect(Collectors.toSet());
    //}
}
