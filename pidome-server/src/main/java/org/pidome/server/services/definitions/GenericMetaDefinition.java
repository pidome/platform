/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.definitions;

import javax.persistence.MappedSuperclass;
import org.pidome.server.entities.base.HostBoundEntity;

/**
 * Generic definition base.
 *
 * @author johns
 */
@MappedSuperclass
public abstract class GenericMetaDefinition extends HostBoundEntity {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The name of the module.
     */
    private String name;

    /**
     * The description of the module.
     */
    private String description;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Updates the generic information as name and description with given
     * definition.
     *
     * The given meta definition must have the same id as the one being updated.
     *
     * @param metaDefinition The meta definition to update this definition with.
     */
    public void updateGenericMeta(final GenericMetaDefinition metaDefinition) {
        this.setName(metaDefinition.getName());
        this.setDescription(metaDefinition.getDescription());
    }

}
