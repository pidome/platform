/**
 * Hardware services.
 * <p>
 * Provides hardware services for other modules to interact with PiDome connected hardware.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.services.hardware;
