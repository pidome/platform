/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.items;

import org.pidome.server.entities.items.ItemCommand;

/**
 * A command for an item.
 *
 * @author johns
 */
public class BaseItemCommand extends ItemAction {

    /**
     * the action to perform.
     */
    private ItemCommand itemCommand;

    /**
     * the item command.
     *
     * @return the action
     */
    public ItemCommand getItemCommand() {
        return itemCommand;
    }

    /**
     * the action to perform.
     *
     * @param itemCommand the action to set
     */
    public void setItemCommand(final ItemCommand itemCommand) {
        this.itemCommand = itemCommand;
    }

}
