/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.rules;

/**
 * Lowest interface for a rule.
 *
 * @author johns
 */
public interface Rule {

    /**
     * Returns the rule name.
     *
     * @return Name of the rule.
     */
    String getName();

}
