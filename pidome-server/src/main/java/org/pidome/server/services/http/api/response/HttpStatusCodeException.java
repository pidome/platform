/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.response;

/**
 * Exception used to throw HTTP status code errors.
 *
 * @author John Sirach
 */
public class HttpStatusCodeException extends RuntimeException {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The status code to return with this exception.
     */
    private final int statusCode;

    /**
     * Creates a new instance of <code>RestResourceNotFoundException</code>.
     *
     * This method falls back to a 500 server error with the default 500 server
     * error message.
     */
    public HttpStatusCodeException() {
        this(ApiResponseCode.HTTP_500);
    }

    /**
     * Creates a new instance of <code>RestResourceNotFoundException</code>
     * without detail message.
     *
     * This exception creates a default status reason. When a meaningfull reason
     * can be supplied please use
     * <code>HttpStatusCodeException(int httpStatusCode, String msg)</code>
     *
     * @param responseCode The response status enum.
     */
    public HttpStatusCodeException(final ApiResponseCode responseCode) {
        super(responseCode.getResponseMessage());
        this.statusCode = responseCode.getResponseCode();
    }

    /**
     * Creates a new instance of <code>RestResourceNotFoundException</code>
     * without detail message.This exception creates a default status reason.
     *
     * When a meaningfull reason can be supplied please use
     * <code>HttpStatusCodeException(int httpStatusCode, String msg)</code>
     *
     * @param responseCode The response status enum.
     * @param cause The cause of the error.
     */
    public HttpStatusCodeException(final ApiResponseCode responseCode, final Throwable cause) {
        super(responseCode.getResponseMessage(), cause);
        this.statusCode = responseCode.getResponseCode();
    }

    /**
     * Constructs an instance of <code>RestResourceNotFoundException</code> with
     * the specified detail message.
     *
     * @param responseCode The response status enum.
     * @param msg the detail message.
     */
    public HttpStatusCodeException(final ApiResponseCode responseCode, final String msg) {
        super(msg);
        this.statusCode = responseCode.getResponseCode();
    }

    /**
     * Constructs an instance of <code>RestResourceNotFoundException</code> with
     * the specified detail message.
     *
     * @param responseCode The response status enum.
     * @param msg the detail message.
     * @param cause The cause of the exception.
     */
    public HttpStatusCodeException(final ApiResponseCode responseCode, final String msg, final Throwable cause) {
        super(msg, cause);
        this.statusCode = responseCode.getResponseCode();
    }

    /**
     * Returns the status code given in th exception constructor.
     *
     * @return The status code.
     */
    public int getStatusCode() {
        return this.statusCode;
    }

}
