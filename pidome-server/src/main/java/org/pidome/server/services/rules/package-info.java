/**
 * PiDome rules.
 * <p>
 * Provides the service and the rules for actors.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.services.rules;
