/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.hardware;

/**
 * Exception thrown from the hardware service.
 *
 * @author johns
 */
public class HardwareServiceException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>HardwareServiceException</code> without
     * detail message.
     */
    public HardwareServiceException() {
    }

    /**
     * Constructs an instance of <code>HardwareServiceException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public HardwareServiceException(final String msg) {
        super(msg);
    }

    /**
     * Constructs an instance of <code>HardwareServiceException</code> with the
     * specified cause.
     *
     * @param cause The original cause when the exception is thrown.
     */
    public HardwareServiceException(final Throwable cause) {
        super(cause);
    }

    /**
     * Constructs an instance of <code>HardwareServiceException</code> with the
     * specified detail message and cause.
     *
     * @param msg The detail message.
     * @param cause The original cause when the exception is thrown.
     */
    public HardwareServiceException(final String msg, final Throwable cause) {
        super(msg, cause);
    }

}
