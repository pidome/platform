/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.network.broadcast;

/**
 * The message used for broadcasting.
 *
 * @author John Sirach
 */
public class DiscoveryBroadcastMessage {

    /**
     * The ip address of the server.
     */
    private String s = "";
    /**
     * Port on which http lives.
     */
    private int p = 0;
    /**
     * Location of the api.
     */
    private String l = "";
    /**
     * If the server is a master in a clustered environment.
     */
    private boolean m = false;

    /**
     * @return the s
     */
    public String getS() {
        return s;
    }

    /**
     * @param serverHost the srv to set
     */
    public void setS(final String serverHost) {
        this.s = serverHost;
    }

    /**
     * @return the httpPort
     */
    public int getP() {
        return p;
    }

    /**
     * @param httpPort the httpPort to set
     */
    public void setP(final int httpPort) {
        this.p = httpPort;
    }

    /**
     * Sets the api location.
     *
     * @param apiLocation location of the api.
     */
    public void setL(final String apiLocation) {
        this.l = apiLocation;
    }

    /**
     * Returns the location of the api.
     *
     * @return The locaiton of the api.
     */
    public String getL() {
        return this.l;
    }

    /**
     * Returns if the broadcast is from a master.
     *
     * @return the m
     */
    public boolean isM() {
        return m;
    }

    /**
     * Sets if the broadcast if from a master.
     *
     * @param m the m to set
     */
    public void setM(final boolean m) {
        this.m = m;
    }

}
