/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.hardware;

import org.pidome.server.system.hardware.Peripheral;

/**
 * The hardware event.
 *
 * @author johns
 */
public class HardwareEventBody {

    /**
     * The peripheral in the event.
     */
    private Peripheral peripheral;

    /**
     * @return the peripheral
     */
    public Peripheral getPeripheral() {
        return peripheral;
    }

    /**
     * @param peripheral the peripheral to set
     */
    public void setPeripheral(final Peripheral peripheral) {
        this.peripheral = peripheral;
    }

}
