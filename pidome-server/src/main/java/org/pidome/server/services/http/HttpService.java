/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http;

import io.vertx.core.Handler;
import io.vertx.core.Promise;
import static io.vertx.core.http.HttpHeaders.AUTHORIZATION;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.core.net.PfxOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.entities.users.Role;
import org.pidome.server.services.AbstractService;
import org.pidome.server.services.ServiceException;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.authentication.AuthenticationService;
import org.pidome.server.services.authentication.PidomeAuthUser;
import org.pidome.server.services.cluster.ClusterService;
import org.pidome.server.services.events.EventMessageHeaders;
import org.pidome.server.services.events.EventService;
import org.pidome.server.services.events.EventSeverity;
import org.pidome.server.services.events.EventType;
import org.pidome.server.services.http.WebSocketProsumer.WebocketNotification;
import org.pidome.server.services.http.api.ApiResources;
import org.pidome.server.services.http.api.response.ApiResponseCode;
import org.pidome.server.services.http.api.response.HttpStatusCodeException;
import org.pidome.server.services.network.NetworkService;
import org.pidome.server.services.network.NoInterfaceAvailableException;
import org.pidome.server.system.VertXHandler;
import org.pidome.server.system.config.SystemConfig;

/**
 * Main class responsible for maintaining http based services. These services
 * comprehend: Web service: Regular UI, API Service: The service for API
 * requests. Websocket service: Service for supplying the API over websockets.
 *
 * @author John
 */
public final class HttpService extends AbstractService {

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(HttpService.class);

    /**
     * Server options.
     */
    private HttpServerOptions httpServerOptions;

    /**
     * The Vert.x webservice router instance.
     */
    private Router router;

    /**
     * The http server serving the clients.
     */
    private HttpServer httpServer;

    /**
     * Default http port.
     */
    public static final int HTTP_PORT = 8080;

    /**
     * Maximum age of items transfered over tls.
     */
    private static final String TRANSPORT_SECURITY_MAX_AGE = "15768000";

    /**
     * List of active websocket prosumers.
     */
    private List<WebSocketProsumer> websocketProsumers = Collections.synchronizedList(new ArrayList<>());

    /**
     * Constructor.
     */
    public HttpService() {

    }

    /**
     * Starts the http service.
     *
     * @param startPromise The promise given to start the service.
     */
    @Override
    public void start(final Promise<Void> startPromise) {
        try {
            setOpts();
            setupRouters();
            httpServer = VertXHandler.getInstance().getVertX().createHttpServer(httpServerOptions)
                    .requestHandler(router)
                    .websocketHandler(webSocket -> {
                        if (webSocket.path().equals("/events")) {
                            Promise<Integer> authPromise = Promise.promise();
                            webSocket.setHandshake(authPromise);
                            authenticateWebsocketConnectionRequest(webSocket, authPromise).future().setHandler(ifHandler -> {
                                if (ifHandler.succeeded()) {
                                    ServiceHandler.service(ClusterService.class).ifPresent(clusterService -> {
                                        attachEventBusHandler(webSocket, ifHandler.result());
                                    });
                                }
                            });
                        }
                    })
                    .listen(res -> {
                        if (res.succeeded()) {
                            try {
                                LOG.info("Webservices started on [https://{}:{}]", ServiceHandler.getInstance().getService(NetworkService.class).get().getInterfaceInUse().getIpAddressAsString(), res.result().actualPort());
                            } catch (NoInterfaceAvailableException ex) {
                                LOG.info("Webservices started on port [{}]", res.result().actualPort());
                            }
                            startPromise.complete();
                        } else {
                            startPromise.fail(new ServiceException("Failed to start webservices", res.cause()));
                        }
                    });
        } catch (NoInterfaceAvailableException ex) {
            startPromise.fail(new ServiceException("Unable to determine the host", ex));
        }
    }

    /**
     * Sends a message over a websocket to a user referenced by the token
     * reference.
     *
     * @param user The use to send the message to.
     * @param tokenReference The token reference for the connection.
     */
    public void revokeSocketMessage(final PidomeAuthUser user, final UUID tokenReference) {
        try {
            final String token = ServiceHandler.service(AuthenticationService.class).get().getTokenByReferenceId(user, tokenReference).getToken();
            websocketProsumers.forEach(prosumer -> {
                if (prosumer.getUser().getUid() == user.getUid() && prosumer.getToken().equals(token)) {
                    EventMessageHeaders headers = new EventMessageHeaders(Role.USER);
                    headers.setAckRequired(false);
                    headers.setSeverity(EventSeverity.MAJOR);
                    headers.setTargetUid(user.getUid());
                    headers.setType(EventType.DELETE);
                    headers.setDomain("org.pidome.server.services.authentication");
                    headers.setMessage("Login session from this location is revoked");
                    WebocketNotification notification = new WebocketNotification();
                    notification.setHeaders(headers);
                    prosumer.postToSocket(notification);
                    prosumer.closeSocket();
                }
            });
        } catch (Exception ex) {
            LOG.warn("Unable to send a direct message to user [{}]", user, ex);
        }
    }

    /**
     * Attaches the handlers to the websocket.
     *
     * @param webSocket The websocket to attach the handlers to.
     * @param user The user for whom the socket consumer must be registered.
     */
    private void attachEventBusHandler(final ServerWebSocket webSocket, final PidomeAuthUser user) {
        final WebSocketProsumer consumer = new WebSocketProsumer(webSocket, user);
        ServiceHandler.getInstance().getService(EventService.class).ifPresent(service -> {
            this.websocketProsumers.add(consumer);
            service.registerEventBusHandler(consumer);
        });
        webSocket.frameHandler(frame -> {
            /// drop incoming frames, these are not allowed
        }).closeHandler(closeHandler -> {
            this.websocketProsumers.remove(consumer);
            ServiceHandler.getInstance().getService(EventService.class).ifPresent(service -> {
                service.unRegisterEventBusHandler(consumer);
            });
        });
    }

    /**
     * Authenticates a new websocket connection.
     *
     * @param webSocket The socket connection to authenticate.
     * @param authPromise The promise to fulfil.
     * @return Promise when fulfilled containing the auth user, otherwise
     * failure with cause exception.
     */
    private Promise<PidomeAuthUser> authenticateWebsocketConnectionRequest(final ServerWebSocket webSocket, final Promise<Integer> authPromise) {
        final Promise<PidomeAuthUser> resultPromise = Promise.promise();
        VertXHandler.getInstance().getVertX().executeBlocking((Handler<Promise<PidomeAuthUser>>) promise -> {
            if (webSocket.isSsl()) {
                ServiceHandler.getInstance().getService(AuthenticationService.class).ifPresentOrElse(service -> {
                    final String token = getTokenFromWebsocket(webSocket);
                    if (!service.isTokenRevoked(token)) {
                        promise.complete(service.getUser(token));
                    } else {
                        promise.fail(new HttpStatusCodeException(ApiResponseCode.HTTP_401, "Authentication failed"));
                    }
                }, () -> {
                    promise.fail(new HttpStatusCodeException(ApiResponseCode.HTTP_503, "Authentication not available"));
                });
            } else {
                promise.fail(new HttpStatusCodeException(ApiResponseCode.HTTP_400, "Socket must be SSL"));
            }
        }, result -> {
            if (result.succeeded()) {
                authPromise.complete(ApiResponseCode.HTTP_101.getResponseCode());
                resultPromise.complete(result.result());
            } else {
                authPromise.fail(result.cause());
                resultPromise.fail(result.cause());
                LOG.error("Authentication failed for websocket.", result.cause());
            }
        });
        return resultPromise;
    }

    /**
     * Returns a token on a websocket.
     *
     * @param webSocket The websocket to get the token from.
     * @return The token, null if no token is present.
     */
    protected static String getTokenFromWebsocket(final ServerWebSocket webSocket) {
        if (webSocket.headers().get(AUTHORIZATION) != null) {
            return AuthenticationService.trimWebBearerToken(webSocket.headers().get(AUTHORIZATION));
        } else if (webSocket.query() != null) {
            return webSocket.query();
        } else {
            return "";
        }
    }

    /**
     * Stops the http service.
     *
     * @param stopPromise The future given to detect stopping was successful.
     */
    @Override
    public void stop(final Promise<Void> stopPromise) {
        httpServer.close(res -> {
            if (res.succeeded()) {
                LOG.info("Webservices stopped");
                stopPromise.complete();
            } else {
                stopPromise.fail(new ServiceException("Failed to stop webservices", res.cause()));
            }
        });
    }

    /**
     * Returns the host bound to.
     *
     * @return The host as string, this can be either a name or ip addres.
     */
    public String getBoundHostAsString() {
        return httpServerOptions.getHost();
    }

    /**
     * Install all the routes to be able to service web service users.
     */
    private void setupRouters() {
        final String apiLocation = SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "http.rest", "/api");

        router = Router.router(VertXHandler.getInstance().getVertX());

        LOG.trace("Creating body handler");
        router.route().handler(BodyHandler.create());
        appendSecurityHeaders();

        LOG.info("Mounting [{}] as the API entry point for all REST calls", apiLocation);
        Router apiRouter = ApiResources.getInstance().build();
        ServiceHandler.getInstance().getService(AuthenticationService.class).ifPresent(authService -> {
            authService.addToRoute(apiRouter, apiLocation + "/auth/service/");
        });
        router.mountSubRouter(apiLocation, apiRouter);

        String staticPath = SystemConfig.getResourcePath("http/" + SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "http.interface", "default"));
        LOG.info("Serving static files from: [{}, {}]", staticPath, new File(staticPath).exists());
        StaticHandler staticHandler
                = StaticHandler.create(staticPath)
                        .setCachingEnabled(true)
                        .setDefaultContentEncoding("UTF-8")
                        .setIndexPage("index.html")
                        .setIncludeHidden(false);
        router.route("/*").handler(staticHandler);

    }

    /**
     * Adds headers to improve security a bit.
     */
    private void appendSecurityHeaders() {
        router.route().handler(ctx -> {
            ctx.response()
                    // prevents Internet Explorer from MIME - sniffing a
                    // response away from the declared content-type
                    .putHeader("X-Content-Type-Options", "nosniff")
                    // Strict HTTPS (for about ~6Months)
                    .putHeader("Strict-Transport-Security", "max-age=" + TRANSPORT_SECURITY_MAX_AGE)
                    // IE8+ do not allow opening of attachments in the context of this resource
                    .putHeader("X-Download-Options", "noopen")
                    // enable XSS for IE
                    .putHeader("X-XSS-Protection", "1; mode=block")
                    // deny frames
                    .putHeader("X-FRAME-OPTIONS", "DENY");
            ctx.next();
        });
    }

    /**
     * Set's the options for the http service.
     *
     * @throws NoInterfaceAvailableException When there is no interface
     * available to bind to.
     */
    private void setOpts() throws NoInterfaceAvailableException {
        final String hostIp;
        final NetworkService networkService = ServiceHandler.getInstance().getService(NetworkService.class).get();
        if (SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "http.ip", "network.ip").equals("network.ip")) {
            hostIp = networkService.getInterfaceInUse().getIpAddressAsString();
        } else {
            hostIp = SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "http.ip", networkService.getInterfaceInUse().getIpAddressAsString());
        }

        httpServerOptions = new HttpServerOptions()
                .setUseAlpn(true)
                .setSsl(true)
                .setPort(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "http.port", HTTP_PORT))
                .setHost(hostIp)
                .setLogActivity(true)
                .setPfxKeyCertOptions(
                        new PfxOptions()
                                .setPath(
                                        SystemConfig.getResourcePath(SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "server.keystore", ""))
                                )
                                .setPassword(
                                        SystemConfig.getSetting(SystemConfig.Type.SYSTEM, "server.certpass", "")
                                )
                );
    }

}
