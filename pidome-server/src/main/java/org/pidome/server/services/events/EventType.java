/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.events;

/**
 * The type of event occurred.
 *
 * @author johns
 */
public enum EventType {

    /**
     * An add event.
     */
    ADD,
    /**
     * A modification event.
     */
    MODIFY,
    /**
     * A deletion event.
     */
    DELETE,
    /**
     * Informative notification.
     */
    NOTIFY,
    /**
     * Loaded notification.
     */
    LOADED,
    /**
     * Unloaded notification.
     */
    UNLOADED;
}
