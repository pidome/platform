/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.zandero.rest.exception.ExceptionHandler;
import com.zandero.rest.exception.ExecuteException;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import javax.persistence.NoResultException;
import javax.persistence.OptimisticLockException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.presentation.input.InputFormException;
import org.pidome.tools.utilities.Serialization;

/**
 * Base class formatting the exposure of the exceptions thrown.
 *
 * @author John Sirach
 */
public class GenericExceptionHandler implements ExceptionHandler<Throwable> {

    /**
     * Logger for the auth rest api.
     */
    private static final Logger LOG = LogManager.getLogger(GenericExceptionHandler.class);

    /**
     * Writes the exception back to the response.
     *
     * This method also handles some generic global exceptions from for example
     * basic CRUD operations.
     *
     * @param t The exception thrown.
     * @param request The request done by a client.
     * @param response The response object used for providing the exception
     * response.
     * @throws Throwable When it is not possible to return to the requester or
     * write the exception to a log
     */
    @Override
    @SuppressFBWarnings(value = {"BC_UNCONFIRMED_CAST"})
    public void write(final Throwable t, final HttpServerRequest request, final HttpServerResponse response) throws Throwable {
        String exceptionResponse = exceptionFormatter(t, response.getStatusCode());
        if (!response.ended()) {
            if (t instanceof IllegalArgumentException) {
                response.setStatusCode(ApiResponseCode.HTTP_400.getResponseCode());
            } else if (t instanceof OptimisticLockException) {
                response.setStatusCode(ApiResponseCode.HTTP_409.getResponseCode());
            } else if (t instanceof NoResultException) {
                response.setStatusCode(ApiResponseCode.HTTP_404.getResponseCode());
            } else if (t instanceof ExecuteException) {
                ExecuteException execExcept = (ExecuteException) t;
                response.setStatusCode(execExcept.getStatusCode());
            } else {
                response.setStatusCode(ApiResponseCode.HTTP_500.getResponseCode());
            }
            response.end(exceptionResponse);
        } else {
            LOG.error("Unable to write an exception response to HTTP. Posting to log:\n", (Exception) t);
        }
    }

    /**
     * Formats an exception to representable format.
     *
     * @param thr The thrown exception.
     * @param statusCode The status code to report.
     * @return Representable exception string.
     */
    public static String exceptionFormatter(final Throwable thr, final int statusCode) {
        StringBuilder builder = new StringBuilder("{");
        builder.append("\"code\":").append(statusCode).append(",");
        builder.append("\"message\":\"").append(thr.getMessage().replaceAll("\"", "'").replace("\n", "")).append("\"");
        if (thr.getCause() != null && thr.getCause() instanceof InputFormException) {
            try {
                formErrorsBuilder(builder, (InputFormException) thr.getCause());
            } catch (JsonProcessingException ex) {
                LogManager.getLogger().error("Could not serialize supplied form errors, so here it is: [{}]", ((InputFormException) thr.getCause()).getInputErrors(), ex);
            }
        }
        //if (isDebug) {
        try {
            causeBuilder(builder, thr);
        } catch (JsonProcessingException ex) {
            LogManager.getLogger().error("Could not serialize stacktrace, so here it is: [{}]", thr, ex);
        }
        //}
        return builder.append("}").toString();
    }

    /**
     * Builds the form errors available in the form error exception.
     *
     * @param builder The stringbuilder.
     * @param ex The input form error exception.
     * @throws JsonProcessingException When parsing the error list fails.
     */
    private static void formErrorsBuilder(final StringBuilder builder, final InputFormException ex) throws JsonProcessingException {
        builder.append(",\"formErrors\":").append(Serialization.getDefaultObjectMapper().writeValueAsString(ex.getInputErrors()));
    }

    /**
     * Appends causes to the exception output.
     *
     * @param builder The builder to append the cause to.
     * @param cause The cause to be appended.
     * @return The builder.
     * @throws JsonProcessingException When building the cause fails.
     */
    private static StringBuilder causeBuilder(final StringBuilder builder, final Throwable cause) throws JsonProcessingException {
        if (cause.getCause() != null) {
            causeBuilder(builder, cause.getCause());
        } else {
            builder.append(",\"details\":").append(Serialization.getDefaultObjectMapper().writeValueAsString(cause));
        }
        return builder;
    }

}
