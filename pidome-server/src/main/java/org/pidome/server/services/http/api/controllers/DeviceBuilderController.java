/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.controllers;

import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.UUID;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import org.pidome.platform.modules.devices.ComposedDevice;
import org.pidome.platform.modules.devices.Device;
import static org.pidome.platform.modules.devices.DeviceBuilderType.PROVIDED;
import org.pidome.platform.modules.devices.discovery.DiscoveredDevice;
import org.pidome.platform.modules.items.Item;
import org.pidome.server.entities.items.DiscoveredItemDefinition;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.http.api.ApiControllerResource;
import org.pidome.server.services.http.api.response.ApiResponseCode;
import org.pidome.server.services.http.api.response.HttpStatusCodeException;
import org.pidome.server.services.installer.InstallerService;
import org.pidome.server.services.items.ItemService;
import org.pidome.server.services.modules.ModuleService;
import org.pidome.server.system.installer.packages.PackageClassLoaderException;
import org.pidome.server.system.installer.packages.PackageStore;
import org.pidome.server.system.modules.ActiveModuleContainer;
import org.pidome.server.system.modules.devices.DeviceDefinition;
import org.pidome.server.system.modules.devices.builder.DeviceBuilderConfiguration;

/**
 * Controller for the device builder.
 *
 * @author johns
 */
@Path("builder")
@Produces("application/json")
@Consumes("application/json")
@RolesAllowed("POWER")
@SuppressWarnings("CPD-START")
public class DeviceBuilderController extends ApiControllerResource {

    /**
     * Returns the list of controls for a device which has controls provided for
     * the builder.
     *
     * <p>
     * Returns a list of groups for devices providing the provided builder type.
     * with provided you are supplied with a set of groups and all possible
     * controls usable within that specific group.
     * <p>
     * It is like receiving a full device without the address and other
     * properties like name and description. With this you should be able to
     * visualize the controls and show which options are set for these.
     * <p>
     * The controls provided are not free build with the provided type. You
     * can't defer from the provided controls as these would not be supported by
     * the requested device. For completely free build of a device where these
     * are 100% configurable it should provide the USER build type.
     *
     * @param packageId The id of the package where this device is present.
     * @param deviceClass Full path to a device class for which to build.
     * @return List of device control groups for the builder.
     * @throws HttpStatusCodeException On failure of retrieval of a device.
     * @todo make it return a generic builder configuration.
     */
    @GET
    @Path("/packages/{package}/devices/{device}")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "Succesfully returns the groups for the given device."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the device is not available"),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_400, condition = "When the device does not support building")
    })
    public DeviceBuilderConfiguration getProvidedBuilder(final @PathParam("package") UUID packageId, final @PathParam("device") String deviceClass) throws HttpStatusCodeException {
        DeviceBuilderConfiguration configuration = getInitialConfigurationFilledByDevice(packageId, deviceClass);
        return configuration;
    }

    /**
     * Returns a builder based on the discovery id.
     *
     * This method is able to return any builder type. so it's important to
     * check which one is returned as each one has different requirements and
     * features.
     *
     * @param discoveredId The id of the discovered device.
     * @return A builder configuration.
     * @throws HttpStatusCodeException On failure of retrieving the builder
     * configuration of the given discovered device id.
     */
    @GET
    @Path("/discovery/devices/{discoveredId}")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "Succesfully returns the device builder configuration."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_400, condition = "When the device does not support building"),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the device is not available"),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_409, condition = "When the found item is not of type DEVICE"),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_500, condition = "On server error")
    })
    public DeviceBuilderConfiguration getBuilderFromDiscovered(final @PathParam("discoveredId") UUID discoveredId) throws HttpStatusCodeException {
        try {
            DiscoveredItemDefinition definition = getItemService().getDiscoveredItem(discoveredId);
            if (definition.getType().equals(Item.ItemType.DEVICE)) {
                DiscoveredDevice device = (DiscoveredDevice) definition.getDiscoveredItem();

                DeviceBuilderConfiguration configuration = getInitialConfigurationFilledByDevice(definition.getPackageId(), device.getDevice().getCanonicalName());
                configuration.setDeviceClass(device.getDevice().getCanonicalName());
                configuration.setPackageId(definition.getPackageId());
                configuration.setDeviceContainerId(definition.getContainerId());
                configuration.setDeviceAddress(device.getAddress());
                configuration.setDeviceParameters(device.getParameters());
                return configuration;
            } else {
                throw new HttpStatusCodeException(ApiResponseCode.HTTP_409, "Requested resource is not of type DEVICE");
            }
        } catch (NoSuchElementException ex) {
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_404, ex);
        }
    }

    /**
     * Creates a device definition to be used as a device on the server.
     *
     * @param definition The definition to build a device.
     * @return The definition UUID
     * @throws HttpStatusCodeException When saving the configuration fails.
     */
    @POST
    @Path("/create")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_201, condition = "Succesfully created the device definition and returns the definition UUID"),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_400, condition = "When the device does not support building"),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the device is not available"),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_409, condition = "When the found item is not of type DEVICE"),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_500, condition = "On server error")
    })
    public UUID createDeviceDefinition(final DeviceDefinition definition) throws HttpStatusCodeException {

        ModuleService service = ServiceHandler.getInstance().getService(ModuleService.class)
                .orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_503, "Module service not available"));
        ActiveModuleContainer container = service
                .getActiveModuleContainer(definition.getContainerId())
                .orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_500, "Module/Container not available"));

        final DeviceBuilderConfiguration baseConfig = getInitialConfigurationFilledByDevice(
                definition.getPackageId(),
                definition.getItemClass()
        );

        definition.setBuilderType(baseConfig.getBuilderType());
        definition.setModuleDefinition(
                container.getModuleDefinition()
        );
        definition.save();

        return definition.getId();
    }

    /**
     * Returns a device instance of the given type.
     *
     * @param packageId The package id.
     * @param deviceClass The device class.
     * @return An instance of the given device.
     * @throws HttpStatusCodeException When loading fails.
     */
    @SuppressWarnings({"unchecked"})
    private DeviceBuilderConfiguration getInitialConfigurationFilledByDevice(final UUID packageId,
            final String deviceClass) throws HttpStatusCodeException {
        try (PackageStore.UnboundPackageLoad<? extends Device> loader = new PackageStore.UnboundPackageLoad<>(getInstallerService().getPackageById(packageId), deviceClass)) {
            Device device = loader.get();
            if (!device.getDeviceBuilderType().getSupportsCustomBuild()) {
                throw new HttpStatusCodeException(ApiResponseCode.HTTP_409, "Builder type " + device.getDeviceBuilderType() + " is not of a type which supports a custom build.");
            }
            Device.DeviceStructureBuilder builder = new Device.DeviceStructureBuilder();
            DeviceBuilderConfiguration configuration = new DeviceBuilderConfiguration();
            configuration.setBuilderType(device.getDeviceBuilderType());
            configuration.setDeviceParameters(device.getParameters());
            switch (device.getDeviceBuilderType()) {
                case PROVIDED:
                    ((ComposedDevice) device).getProvidedControls(builder);
                    configuration.setGroups(builder.build());
                    break;
                case USER:
                    device.getAddress().setAddress(null); // Unset the address, not interesting during configuration.
                    configuration.setDeviceAddress(device.getAddress());
                    break;
                default:
                    throw new HttpStatusCodeException(ApiResponseCode.HTTP_409, "Unsupported builder type " + device.getDeviceBuilderType());
            }
            return configuration;
        } catch (IOException | PackageClassLoaderException ex) {
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_500, ex);
        }
    }

    /**
     * Returns the item service.
     *
     * @return The item service.
     * @throws HttpStatusCodeException When the item service is not available.
     */
    private ItemService getItemService() throws HttpStatusCodeException {
        return getService(ItemService.class)
                .orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_503));
    }

    /**
     * Returns the installer service.
     *
     * @return The installer service.
     * @throws HttpStatusCodeException When the installer service is not
     * available.
     */
    private InstallerService getInstallerService() throws HttpStatusCodeException {
        return getService(InstallerService.class)
                .orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_503));
    }

}
