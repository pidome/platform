/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.authentication;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.PubSecKeyOptions;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
import io.vertx.ext.jwt.JWTOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.JWTAuthHandler;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.bouncycastle.openssl.jcajce.JcaPKCS8Generator;
import org.bouncycastle.util.io.pem.PemObject;
import org.pidome.server.entities.users.UserLogin;
import org.pidome.server.services.AbstractService;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.http.api.auth.LoginObject;
import org.pidome.server.services.network.NetworkService;
import org.pidome.server.services.network.NoInterfaceAvailableException;
import org.pidome.server.system.database.Database;
import org.pidome.server.system.security.CertificateStore;

/**
 * Service responsible for authentication services.
 *
 * @author John Sirach
 */
@SuppressWarnings("CPD-START")
public class AuthenticationService extends AbstractService {

    /**
     * Logger for the auth rest api.
     */
    private static final Logger LOG = LogManager.getLogger(AuthenticationService.class);

    /**
     * Local cache of tokens.
     */
    private final transient Set<String> tokenCache = new HashSet<>();

    /**
     * The authentication handler.
     */
    private transient PidomeAuthProvider jwtAauthProvider;

    /**
     * the jwt auth options.
     */
    private transient JWTOptions jwtAuthOptions;

    /**
     * The token algorithm.
     */
    private static final String JWT_ALGORITHM = "RS256";

    /**
     * UID string literal.
     */
    protected static final String UID = "uid";

    /**
     * Token originating info string literal.
     */
    private static final String TOKEN_ORIGINATING_INFO = "tokenOriginatingInfo";

    /**
     * Position in the route to put the JWT auth handler.
     */
    private static final int AUTH_ROUTE_ORDER = -9;

    /**
     * Authenticates using a username and password.
     *
     * Authenticates the given username and password and if correct returns a
     * token.
     *
     * @param loginObject Object used for authentification of an user.
     * @param resultHandler The result of the authentication, when succesful
     * includes a token.
     */
    public void authenticate(final LoginObject loginObject, final Handler<AsyncResult<PidomeAuthToken>> resultHandler) {
        if (loginObject.getUsername() == null || loginObject.getUsername().isBlank()) {
            resultHandler.handle(Future.failedFuture("Username is missing"));
            return;
        }
        if (loginObject.getPassword() == null || loginObject.getPassword().isBlank()) {
            resultHandler.handle(Future.failedFuture("Password is missing"));
            return;
        }
        try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            TypedQuery<UserLogin> query = autoManager.getManager().createNamedQuery("FROM_USERLOGIN", UserLogin.class);
            query.setParameter("userName", loginObject.getUsername());
            UserLogin login = query.getSingleResult();
            if (login.passwordEquals(loginObject.getPassword())) {
                login.setLastLogin(new Date());
                final PidomeAuthToken pidomeToken = new PidomeAuthToken();
                pidomeToken.setToken(
                        this.jwtAauthProvider.generateToken(
                                new JsonObject()
                                        .put(UID, login.getId())
                                        .put("unq", UUID.randomUUID().toString()),
                                jwtAuthOptions
                                        .setPermissions(Arrays.asList(login.getRole().name()))
                        )
                );
                pidomeToken.setUid(login.getId());
                pidomeToken.setType("bearer");
                pidomeToken.setTokenOriginatingInfo(loginObject.getLoginSource());
                try {
                    if (pidomeToken.getToken() == null || pidomeToken.getToken().isBlank()) {
                        throw new Exception("Token not generated during login");
                    }
                    autoManager.getManager().getTransaction().begin();
                    autoManager.getManager().persist(loginObject.getLoginSource());
                    autoManager.getManager().persist(pidomeToken);
                    autoManager.getManager().merge(login);
                    autoManager.getManager().getTransaction().commit();
                    tokenCache.add(pidomeToken.getToken());
                    resultHandler.handle(Future.succeededFuture(pidomeToken));
                } catch (Exception ex) {
                    LOG.warn("Unable to persist login information for user [{}]: [{}]", loginObject.getUsername(), ex.getMessage());
                    resultHandler.handle(Future.failedFuture("Invalid login"));
                }
            } else {
                LOG.warn("Password does not match for [{}]", loginObject.getUsername());
                resultHandler.handle(Future.failedFuture("Invalid login"));
            }
        } catch (NoResultException ex) {
            LOG.warn("User [{}] not found or error [{}]", loginObject.getUsername(), ex.getMessage());
            resultHandler.handle(Future.failedFuture("Invalid login"));
        }
    }

    /**
     * Checks if a token has not been revoked on the server.
     *
     * @param token The token to look for.
     * @return Returns true when a token has been revoked.
     */
    public final boolean isTokenRevoked(final String token) {
        if (tokenCache.contains(token)) {
            return false;
        } else {
            try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {

                CriteriaBuilder builder = autoManager.getManager().getCriteriaBuilder();
                CriteriaQuery<PidomeAuthToken> criteria = builder.createQuery(PidomeAuthToken.class);
                Root<PidomeAuthToken> from = criteria.from(PidomeAuthToken.class);

                criteria.where(builder.equal(from.get("token"), token));

                PidomeAuthToken pidomeToken = autoManager.getManager().createQuery(criteria.select(from)).getSingleResult();
                tokenCache.add(pidomeToken.getToken());
                return false;
            } catch (NoResultException ex) {
                return true;
            }
        }
    }

    /**
     * Revokes a token.
     *
     * @param token The token to revoke.
     */
    public void revokeToken(final String token) {
        this.tokenCache.remove(token);
        try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {

            CriteriaBuilder builder = autoManager.getManager().getCriteriaBuilder();
            CriteriaDelete<PidomeAuthToken> criteria = builder.createCriteriaDelete(PidomeAuthToken.class);
            Root<PidomeAuthToken> from = criteria.from(PidomeAuthToken.class);
            criteria.where(builder.equal(from.get("token"), token));

            autoManager.getManager().getTransaction().begin();
            autoManager.getManager().createQuery(criteria).executeUpdate();
            autoManager.getManager().getTransaction().commit();

        } catch (Exception ex) {
            LOG.error("Problem deleting token", ex);
            throw ex;
        }
    }

    /**
     * Returns a list of token references.
     *
     * @param user The user to gather the references for.
     * @return List of token references.
     * @throws java.lang.Exception When an error occurs during references
     * retrieval.
     */
    public List<PidomeAuthTokenReference> getTokenReferences(final PidomeAuthUser user) throws Exception {
        try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {

            CriteriaBuilder builder = autoManager.getManager().getCriteriaBuilder();
            CriteriaQuery<PidomeAuthTokenReference> criteriaQuery = builder.createQuery(PidomeAuthTokenReference.class);
            Root<PidomeAuthToken> fromRoot = criteriaQuery.from(PidomeAuthToken.class);
            Root<PidomeAuthTokenReference> joinRoot = criteriaQuery.from(PidomeAuthTokenReference.class);

            Join<PidomeAuthTokenReference, PidomeAuthToken> joinToken = fromRoot.join(TOKEN_ORIGINATING_INFO, JoinType.INNER);
            joinToken.on(
                    builder.equal(fromRoot.get(TOKEN_ORIGINATING_INFO), joinRoot.get("id"))
            );

            criteriaQuery.select(joinRoot);

            criteriaQuery.where(builder.equal(fromRoot.get(UID), user.getUid()));

            return autoManager.getManager()
                    .createQuery(criteriaQuery)
                    .getResultList();

        } catch (Exception ex) {
            LOG.error("Problem gathering token references", ex);
            throw ex;
        }
    }

    /**
     * Returns a token references.
     *
     * @param user The user to gather the references for.
     * @param referenceId The reference id.
     * @return Token references.
     * @throws java.lang.Exception When an error occurs during reference
     * retrieval.
     */
    public final PidomeAuthTokenReference getTokenReference(final PidomeAuthUser user, final UUID referenceId) throws Exception {
        try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {

            CriteriaBuilder builder = autoManager.getManager().getCriteriaBuilder();
            CriteriaQuery<PidomeAuthTokenReference> criteriaQuery = builder.createQuery(PidomeAuthTokenReference.class);
            Root<PidomeAuthToken> fromRoot = criteriaQuery.from(PidomeAuthToken.class);
            Root<PidomeAuthTokenReference> joinRoot = criteriaQuery.from(PidomeAuthTokenReference.class);

            Join<PidomeAuthTokenReference, PidomeAuthToken> joinToken = fromRoot.join(TOKEN_ORIGINATING_INFO, JoinType.INNER);
            joinToken.on(
                    builder.equal(fromRoot.get(TOKEN_ORIGINATING_INFO), joinRoot.get("id"))
            );

            criteriaQuery.select(joinRoot);

            criteriaQuery.where(
                    builder.and(
                            builder.equal(fromRoot.get(UID), user.getUid()),
                            builder.equal(joinRoot.get("id"), referenceId)
                    )
            );

            return autoManager.getManager()
                    .createQuery(criteriaQuery)
                    .getSingleResult();

        } catch (Exception ex) {
            LOG.error("Problem gathering token reference", ex);
            throw ex;
        }
    }

    /**
     * The authuser of the token.
     *
     * @param token the token to decode.
     * @return The auth user of the decoded token.
     */
    public final PidomeAuthUser getUser(final String token) {
        if (this.jwtAvailable()) {
            return this.jwtAauthProvider.decodeToken(token);
        }
        return null;
    }

    /**
     * Returns a token by it's given reference id.
     *
     * @param user The user to gather the references for.
     * @param referenceId The reference id.
     * @return The token.
     * @throws java.lang.Exception When an error occurs during reference
     * retrieval.
     */
    public final PidomeAuthToken getTokenByReferenceId(final PidomeAuthUser user, final UUID referenceId) throws Exception {
        try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {

            CriteriaBuilder builder = autoManager.getManager().getCriteriaBuilder();
            CriteriaQuery<PidomeAuthToken> criteriaQuery = builder.createQuery(PidomeAuthToken.class);
            Root<PidomeAuthToken> fromRoot = criteriaQuery.from(PidomeAuthToken.class);
            Root<PidomeAuthTokenReference> joinRoot = criteriaQuery.from(PidomeAuthTokenReference.class);

            Join<PidomeAuthTokenReference, PidomeAuthToken> joinToken = fromRoot.join(TOKEN_ORIGINATING_INFO, JoinType.INNER);
            joinToken.on(
                    builder.equal(fromRoot.get(TOKEN_ORIGINATING_INFO), joinRoot.get("id"))
            );

            criteriaQuery.select(fromRoot);

            criteriaQuery.where(
                    builder.and(
                            builder.equal(fromRoot.get(UID), user.getUid()),
                            builder.equal(joinRoot.get("id"), referenceId)
                    )
            );

            return autoManager.getManager()
                    .createQuery(criteriaQuery)
                    .getSingleResult();

        } catch (Exception ex) {
            LOG.error("Problem gathering token reference", ex);
            throw ex;
        }
    }

    /**
     * Revokes a token by it's reference id.
     *
     * @param user The user to gather the references for.
     * @param referenceId The reference id.
     * @throws Exception Thrown when an error occurs during retrieval by
     * reference or deletion.
     */
    public void revokeTokenByReference(final PidomeAuthUser user, final UUID referenceId) throws Exception {
        revokeToken(
                getTokenByReferenceId(user, referenceId).getToken()
        );
    }

    /**
     * Adds the token service to the given route.
     *
     * @param router The router to add the auth to.
     * @param exludePath The start path to be excluded from.
     */
    public void addToRoute(final Router router, final String exludePath) {
        if (jwtAvailable()) {
            LOG.info("Applying authentication provider [{}] with authentication endpoint [{}]", this.jwtAauthProvider, exludePath);
            JWTAuthHandler auth = JWTAuthHandler.create(this.jwtAauthProvider, exludePath);
            router.route().order(AUTH_ROUTE_ORDER).handler(auth);
        }
    }

    /**
     * Returns if JWT key functionlity is available.
     *
     * @return if available.
     */
    private boolean jwtAvailable() {
        return this.jwtAauthProvider != null && this.jwtAuthOptions != null;
    }

    /**
     * Initializes the token service.
     */
    private void setupTokenService() {
        LOG.info("Setting up JWT security token implementation");
        try (FileInputStream is = new FileInputStream(System.getProperty("javax.net.ssl.keyStore"))) {

            KeyStore keystore = KeyStore.getInstance(CertificateStore.KEYSTORE_TYPE);
            keystore.load(is, System.getProperty("javax.net.ssl.keyStorePassword").toCharArray());

            Key key = keystore.getKey(CertificateStore.PRIMARY_ALIAS, System.getProperty("javax.net.ssl.keyStorePassword").toCharArray());
            if (key instanceof PrivateKey) {
                Certificate cert = keystore.getCertificate(CertificateStore.PRIMARY_ALIAS);
                PublicKey publicKey = cert.getPublicKey();

                final NetworkService networkService = ServiceHandler.getInstance().getService(NetworkService.class).get();

                jwtAuthOptions = new JWTOptions()
                        .setIssuer(networkService.getInterfaceInUse().getIpAddressAsString())
                        .setAlgorithm(JWT_ALGORITHM)
                        .setSubject("PiDome API")
                        .setNoTimestamp(false)
                        .setAudience(Arrays.asList("human"));

                JcaPKCS8Generator gen1 = new JcaPKCS8Generator((PrivateKey) key, null);
                PemObject obj1 = gen1.generate();
                try (StringWriter sw1 = new StringWriter(); JcaPEMWriter pw = new JcaPEMWriter(sw1)) {
                    pw.writeObject(obj1);
                    pw.flush();
                    String pkcs8Key1 = sw1.toString();

                    JWTAuthOptions config = new JWTAuthOptions()
                            .addPubSecKey(new PubSecKeyOptions()
                                    .setAlgorithm(JWT_ALGORITHM)
                                    .setPublicKey(Base64.getEncoder().encodeToString(publicKey.getEncoded()))
                                    .setSecretKey(pkcs8Key1.replace("-----BEGIN PRIVATE KEY-----", "").replace("-----END PRIVATE KEY-----", ""))
                            )
                            .setJWTOptions(jwtAuthOptions);

                    JWTAuth.create(vertx, config);
                    jwtAauthProvider = new PidomeAuthProvider(config);
                }
            }

            try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {

                CriteriaBuilder builder = autoManager.getManager().getCriteriaBuilder();
                CriteriaQuery<PidomeAuthToken> criteria = builder.createQuery(PidomeAuthToken.class);
                Root<PidomeAuthToken> from = criteria.from(PidomeAuthToken.class);
                CriteriaQuery<PidomeAuthToken> select = criteria.select(from);

                ((List<PidomeAuthToken>) autoManager.getManager().createQuery(select).getResultList()).forEach(token -> {
                    this.tokenCache.add(token.getToken());
                });
            }

        } catch (NoInterfaceAvailableException | IOException | KeyStoreException | NoSuchAlgorithmException | CertificateException | UnrecoverableKeyException ex) {
            LOG.error("Unable to setup JWT tokens, server API will be unsecured", ex);
        }
    }

    /**
     * Trims the bearer from a web header.
     *
     * @param webAuthHeader The web header.
     * @return Plain token.
     */
    public static final String trimWebBearerToken(final String webAuthHeader) {
        return webAuthHeader.replace("Bearer ", "").trim();
    }

    /**
     * Starts the service.
     *
     * @param startPromise The future to signal when start has completed.
     */
    @Override
    public void start(final Promise<Void> startPromise) {
        setupTokenService();
        startPromise.complete();
    }

    /**
     * Stops the service.
     *
     * @param stopPromise The future to signal when a stop has been completed.
     */
    @Override
    public void stop(final Promise<Void> stopPromise) {
        stopPromise.complete();
    }

}
