/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.items;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import java.util.UUID;

/**
 * An action for an item.
 *
 * @author johns
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.EXISTING_PROPERTY,
        property = "type",
        visible = true)
@JsonSubTypes({
    @JsonSubTypes.Type(value = BaseItemCommand.class, name = "COMMAND"),
    @JsonSubTypes.Type(value = BaseItemMethod.class, name = "METHOD")
})
@SuppressWarnings("PMD.AbstractClassWithoutAbstractMethod")
public abstract class ItemAction {

    /**
     * The action type.
     */
    private ItemActionType type;

    /**
     * The id of the item for whom it may concern.
     */
    private UUID itemId;

    /**
     * The type of action for an item.
     */
    public enum ItemActionType {
        /**
         * The items is required to perform a command.
         *
         * A command is an action which performs an action which is to be
         * interpreted by a user. Play a media file, turn a switch.
         */
        COMMAND,
        /**
         * The item is required to execute a method.
         *
         * A method is not an action which results in a action performed by an
         * item. A method can update a view, store settings, etc..
         */
        METHOD
    }

    /**
     * Returns the item action type.
     *
     * @return The action type.
     */
    public ItemActionType getType() {
        return this.type;
    }

    /**
     * Sets the item type.
     *
     * @param type The type to set.
     */
    public void setType(final ItemActionType type) {
        this.type = type;
    }

    /**
     * The id of the item for whom it may concern.
     *
     * @return the itemId
     */
    public UUID getItemId() {
        return itemId;
    }

    /**
     * The id of the item for whom it may concern.
     *
     * @param itemId the itemId to set
     */
    public void setItemId(final UUID itemId) {
        this.itemId = itemId;
    }

}
