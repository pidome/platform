/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.security;

import io.vertx.core.Promise;
import org.pidome.server.services.AbstractService;
import org.pidome.server.system.security.CertificateStore;

/**
 * Service which handles security in general.
 *
 * @author johns
 */
public class SecurityService extends AbstractService {

    /**
     * Starts the security service.
     *
     * @param startPromise Promise to determine when it is completed
     */
    @Override
    public void start(final Promise<Void> startPromise) {
        CertificateStore.getInstance();
        startPromise.complete();
    }

    /**
     * Stops the security service.
     *
     * @param stopPromise Promise to determine when it is completed
     */
    @Override
    public void stop(final Promise<Void> stopPromise) {
        stopPromise.complete();
    }

}
