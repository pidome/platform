/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.items;

import java.util.List;
import org.pidome.platform.modules.items.Item;
import org.pidome.platform.modules.items.ItemDataNotification;

/**
 * The hardware event.
 *
 * @author johns
 */
public class ItemEventBody {

    /**
     * The item event type.
     */
    public enum ItemEventType {
        /**
         * Item(s) added.
         */
        ADDED,
        /**
         * Item(s) removed.
         */
        REMOVED,
        /**
         * Item(s) updated.
         */
        UPDATED,
        /**
         * Item(s) discovered.
         */
        DISCOVERED

    }

    /**
     * The peripheral in the event.
     */
    private List<Item> items;

    /**
     * the item event type.
     */
    private ItemEventType type;

    /**
     * Item data notification.
     */
    private ItemDataNotification dataNotification;

    /**
     * @return the items
     */
    public List<Item> getItems() {
        return items;
    }

    /**
     * the item event type.
     *
     * @return the type
     */
    public ItemEventType getType() {
        return type;
    }

    /**
     * the item event type.
     *
     * @param type the type to set
     */
    public void setType(final ItemEventType type) {
        this.type = type;
    }

    /**
     * @param items the items to set
     */
    public void setItems(final List<Item> items) {
        this.items = items;
    }

    /**
     * Sets the data notification for items.
     *
     * @param dataNotification the notification.
     */
    public void setDataNotification(final ItemDataNotification dataNotification) {
        this.dataNotification = dataNotification;
    }

    /**
     * Returns the item data notification.
     *
     * @return The data notification.
     */
    public ItemDataNotification getDataNotification() {
        return this.dataNotification;
    }

}
