/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.premises;

import io.vertx.core.Promise;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.presentation.icon.IconType;
import org.pidome.server.entities.premises.Premises;
import org.pidome.server.entities.premises.PremisesIcon;
import org.pidome.server.entities.premises.PremisesSection;
import org.pidome.server.entities.premises.Property;
import org.pidome.server.entities.premises.PropertyLevel;
import org.pidome.server.entities.premises.PropertySection;
import org.pidome.server.services.AbstractService;
import org.pidome.server.system.database.Database;
import org.pidome.server.system.database.Database.AutoClosableEntityManager;
import org.pidome.server.system.database.DatabaseUtils;

/**
 * Premises service.
 *
 * @author John Sirach
 */
@SuppressWarnings("CPD-START")
public final class PremisesService extends AbstractService {

    /**
     * Logger for the auth rest api.
     */
    private static final Logger LOG = LogManager.getLogger(PremisesService.class);

    /**
     * Parameter used for properties collection.
     */
    private static final String PROPERTIES_PARAMETER = "properties";

    /**
     * Parameter used for property levels.
     */
    private static final String PROPERTYLEVEL_PARAMETER = "propertyLevels";

    /**
     * Parameter used for sections.
     */
    private static final String SECTIONS_PARAMETER = "sections";

    /**
     * Parameter used for ids.
     */
    private static final String ID_PARAMETER = "id";

    /**
     * Returns all premises levels.
     *
     * @return All levels known on the premises.
     */
    public List<Premises> getPremises() {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {

            CriteriaQuery<Premises> criteriaQuery = autoManager.getManager().getCriteriaBuilder().createQuery(Premises.class);
            Root<Premises> from = criteriaQuery.from(Premises.class);
            CriteriaQuery<Premises> select = criteriaQuery.select(from);

            TypedQuery<Premises> typedQuery = autoManager.getManager().createQuery(select);
            return typedQuery.getResultList();
        }
    }

    /**
     * Returns a premises based on the given id.
     *
     * @param id The id of the premises to etch.
     * @return the selected premises.
     */
    public Premises getPremises(final UUID id) {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            return autoManager.getManager().find(Premises.class, id);
        }
    }

    /**
     * Adds a premises.
     *
     * @param premises The premises to add.
     * @return The newly persisted premises.
     */
    public Premises addPremises(final Premises premises) {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            autoManager.getManager().getTransaction().begin();
            autoManager.getManager().persist(premises);
            autoManager.getManager().flush();
            autoManager.getManager().getTransaction().commit();
            return premises;
        }
    }

    /**
     * Patches a premises.
     *
     * The patching does not alter the property assignments.
     *
     * @param premises the premises to update.
     */
    public void patchPremises(final Premises premises) {
        Premises original = getPremises(premises.getId());
        original.setLocation(premises.getLocation());
        original.setName(premises.getName());
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            autoManager.getManager().getTransaction().begin();
            autoManager.getManager().merge(original);
            autoManager.getManager().getTransaction().commit();
        }
    }

    /**
     * Deletes a premises from the system.
     *
     * This method is recursive and deletes all properties on the premises.
     *
     * @param premisesId The premises id to delete.
     */
    public void deletePremises(final UUID premisesId) {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            autoManager.getManager().getTransaction().begin();
            autoManager.getManager().remove(autoManager.getManager().find(Premises.class, premisesId));
            autoManager.getManager().getTransaction().commit();
        }
    }

    /**
     * Returns a list of premises sections.
     *
     * @param premisesId The id of the premises to gather the sections from.
     * @return List of premises sections.
     */
    public List<PremisesSection> getPremisesSections(final UUID premisesId) {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {

            CriteriaBuilder builder = autoManager.getManager().getCriteriaBuilder();
            CriteriaQuery<PremisesSection> criteriaQuery = builder.createQuery(PremisesSection.class);
            Root<Premises> fromPremises = criteriaQuery.from(Premises.class);
            Root<PremisesSection> joinSections = criteriaQuery.from(PremisesSection.class);

            fromPremises.joinSet(SECTIONS_PARAMETER, JoinType.INNER);

            criteriaQuery.select(joinSections);
            criteriaQuery.where(builder.equal(fromPremises.get(ID_PARAMETER), premisesId));

            return autoManager.getManager()
                    .createQuery(criteriaQuery)
                    .getResultList();

        } catch (Exception ex) {
            LOG.error("Problem gathering token references", ex);
            throw ex;
        }
    }

    /**
     * Return a single premises section.
     *
     * @param premisesId The id of the premises to retrieve.
     * @param sectionId The id of the section to retrieve.
     * @return the requested section.
     */
    public PremisesSection getPremisesSection(final UUID premisesId, final UUID sectionId) {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {

            CriteriaBuilder builder = autoManager.getManager().getCriteriaBuilder();
            CriteriaQuery<PremisesSection> criteriaQuery = builder.createQuery(PremisesSection.class);
            Root<Premises> fromPremises = criteriaQuery.from(Premises.class);
            Root<PremisesSection> joinSections = criteriaQuery.from(PremisesSection.class);

            fromPremises.joinSet(SECTIONS_PARAMETER, JoinType.INNER);

            criteriaQuery.select(joinSections);
            criteriaQuery.where(
                    builder.and(
                            builder.equal(fromPremises.get(ID_PARAMETER), premisesId),
                            builder.equal(joinSections.get(ID_PARAMETER), sectionId)
                    )
            );

            return autoManager.getManager().createQuery(criteriaQuery)
                    .getSingleResult();
        }
    }

    /**
     * Adds a section to the given premises identified by the given id.
     *
     * @param premisesId The premises id.
     * @param section The section to add.
     * @return The added premises section.
     */
    public PremisesSection addPremisesSection(final UUID premisesId, final PremisesSection section) {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            EntityManager manager = autoManager.getManager();
            Premises premises = manager.find(Premises.class, premisesId);
            manager.getTransaction().begin();

            manager.persist(section);
            premises.getSections().add(section);
            manager.merge(premises);

            manager.getTransaction().commit();

            return section;

        }
    }

    /**
     * A shallow update of a section object in the given premises.
     *
     * @param premisesId The id of the premises where the to be patched object
     * is present.
     * @param section The premises section to be patched.
     */
    public void patchPremisesSection(final UUID premisesId, final PremisesSection section) {
        final PremisesSection original = this.getPremisesSection(premisesId, section.getId());
        original.setName(section.getName());
        original.setIcon(section.getIcon());
        original.setPosX(section.getPosX());
        original.setPosY(section.getPosY());
        original.setIcon(section.getIcon());
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            autoManager.getManager().getTransaction().begin();
            autoManager.getManager().merge(original);
            autoManager.getManager().getTransaction().commit();
        }
    }

    /**
     * Deletes a section from the system.
     *
     * @param premisesId The premises id to delete the section on.
     * @param sectionId The id of the section to delete.
     */
    public void deletePremisesSection(final UUID premisesId, final UUID sectionId) {
        this.getPremisesSection(premisesId, sectionId);
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            autoManager.getManager().getTransaction().begin();
            autoManager.getManager().remove(autoManager.getManager().find(PremisesSection.class, sectionId));
            autoManager.getManager().getTransaction().commit();
        }
    }

    /**
     * Returns a list of properties on a premises.
     *
     * @param premisesId The premises to get the properties from.
     * @return List of properties on the premises.
     */
    public List<Property> getProperties(final UUID premisesId) {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {

            CriteriaBuilder builder = autoManager.getManager().getCriteriaBuilder();
            CriteriaQuery<Property> criteriaQuery = builder.createQuery(Property.class);
            Root<Premises> fromRoot = criteriaQuery.from(Premises.class);
            Root<Property> joinRoot = criteriaQuery.from(Property.class);

            fromRoot.joinSet(PROPERTIES_PARAMETER, JoinType.INNER);

            criteriaQuery.select(joinRoot);
            criteriaQuery.where(builder.equal(fromRoot.get(ID_PARAMETER), premisesId));

            return autoManager.getManager().createQuery(criteriaQuery)
                    .getResultList();
        }
    }

    /**
     * Return a property on a given premises.
     *
     * @param premisesId id of the premises.
     * @param propertyId id of the property.
     * @return The property when existing on the given premises.
     */
    public Property getProperty(final UUID premisesId, final UUID propertyId) {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {

            CriteriaBuilder builder = autoManager.getManager().getCriteriaBuilder();
            CriteriaQuery<Property> criteriaQuery = builder.createQuery(Property.class);
            Root<Premises> fromPremises = criteriaQuery.from(Premises.class);
            Root<Property> fromProperty = criteriaQuery.from(Property.class);

            fromPremises.joinSet(PROPERTIES_PARAMETER, JoinType.LEFT);

            criteriaQuery.select(fromProperty);
            criteriaQuery.where(
                    builder.and(
                            builder.equal(fromPremises.get(ID_PARAMETER), premisesId),
                            builder.equal(fromProperty.get(ID_PARAMETER), propertyId)
                    )
            );

            return autoManager.getManager().createQuery(criteriaQuery)
                    .getSingleResult();
        }
    }

    /**
     * Adds a property to the given premises identified by the given id.
     *
     * @param premisesId The premises id.
     * @param property The property to add.
     * @return The added property on success.
     */
    public Property addProperty(final UUID premisesId, final Property property) {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            EntityManager manager = autoManager.getManager();
            Premises premises = manager.find(Premises.class, premisesId);
            manager.getTransaction().begin();

            manager.persist(property);
            premises.getProperties().add(property);
            manager.merge(premises);

            manager.getTransaction().commit();

            return property;

        }
    }

    /**
     * A shallow update of a property object in the given premises.
     *
     * This method does not modify the level collection in the property.
     *
     * @param premisesId The id of the premises where the to be patched object
     * is present.
     * @param property The property to be patched.
     */
    public void patchProperty(final UUID premisesId, final Property property) {
        final Property original = this.getProperty(premisesId, property.getId());
        original.setName(property.getName());
        original.setIcon(property.getIcon());
        original.setLocation(property.getLocation());
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            autoManager.getManager().getTransaction().begin();
            autoManager.getManager().merge(original);
            autoManager.getManager().getTransaction().commit();
        }
    }

    /**
     * Deletes a property from the system.
     *
     * This method is recursive and deletes all property levels and sections.
     *
     * @param premisesId The premises id to delete.
     * @param propertyId The id of the property to delete.
     */
    public void deleteProperty(final UUID premisesId, final UUID propertyId) {
        this.getProperty(premisesId, propertyId);
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            autoManager.getManager().getTransaction().begin();
            autoManager.getManager().remove(autoManager.getManager().find(Property.class, propertyId));
            autoManager.getManager().getTransaction().commit();
        }
    }

    /**
     * Returns a list of property levels.
     *
     * @param premisesId The id of the property premises.
     * @param propertyId The id of property from which the levels should be
     * returned.
     * @return List of property levels when present, otherwise an empty list.
     */
    public List<PropertyLevel> getPropertyLevels(final UUID premisesId, final UUID propertyId) {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {

            CriteriaBuilder builder = autoManager.getManager().getCriteriaBuilder();
            CriteriaQuery<PropertyLevel> criteriaQuery = builder.createQuery(PropertyLevel.class);
            Root<Premises> premisesRoot = criteriaQuery.from(Premises.class);
            Root<Property> propertyRoot = criteriaQuery.from(Property.class);
            Root<PropertyLevel> propertyLevelRoot = criteriaQuery.from(PropertyLevel.class);

            premisesRoot.joinSet(PROPERTIES_PARAMETER, JoinType.INNER);
            propertyRoot.joinSet(PROPERTYLEVEL_PARAMETER, JoinType.INNER);

            criteriaQuery.select(propertyLevelRoot);
            criteriaQuery.where(
                    builder.and(
                            builder.equal(premisesRoot.get(ID_PARAMETER), premisesId),
                            builder.equal(propertyRoot.get(ID_PARAMETER), premisesId)
                    )
            );

            return autoManager.getManager().createQuery(criteriaQuery)
                    .getResultList();
        }
    }

    /**
     * Returns a single level on/in a property.
     *
     * @param premisesId The id of the premises containing the property on which
     * the level resides.
     * @param propertyId The id of the property on which the level resides.
     * @param levelId The id of the premises level to return.
     * @return A premises level.
     */
    @SuppressWarnings("PMD.PreserveStackTrace")
    public PropertyLevel getPropertyLevel(final UUID premisesId, final UUID propertyId, final UUID levelId) {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {

            CriteriaBuilder builder = autoManager.getManager().getCriteriaBuilder();
            CriteriaQuery<PropertyLevel> criteriaQuery = builder.createQuery(PropertyLevel.class);
            Root<Premises> premisesRoot = criteriaQuery.from(Premises.class);
            Root<Property> propertyRoot = criteriaQuery.from(Property.class);
            Root<PropertyLevel> levelRoot = criteriaQuery.from(PropertyLevel.class);

            premisesRoot.joinSet(PROPERTIES_PARAMETER, JoinType.INNER);
            propertyRoot.joinSet(PROPERTYLEVEL_PARAMETER, JoinType.INNER);

            criteriaQuery.select(levelRoot);

            criteriaQuery.where(
                    builder.and(
                            builder.equal(premisesRoot.get(ID_PARAMETER), premisesId),
                            builder.equal(propertyRoot.get(ID_PARAMETER), propertyId),
                            builder.equal(levelRoot.get(ID_PARAMETER), levelId)
                    )
            );

            return autoManager.getManager().createQuery(criteriaQuery)
                    .getSingleResult();
        }
    }

    /**
     * Adds a level on a property.
     *
     * @param premisesId The id of the premises.
     * @param propertyId The id of the property.
     * @param level The level to add to the property with the given id.
     * @return The added property level.
     */
    public PropertyLevel addPropertyLevel(final UUID premisesId, final UUID propertyId, final PropertyLevel level) {
        Property property = this.getProperty(premisesId, propertyId);
        property.getPropertyLevels().add(level);
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            autoManager.getManager().getTransaction().begin();
            autoManager.getManager().persist(level);
            autoManager.getManager().merge(property);
            autoManager.getManager().getTransaction().commit();

            return level;

        }
    }

    /**
     * Shallow update of a property level.
     *
     * This does not modify the sections on a level. Use the appropriate method
     * for this.
     *
     * @param premisesId The id of the premises of the property where the level
     * resides.
     * @param propertyId The id of the property where the level should be
     * updated.
     * @param level The level to patch.
     */
    public void patchPropertyLevel(final UUID premisesId, final UUID propertyId, final PropertyLevel level) {
        PropertyLevel original = this.getPropertyLevel(premisesId, propertyId, level.getId());
        original.setIcon(level.getIcon());
        original.setImage(level.getImage());
        original.setIndex(level.getIndex());
        original.setName(level.getName());
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            autoManager.getManager().getTransaction().begin();
            autoManager.getManager().merge(original);
            autoManager.getManager().getTransaction().commit();
        }
    }

    /**
     * Deletes the given level from the property.
     *
     * @param premisesId The premises id of the premises with the property
     * containing the level.
     * @param propertyId The id of the property having the level.
     * @param levelId the id of the level to be deleted.
     */
    public void deletePropertyLevel(final UUID premisesId, final UUID propertyId, final UUID levelId) {
        this.getPropertyLevel(premisesId, propertyId, levelId);
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            autoManager.getManager().getTransaction().begin();
            autoManager.getManager().remove(autoManager.getManager().find(PropertyLevel.class, levelId));
            autoManager.getManager().getTransaction().commit();
        }
    }

    /**
     * Returns a list of property level sections.
     *
     * @param premisesId The id of the premises.
     * @param propertyId The id of the sections.
     * @param levelId The id of the property level.
     * @return a list of property level sections.
     */
    public List<PropertySection> getPropertyLevelSections(final UUID premisesId, final UUID propertyId, final UUID levelId) {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {

            CriteriaBuilder builder = autoManager.getManager().getCriteriaBuilder();
            CriteriaQuery<PropertySection> criteriaQuery = builder.createQuery(PropertySection.class);
            Root<Premises> premisesRoot = criteriaQuery.from(Premises.class);
            Root<Property> propertyRoot = criteriaQuery.from(Property.class);
            Root<PropertyLevel> levelRoot = criteriaQuery.from(PropertyLevel.class);
            Root<PropertySection> sectionRoot = criteriaQuery.from(PropertySection.class);

            premisesRoot.joinSet(PROPERTIES_PARAMETER, JoinType.INNER);
            propertyRoot.joinSet(PROPERTYLEVEL_PARAMETER, JoinType.INNER);
            levelRoot.joinSet(SECTIONS_PARAMETER, JoinType.INNER);

            criteriaQuery.select(sectionRoot);

            criteriaQuery.where(
                    builder.and(
                            builder.equal(premisesRoot.get(ID_PARAMETER), premisesId),
                            builder.equal(propertyRoot.get(ID_PARAMETER), propertyId),
                            builder.equal(levelRoot.get(ID_PARAMETER), levelId)
                    )
            );

            return autoManager.getManager().createQuery(criteriaQuery)
                    .getResultList();
        }
    }

    /**
     * Adds a property section to a property level.
     *
     * @param premisesId The premises the property is on.
     * @param propertyId The property containing the level.
     * @param levelId The level on which the section should be added.
     * @param section The section to add.
     * @return The poperty section added.
     */
    public PropertySection addPropertyLevelSection(final UUID premisesId, final UUID propertyId, final UUID levelId, final PropertySection section) {
        PropertyLevel propertyLevel = this.getPropertyLevel(premisesId, propertyId, levelId);
        propertyLevel.getSections().add(section);
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            autoManager.getManager().getTransaction().begin();
            autoManager.getManager().persist(section);
            autoManager.getManager().merge(propertyLevel);
            autoManager.getManager().getTransaction().commit();
            return section;
        }
    }

    /**
     * Updates a property section.
     *
     * @param premisesId The premises the property is on.
     * @param propertyId The property containing the level.
     * @param levelId The level on which the section should be added.
     * @param section The section to update.
     */
    public void patchPropertyLevelSection(final UUID premisesId, final UUID propertyId, final UUID levelId, final PropertySection section) {
        PropertySection original = this.getPropertyLevelSection(premisesId, propertyId, levelId, section.getId());
        original.setIcon(section.getIcon());
        original.setName(section.getName());
        original.setPosX(section.getPosX());
        original.setPosY(section.getPosY());
        original.setVisualRepresentation(section.getVisualRepresentation());
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            autoManager.getManager().getTransaction().begin();
            autoManager.getManager().merge(original);
            autoManager.getManager().getTransaction().commit();
        }
    }

    /**
     * Returns a single section on a property level.
     *
     * @param premisesId The premises the property is on.
     * @param propertyId The property containing the level.
     * @param levelId The level on which the section should be added.
     * @param sectionId The id of the section.
     * @return The property section when found.
     */
    public PropertySection getPropertyLevelSection(final UUID premisesId, final UUID propertyId, final UUID levelId, final UUID sectionId) {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {

            CriteriaBuilder builder = autoManager.getManager().getCriteriaBuilder();
            CriteriaQuery<PropertySection> criteriaQuery = builder.createQuery(PropertySection.class);
            Root<Premises> premisesRoot = criteriaQuery.from(Premises.class);
            Root<Property> propertyRoot = criteriaQuery.from(Property.class);
            Root<PropertyLevel> levelRoot = criteriaQuery.from(PropertyLevel.class);
            Root<PropertySection> sectionRoot = criteriaQuery.from(PropertySection.class);

            premisesRoot.joinSet(PROPERTIES_PARAMETER, JoinType.INNER);
            propertyRoot.joinSet(PROPERTYLEVEL_PARAMETER, JoinType.INNER);
            levelRoot.joinSet(SECTIONS_PARAMETER, JoinType.INNER);

            criteriaQuery.select(sectionRoot);

            criteriaQuery.where(
                    builder.and(
                            builder.equal(premisesRoot.get(ID_PARAMETER), premisesId),
                            builder.equal(propertyRoot.get(ID_PARAMETER), propertyId),
                            builder.equal(levelRoot.get(ID_PARAMETER), levelId),
                            builder.equal(sectionRoot.get(ID_PARAMETER), sectionId)
                    )
            );

            return autoManager.getManager().createQuery(criteriaQuery)
                    .getSingleResult();
        }
    }

    /**
     * Deletes the given level from the property.
     *
     * @param premisesId The premises id of the premises with the property
     * containing the level.
     * @param propertyId The id of the property having the level.
     * @param levelId the id of the level containing the section.
     * @param sectionId the id of the section to be deleted.
     */
    public void deletePropertyLevelSection(final UUID premisesId, final UUID propertyId, final UUID levelId, final UUID sectionId) {
        this.getPropertyLevelSection(premisesId, propertyId, levelId, sectionId);
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            autoManager.getManager().getTransaction().begin();
            autoManager.getManager().remove(autoManager.getManager().find(PropertySection.class, sectionId));
            autoManager.getManager().getTransaction().commit();
        }
    }

    /**
     * Returns the amount of premises available.
     *
     * @return total amount of premises levels.
     * @throws Exception When an error occurs during count.
     */
    public long getPremisesListSize() throws Exception {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            return DatabaseUtils.getRowCount(autoManager.getManager(), Premises.class);
        }
    }

    /**
     * Returns the amount of property levels available.
     *
     * @return total amount of premises levels.
     * @throws Exception When an error occurs during count.
     */
    public long getPropertiesListSize() throws Exception {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            return DatabaseUtils.getRowCount(autoManager.getManager(), Property.class);
        }
    }

    /**
     * Returns the amount of premises levels available.
     *
     * @param premises the premises to get the amount of properties on.
     * @return total amount of premises levels.
     * @throws Exception When an error occurs during count.
     */
    public long getPropertiesListSize(final Premises premises) throws Exception {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            return DatabaseUtils.getRowCount(autoManager.getManager(), Property.class);
        }
    }

    /**
     * Returns the amount of premises levels available.
     *
     * @return total amount of premises levels.
     * @throws Exception When an error occurs during count.
     */
    public long getPropertLevelsListSize() throws Exception {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            return DatabaseUtils.getRowCount(autoManager.getManager(), PropertyLevel.class);
        }
    }

    /**
     * Returns the amount of premises levels available.
     *
     * @param property The propty to get the amount of levels from.
     * @return total amount of premises levels.
     * @throws Exception When an error occurs during count.
     */
    public long getPropertLevelsListSize(final Property property) throws Exception {
        try (AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            return DatabaseUtils.getRowCount(autoManager.getManager(), PropertyLevel.class);
        }
    }

    /**
     * Start the premises service.
     *
     * @param startPromise promise to determine a correct or failed start.
     */
    @Override
    public void start(final Promise<Void> startPromise) {
        prefillData();
        startPromise.complete();
    }

    /**
     * Prefills the data when minimal data is not present.
     *
     * Does a check if minimal
     *
     * 1 premises is present, 1 property on a premises, 1 property level in a
     * property. 1 property section on a property level.
     *
     */
    private void prefillData() {
        try (AutoClosableEntityManager closableManager = Database.getInstance().getNewAutoClosableManager()) {
            EntityManager manager = closableManager.getManager();
            Premises defaultPremises = manager.find(Premises.class, UUID.randomUUID());
            if (defaultPremises == null) {
                Premises premises = new Premises();
                premises.setName("My premises");
                PremisesIcon premisesIcon = new PremisesIcon();
                premisesIcon.setIconType(IconType.FONT_AWESOME);
                premisesIcon.setIcon("map");
                premises.setIcon(premisesIcon);

                PremisesSection premisesSection = new PremisesSection();
                premisesSection.setName("Driveway");
                PremisesIcon premisesSectionIcon = new PremisesIcon();
                premisesSectionIcon.setIconType(IconType.FONT_AWESOME);
                premisesSectionIcon.setIcon("road");
                premisesSection.setIcon(premisesSectionIcon);
                premises.setSections(Set.of(premisesSection));

                Property property = new Property();
                property.setName("My property");
                PremisesIcon propertyIcon = new PremisesIcon();
                propertyIcon.setIconType(IconType.FONT_AWESOME);
                propertyIcon.setIcon("home");
                property.setIcon(propertyIcon);
                premises.setProperties(Set.of(property));

                PropertyLevel level = new PropertyLevel();
                level.setName("Ground floor");
                PremisesIcon levelIcon = new PremisesIcon();
                levelIcon.setIconType(IconType.FONT_AWESOME);
                levelIcon.setIcon("minus");
                level.setIcon(levelIcon);
                property.setPropertyLevels(Set.of(level));

                PropertySection section = new PropertySection();
                section.setName("Living room");
                PremisesIcon sectionIcon = new PremisesIcon();
                sectionIcon.setIconType(IconType.FONT_AWESOME);
                sectionIcon.setIcon("couch");
                section.setIcon(sectionIcon);
                level.setSections(Set.of(section));

                manager.getTransaction().begin();
                manager.persist(section);
                manager.persist(level);
                manager.persist(property);
                manager.persist(premisesSection);
                manager.persist(premises);
                manager.getTransaction().commit();
            }
        }
    }

    /**
     * Stops the service.
     *
     * @param stopPromise Promise to determine the service stoppped correctly or
     * not.
     */
    @Override
    public void stop(final Promise<Void> stopPromise) {
        stopPromise.complete();
    }

}
