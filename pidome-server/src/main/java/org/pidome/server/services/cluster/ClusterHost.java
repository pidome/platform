/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.cluster;

/**
 * Class providing host information.
 *
 * @author John Sirach
 */
public class ClusterHost {

    /**
     * the primary identification of this host.
     */
    private HostIdentification hostIdentification;

    /**
     * The host information of the master host.
     */
    private ClusterMode mode;

    /**
     * The ip address of the server.
     */
    private String serverIp;

    /**
     * The name of the server.
     */
    private String serverName;

    /**
     * The server version.
     */
    private ServerVersion version;

    /**
     * The platform info.
     */
    private PlatformInfo platform;

    /**
     * @return the nodeId
     */
    public HostIdentification getHostIdentification() {
        return this.hostIdentification;
    }

    /**
     * Set the host identification.
     *
     * @param hostIdentification The host identification to set.
     */
    public void setHostIdentification(final HostIdentification hostIdentification) {
        this.hostIdentification = hostIdentification;
    }

    /**
     * @return the serverIp
     */
    public String getServerIp() {
        return serverIp;
    }

    /**
     * @param serverIp the serverIp to set
     */
    protected void setServerIp(final String serverIp) {
        this.serverIp = serverIp;
    }

    /**
     * @return the serverName
     */
    public String getServerName() {
        return serverName;
    }

    /**
     * @param serverName the serverName to set.
     */
    protected void setServerName(final String serverName) {
        this.serverName = serverName;
    }

    /**
     * @return the version
     */
    public ClusterHost.ServerVersion getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    protected void setVersion(final ClusterHost.ServerVersion version) {
        this.version = version;
    }

    /**
     * @return the mode
     */
    public ClusterMode getMode() {
        return mode;
    }

    /**
     * @param mode the mode to set
     */
    protected void setMode(final ClusterMode mode) {
        this.mode = mode;
    }

    /**
     * @return the platform
     */
    public PlatformInfo getPlatform() {
        return platform;
    }

    /**
     * @param platform the platform to set
     */
    protected void setPlatform(final PlatformInfo platform) {
        this.platform = platform;
    }

    /**
     * The server version.
     */
    public static class ServerVersion {

        /**
         * Major version.
         */
        private int major = 1;
        /**
         * Minor version.
         */
        private int minor = 0;
        /**
         * Patch version.
         */
        private int patch = 0;
        /**
         * Build version.
         */
        private int build = 0;
        /**
         * Release name.
         */
        private String name = "octopi";
        /**
         * Release type.
         */
        private String type = "local";
        /**
         * Build date.
         */
        private String date = "0000-00-00";

        /**
         * If the current version is a snapshot version or not.
         */
        private boolean snapshot = false;

        /**
         * @return the major
         */
        public int getMajor() {
            return major;
        }

        /**
         * @param major the major to set
         */
        protected void setMajor(final int major) {
            this.major = major;
        }

        /**
         * @return the minor
         */
        public int getMinor() {
            return minor;
        }

        /**
         * @param minor the minor to set
         */
        protected void setMinor(final int minor) {
            this.minor = minor;
        }

        /**
         * @return the patch
         */
        public int getPatch() {
            return patch;
        }

        /**
         * @param patch the patch to set
         */
        protected void setPatch(final int patch) {
            this.patch = patch;
        }

        /**
         * @return the build
         */
        public int getBuild() {
            return build;
        }

        /**
         * @param build the build to set
         */
        protected void setBuild(final int build) {
            this.build = build;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        protected void setName(final String name) {
            this.name = name;
        }

        /**
         * @return the type
         */
        public String getType() {
            return type;
        }

        /**
         * @param type the type to set
         */
        protected void setType(final String type) {
            this.type = type;
        }

        /**
         * @return the date
         */
        public String getDate() {
            return date;
        }

        /**
         * @param date the date to set
         */
        protected void setDate(final String date) {
            this.date = date;
        }

        /**
         * @return the snapshot
         */
        public boolean isSnapshot() {
            return snapshot;
        }

        /**
         * @param snapshot the snapshot to set
         */
        protected void setSnapshot(final boolean snapshot) {
            this.snapshot = snapshot;
        }

    }

    /**
     * Identifying the platform running on.
     */
    public static class PlatformInfo {

        /**
         * The reported arch.
         */
        private String arch;

        /**
         * The reported operating system.
         */
        private String os;

        /**
         * If it is a raspberry pi or not.
         */
        private boolean pi;

        /**
         * The java vendor info.
         */
        private String javaVendorInfo;
        /**
         * The java version info.
         */
        private String javaVersionInfo;

        /**
         * @return the arch
         */
        public String getArch() {
            return arch;
        }

        /**
         * @return the os
         */
        public String getOs() {
            return os;
        }

        /**
         * @return the pi
         */
        public boolean isPi() {
            return pi;
        }

        /**
         * @param arch the arch to set
         */
        protected void setArch(final String arch) {
            this.arch = arch;
        }

        /**
         * @param os the os to set
         */
        protected void setOs(final String os) {
            this.os = os;
        }

        /**
         * @param pi the pi to set
         */
        protected void setPi(final boolean pi) {
            this.pi = pi;
        }

        /**
         * @return the javaBuildInfo
         */
        public String getJavaVendorInfo() {
            return javaVendorInfo;
        }

        /**
         * @param javaVendorInfo the javaBuildInfo to set
         */
        protected void setJavaVendorInfo(final String javaVendorInfo) {
            this.javaVendorInfo = javaVendorInfo;
        }

        /**
         * @return the javaVersionInfo
         */
        public String getJavaVersionInfo() {
            return javaVersionInfo;
        }

        /**
         * @param javaVersionInfo the javaVersionInfo to set
         */
        protected void setJavaVersionInfo(final String javaVersionInfo) {
            this.javaVersionInfo = javaVersionInfo;
        }

    }

}
