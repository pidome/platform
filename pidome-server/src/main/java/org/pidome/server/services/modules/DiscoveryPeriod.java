/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.modules;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.pidome.server.system.utils.LabeledEnum;

/**
 * Rule to be applied for periodic actions.
 *
 * @author johns
 */
public class DiscoveryPeriod {

    /**
     * Types supported by discovery.
     */
    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum Type implements LabeledEnum {
        /**
         * Discovery.
         */
        CONTINUOUS("Continuous"),
        /**
         * Scan.
         */
        ONE_SHOT("One shot");

        /**
         * The label.
         */
        private final String label;

        /**
         * constructor.
         *
         * @param label The label to set.
         */
        Type(final String label) {
            this.label = label;
        }

        /**
         * @return The label.
         */
        @Override
        public String getLabel() {
            return this.label;
        }

        /**
         * @return The value.
         */
        @Override
        public Object getValue() {
            return this.name();
        }

    }

    /**
     * Length used in the rule.
     */
    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum Length implements LabeledEnum {
        /**
         * One.
         */
        ONE("One", 1),
        /**
         * Five.
         */
        FIVE("Five", 5),
        /**
         * Ten.
         */
        TEN("Ten", 10),
        /**
         * Indefinitely.
         */
        INDEFINITE("Indefinite", 0);

        /**
         * the label.
         */
        private final String label;

        /**
         * The value.
         */
        private final int value;

        /**
         * Enum constructor.
         *
         * @param label the label to show.
         * @param value The value.
         */
        Length(final String label, final int value) {
            this.label = label;
            this.value = value;
        }

        /**
         * Returns the value.
         *
         * @return The value.
         */
        @Override
        public Integer getValue() {
            return this.value;
        }

        /**
         * Return the label to show.
         *
         * @return The label.
         */
        @Override
        public String getLabel() {
            return this.label;
        }

    }

    /**
     * The period unit.
     */
    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum Unit implements LabeledEnum {
        /**
         * Minutes.
         */
        MINUTES("Minutes", 60),
        /**
         * Hours.
         */
        HOURS("Hours", 3600),
        /**
         * Days.
         */
        DAYS("Days", 86400),
        /**
         * Months.
         */
        MONTHS("Months", 2678400),
        /**
         * Years.
         */
        YEARS("Years", 31536000);

        /**
         * the label.
         */
        private final String label;

        /**
         * the length.
         */
        private final long seconds;

        /**
         * Enum constructor.
         *
         * @param label the label to show.
         * @param seconds the amount of seconds.
         */
        Unit(final String label, final long seconds) {
            this.label = label;
            this.seconds = seconds;
        }

        /**
         * Return the label to show.
         *
         * @return The label.
         */
        @Override
        public String getLabel() {
            return this.label;
        }

        /**
         * The value.
         *
         * @return The value.
         */
        @Override
        public Object getValue() {
            return this.name();
        }

        /**
         * The amount of seconds in this unit.
         *
         * @return The amount of seconds.
         */
        public long getSeconds() {
            return this.seconds;
        }

    }

    /**
     * The type to use.
     */
    private Type type = Type.CONTINUOUS;

    /**
     * The unit to use.
     */
    private Unit unit = Unit.MINUTES;

    /**
     * The unit length to use.
     */
    private Length length = Length.ONE;

    /**
     * The type to use.
     *
     * @return the type
     */
    public Type getType() {
        return type;
    }

    /**
     * The type to use.
     *
     * @param type the type to set
     */
    public void setType(final Type type) {
        this.type = type;
    }

    /**
     * The unit to use.
     *
     * @return the unit
     */
    public Unit getUnit() {
        return unit;
    }

    /**
     * The unit to use.
     *
     * @param unit the unit to set
     */
    public void setUnit(final Unit unit) {
        this.unit = unit;
    }

    /**
     * The unit length to use.
     *
     * @return the length
     */
    public Length getLength() {
        return length;
    }

    /**
     * The unit length to use.
     *
     * @param length the length to set
     */
    public void setLength(final Length length) {
        this.length = length;
    }

    /**
     * Calculates the length of the period set.
     *
     * @return The total amount in seconds.
     */
    public final long getPeriodLength() {
        return length.getValue() * unit.getSeconds();
    }

}
