/**
 * Generic definitions used in services.
 * <p>
 * Provides generic definitions to be used within services.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.services.definitions;
