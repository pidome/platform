/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.modules;

import org.pidome.server.services.events.EventAddress;
import org.pidome.server.services.events.EventHandler;

/**
 * An hardware event.
 *
 * @author johns
 */
public class ModuleEventHandler extends EventHandler<ModuleEventBody> {

    /**
     * Constructor.
     */
    public ModuleEventHandler() {
        super(EventAddress.MODULES);
    }

    /**
     * Returns the event.
     *
     * @return The event.
     */
    public ModuleEventBody getEvent() {
        return this.getBody();
    }

}
