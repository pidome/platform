/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.events;

import io.vertx.core.eventbus.Message;

/**
 * Encapsulating class for a notification.
 *
 * @author johns
 * @param <T> The object in the event.
 */
@SuppressWarnings("CPD-START")
public abstract class EventHandler<T> extends EventBusMessageHandler<T> {

    /**
     * The notification headers.
     */
    private EventMessageHeaders headers;

    /**
     * The body of the event.
     */
    private T body;

    /**
     * Constructor for setting the address of the event.
     *
     * @param eventsAddress The event target address.
     */
    public EventHandler(final EventAddress eventsAddress) {
        super(eventsAddress);
    }

    /**
     * Handles a message.
     *
     * @param messageHeaders The headers of the message.
     * @param messageBody The plain message self.
     */
    @Override
    public void handleMessage(final EventMessageHeaders messageHeaders, final Message<T> messageBody) {
        this.headers = messageHeaders;
        this.body = messageBody.body();
    }

    /**
     * Returns the headers of the notification.
     *
     * @return The notification headers.
     */
    public EventMessageHeaders getHeaders() {
        return this.headers;
    }

    /**
     * The headers to set.
     *
     * @param headers The headers to set.
     */
    protected void setHeaders(final EventMessageHeaders headers) {
        this.headers = headers;
    }

    /**
     * Returns the body of the event.
     *
     * @return The event body.
     */
    protected T getBody() {
        return this.body;
    }

    /**
     * Set's the body.
     *
     * @param body The body to set.
     */
    protected void setBody(final T body) {
        this.body = body;
    }

    /**
     * An optional message to send.
     *
     * @return the message
     */
    public String getMessage() {
        return this.headers.getMessage();
    }

    /**
     * An optional message to send.
     *
     * @param message the message to set
     */
    protected void setMessage(final String message) {
        this.headers.setMessage(message);
    }

}
