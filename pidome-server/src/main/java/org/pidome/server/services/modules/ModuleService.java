/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.modules;

import io.vertx.core.Future;
import io.vertx.core.Promise;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.hardware.driver.HardwareDriver;
import org.pidome.platform.hardware.driver.Transport;
import org.pidome.platform.modules.ModuleBase;
import org.pidome.platform.modules.ModuleCapabilities;
import org.pidome.platform.modules.ModuleType;
import org.pidome.server.services.AbstractService;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.events.EventService;
import org.pidome.server.services.hardware.HardwareService;
import org.pidome.server.system.database.Database;
import org.pidome.server.system.modules.ActiveModuleContainer;
import org.pidome.server.system.modules.ModuleLoader;

/**
 * Service for handling modules.
 *
 * @author johns
 */
public class ModuleService extends AbstractService {

    /**
     * Class root logger.
     */
    private static final Logger LOG = LogManager.getLogger(ModuleService.class);

    /**
     * The module store.
     */
    private ModuleLoader loader;

    /**
     * Returns a list by it's type.
     *
     * This method is not used to identifiable return a list of modules, it only
     * promises modules of the given moduleType is returned.
     *
     * @param moduleType The type to filter on.
     * @return The modules based on the type.
     */
    public Optional<List<ModuleDefinition>> getModulesByType(final ModuleType moduleType) {
        return Optional.ofNullable(
                loader.getModulesFilterByType(moduleType.getImplementation())
        );
    }

    /**
     * Returns a list by it's type.
     *
     * This method is not used to identifiable return a list of modules, it only
     * promises modules of the given moduleType is returned.
     *
     * @param moduleType The type to filter on.
     * @param transport The transport target to filter on.
     * @return The modules based on the type.
     */
    public Optional<List<ModuleDefinition>> getModulesByType(final ModuleType moduleType, final Transport.SubSystem transport) {
        return Optional.ofNullable(
                loader.getModulesFilterByType(moduleType.getImplementation()).stream()
                        .filter(module -> module.getTransportTarget().equals(transport))
                        .collect(Collectors.toList())
        );
    }

    /**
     * Returns a list of all available modules.
     *
     * @return The list of modules.
     */
    public Map<ModuleType, List<ModuleDefinition>> getAllModules() {
        return loader.getAllModules();
    }

    /**
     * Returns a list of all active modules.
     *
     * @return The list of active modules.
     */
    public List<ActiveModuleContainer> getActiveModules() {
        return loader.getActiveModules();
    }

    /**
     * Returns the module definition by it's id.
     *
     * @param definitionId The definition id of the module.
     * @return The module definition.
     */
    public Optional<ModuleDefinition> getDefinitionById(final UUID definitionId) {
        return loader.getModuleDefinitionById(definitionId);
    }

    /**
     * Starts persisted modules.
     *
     * This method does not fail the start of the service.
     */
    private void startPersistedModules() {
        loader.loadPersistedConfigurations();
    }

    /**
     * Loads a persisted module for the given hardware.
     *
     * @param vendorId The hardware vendor id.
     * @param productId The hardware product id.
     * @param serial The hardware serial number.
     */
    public void loadModulesForPeripheral(final String vendorId, final String productId, final String serial) {
        Set<ModuleLoaderConfiguration> definitions = new HashSet<>();
        try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {

            CriteriaBuilder cb = autoManager.getManager().getCriteriaBuilder();
            CriteriaQuery<ModuleLoaderConfiguration> cq = cb.createQuery(ModuleLoaderConfiguration.class);
            Root<ModuleLoaderConfiguration> rootEntry = cq.from(ModuleLoaderConfiguration.class);
            CriteriaQuery<ModuleLoaderConfiguration> select = cq.select(rootEntry);

            select.where(
                    cb.equal(rootEntry.get("vendorId"), vendorId),
                    cb.equal(rootEntry.get("productId"), productId),
                    cb.equal(rootEntry.get("productSerial"), serial)
            );

            try {
                definitions.addAll(autoManager.getManager().createQuery(select).getResultList()
                        .stream().collect(Collectors.toSet()));
            } catch (NoResultException nr) {
                LOG.debug("No persisted configurations present for [{}], [{}], [{}]", vendorId, productId, serial);
            }
        }
        definitions.forEach(configuration -> {
            startModule(configuration).setHandler(result -> {
                if (result.succeeded()) {
                    LOG.info("Module [{}] started", configuration.getModuleId());
                } else {
                    LOG.error("Module [{}] failed to start", configuration.getModuleId());
                }
            });
        });
    }

    /**
     * Starts a module.
     *
     * @param configuration The configuration to start a module.
     * @return The result of starting a module.
     */
    public Future<ActiveModuleContainer> startModule(final ModuleLoaderConfiguration configuration) {
        Promise<ActiveModuleContainer> promise = Promise.promise();
        final Future<ActiveModuleContainer> actionFuture = loader.startModule(configuration);
        actionFuture.setHandler(futureResult -> {
            promise.handle(futureResult);
            if (futureResult.succeeded() && configuration.getId() == null) {
                ServiceHandler.service(HardwareService.class)
                        .ifPresent(service -> {
                            service.getPeripheralByKey(configuration.getPeripheralKey()).ifPresent(peripheral -> {
                                configuration.setVendorId(peripheral.getVendorId());
                                configuration.setProductId(peripheral.getProductId());
                                configuration.setProductSerial(peripheral.getSerial());
                            });
                        });
                final EntityManager manager = Database.getInstance().getNewManager();
                manager.getTransaction().begin();
                manager.persist(configuration);
                manager.getTransaction().commit();
            }
        });
        return promise.future();
    }

    /**
     * Stops the module by the given module id.
     *
     * @param activeContainerId The module id to stop the module for.
     * @return Future indicating the stop result.
     */
    public Future<Void> stopModule(final String activeContainerId) {
        return loader.stopModule(activeContainerId);
    }

    /**
     * Returns an active module container.
     *
     * @param activeContainerId The id of the module container.
     * @return The module container if present.
     */
    public Optional<ActiveModuleContainer> getActiveModuleContainer(final String activeContainerId) {
        return loader.getActiveModules().stream()
                .filter(module -> module.getId().equals(activeContainerId))
                .findFirst();
    }

    /**
     * Returns an active module from a module container.
     *
     * @param activeContainerId The id of the module container.
     * @return The module.
     * @throws ModuleException When there is no module container with the given
     * id.
     */
    public ModuleBase getActiveModule(final String activeContainerId) throws ModuleException {
        return getActiveModuleContainer(activeContainerId)
                .orElseThrow(() -> new ModuleException("No module with given id"))
                .getModule();
    }

    /**
     * Returns a list of modules which provides the given capabilities.
     *
     * @param capabilities The capabilities to return for.
     * @return List of given capabilities capable module.
     */
    public List<ActiveModuleContainer> getModulesWithCapabilities(final ModuleCapabilities... capabilities) {
        return getModulesWithCapabilities(Arrays.asList(capabilities));
    }

    /**
     * Returns a list of modules which provides the given capabilities.
     *
     * @param capabilities The capabilities to return for.
     * @return List of given capabilities capable module.
     */
    public List<ActiveModuleContainer> getModulesWithCapabilities(final List<ModuleCapabilities> capabilities) {
        return getActiveModules().stream()
                .filter(container -> capabilities.stream().anyMatch(element -> container.getModuleCapabilities().contains(element)))
                .collect(Collectors.toList());
    }

    /**
     * Called by the driver service to signal a driver is being stopped which
     * should also stop linked modules.
     *
     * @param <D> The driver type.
     * @param driver The driver.
     */
    public <D extends HardwareDriver> void stoppingDriver(final D driver) {
        loader.stopModulesWithDriver(driver);
    }

    /**
     * Starts the module service.
     *
     * @param startPromise Promise to indicate the service has started.
     */
    @Override
    public void start(final Promise<Void> startPromise) {
        loader = new ModuleLoader();
        ServiceHandler.service(EventService.class).ifPresent(service -> {
            service.registerCodec(ModuleEventBody.class, new ModuleEventEncoder());
        });
        startPromise.handle(loader.init());
        startPersistedModules();
    }

    /**
     * Stops the module service.
     *
     * @param stopPromise Promise to indicate the service has stopped.
     */
    @Override
    public void stop(final Promise<Void> stopPromise) {
        ServiceHandler.service(EventService.class).ifPresent(service -> {
            service.unregisterCodec(ModuleEventBody.class);
        });
        stopPromise.complete();
    }

}
