/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.controllers;

import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.auth.User;
import java.util.List;
import java.util.UUID;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import org.pidome.server.entities.premises.Premises;
import org.pidome.server.entities.premises.PremisesSection;
import org.pidome.server.entities.premises.Property;
import org.pidome.server.entities.premises.PropertyLevel;
import org.pidome.server.entities.premises.PropertySection;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.http.api.ApiControllerResource;
import org.pidome.server.services.http.api.response.ApiResponseCode;
import org.pidome.server.services.http.api.response.HttpStatusCodeException;
import org.pidome.server.services.premises.PremisesService;

/**
 * The controller for premises and properties.
 *
 * @author John Sirach
 */
@Path("premises")
@Produces("application/json")
@StatusCodes({
    @ResponseCode(code = ApiResponseCode.Code.HTTP_503, condition = "When the premises service is not available.")
})
public class PremisesController extends ApiControllerResource {

    /**
     * Returns a list of premises.
     *
     * @param user The logged in user to select the allowed premises.
     * @return The premises for the given user.
     */
    @GET
    @RolesAllowed("USER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "With the premises returned.")
    })
    public List<Premises> getPremises(final @Context User user) {
        return getPremisesService().getPremises();
    }

    /**
     * Returns the given premises.
     *
     * This method only returns the premises the user is allowed to retrieve.
     *
     * @param premisesId The id of the premises requested to return.
     * @param user The user which should be allowed to request the premises.
     * @return The premises if the given user is allowed to fetch this.
     * @throws HttpStatusCodeException when an error occurs.
     */
    @GET
    @RolesAllowed("USER")
    @Path("{premisesId}")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "With the premises returned."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When not found.")
    })
    public Premises getPremises(final @PathParam("premisesId") UUID premisesId, final @Context User user) {
        return getPremisesService().getPremises(premisesId);
    }

    /**
     * Add a premises to the server.
     *
     * @param premises The premises to add.
     * @return The added premises.
     */
    @POST
    @RolesAllowed("ADMIN")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "When the premises is added.")
    })
    @Consumes("application/json")
    public Premises addPremises(final Premises premises) {
        return this.getPremisesService().addPremises(premises);
    }

    /**
     * Shallow update of a premises.If a property needs to switch premises use
     * the appropriate method, this one only allows for a shallow update.
     *
     *
     * @param premises The premises to update/patch.
     * @param response Http response object.
     */
    @PATCH
    @RolesAllowed("ADMIN")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_204, condition = "When the premises is added."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the premises to update does not exist.")
    })
    @Consumes("application/json")
    public void patchPremises(final Premises premises, final @Context HttpServerResponse response) {
        this.getPremisesService().patchPremises(premises);
        this.createHttpResponse(response, ApiResponseCode.HTTP_204);
    }

    /**
     * Deletes a premises from the system.
     *
     * The deletion of a premises is a recursive action, it removes all
     * properties and sections.
     *
     * This also includes all the property levels and level sections. Everything
     * linked to section on these premises sections and property level sections
     * will retain, but are unbounded and will behave as such.
     *
     * @param premisesId The id of the premises to delete.
     * @param response Http response object.
     */
    @DELETE
    @RolesAllowed("ADMIN")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_204, condition = "When the premises is deleted."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the premises to delete does not exist.")
    })
    @Path("{premisesId}")
    public void deletePremises(final @PathParam("premisesId") UUID premisesId, final @Context HttpServerResponse response) {
        this.getPremisesService().deletePremises(premisesId);
        this.createHttpResponse(response, ApiResponseCode.HTTP_204);
    }

    /**
     * Returns the sections on a premises.
     *
     * @param premisesId The id of the premises.
     * @return The list of sections.
     */
    @GET
    @RolesAllowed("USER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "On returning premises sections"),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the premises does not exist.")
    })
    @Path("{premisesId}/sections")
    public List<PremisesSection> getPremisesSections(final @PathParam("premisesId") UUID premisesId) {
        return this.getPremisesService().getPremisesSections(premisesId);
    }

    /**
     * Returns a single premises section.
     *
     * @param premisesId The id of the premises.
     * @param sectionId The id of the section.
     * @return A single section.
     */
    @GET
    @RolesAllowed("USER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "On returning a premises section"),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the section does not exist.")
    })
    @Path("{premisesId}/sections/{sectionId}")
    public PremisesSection getPremisesSection(final @PathParam("premisesId") UUID premisesId,
            final @PathParam("sectionId") UUID sectionId) {
        return this.getPremisesService().getPremisesSection(premisesId, sectionId);
    }

    /**
     * Creates a single premises section.
     *
     * @param premisesId The id of the premises.
     * @param section The section to add.
     * @return The added section on success.
     */
    @POST
    @RolesAllowed("ADMIN")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "On creating a premises section"),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the section does not exist.")
    })
    @Path("{premisesId}/sections")
    @Consumes("application/json")
    public PremisesSection createPremisesSection(final @PathParam("premisesId") UUID premisesId,
            final PremisesSection section) {
        return this.getPremisesService().addPremisesSection(premisesId, section);
    }

    /**
     * Updates a single premises section.
     *
     * @param premisesId The id of the premises.
     * @param response the response object.
     * @param section The section to add.
     */
    @PATCH
    @RolesAllowed("ADMIN")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_204, condition = "On updating a premises section"),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the section does not exist.")
    })
    @Path("{premisesId}/sections")
    @Consumes("application/json")
    public void patchPremisesSection(final @PathParam("premisesId") UUID premisesId,
            final @Context HttpServerResponse response,
            final PremisesSection section) {
        this.getPremisesService().patchPremisesSection(premisesId, section);
        this.createHttpResponse(response, ApiResponseCode.HTTP_204);
    }

    /**
     * Deletes a single premises section.
     *
     * @param premisesId The id of the premises.
     * @param sectionId The id of the section.
     * @param response The response object.
     */
    @DELETE
    @RolesAllowed("ADMIN")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_204, condition = "On succesfully deleting a premises section"),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the section does not exist.")
    })
    @Path("{premisesId}/sections/{sectionId}")
    public void deletePremisesSection(final @PathParam("premisesId") UUID premisesId,
            final @PathParam("sectionId") UUID sectionId,
            final @Context HttpServerResponse response) {
        this.getPremisesService().deletePremisesSection(premisesId, sectionId);
        this.createHttpResponse(response, ApiResponseCode.HTTP_204);
    }

    /**
     * Returns all properties on a premises.
     *
     * @param premisesId The id of the premises.
     * @param user The user requesting it.
     * @return List of properties on the premises.
     */
    @GET
    @Path("{premisesId}/properties")
    @RolesAllowed("USER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "When the property is returned."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the property does not exist.")
    })
    public List<Property> getProperties(final @PathParam("premisesId") UUID premisesId, final @Context User user) {
        return getPremisesService().getProperties(premisesId);
    }

    /**
     * Returns the property if the user has acces to it.
     *
     * @param premisesId The id of the premises.
     * @param propertyId The id of the property.
     * @param user The user requesting it.
     * @return The property requested.
     */
    @GET
    @Path("{premisesId}/properties/{propertyId}")
    @RolesAllowed("USER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "When the property is returned."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the property does not exist.")
    })
    public Property getProperty(final @PathParam("premisesId") UUID premisesId,
            final @PathParam("propertyId") UUID propertyId,
            final @Context User user) {
        return getPremisesService().getProperty(premisesId, propertyId);
    }

    /**
     * Deletes a property.
     *
     * This method is recursive, it deletes all property levels and sections.
     *
     * @param premisesId The id of the premises.
     * @param propertyId The id of the property.
     * @param response The response object.
     */
    @DELETE
    @Path("{premisesId}/properties/{propertyId}")
    @RolesAllowed("ADMIN")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_204, condition = "When the property is deleted."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the property does not exist.")
    })
    public void deleteProperty(final @PathParam("premisesId") UUID premisesId,
            final @PathParam("propertyId") UUID propertyId,
            final @Context HttpServerResponse response) {
        getPremisesService().deleteProperty(premisesId, propertyId);
        this.createHttpResponse(response, ApiResponseCode.HTTP_204);
    }

    /**
     * Adds a property.
     *
     * @param premisesId The id of the premises to add the property to.
     * @param property The property to add.
     * @return The added property on success.
     */
    @POST
    @Path("{premisesId}/properties")
    @RolesAllowed("ADMIN")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "When the property is added."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the premises does not exist.")
    })
    @Consumes("application/json")
    public Property addProperty(final @PathParam("premisesId") UUID premisesId, final Property property) {
        return getPremisesService().addProperty(premisesId, property);
    }

    /**
     * Patches a property.
     *
     * This method only allows a shallow update, to move levels in a property
     * use the appropriate method.
     *
     * @param premisesId The id of the premises to patch the property.
     * @param property The property to patch.
     * @param response The http response object.
     */
    @PATCH
    @Path("{premisesId}/properties")
    @RolesAllowed("ADMIN")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_204, condition = "When the property is updated."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the property to update does not exist.")
    })
    @Consumes("application/json")
    public void patchProperty(final @PathParam("premisesId") UUID premisesId, final Property property, final @Context HttpServerResponse response) {
        this.getPremisesService().patchProperty(premisesId, property);
        this.createHttpResponse(response, ApiResponseCode.HTTP_204);
    }

    /**
     * Returns all property levels.
     *
     * @param premisesId The id of the premises.
     * @param propertyId The id of the property.
     * @param user The user which should be allowed to request the level.
     * @return The premises level if the given user is allowed to fetch this.
     */
    @GET
    @RolesAllowed("USER")
    @Path("{premisesId}/properties/{propertyId}/levels")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "With the property levels returned."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the property is not found.")
    })
    public List<PropertyLevel> getPropertyLevels(final @PathParam("premisesId") UUID premisesId,
            final @PathParam("propertyId") UUID propertyId, final @Context User user) {
        return getPremisesService().getPropertyLevels(premisesId, propertyId);
    }

    /**
     * Returns a level on a property.
     *
     * @param premisesId The id of the premises.
     * @param propertyId The id of the property.
     * @param propertyLevelId The id of the level requested to return.
     * @param user The user which should be allowed to request the level.
     * @return The premises level if the given user is allowed to fetch this.
     */
    @GET
    @RolesAllowed("USER")
    @Path("{premisesId}/properties/{propertyId}/levels/{propertyLevelId}")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "With the property level returned."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the level is not found.")
    })
    public PropertyLevel getPropertyLevel(final @PathParam("premisesId") UUID premisesId,
            final @PathParam("propertyId") UUID propertyId,
            final @PathParam("propertyLevelId") UUID propertyLevelId, final @Context User user) {
        return getPremisesService().getPropertyLevel(premisesId, propertyId, propertyLevelId);
    }

    /**
     * Adds a level on a property.
     *
     * @param premisesId The id of the premises.
     * @param propertyId The id of the property.
     * @param propertyLevel The property level to add.
     * @return The added property level.
     */
    @POST
    @RolesAllowed("ADMIN")
    @Path("{premisesId}/properties/{propertyId}/levels")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "When the property level is created")
    })
    @Consumes("application/json")
    public PropertyLevel addPropertyLevel(final @PathParam("premisesId") UUID premisesId,
            final @PathParam("propertyId") UUID propertyId,
            final PropertyLevel propertyLevel) {
        return getPremisesService().addPropertyLevel(premisesId, propertyId, propertyLevel);
    }

    /**
     * Updates a property level.
     *
     * @param premisesId The id of the premises.
     * @param propertyId The id of the property.
     * @param propertyLevel The property level to update.
     * @param response The http response object.
     * @throws HttpStatusCodeException On failure.
     */
    @PATCH
    @RolesAllowed("ADMIN")
    @Path("{premisesId}/properties/{propertyId}/levels")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_204, condition = "When the property level is updated"),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the property level does not exist")
    })
    @Consumes("application/json")
    public void patchPropertyLevel(final @PathParam("premisesId") UUID premisesId,
            final @PathParam("propertyId") UUID propertyId,
            final PropertyLevel propertyLevel,
            final @Context HttpServerResponse response) {
        getPremisesService().patchPropertyLevel(premisesId, propertyId, propertyLevel);
        this.createHttpResponse(response, ApiResponseCode.HTTP_204);
    }

    /**
     * Removes a property level.
     *
     * @param premisesId The id of the premises.
     * @param propertyId The id of the property.
     * @param propertyLevelId The property level to delete.
     * @param response The HTTP response object.
     */
    @DELETE
    @RolesAllowed("ADMIN")
    @Path("{premisesId}/properties/{propertyId}/levels/{propertyLevelId}")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_204, condition = "When the property level is deleted"),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the property level does not exist.")
    })
    public void deletePropertyLevel(final @PathParam("premisesId") UUID premisesId,
            final @PathParam("propertyId") UUID propertyId,
            final @PathParam("propertyLevelId") UUID propertyLevelId,
            final @Context HttpServerResponse response) {
        getPremisesService().deletePropertyLevel(premisesId, propertyId, propertyLevelId);
        this.createHttpResponse(response, ApiResponseCode.HTTP_204);
    }

    /**
     * Returns a list of sections on a property level.
     *
     * These sections can be refered to as rooms, hallways, cubicles, whatever.
     *
     * @param premisesId The id of the premises.
     * @param propertyId The id of the property.
     * @param propertyLevelId The id of the property level.
     * @return List of sections on a property level.
     */
    @GET
    @RolesAllowed("USER")
    @Path("{premisesId}/properties/{propertyId}/levels/{propertyLevelId}/sections")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "On succesfull return"),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the property level does not exist.")
    })
    public List<PropertySection> getPropertyLevelSections(final @PathParam("premisesId") UUID premisesId,
            final @PathParam("propertyId") UUID propertyId,
            final @PathParam("propertyLevelId") UUID propertyLevelId) {
        return this.getPremisesService().getPropertyLevelSections(premisesId, propertyId, propertyLevelId);
    }

    /**
     * Creates a section on a property level.
     *
     * @param premisesId The id of the premises.
     * @param propertyId The id of the property.
     * @param propertyLevelId The id of the property level.
     * @param propertyLevelSection The section to add.
     * @return The added section on success.
     */
    @POST
    @RolesAllowed("ADMIN")
    @Path("{premisesId}/properties/{propertyId}/levels/{propertyLevelId}")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "When the property level section is added"),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the property level does not exist.")
    })
    @Consumes("application/json")
    public PropertySection addPropertyLevelSection(final @PathParam("premisesId") UUID premisesId,
            final @PathParam("propertyId") UUID propertyId,
            final @PathParam("propertyLevelId") UUID propertyLevelId,
            final PropertySection propertyLevelSection) {
        return this.getPremisesService().addPropertyLevelSection(premisesId, propertyId, propertyLevelId, propertyLevelSection);
    }

    /**
     * Updates a section on a property level.
     *
     * @param premisesId The id of the premises.
     * @param propertyId The id of the property
     * @param response The response object.
     * @param propertyLevelId The id of the property level.
     * @param propertyLevelSection The section to update.
     */
    @PATCH
    @RolesAllowed("ADMIN")
    @Path("{premisesId}/properties/{propertyId}/levels/{propertyLevelId}/sections")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_204, condition = "When the property level section is updated"),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the property level does not exist.")
    })
    @Consumes("application/json")
    public void patchPropertyLevelSection(final @PathParam("premisesId") UUID premisesId,
            final @PathParam("propertyId") UUID propertyId,
            final @PathParam("propertyLevelId") UUID propertyLevelId,
            final @Context HttpServerResponse response,
            final PropertySection propertyLevelSection) {
        this.getPremisesService().patchPropertyLevelSection(premisesId, propertyId, propertyLevelId, propertyLevelSection);
        this.createHttpResponse(response, ApiResponseCode.HTTP_204);
    }

    /**
     * Updates a section on a property level.
     *
     * @param premisesId The id of the premises.
     * @param propertyId The id of the property.
     * @param propertyLevelId The id of the property level.
     * @param sectionId The id of the section to retrieve.
     * @return The property level section
     */
    @GET
    @RolesAllowed("USER")
    @Path("{premisesId}/properties/{propertyId}/levels/{propertyLevelId}/sections/{sectionId}")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "When the property level section is returned"),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the property level section does not exist.")
    })
    public PropertySection getPropertyLevelSection(final @PathParam("premisesId") UUID premisesId,
            final @PathParam("propertyId") UUID propertyId,
            final @PathParam("propertyLevelId") UUID propertyLevelId,
            final @PathParam("sectionId") UUID sectionId) {
        return this.getPremisesService().getPropertyLevelSection(premisesId, propertyId, propertyLevelId, sectionId);
    }

    /**
     * Deletes a section on a property level.
     *
     * @param premisesId The id of the premises.
     * @param propertyId The id of the property.
     * @param propertyLevelId The id of the property level.
     * @param sectionId The id of the section to retrieve.
     */
    @DELETE
    @RolesAllowed("ADMIN")
    @Path("{premisesId}/properties/{propertyId}/levels/{propertyLevelId}/sections/{sectionId}")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_204, condition = "When the property level section is deleted"),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the property level section does not exist.")
    })
    public void deletePropertyLevelSection(final @PathParam("premisesId") UUID premisesId,
            final @PathParam("propertyId") UUID propertyId,
            final @PathParam("propertyLevelId") UUID propertyLevelId,
            final @PathParam("sectionId") UUID sectionId) {
        this.getPremisesService().deletePropertyLevelSection(premisesId, propertyId, propertyLevelId, sectionId);
    }

    /**
     * Returns the premises service.
     *
     * @return The premises service
     * @throws HttpStatusCodeException When the service is not available with a
     * code 500.
     */
    private PremisesService getPremisesService() throws HttpStatusCodeException {
        if (ServiceHandler.getInstance().getService(PremisesService.class).isPresent()) {
            return ServiceHandler.getInstance().getService(PremisesService.class).get();
        } else {
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_503, "Premises service not available");
        }
    }

}
