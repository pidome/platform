/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.controllers;

import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;
import io.vertx.core.Future;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.entities.system.SystemLocale;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.http.api.ApiControllerResource;
import org.pidome.server.services.http.api.response.ApiResponseCode;
import org.pidome.server.services.http.api.response.HttpStatusCodeException;
import org.pidome.server.services.network.NetInterface;
import org.pidome.server.services.network.NetworkService;
import org.pidome.server.services.network.broadcast.DiscoveryBroadcastMessage;
import org.pidome.server.services.network.broadcast.DiscoveryBroadcastService;
import org.pidome.server.services.network.broadcast.ServicesConfig;
import org.pidome.server.system.config.PiDomeLocale;
import org.pidome.server.system.database.DatabaseService;

/**
 * Controller for system API's.
 *
 * @author John Sirach
 */
@Path("system")
@Produces("application/json")
@Consumes("application/json")
public class SystemApiController extends ApiControllerResource {

    /**
     * Controller logger.
     */
    private static final Logger LOG = LogManager.getLogger(SystemApiController.class);

    /**
     * Sets a new locale.
     *
     * Following formats is supported:
     *
     * "default": Returns default locale.
     *
     * "language[_country[_variant]]": Returns Locale based on language,
     * language and country or language, country and variant.
     *
     * @DocumentationExample {"tag": "en_US"}
     *
     * @param locale String representing a new locale.
     * @throws HttpStatusCodeException On invalid locale.
     * @return The new set locale.
     */
    @PUT
    @Path("locale")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "When locale update succeeded."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_400, condition = "On unsupported locale.")
    })
    public final Locale setLocale(final SystemLocale locale) throws HttpStatusCodeException {
        try {
            return PiDomeLocale.setNewLocale(locale.getTag(), true);
        } catch (Exception ex) {
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_400, ex);
        }
    }

    /**
     * Returns the current system locale.
     *
     * @return the system locale.
     */
    @GET
    @Path("locale")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "When locale return succeeded.")
    })
    public final SystemLocale getLocale() {
        Locale locale = PiDomeLocale.getLocale();
        SystemLocale local = new SystemLocale();
        local.setTag(locale.toString());
        local.setCountry(locale.getDisplayName());
        return local;
    }

    /**
     * Returns the available system locales.
     *
     * @return the system locales.
     */
    @GET
    @Path("locales")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "When locale list return succeeded.")
    })
    public final List<SystemLocale> getLocales() {
        List<SystemLocale> localeList = new ArrayList<>();
        for (Locale locale : PiDomeLocale.getAvailableLocales()) {
            SystemLocale local = new SystemLocale();
            local.setTag(locale.toString());
            local.setCountry(locale.getDisplayName());
            localeList.add(local);
        }
        localeList.sort((c1, c2) -> c1.getCountry().compareTo(c2.getCountry()));
        return localeList;
    }

    /**
     * Returns a list of feasible network interfaces to be used with the system.
     *
     * When not interfaces are present the list is empty.
     *
     * @return Network interfaces list.
     * @throws HttpStatusCodeException On failing network service.
     */
    @GET
    @Path("network")
    @RolesAllowed("USER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "When returning a list of interfaces succeeded."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_503, condition = "When the service supplying network methods is not available.")
    })
    public final List<NetInterface> getNetworkInterfaces() throws HttpStatusCodeException {
        return getService(NetworkService.class)
                .orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_503))
                .getInterfaces();
    }

    /**
     * Check if the received broadcast is the broadcast originating from this
     * server.
     *
     * This method does break with the POST methodology, but provides
     * confirmation for the endpoint implementor.
     *
     * So, why would you use this. The ip and port are known, right? Well yes
     * and no. Services are able to live on other ports than which you are used
     * to. Also when there are multiple services running this helps in
     * identifying the correct server.
     *
     * @param message The message received by the broadcast.
     * @return Boolean if the message is originated from this server.
     * @throws HttpStatusCodeException When the broadcast service is not
     * available.
     */
    @POST
    @Path("discovery")
    @RolesAllowed("USER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "When the service configuration is able to be returned"),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_400, condition = "When the message is invalid."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_500, condition = "When a system error occurs."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_503, condition = "When there is no discovery service running")
    })
    public final ServicesConfig serviceDiscovery(final DiscoveryBroadcastMessage message) throws HttpStatusCodeException {
        DiscoveryBroadcastMessage original = ServiceHandler.getInstance().getService(DiscoveryBroadcastService.class)
                .orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_503))
                .getBroadcastMessage();
        if (original.getP() == message.getP() && original.getS().equals(message.getS())) {
            try {
                ServicesConfig servicesConfig = new ServicesConfig();
                servicesConfig.setDebugPort(0);
                servicesConfig.setHttpPort(original.getP());
                servicesConfig.setMqttPort(0);
                servicesConfig.setApiLocation(original.getL());
                servicesConfig.setEventsLocation("/events");
                servicesConfig.setServerAddress(InetAddress.getByName(original.getS()));
                return servicesConfig;
            } catch (UnknownHostException ex) {
                LOG.error("Host not found", ex);
                throw new HttpStatusCodeException(ApiResponseCode.HTTP_400, "Server address seems invalid [" + ex.getMessage() + "]", ex);
            }
        }
        throw new HttpStatusCodeException(ApiResponseCode.HTTP_400, "Invalid message");
    }

    /**
     * Returns a list of feasible network interfaces to be used with the system.
     *
     * When not interfaces are present the list is empty.
     *
     * @return Network interfaces list.
     * @throws HttpStatusCodeException On failing network service.
     */
    @GET
    @Path("export/database")
    @RolesAllowed("ADMIN")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "On succesfull export"),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_500, condition = "On a failing export."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_503, condition = "When the database service is not available")
    })
    public final Future<Void> performExport() throws HttpStatusCodeException {
        return getService(DatabaseService.class)
                .orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_503, "Database services not available"))
                .runFullExport();
    }

}
