/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.items;

import org.pidome.server.services.events.EventAddress;
import org.pidome.server.services.events.EventHandler;

/**
 * An hardware event.
 *
 * @author johns
 */
public class ItemEventHandler extends EventHandler<ItemEventBody> {

    /**
     * Constructor.
     */
    public ItemEventHandler() {
        super(EventAddress.ITEMS);
    }

    /**
     * Returns the event.
     *
     * @return The event.
     */
    public ItemEventBody getEvent() {
        return this.getBody();
    }

}
