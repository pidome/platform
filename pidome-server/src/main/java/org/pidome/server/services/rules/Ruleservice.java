/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.rules;

import io.vertx.core.Promise;
import org.pidome.server.services.AbstractService;

/**
 * Service providing rules.
 *
 * @author johns
 */
public class Ruleservice extends AbstractService {

    /**
     * Starts the service.
     *
     * @param startPromise Promise to complete or fail the service start.
     */
    @Override
    public void start(final Promise<Void> startPromise) {
        startPromise.complete();
    }

    /**
     * Stops the service.
     *
     * @param stopPromise Promise to complete or fail the service stop.
     */
    @Override
    public void stop(final Promise<Void> stopPromise) {
        stopPromise.complete();
    }

}
