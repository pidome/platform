/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.controllers;

import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;
import io.vertx.core.http.HttpServerResponse;
import java.util.Objects;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.entities.users.UserLogin;
import org.pidome.server.entities.users.person.Person;
import org.pidome.server.services.http.api.ApiControllerResource;
import org.pidome.server.services.http.api.response.ApiResponseCode;
import org.pidome.server.services.http.api.response.HttpStatusCodeException;
import org.pidome.server.system.database.Database;

/**
 * Controller for the person API.
 *
 * @author John Sirach
 * @todo Add the person service and finish the controller endpoint.
 */
@Path("person")
@Produces("application/json")
@Consumes("application/json")
public class PersonApiController extends ApiControllerResource {

    /**
     * Logger for the person rest api.
     */
    private static final Logger LOG = LogManager.getLogger(PersonApiController.class);

    /**
     * Returns the person identified by the user logged in.
     *
     * @param response The response object.
     * @param user The user logged in, if logged in.
     * @return The Person of the user logged in.
     * @throws HttpStatusCodeException On error of retrieving self.
     */
    @GET
    @RolesAllowed("USER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "When self is returned.")
    })
    public final Person getMe(final @Context HttpServerResponse response, final @Context UserLogin user) throws HttpStatusCodeException {
        if (user == null) {
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_401);
        }
        return user.getPerson();
    }

    /**
     * Adds a {@link org.pidome.server.entities.users.UserLogin} to the system.
     *
     * @param person The Person to add.
     * @return The Person id of the added user.
     */
    @POST
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "When person is added."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_403, condition = "When not authorized."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_500, condition = "When authentication fails due to system reasons.")
    })
    public final Integer addPerson(final Person person) {
        return 1;
    }

    /**
     * Updates the person logged in.
     *
     * @param person The person data to update.
     * @param user The user logged in.
     * @return The response containing the updated person or error message.
     * @throws HttpStatusCodeException On retrieval failure.
     */
    @PATCH
    @RolesAllowed("USER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "When succesfully updated. The Person updated is returned"),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_401, condition = "When trying to update a person which is not the requester"),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When no person object is found for this user."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_500, condition = "When there is an error in the backend system")
    })
    public final Person updatePerson(final Person person, final @Context UserLogin user) throws HttpStatusCodeException {
        try {
            if (Objects.equals(person.getId(), user.getPerson().getId())) {
                try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
                    autoManager.getManager().getTransaction().begin();
                    autoManager.getManager().merge(person);
                    autoManager.getManager().getTransaction().commit();
                }
            } else {
                throw new HttpStatusCodeException(ApiResponseCode.HTTP_401);
            }
        } catch (Exception ex) {
            LOG.error("Database error occured [{}]", ex.getMessage(), ex);
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_500, ex);
        }
        return person;
    }

    /**
     * Returns a Person by id.
     *
     * @param id The id of the Person to retrieve.
     * @return The Person requested.
     * @throws HttpStatusCodeException on sql error.
     */
    @GET
    @Path("{id}")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "When returning a person succeeded."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_403, condition = "When not authorized."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_500, condition = "When authentication fails due to system reasons.")
    })
    public final Person getPerson(final @PathParam("id") Integer id) throws HttpStatusCodeException {
        try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            return autoManager.getManager().find(Person.class, id);
        } catch (Exception ex) {
            LOG.error("Unable get person for id [{}]: [{}]", id, ex.getMessage());
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_500, "Server error", ex);
        }
    }

    /**
     * Deletes a Person by id.
     *
     * @param id The id of the Person to delete.
     * @return true when deleted.
     */
    @DELETE
    @Path("{id}")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "When deletion succeeded."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_403, condition = "When not authorized."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_500, condition = "When authentication fails due to system reasons.")
    })
    public final boolean deletePerson(final @PathParam("id") Integer id) {
        return true;
    }

}
