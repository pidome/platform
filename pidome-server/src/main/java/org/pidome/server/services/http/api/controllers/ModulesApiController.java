/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.controllers;

import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;
import com.webcohesion.enunciate.metadata.rs.TypeHint;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.hardware.driver.Transport;
import org.pidome.platform.modules.ModuleCapabilities;
import org.pidome.platform.modules.ModuleType;
import org.pidome.platform.presentation.Presentation;
import org.pidome.server.entities.modules.DiscoveryPeriodOptions;
import org.pidome.server.services.http.api.ApiControllerResource;
import org.pidome.server.services.http.api.response.ApiResponseCode;
import org.pidome.server.services.http.api.response.HttpStatusCodeException;
import org.pidome.server.services.modules.DiscoveryException;
import org.pidome.server.services.modules.DiscoveryPeriod;
import org.pidome.server.services.modules.ModuleDefinition;
import org.pidome.server.services.modules.ModuleException;
import org.pidome.server.services.modules.ModuleLoaderConfiguration;
import org.pidome.server.services.modules.ModuleService;
import org.pidome.server.system.modules.ActiveModuleContainer;

/**
 * Controller for controlling and maintaining the configurations, adding and
 * removing of modules on the system.
 *
 * @author johns
 */
@Path("modules")
@Produces("application/json")
@Consumes("application/json")
@SuppressWarnings("CPD-START")
public class ModulesApiController extends ApiControllerResource {

    /**
     * Controller logger.
     */
    private static final Logger LOG = LogManager.getLogger(InstallerApiController.class);

    /**
     * No available module service literal.
     */
    private static final String MODULE_SERVICE_UNAVAILABLE = "Module service is not available";

    /**
     * Returns a list of all available modules.
     *
     * @return List of modules.
     * @throws HttpStatusCodeException On service failure.
     */
    @GET
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "Successfully returns the modules.")
    })
    public Map<ModuleType, List<ModuleDefinition>> getModules() throws HttpStatusCodeException {
        return getModuleService()
                .getAllModules();
    }

    /**
     * Returns a list of all currently active modules.
     *
     * @return List of active modules.
     * @throws HttpStatusCodeException See API status codes.
     */
    @GET
    @Path("active")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "Successfully returns the modules.")
    })
    public List<ActiveModuleContainer> getActiveModules() throws HttpStatusCodeException {
        return getModuleService()
                .getActiveModules();
    }

    /**
     * Returns a list of all available modules of the given type.
     *
     * @param type The type of modules.
     * @return List of modules of the given type.
     * @throws HttpStatusCodeException See API status codes.
     */
    @GET
    @Path("type/{type}")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "Successfully returns the modules."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "If the module type does not exist.")
    })
    public List<ModuleDefinition> getModules(final @PathParam("type") ModuleType type) throws HttpStatusCodeException {
        return getModuleService()
                .getModulesByType(type)
                .orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_404));
    }

    /**
     * Returns a list of all available modules of the given type applicable to a
     * specific transport type.
     *
     * If the target is set, the list returned will be filtered with the
     * supplied target. Exclusive modules which do not target the given target
     * parameter are excluded. Modules not configured with targets and/or target
     * exclusivity are returned. This method fails silently as if the filter
     * content does not exist.
     *
     * @param type The type of modules.
     * @param transport The transport type.
     * @param target The target for exclusivity, may be omitted.
     * @return List of modules of the given type.
     * @throws HttpStatusCodeException See API status codes.
     */
    @GET
    @Path("type/{type}/{transport}")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "Successfully returns the modules, even empty lists")
    })
    public List<ModuleDefinition> getModules(
            final @PathParam("type") ModuleType type,
            final @PathParam("transport") Transport.SubSystem transport,
            final @QueryParam("target") String target) throws HttpStatusCodeException {
        return getModuleService()
                .getModulesByType(type, transport)
                .map(mapper -> mapper.stream()
                .filter(module -> {
                    if (target != null && module.getTargets() != null) {
                        if (module.getTargets().contains(target)) {
                            /// If the module target equals the given target, include it in the list.
                            return true;
                        } else if (module.isTargetExclusive()) {
                            /// If the module does not meet the target and is set exclusive for the configured target not met, exclude the module.
                            return false;
                        }
                    }
                    /// Include a module which has no target configuration, or if no target is set.
                    return true;
                })
                .collect(Collectors.toList()))
                .orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_404));
    }

    /**
     * Starts a module with the given configuration.
     *
     * @param configuration The configuration.
     * @return Future.
     * @throws HttpStatusCodeException On failure in starting a module.
     */
    @POST
    @Path("module/start")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "Successfully starts the module."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_400, condition = "When faulty data is given."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_500, condition = "On server error attempting to start a module.")
    })
    @TypeHint(value = TypeHint.NONE.class)
    public Future startModule(final ModuleLoaderConfiguration configuration) throws HttpStatusCodeException {
        ModuleService moduleService = getModuleService();
        RestTools.assertNotNull(configuration.getModuleId(), ApiResponseCode.HTTP_400, "Missing module");
        Promise<Void> startPromise = Promise.promise();
        moduleService.startModule(configuration).setHandler(result -> {
            if (result.succeeded()) {
                startPromise.complete();
            } else {
                startPromise.fail(result.cause());
            }
        });
        return startPromise.future();
    }

    /**
     * Returns module information on a running module.
     *
     * @param activeContainerId The definition id of a container with a live
     * running module.
     * @return A presentation with module info.
     * @throws HttpStatusCodeException On service error.
     */
    @GET
    @Path("module/{activeContainerId}/info")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "On returning a presentation with module info."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When there is no module container with the given id.")
    })
    public Presentation getModuleInfo(final @PathParam("activeContainerId") String activeContainerId) throws HttpStatusCodeException {
        try {
            return getModuleService()
                    .getActiveModule(activeContainerId)
                    .getModuleInfo();
        } catch (ModuleException ex) {
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_404, ex.getMessage(), ex);
        }
    }

    /**
     * Returns module information on a running module.
     *
     * An empty list can be returned when a module with the given capabilities
     * is not available.
     *
     * @param capabilities The capabilities requested.
     * @return A presentation with module info.
     * @throws HttpStatusCodeException On service error.
     */
    @GET
    @Path("capabilities")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "On returning a presentation with module info."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_400, condition = "When a/the given capabilitie(s) does not exist")
    })
    public List<ActiveModuleContainer> getModulesWithCapabilities(
            final @QueryParam("list") String capabilities) throws HttpStatusCodeException {
        try {
            List<ModuleCapabilities> listCapab = new ArrayList<>(Arrays.asList(capabilities.trim().split(",")))
                    .stream().map(text -> ModuleCapabilities.valueOf(text))
                    .collect(Collectors.toList());
            return getModuleService()
                    .getModulesWithCapabilities(listCapab);
        } catch (Exception ex) {
            LOG.warn("Unable to gather with given capabilitie(s): {}", capabilities, ex);
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_400, ex);
        }
    }

    /**
     * Stops a module by the given module id.
     *
     * @param activeContainerId The definition id of a container with a live
     * running module.
     * @return Future indicating a module has been stopped or exception occurred
     * within stopping.
     * @throws HttpStatusCodeException On service error.
     */
    @GET
    @Path("module/{activeContainerId}/stop")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "Successfully stops the module."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_500, condition = "When a module fails to stop because of server error.")
    })
    @TypeHint(value = TypeHint.NONE.class)
    public Future stopModule(final @PathParam("activeContainerId") String activeContainerId) throws HttpStatusCodeException {
        return getModuleService()
                .stopModule(activeContainerId);
    }

    /**
     * Returns options to be used for discovery for an active module.
     *
     * @return The possible discovery options.
     */
    @GET
    @Path("discovery/options")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "On returning the options.")
    })
    public DiscoveryPeriodOptions getDiscoveryOptions() {
        return new DiscoveryPeriodOptions();
    }

    /**
     * Returns options to be used for discovery for an active module.
     *
     * @param activeContainerId The container on which discovery should be
     * enabled.
     * @param period The period to enable.
     * @throws HttpStatusCodeException On failure to start discovery.
     */
    @PUT
    @Path("module/{activeContainerId}/discovery/start")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "On succesfully activate discovery."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_409, condition = "When the module does not support discovery."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_500, condition = "When all data is correct but a server error occurs.")
    })
    public void activateDiscovery(final @PathParam("activeContainerId") String activeContainerId, final DiscoveryPeriod period) throws HttpStatusCodeException {
        try {
            getModuleService()
                    .getActiveModuleContainer(activeContainerId)
                    .orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_404))
                    .startDiscovery(period);
        } catch (DiscoveryException ex) {
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_409, ex.getMessage(), ex);
        }
    }

    /**
     * Returns options to be used for discovery for an active module.
     *
     * This method fails silently when a module does not support discovery.
     *
     * @param activeContainerId The container on which discovery should be
     * cancelled.
     * @throws HttpStatusCodeException On failure to start discovery.
     */
    @GET
    @Path("module/{activeContainerId}/discovery/stop")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "On succesfully activate discovery."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the module is not found.")
    })
    public void cancelDiscovery(final @PathParam("activeContainerId") String activeContainerId) throws HttpStatusCodeException {
        getModuleService()
                .getActiveModuleContainer(activeContainerId)
                .orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_404, "Not found"))
                .cancelDiscovery();
    }

    /**
     * Returns the module service when available.
     *
     * @return The module service.
     * @throws HttpStatusCodeException When the module service is not available.
     */
    private ModuleService getModuleService() throws HttpStatusCodeException {
        return this.getService(ModuleService.class)
                .orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_503, MODULE_SERVICE_UNAVAILABLE));
    }

}
