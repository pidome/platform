/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.items;

import java.util.Set;
import org.pidome.server.entities.items.DiscoveredItemDefinition;

/**
 * An items mutation object.
 *
 * @author johns
 */
public final class ItemsDiscoveryMutationEvent {

    /**
     * The mutation type.
     */
    private final ItemsMutationEvent.MutationType type;

    /**
     * The items in the event.
     */
    private final Set<DiscoveredItemDefinition> items;

    /**
     * Event constructor.
     *
     * @param mutationType the mutation type.
     * @param mutationItems The items to be included in this event.
     */
    public ItemsDiscoveryMutationEvent(final ItemsMutationEvent.MutationType mutationType, final Set<DiscoveredItemDefinition> mutationItems) {
        this.items = mutationItems;
        this.type = mutationType;
    }

    /**
     * Return the mutation type.
     *
     * @return The mutation type.
     */
    public ItemsMutationEvent.MutationType getType() {
        return this.type;
    }

    /**
     * Returns the items in the event.
     *
     * @return The items.
     */
    public Set<DiscoveredItemDefinition> getItems() {
        return this.items;
    }

}
