/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.modules;

import java.util.List;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import static org.bouncycastle.asn1.x500.style.RFC4519Style.description;
import org.pidome.platform.hardware.driver.Transport;
import org.pidome.platform.modules.ModuleType;
import org.pidome.server.services.definitions.GenericMetaDefinition;

/**
 * This class provides a module candidate for which a configuration can be
 * loaded.
 *
 * @author John Sirach
 */
@Entity
public class ModuleDefinition extends GenericMetaDefinition {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The server package providing this module.
     */
    private UUID providedBy;

    /**
     * The class for the module.
     */
    private String moduleClass;

    /**
     * The module type.
     */
    @Enumerated(EnumType.STRING)
    private ModuleType moduleType;

    /**
     * Targets a specific entity.
     *
     * @todo Change to local field instead of local table.
     */
    @ElementCollection(fetch = FetchType.EAGER)
    @Column(columnDefinition = "varchar")
    private List<String> targets;

    /**
     * If the module is exclusive for the given targets.
     */
    private boolean targetExclusive;

    /**
     * If the module is a preferred module.
     *
     * This is always false, unless a request is done to retrieve a preferred
     * module.
     */
    private boolean preferred;

    /**
     * The transport target.
     */
    private Transport.SubSystem transportTarget;

    /**
     * @return the module.
     */
    public String getModuleClass() {
        return moduleClass;
    }

    /**
     * @param moduleClass the module class to set
     */
    public void setModuleClass(final String moduleClass) {
        this.moduleClass = moduleClass;
    }

    /**
     * @return the providedBy
     */
    public UUID getProvidedBy() {
        return providedBy;
    }

    /**
     * @param providedBy the providedBy to set
     */
    public void setProvidedBy(final UUID providedBy) {
        this.providedBy = providedBy;
    }

    /**
     * The module type.
     *
     * @return the moduleType
     */
    public ModuleType getModuleType() {
        return moduleType;
    }

    /**
     * The module type.
     *
     * @param moduleType the moduleType to set
     */
    public void setModuleType(final ModuleType moduleType) {
        this.moduleType = moduleType;
    }

    /**
     * If the module is a preferred module.
     *
     * @param preferred the preferred to set
     */
    public void setPreferred(final boolean preferred) {
        this.preferred = preferred;
    }

    /**
     * If the module is a preferred module.
     *
     * This is always false, unless a request is done to retrieve a preferred
     * module.
     *
     * @return the preferred
     */
    public boolean isPreferred() {
        return preferred;
    }

    /**
     * Targets a specific entity.
     *
     * @return the targets
     */
    public List<String> getTargets() {
        return targets;
    }

    /**
     * Targets a specific entity.
     *
     * @param targets the targets to set
     */
    public void setTargets(final List<String> targets) {
        this.targets = targets;
    }

    /**
     * If the module is exclusive for the given targets.
     *
     * @return the targetExclusive
     */
    public boolean isTargetExclusive() {
        return targetExclusive;
    }

    /**
     * If the module is exclusive for the given targets.
     *
     * @param targetExclusive the targetExclusive to set
     */
    public void setTargetExclusive(final boolean targetExclusive) {
        this.targetExclusive = targetExclusive;
    }

    /**
     * The transport target.
     *
     * @return the transportTarget
     */
    public Transport.SubSystem getTransportTarget() {
        return transportTarget;
    }

    /**
     * The transport target.
     *
     * @param transportTarget the transportTarget to set
     */
    public void setTransportTarget(final Transport.SubSystem transportTarget) {
        this.transportTarget = transportTarget;
    }

    /**
     * String representation of this class.
     *
     * @return This class as String.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("ModuleCandidate:[");
        builder.append("name:").append(this.getName()).append(",");
        builder.append("description:").append(description).append(",");
        builder.append("module:").append(moduleClass).append("]");
        return builder.toString();
    }

    /**
     * Hash code generation.
     *
     * @return The hash code.
     */
    @Override
    @SuppressWarnings("CPD-START")
    public int hashCode() {
        return super.hashCode();
    }

    /**
     * Definition equality check.
     *
     * @param obj The object to check against.
     * @return true when equal.
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return super.equals(obj);
    }

}
