/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.presentation;

import org.pidome.platform.presentation.input.InputForm;

/**
 * Container for forms for the frontend.
 *
 * The target is implementation specific. When a <code>PresentationForm</code>
 * is communicated with the outside world it is expected to be returned as the
 * PresentationForm.
 *
 * @author johns
 */
public class SubmittedForm extends InputForm {

}
