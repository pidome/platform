/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.ServerWebSocket;
import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.services.authentication.PidomeAuthUser;
import static org.pidome.server.services.events.EventAddress.WEBSOCKET;
import org.pidome.server.services.events.EventHandler;
import org.pidome.server.services.events.EventMessageHeaders;
import org.pidome.server.system.config.SystemConfig;
import org.pidome.tools.utilities.Serialization;

/**
 * Class responsible to produce message to be pushed onto a websocket.
 *
 * This class is general purpose in handling JsonObjects.
 *
 * @author johns
 */
public class WebSocketProsumer extends EventHandler<String> {

    /**
     * The logger.
     */
    private static final Logger LOG = LogManager.getLogger(WebSocketProsumer.class);

    /**
     * The websocket for the user.
     */
    private final ServerWebSocket websocket;

    /**
     * the user of the websocket.
     */
    private final PidomeAuthUser user;

    /**
     * the user token.
     */
    private final String token;

    /**
     * An user based websocket consumer.
     *
     * @param websocket The websocket.
     * @param user The user.
     */
    protected WebSocketProsumer(final ServerWebSocket websocket, final PidomeAuthUser user) {
        super(WEBSOCKET);
        this.websocket = websocket;
        this.user = user;
        this.token = HttpService.getTokenFromWebsocket(websocket);
    }

    /**
     * Returns the prosumer user.
     *
     * @return The user of this connection.
     */
    protected final PidomeAuthUser getUser() {
        return this.user;
    }

    /**
     * Returns the token on this socket.
     *
     * @return The token on the socket.
     */
    protected final String getToken() {
        return this.token;
    }

    /**
     * Handles incoming messages and if these should be broadcasted on the
     * message bus.
     *
     * @param headers The headers to handle.
     * @param eventMessage The message to handle.
     */
    @Override
    public void handleMessage(final EventMessageHeaders headers, final Message<String> eventMessage) {
        headers.getTargetUid().ifPresentOrElse(uid -> {
            user.isStrictAuthorized(headers.getMinimumRoleTarget().name(), uid, authorized -> {
                if (authorized.succeeded()) {
                    postToSocket(constructTransferObject(headers, eventMessage.body()));
                }
            });
        }, () -> {
            user.isAuthorized(headers.getMinimumRoleTarget().name(), authorized -> {
                if (authorized.succeeded()) {
                    postToSocket(constructTransferObject(headers, eventMessage.body()));
                }
            });
        });
    }

    /**
     * Creates the message to be send over the websocket.
     *
     * @param headers The headers to send.
     * @param body The body to send.
     * @return The websocket notification.
     */
    private WebocketNotification constructTransferObject(final EventMessageHeaders headers, final Object body) {
        WebocketNotification notification = new WebocketNotification();
        notification.setHeaders(headers);
        notification.setMessage(headers.getMessage());
        notification.setBody(body);
        return notification;
    }

    /**
     * Posts the actual message to the websocket.
     *
     * @param message The message to post.
     */
    protected void postToSocket(final WebocketNotification message) {
        if (!this.websocket.isClosed()) {
            ObjectMapper mapper = Serialization.getDefaultObjectMapper();
            try {
                try {
                    if (SystemConfig.isDevMode()) {
                        this.websocket.writeFinalTextFrame(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(message));
                    }
                } catch (IOException ex) {
                    LOG.warn("Unable to write [{}] pretty to the websocket", message, ex);
                    this.websocket.writeFinalTextFrame(mapper.writeValueAsString(message));
                }
            } catch (JsonProcessingException ex) {
                LOG.error("Unable to write [{}] to the websocket at all", message, ex);
            }
        }
    }

    /**
     * Closes the socket connected to this prosumer.
     */
    protected void closeSocket() {
        new Thread(() -> {
            websocket.close();
        }).start();
    }

    /**
     * Class used for exposing notification data on the websocket.
     */
    protected static class WebocketNotification {

        /**
         * the event headers.
         */
        private EventMessageHeaders headers;

        /**
         * the event body.
         */
        private Object body;

        /**
         * The message in the event.
         */
        private String message;

        /**
         * the event headers.
         *
         * @return the headers
         */
        public EventMessageHeaders getHeaders() {
            return headers;
        }

        /**
         * the event headers.
         *
         * @param headers the headers to set
         */
        public void setHeaders(final EventMessageHeaders headers) {
            this.headers = headers;
            this.setMessage(headers.getMessage());
        }

        /**
         * the event body.
         *
         * @return the body
         */
        public Object getBody() {
            return body;
        }

        /**
         * the event body.
         *
         * @param body the body to set
         */
        public void setBody(final Object body) {
            this.body = body;
        }

        /**
         * The message in the event.
         *
         * @return the message
         */
        public String getMessage() {
            return message;
        }

        /**
         * The message in the event.
         *
         * @param message the message to set
         */
        public void setMessage(final String message) {
            this.message = message;
        }

    }

}
