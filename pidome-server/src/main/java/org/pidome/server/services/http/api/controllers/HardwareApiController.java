/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;
import com.webcohesion.enunciate.metadata.rs.TypeHint;
import com.webcohesion.enunciate.metadata.rs.TypeHint.NONE;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import org.pidome.platform.hardware.driver.HardwareDriver;
import org.pidome.platform.hardware.driver.Transport;
import org.pidome.platform.presentation.Presentation;
import org.pidome.platform.presentation.input.InputForm;
import org.pidome.server.services.hardware.DriverDefinition;
import org.pidome.server.services.hardware.HardwareService;
import org.pidome.server.services.http.api.ApiControllerResource;
import org.pidome.server.services.http.api.response.ApiResponseCode;
import org.pidome.server.services.http.api.response.HttpStatusCodeException;
import org.pidome.server.system.hardware.HardwareComponent;
import org.pidome.server.system.hardware.Peripheral;

/**
 * Controller for controlling and maintaining the configurations, adding and
 * removing of peripherals to the system.
 *
 * @author John Sirach
 * @since 1.0
 */
@Path("hardware")
@Produces("application/json")
@Consumes("application/json")
@SuppressWarnings("CPD-START")
public final class HardwareApiController extends ApiControllerResource {

    /**
     * Not found literal.
     */
    private static final String NOT_FOUND = "Not found";

    /**
     * Returns a list of connected peripherals.
     *
     * @return List of connected peripherals.
     * @throws HttpStatusCodeException On hardware service failure.
     */
    @GET
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "Succesfully returns the hardware.")
    })
    public Map<HardwareComponent.Interface, HardwareComponent> getHardware() throws HttpStatusCodeException {
        return getHardwareService().getAllHardware();
    }

    /**
     * Returns a list of connected peripherals.
     *
     * @param interfaceKey The key of the hardware component to fetch.
     * @return The hardware component.
     * @throws HttpStatusCodeException When the component is not found.
     */
    @GET
    @Path("interface/{interface}")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "On returning existing interface."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the interface is not found.")
    })
    public HardwareComponent getHardwareComponent(final @PathParam("interface") HardwareComponent.Interface interfaceKey) throws HttpStatusCodeException {
        return getHardwareService().getHardwareOnInterface(interfaceKey)
                .orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_404, NOT_FOUND));
    }

    /**
     * Returns a piece of hardware attached to the given port.
     *
     * PiDome uses custom mapping to hardware ports. This way there is a generic
     * access to any connected device. Before this method is called you need to
     * find out on which virtual port a device is connected by calling
     * <code>getPeripherals</code>
     *
     * @param interfaceKey The interface to retrieve the hardware device from.
     * @param key The device key, must be send url encoded as it may contain
     * characters that must be encoded.
     * @return The piece of hardware
     * @throws HttpStatusCodeException When there is no piece of hardware
     * connected on that port.
     */
    @GET
    @Path("interface/{interface}/{key}")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "On returning existing peripheral on the given interface."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the peripheral or interface is not found.")
    })
    public Peripheral<? extends HardwareDriver> getPeripheral(final @PathParam("interface") HardwareComponent.Interface interfaceKey,
            final @PathParam("key") String key) throws HttpStatusCodeException {
        return getHardwareService().getHardwareOnInterface(interfaceKey)
                .orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_404, NOT_FOUND))
                .getPeripheralByKey(key).orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_404, NOT_FOUND));
    }

    /**
     * Returns statistics for a piece of hardware connected and with a driver
     * active.
     *
     * @param volatilePeripheralId The id of the peripheral to gather the
     * statistics from.
     * @return The piece of hardware
     * @throws HttpStatusCodeException When there is no piece of hardware
     * connected on that port or no driver is active to supply the data.
     */
    @GET
    @Path("peripheral/{volatilePeripheralId}/stats")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "On returning existing peripheral on the given interface."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the peripheral or interface is not found."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_400, condition = "When there is no driver active to be able to supply the data.")
    })
    public Presentation getPeripheralDataStats(
            final @PathParam("volatilePeripheralId") UUID volatilePeripheralId) throws HttpStatusCodeException {
        Peripheral<? extends HardwareDriver> peripheral = getPeripheral(volatilePeripheralId);
        if (peripheral.getDriver() != null) {
            return peripheral.getDriver().getDataInfo();
        } else {
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_400, "No driver active");
        }
    }

    /**
     * Returns the list of hardware peripheral driver candidates which are
     * suitable for the given device.
     *
     * @param volatilePeripheralId The id of the peripheral to get possible
     * drivers for.
     * @return List if hardware drivers capable to service the given key.
     * @throws HttpStatusCodeException When the interface or peripheral is
     * unknown.
     */
    @GET
    @Path("peripheral/{volatilePeripheralId}/drivers")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "On returning a list of drivers capapable of driving the peripheral"),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the interface, peripheral is not found.")
    })
    public List<DriverDefinition> getPeripheralDriverCandidates(
            final @PathParam("volatilePeripheralId") UUID volatilePeripheralId) throws HttpStatusCodeException {
        return getHardwareService().getDriverCollection(
                getPeripheral(volatilePeripheralId).getSubSystem()
        );
    }

    /**
     * Returns a list of hardware peripherals based on the given transport type.
     *
     * The peripheral target is an arbitrary string which makes the method only
     * to return the peripherals which equal the target.
     * <p>
     * Example:
     * <p>
     * Devices on USB have a VID&amp;PID. When a target of VID_0001&amp;PID_0001
     * is given, only devices with these Vendor and Product id are returned.
     * Peripheral targets must be send urlencoded and each target is delimited
     * by a comma (,). Example:
     * <p>
     * <code>?targets=VID_0001&amp;PID_0001,VID_0001&amp;PID_0002</code>
     *
     *
     * Peripheral targets are provided by modules. Look at
     * <code>ModuleDefinition</code> for the target options. Modules are often
     * written for specific targets, so their target definitions are often
     * trustworthy to target the correct peripheral, if a target does not
     * correctly filter, contact the module author.
     *
     * @param transport The transport type to get the peripherals for.
     * @param peripheralTarget The peripheral target, may be omitted.
     * @return List of peripherals of the given transport type.
     * @throws HttpStatusCodeException When the given transport is unknown.
     */
    @GET
    @Path("type/{transport}")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "On returning a list of peripherals of the given transport type"),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_503, condition = "When the hardware service is not available")
    })
    public List<Peripheral<? extends HardwareDriver>> getPeripheralsByTransportType(
            final @PathParam("transport") Transport.SubSystem transport,
            final @QueryParam("targets") String peripheralTarget) throws HttpStatusCodeException {
        List<Peripheral<? extends HardwareDriver>> list = getHardwareService().getHardwarePeripheralsOfType(transport);
        if (peripheralTarget != null && !peripheralTarget.isBlank()) {
            return list.stream()
                    .filter(peripheral -> {
                        return Arrays.asList(peripheralTarget.split(",")).stream().anyMatch(
                                target -> ("VID_" + peripheral.getVendorId() + "&PID_" + peripheral.getProductId()).equals(target)
                        );
                    })
                    .collect(Collectors.toList());
        } else {
            return list;
        }
    }

    /**
     * Returns an input form for the given device for the given driver to be
     * intended to be used.
     *
     * If a device is live (in use) code 405 is returned. If a device needs to
     * be reconfigured please first disconnect using
     * <code>{interface}/{key}/stop</code>
     *
     * @param volatilePeripheralId The id of the peripheral attached.
     * @param driverDefinitionId The id of the driver definition.
     * @return The settings for the given candidate as <code>InputForm</code>.
     * @throws HttpStatusCodeException On server error.
     */
    @GET
    @Path("peripheral/{volatilePeripheralId}/settings/{driverDefinitionId}")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "On returning a presentation of type InputForm for settings purposes."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the peripheral or driver is not found."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "Settings form corruption.")
    })
    public InputForm getPeripheralSettings(
            final @PathParam("volatilePeripheralId") UUID volatilePeripheralId,
            final @PathParam("driverDefinitionId") UUID driverDefinitionId) throws HttpStatusCodeException {
        try {
            HardwareService service = getHardwareService();
            Peripheral<? extends HardwareDriver> peripheral = getPeripheral(volatilePeripheralId);
            if (peripheral.getDriver() != null) {
                return peripheral.getDriver().getConfiguration();
            } else {
                DriverDefinition driverDefinition = service.getDriverDefinitionById(driverDefinitionId)
                        .orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_404, "Driver not found"));
                return service.getConfiguration(peripheral, driverDefinition);
            }
        } catch (Exception ex) {
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_500, ex);
        }
    }

    /**
     * Put a configuration setting on a given peripheral.
     *
     * If a device is live (in use) code 405 is returned.If a device needs to be
     * reconfigured please first disconnect using
     * <code>peripheral/{volatilePeripheralId}/stop</code>
     *
     * @param volatilePeripheralId The id of the peripheral attached.
     * @param driverDefinitionId The id of the driver definition.
     * @param configuration The inputform supplied by
     * <code>peripheral/{volatilePeripheralId}/settings/{driverDefinitionId}</code>
     * with the values set as a <code>JsonObject</code>.
     * @return Future to be handled by the REST handler.
     * @throws HttpStatusCodeException On peripheral or server error.
     */
    @PUT
    @Path("peripheral/{volatilePeripheralId}/start/{driverDefinitionId}")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "When the driver is started."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the interface and/or peripheral is not found."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_409, condition = "When the device is live which does not allow it to be reconfigured")
    })
    @TypeHint(value = NONE.class)
    public Future startPeripheralWithSettings(
            final @PathParam("volatilePeripheralId") UUID volatilePeripheralId,
            final @PathParam("driverDefinitionId") UUID driverDefinitionId,
            final @TypeHint(value = InputForm.class) JsonNode configuration) throws HttpStatusCodeException {
        HardwareService service = getHardwareService();
        Peripheral<? extends HardwareDriver> peripheral = getPeripheral(volatilePeripheralId);
        if (peripheral.getDriver() != null) {
            throw new HttpStatusCodeException(ApiResponseCode.HTTP_409, "Device is in use");
        } else {
            DriverDefinition driverDefinition = service.getDriverDefinitionById(driverDefinitionId)
                    .orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_404, "Driver not found"));

            Promise<Void> resultPromise = Promise.promise();
            service.startDriver(peripheral, driverDefinition, configuration).setHandler(result -> {
                if (result.succeeded()) {
                    resultPromise.complete();
                } else {
                    resultPromise.fail(result.cause());
                }
            });
            return resultPromise.future();
        }
    }

    /**
     * Stops the driver on the peripheral.
     *
     * @param volatilePeripheralId The peripheral id.
     * @return Stop result.
     * @throws HttpStatusCodeException When stopping fails.
     */
    @GET
    @Path("peripheral/{volatilePeripheralId}/stop")
    @RolesAllowed("POWER")
    @StatusCodes({
        @ResponseCode(code = ApiResponseCode.Code.HTTP_200, condition = "When the driver is stopped."),
        @ResponseCode(code = ApiResponseCode.Code.HTTP_404, condition = "When the interface and/or peripheral is not found.")
    })
    @TypeHint(value = NONE.class)
    public Future stopPeripheralDriver(
            final @PathParam("volatilePeripheralId") UUID volatilePeripheralId) throws HttpStatusCodeException {
        HardwareService service = getHardwareService();
        Peripheral<? extends HardwareDriver> peripheral = getPeripheral(volatilePeripheralId);
        Promise<Void> resultPromise = Promise.promise();
        service.stopDriver(peripheral).setHandler(result -> {
            if (result.succeeded()) {
                resultPromise.complete();
            } else {
                resultPromise.fail(result.cause());
            }
        });
        return resultPromise.future();
    }

    /**
     * Returns a peripheral bases on the given volatile id.
     *
     * @param volatilePeripheralId The peripheral id.
     * @return The peripheral requested.
     * @throws HttpStatusCodeException On service unavailability or non existing
     * peripheral.
     */
    private Peripheral<? extends HardwareDriver> getPeripheral(final UUID volatilePeripheralId) throws HttpStatusCodeException {
        return getHardwareService().getPeripheralById(volatilePeripheralId)
                .orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_404, "Peripheral not found"));
    }

    /**
     * Returns the hardware service.
     *
     * @return The hardware service.
     * @throws HttpStatusCodeException When the hardware service is not
     * available.
     */
    private HardwareService getHardwareService() throws HttpStatusCodeException {
        return getService(HardwareService.class)
                .orElseThrow(() -> new HttpStatusCodeException(ApiResponseCode.HTTP_503));
    }

}
