/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.events;

/**
 * The weight of the event.
 *
 * @author johns
 */
public enum EventSeverity {

    /**
     * Normal severity, often just for normal messages.
     */
    TRIVIAL,
    /**
     * A minor impact event, attention wished.
     */
    MINOR,
    /**
     * A major event, attention required.
     */
    MAJOR,
    /**
     * Defqon 1.
     */
    CRITICAL;

}
