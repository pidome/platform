/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.auth;

import java.util.Objects;

/**
 * Object used for password changes.
 *
 * @author John Sirach
 */
public class PassChangeObject {

    /**
     * The current password.
     */
    private String oldPassword;

    /**
     * The new password.
     */
    private String newPassword;

    /**
     * The new password retyped.
     */
    private String retypeNewPassword;

    /**
     * @return the oldPassword
     */
    public String getOldPassword() {
        return oldPassword;
    }

    /**
     * @param oldPassword the oldPassword to set
     */
    public void setOldPassword(final String oldPassword) {
        this.oldPassword = oldPassword;
    }

    /**
     * @return the newPassword
     */
    public String getNewPassword() {
        return newPassword;
    }

    /**
     * @param newPassword the newPassword to set
     */
    public void setNewPassword(final String newPassword) {
        this.newPassword = newPassword;
    }

    /**
     * @return the retypeNewPassword
     */
    public String getRetypeNewPassword() {
        return retypeNewPassword;
    }

    /**
     * @param retypeNewPassword the retypeNewPassword to set
     */
    public void setRetypeNewPassword(final String retypeNewPassword) {
        this.retypeNewPassword = retypeNewPassword;
    }

    /**
     * Checks if the new password and the re typed new passwords are equal.
     *
     * Do not use this server side as it performs equality check on strings. It
     * will not work This object is especially to create a pass change object to
     * be send to the REST interface.
     *
     * @return If the passwords are equal.
     */
    public boolean check() {
        return Objects.equals(this.newPassword, this.retypeNewPassword);
    }

}
