/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.hardware;

import com.fasterxml.jackson.databind.JsonNode;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import org.pidome.platform.hardware.driver.HardwareDriver;
import org.pidome.platform.hardware.driver.Transport;
import org.pidome.platform.presentation.input.InputForm;
import org.pidome.server.services.AbstractService;
import org.pidome.server.services.ServiceHandler;
import org.pidome.server.services.events.EventService;
import org.pidome.server.system.hardware.HardwareComponent;
import org.pidome.server.system.hardware.HardwareRoot;
import org.pidome.server.system.hardware.Peripheral;

/**
 * Service for server attached peripherals.
 *
 * @author John Sirach
 * @since 1.0
 */
public class HardwareService extends AbstractService {

    /**
     * The peripherals store where all connected peripherals are registered.
     */
    private HardwareRoot hardware;

    /**
     * Starts the peripherals service.
     *
     * @param startPromise The future used to detect success or failure.
     */
    @Override
    public final void start(final Promise<Void> startPromise) {
        hardware = new HardwareRoot();
        ServiceHandler.service(EventService.class).ifPresent(service -> {
            service.registerCodec(HardwareEventBody.class, new HardwareEventEncoder());
            //service.registerEventBusHandler(registerConsumer);
        });
        startPromise.handle(hardware.start());
    }

    /**
     * Stops the peripheral service.
     *
     * @param stopPromise The future supplied to determine when the service is
     * stopped.
     */
    @Override
    public final void stop(final Promise<Void> stopPromise) {
        if (hardware != null) {
            hardware.stop();
        }
        ServiceHandler.service(EventService.class).ifPresent(service -> {
            service.unregisterCodec(HardwareEventBody.class);
        });
        stopPromise.complete();
    }

    /**
     * Returns a list of available hardware.
     *
     * @return Hardware available within the system.
     */
    public Map<HardwareComponent.Interface, HardwareComponent> getAllHardware() {
        return hardware.getHardware();
    }

    /**
     * Returns the hardware component of the given interface.
     *
     * @param interfaceKey The interface key of the hardware to retrieve.
     * @return The hardware component of the interface.
     */
    public Optional<HardwareComponent> getHardwareOnInterface(final HardwareComponent.Interface interfaceKey) {
        return hardware.getHardware(interfaceKey);
    }

    /**
     * Returns a peripheral of the given volatile id.
     *
     * @param volatileId The id of the connected peripheral.
     * @return Optionally a peripheral when present, otherwise an empty one.
     */
    public Optional<Peripheral<? extends HardwareDriver>> getPeripheralById(final UUID volatileId) {
        return getAllHardware().values().stream()
                .flatMap(component -> component.getConnectedPeripherals().stream())
                .filter(peripheral -> peripheral.getVolatileId().equals(volatileId))
                .findFirst();
    }

    /**
     * Returns a list of peripherals of the given transport type.
     *
     * @param transport The transport type to receive for.
     * @return The hardware components of the give type.
     */
    public List<Peripheral<? extends HardwareDriver>> getHardwarePeripheralsOfType(final Transport.SubSystem transport) {
        return hardware.getHardware(transport);
    }

    /**
     * Returns a peripheral by it's given key.
     *
     * @param peripheralKey The key of the peripheral.
     * @return The peripheral, or optional empty.
     */
    public Optional<Peripheral<? extends HardwareDriver>> getPeripheralByKey(final String peripheralKey) {
        return this.getAllHardware().values().stream()
                .flatMap(component -> component.getConnectedPeripherals().stream())
                .filter(peripheral -> peripheral.getKey().equals(peripheralKey))
                .findFirst();
    }

    /**
     * Returns a driver definition by it's id.
     *
     * @param definitionId The definition id.
     * @return The Optional definition.
     */
    public Optional<DriverDefinition> getDriverDefinitionById(final UUID definitionId) {
        return this.getDriverCollection().stream()
                .filter(definition -> definition.getId().equals(definitionId))
                .findFirst();
    }

    /**
     * Returns the known drivers categorized by transport in an unmodifiable
     * map.
     *
     * @return Unmodifiable map with transport and associated drivers.
     */
    public final List<DriverDefinition> getDriverCollection() {
        return hardware.getDriverCollection();
    }

    /**
     * Returns a set of driver for the given transport subsystem.
     *
     * @param subSystem The transport subsystem.
     * @return Unmodifiable set of drivers.
     */
    public final List<DriverDefinition> getDriverCollection(final Transport.SubSystem subSystem) {
        return hardware.getDriverCollection(subSystem);
    }

    /**
     * Loads a configuration for the given device and driver.
     *
     * @param peripheral The peripheral to construct the configuration for.
     * @param driverDefinition The driver definition containing the information
     * to load a configuration for the given peripheral.
     * @return An input form with a driver configuration.
     */
    @SuppressWarnings("unchecked")
    public final InputForm getConfiguration(final Peripheral peripheral, final DriverDefinition driverDefinition) {
        return hardware.constructConfigurationFor(peripheral, driverDefinition.getProvidedBy(), driverDefinition.getDriver());
    }

    /**
     * Starts a driver on the given peripheral.
     *
     * @param peripheral The peripheral to apply the configuration to start.
     * @param driverDefinition The driver definition.
     * @param configuration The configuration to apply to the driver to put the
     * peripheral in use.
     * @return A future indicating success or failure.
     */
    @SuppressWarnings("unchecked")
    public final Future<HardwareDriver> startDriver(
            final Peripheral peripheral,
            final DriverDefinition driverDefinition,
            final JsonNode configuration) {
        return hardware.startDriver(peripheral, driverDefinition.getProvidedBy(), driverDefinition.getDriver(), configuration);
    }

    /**
     * Stops a driver on a peripheral.
     *
     * @param peripheral The peripheral to stop the driver on.
     * @return Future indicating stop success or failure.
     */
    public final Future<Void> stopDriver(final Peripheral peripheral) {
        return hardware.stopDriver(peripheral);
    }

    /**
     * Stops a driver on a peripheral.
     *
     * @param peripheralKey The key of the peripheral to stop.
     * @return Future indicating stop success or failure.
     */
    public final Future<Void> stopDriver(final String peripheralKey) {
        return hardware.stopDriver(peripheralKey);
    }

}
