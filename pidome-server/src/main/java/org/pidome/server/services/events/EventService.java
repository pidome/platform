/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.events;

import io.vertx.core.Promise;
import io.vertx.core.eventbus.EventBus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.services.AbstractService;
import org.pidome.server.system.VertXHandler;

/**
 * The event bus is responsible to send messages around in the system.
 *
 * The event bus is the heart of the system. Without the event bus the server
 * would not be able to communicate with all registered components in the
 * server.
 *
 * The event bus makes use of addresses and codecs. The event bus distinguishes
 * public and private events. All private events are shielded for use. As an
 * implementor there is no need to perform any special actions to register on
 * the event bus as this will be done automatically when an implementation is
 * registered on the server.
 *
 * @author John Sirach
 * @since 1.0
 * @todo finalize implementation of the event bus service, will be part of the
 * cluster epic.
 * @todo: Event send queue implementation.
 */
public final class EventService extends AbstractService {

    /**
     * The class logger.
     */
    private static final Logger LOG = LogManager.getLogger(EventService.class);

    /**
     * The event bus used.
     */
    private EventBus eventBus;

    /**
     * Initializes the event bus.
     *
     * @param startPromise The future to determine the bus is available.
     */
    @Override
    public void start(final Promise<Void> startPromise) {
        eventBus = VertXHandler.getInstance().getVertX().eventBus();
        startPromise.complete();
    }

    /**
     * Releases the eventbus locally.
     *
     * @param stopPromise The future used to determine if the bus is shut down.
     */
    @Override
    public void stop(final Promise<Void> stopPromise) {
        stopPromise.complete();
    }

    /**
     * Publishes an event to the event bus.
     *
     * @param event The event to publish.
     */
    @SuppressWarnings("unchecked")
    public void publishEvent(final EventProducer event) {
        event.getTargetAddresses().forEach(address -> {
            EventAddress sendAddress = (EventAddress) address;
            eventBus.publish(
                    address.equals(EventAddress.WEBSOCKET) ? sendAddress.getLocalAddress() : sendAddress.getBroadcastAddress(),
                    event.getBody(),
                    event.getHeaders()
            );
        });
    }

    /**
     * Registers a codec on the event bus.
     *
     * @param <T> The type.
     * @param clazz The class for which the codec applies.
     * @param codec The codec for transforming the class on the bus.
     */
    public <T> void registerCodec(final Class<T> clazz, final DefaultBusCodec<T> codec) {
        try {
            eventBus.registerDefaultCodec(clazz, codec);
        } catch (IllegalStateException ex) {
            LOG.warn("Codec [{}] registration warning", ex);
        }
    }

    /**
     * Removes a codec from the bus.
     *
     * @param <T> The type.
     * @param clazz The class for which the codec applies.
     */
    public <T> void unregisterCodec(final Class<T> clazz) {
        eventBus.unregisterDefaultCodec(clazz);
    }

    /**
     * Registers a data consumer on the event bus.
     *
     * @param registerConsumer The consumer to consume messages.
     */
    public void registerEventBusHandler(final EventBusMessageHandler<?> registerConsumer) {
        try {
            registerConsumer.createEventConsumerHandler(eventBus);
        } catch (ConsumerMissingException ex) {
            LOG.error("Could not register event consumer [{}], {}", registerConsumer, ex.getMessage(), ex);
        }
    }

    /**
     * Registers a data consumer on the event bus.
     *
     * @param unregisterConsumer The consumer to consume messages.
     */
    public void unRegisterEventBusHandler(final EventBusMessageHandler<?> unregisterConsumer) {
        try {
            unregisterConsumer.removeEventConsumerHandler(eventBus);
        } catch (ConsumerMissingException ex) {
            LOG.error("Could not unRegister event consumer [{}], {}", unregisterConsumer, ex.getMessage(), ex);
        }
    }

}
