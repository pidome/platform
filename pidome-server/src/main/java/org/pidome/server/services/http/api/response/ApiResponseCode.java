/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.http.api.response;

/**
 * Collection of used API response codes.
 *
 * @author John Sirach
 * @since 1.0
 */
public enum ApiResponseCode {

    /**
     * Switching protocol.
     */
    HTTP_101(Code.HTTP_101, "Switching protocol"),
    /**
     * Ok with content.
     */
    HTTP_200(Code.HTTP_200, "Ok"),
    /**
     * Created, with content return.
     */
    HTTP_201(Code.HTTP_201, "Created"),
    /**
     * Task accepted, result available in the future.
     */
    HTTP_202(Code.HTTP_202, "Accepted"),
    /**
     * Request fulfilled without content to return.
     */
    HTTP_204(Code.HTTP_204, "No content"),
    /**
     * Unable to fulfill the request because of invalid data in body, url or
     * headers.
     */
    HTTP_400(Code.HTTP_400, "Bad request"),
    /**
     * User is not authorized.
     */
    HTTP_401(Code.HTTP_401, "Not authorized"),
    /**
     * Access is forbidden despite being authorized or not.
     */
    HTTP_403(Code.HTTP_403, "Forbidden"),
    /**
     * the resource requested is not found.
     */
    HTTP_404(Code.HTTP_404, "Not found"),
    /**
     * The method (GET,POST,etc..) is not allowed on the given resource.
     */
    HTTP_405(Code.HTTP_405, "Method not allowed"),
    /**
     * The resource is does not return what has been requested in the accept
     * header.
     */
    HTTP_406(Code.HTTP_406, "Not acceptible"),
    /**
     * The resource is in conflict.
     *
     * A conflict can be an optimistic lock in the database, versions are not
     * correct.
     */
    HTTP_409(Code.HTTP_409, "Conflict"),
    /**
     * The requested resource does not exist anymore, if undetermined if it ever
     * existed use 404.
     */
    HTTP_410(Code.HTTP_410, "Gone"),
    /**
     * An unrecoverable error occurred.
     *
     * An unrecoverable error is for example when an error which can not be
     * solved by other means.
     */
    HTTP_500(Code.HTTP_500, "Server error"),
    /**
     * When the requested service is not available.
     */
    HTTP_503(Code.HTTP_503, "Service unavailable");

    /**
     * The response code.
     */
    private final int responseCode;

    /**
     * the message bound to the response code.
     */
    private final String responseMessage;

    /**
     * Constructor.
     *
     * @param code The response code.
     * @param message The response message.
     */
    ApiResponseCode(final int code, final String message) {
        this.responseCode = code;
        this.responseMessage = message;
    }

    /**
     * The response code.
     *
     * @return The response code.
     */
    public int getResponseCode() {
        return this.responseCode;
    }

    /**
     * The response message.
     *
     * @return The message bound to the response code.
     */
    public String getResponseMessage() {
        return this.responseMessage;
    }

    /**
     * The codes for the responses.
     */
    public static class Code {

        /**
         * Switching protocol.
         */
        public static final int HTTP_101 = 101;
        /**
         * OK.
         */
        public static final int HTTP_200 = 200;
        /**
         * Created.
         */
        public static final int HTTP_201 = 201;
        /**
         * Accepted.
         */
        public static final int HTTP_202 = 202;
        /**
         * OK, but no content returned.
         */
        public static final int HTTP_204 = 204;
        /**
         * Bad request.
         */
        public static final int HTTP_400 = 400;
        /**
         * Unauthorized.
         */
        public static final int HTTP_401 = 401;
        /**
         * Forbidden.
         */
        public static final int HTTP_403 = 403;
        /**
         * Not found.
         */
        public static final int HTTP_404 = 404;
        /**
         * Method not allowed.
         */
        public static final int HTTP_405 = 405;
        /**
         * Not acceptable.
         */
        public static final int HTTP_406 = 406;
        /**
         * Conflict.
         */
        public static final int HTTP_409 = 409;
        /**
         * Gone.
         */
        public static final int HTTP_410 = 410;
        /**
         * A non recoverable server side error.
         */
        public static final int HTTP_500 = 500;
        /**
         * Service not available.
         */
        public static final int HTTP_503 = 503;
    }

}
