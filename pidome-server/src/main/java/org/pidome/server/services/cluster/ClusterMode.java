/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.cluster;

/**
 * The modus of the cluster agent.
 *
 * @author johns
 */
public enum ClusterMode {

    /**
     * Running in self contained mode.
     */
    LOCAL,
    /**
     * The instance is a node in a clustered installation.
     */
    NODE,
    /**
     * The instance is the master in a clustered installation.
     */
    MASTER

}
