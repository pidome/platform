/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.authentication;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.hibernate.annotations.Type;
import org.pidome.platform.presentation.PresentationPlatform;
import org.pidome.server.entities.auth.LoginResource;
import org.pidome.server.entities.base.BaseEntity;

/**
 * A login resource information object which will be bound to a token.
 *
 * The login resource information object adds information to a token to help an
 * end user identifying where the token has been issued. It is loosely based on
 * information which would be available in an http user agent header.
 *
 * The combination of all parameters MUST help an user in identifying where a
 * token has been issued. It is highly advisable all the parameters are present.
 * If the object is not supplied the server will not attempt to read an user
 * agent header. It is the responsibility of the app creator to supply this
 * information.
 *
 * The server will not rely on the user agent information as this is not always
 * available. In app API's do often not supply these and if these are supplied
 * it is unknown how this has been setup and make a proper guess.
 *
 * @author John Sirach
 * @since 1.0
 */
@Entity
public class PidomeAuthTokenReference extends BaseEntity {

    /**
     * Version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * If the session viewed is the current session.
     *
     * This is only applied when the user requesting it is the logged in user.
     * Otherwise always false.
     */
    @Transient
    private boolean currentSession;

    /**
     * When the token has been issued.
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date issuedAt;

    /**
     * Where the login originated from.
     */
    @Type(type = "json")
    @Column(columnDefinition = "LONGVARCHAR")
    @SuppressFBWarnings(value = {"SE_BAD_FIELD"}, justification = "Handled by json type")
    private LoginResource loginResource;

    /**
     * the presentation platform the token is issued on.
     */
    @Enumerated(EnumType.STRING)
    private PresentationPlatform issuedPlatform;

    /**
     * Execute when creating.
     */
    @PrePersist
    protected void onCreate() {
        this.issuedAt = new Date();
    }

    /**
     * @param currentSession the currentSession to set
     */
    public void setCurrentSession(final boolean currentSession) {
        this.currentSession = currentSession;
    }

    /**
     * @return the issuedAt
     */
    public Date getIssuedAt() {
        return (Date) issuedAt.clone();
    }

    /**
     * Where the login originated from.
     *
     * @return the loginResource
     */
    public LoginResource getLoginResource() {
        return loginResource;
    }

    /**
     * Where the login originated from.
     *
     * @param loginResource the loginResource to set
     */
    public void setLoginResource(final LoginResource loginResource) {
        this.loginResource = loginResource;
    }

    /**
     * @return the currentSession
     */
    public boolean isCurrentSession() {
        return currentSession;
    }

    /**
     * @param issuedPlatform the issuedPlatform to set
     */
    public void setIssuedPlatform(final PresentationPlatform issuedPlatform) {
        this.issuedPlatform = issuedPlatform;
    }

    /**
     * @return the issuedPlatform
     */
    public PresentationPlatform getIssuedPlatform() {
        return issuedPlatform;
    }

    /**
     * Calculates hash.
     *
     * @return The hash.
     */
    @Override
    @SuppressWarnings("CPD-START")
    public int hashCode() {
        return super.hashCode();
    }

    /**
     * Equality check.
     *
     * @param obj the object to check.
     * @return If equals.
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return super.equals(obj);
    }
}
