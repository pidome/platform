/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.cluster;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import org.pidome.server.entities.base.BaseEntity;

/**
 * The primary identification of an host.
 *
 * @author johns
 */
@Entity
public class HostIdentification extends BaseEntity {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The id of the node.
     */
    @Column(unique = true)
    private UUID nodeId;

    /**
     * The name of the node.
     */
    private String nodeName;

    /**
     * The id of the node.
     *
     * @return the nodeId
     */
    public UUID getNodeId() {
        return nodeId;
    }

    /**
     * The id of the node.
     *
     * @param nodeId the nodeId to set
     */
    public void setNodeId(final UUID nodeId) {
        this.nodeId = nodeId;
    }

    /**
     * The name of the node.
     *
     * @return the nodeName
     */
    public String getNodeName() {
        return nodeName;
    }

    /**
     * The name of the node.
     *
     * @param nodeName the nodeName to set
     */
    public void setNodeName(final String nodeName) {
        this.nodeName = nodeName;
    }

    /**
     * Generate hash code.
     *
     * @return The object hash.
     */
    @Override
    @SuppressWarnings("CPD-START")
    public int hashCode() {
        return super.hashCode();
    }

    /**
     * Equality check.
     *
     * @param obj the object to check.
     * @return If equals.
     */
    @Override
    public boolean equals(final Object obj) {
        return super.equals(obj);
    }

}
