/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.modules;

/**
 * Exception thrown when there is a generic error with a module.
 *
 * @author johns
 */
public class ModuleException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>ModuleException</code> without detail
     * message.
     */
    public ModuleException() {
    }

    /**
     * Constructs an instance of <code>ModuleException</code> with the specified
     * detail message.
     *
     * @param msg the detail message.
     */
    public ModuleException(final String msg) {
        super(msg);
    }

    /**
     * Constructs an instance of <code>ModuleException</code> with the specified
     * detail message.
     *
     * @param msg the detail message.
     * @param cause The cause of the exception.
     */
    public ModuleException(final String msg, final Throwable cause) {
        super(msg, cause);
    }
}
