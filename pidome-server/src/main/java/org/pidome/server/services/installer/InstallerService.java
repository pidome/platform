/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.installer;

import io.vertx.core.Promise;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.server.services.AbstractService;
import org.pidome.server.system.database.Database;
import org.pidome.server.system.installer.packages.PackageStore;
import org.pidome.server.system.installer.packages.ServerPackage;
import org.pidome.server.system.installer.repositories.RepositoryContainer;
import org.pidome.server.system.installer.repositories.maven.Maven2Repository;

/**
 * Service for the installations.
 *
 * This is a broad implementation of an installation service. It is the main
 * container to add and remove repositories and install all packages from
 * hardware to extensions. This service includes a service to perform updates.
 *
 * @author John
 */
@SuppressWarnings("CPD-START")
public class InstallerService extends AbstractService {

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(InstallerService.class);

    /**
     * Return a list of repository containers.
     *
     * A repository container is a set of repositories.This helps in grouping /*
     * repositories from a single publisher. for example snapshot and release /*
     * artifacts.
     *
     * @return The list of known containers
     * @throws org.pidome.server.services.installer.InstallerException When
     * there is a retrieval error
     */
    public List<RepositoryContainer> getRepositoryContainers() throws InstallerException {
        try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            return autoManager.getManager().createQuery("SELECT r FROM RepositoryContainer r", RepositoryContainer.class).getResultList();
        } catch (Exception ex) {
            throw new InstallerException("Unable to fetch repository containers", ex);
        }
    }

    /**
     * Returns the repository identified by id.
     *
     * @param id The id of the container to retrieve.
     * @return The list of known containers
     * @throws org.pidome.server.services.installer.InstallerException When
     * there is a retrieval error
     */
    public RepositoryContainer getRepositoryContainerById(final long id) throws InstallerException {
        try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            return autoManager.getManager().find(RepositoryContainer.class, id);
        } catch (Exception ex) {
            throw new InstallerException("Repository container not found", ex);
        }
    }

    /**
     * Updates a container.
     *
     * @param container The container to update.
     * @throws org.pidome.server.services.installer.InstallerException When
     * there is an update error
     */
    public void updateRepositoryContainer(final RepositoryContainer container) throws InstallerException {
        try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            if (!isSystemContainer(container)) {
                autoManager.getManager().getTransaction().begin();
                autoManager.getManager().merge(container);
                autoManager.getManager().getTransaction().commit();
            } else {
                throw new InstallerException("System repositories can not be updated.");
            }
        } catch (Exception ex) {
            throw new InstallerException("Unable to update repositories container", ex);
        }
    }

    /**
     * Deletes a repository container.
     *
     * When a container is deleted, all related repositories are also deleted.
     * Orphaned packages should be removed or updated manually.
     *
     * @param container The container to update.
     * @throws org.pidome.server.services.installer.InstallerException When
     * there is a retrieval error
     */
    public void deleteRepositoryContainer(final RepositoryContainer container) throws InstallerException {
        try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            if (!isSystemContainer(container)) {
                EntityManager manager = autoManager.getManager();
                manager.getTransaction().begin();
                manager.remove(manager.contains(container) ? container : manager.merge(container));
                manager.getTransaction().commit();
            } else {
                throw new InstallerException("System repositories can not be deleted.");
            }
        } catch (Exception ex) {
            throw new InstallerException("Unable to delete repositories container", ex);
        }
    }

    /**
     * If the given container is a system container.
     *
     * @param container The container to check if it is a system container.
     * @return true when the given container is a system container.
     */
    private boolean isSystemContainer(final RepositoryContainer container) {
        return container.isReadOnly();
    }

    /**
     * Creates the minimal required repositories to update at least pidome
     * systems.
     */
    private void createRequiredRepositories() {
        try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {

            autoManager.getManager().getTransaction().begin();

            /// setup primary repositories.
            RepositoryContainer repoContainer = new RepositoryContainer();
            repoContainer.setRepositoryProviderId("pidome-core");
            repoContainer.setRepositoryProviderName("PiDome platform");
            repoContainer.setRepositoryProviderDescription("Official PiDome platform installation locations");
            repoContainer.setReadOnly(true);
            repoContainer.setActive(true);

            Maven2Repository productionRepo = new Maven2Repository();
            productionRepo.setExperimental(false);
            productionRepo.setRepositoryLocation("http://artifacts.pidome.org:9000/repository/pidome-releases/");
            productionRepo.setLocalRepositoryLocation("pidome-releases");
            productionRepo.setRepositoryDescription("PiDome official releases repository which contains released PiDome platform/server libraries.");
            autoManager.getManager().persist(productionRepo);

            repoContainer.setRepositories(List.of(productionRepo));

            autoManager.getManager().persist(repoContainer);

            /// setup primary repositories.
            RepositoryContainer repoComponentsContainer = new RepositoryContainer();
            repoComponentsContainer.setRepositoryProviderId("pidome-components");
            repoComponentsContainer.setRepositoryProviderName("PiDome components");
            repoComponentsContainer.setRepositoryProviderDescription("Official PiDome plugins and extensions installation locations");
            repoComponentsContainer.setReadOnly(false);
            repoComponentsContainer.setActive(true);

            Maven2Repository snapshotComponentRepo = new Maven2Repository();
            snapshotComponentRepo.setExperimental(true);
            snapshotComponentRepo.setRepositoryLocation("http://artifacts.pidome.org:9000/repository/pidome-components-snapshots/");
            snapshotComponentRepo.setLocalRepositoryLocation("pidome-snapshots");
            snapshotComponentRepo.setRepositoryDescription("PiDome official snapshot repository which contains development, unstable and incubating versions of PiDome libraries.");
            autoManager.getManager().persist(snapshotComponentRepo);

            Maven2Repository productionComponentsRepo = new Maven2Repository();
            productionComponentsRepo.setExperimental(false);
            productionComponentsRepo.setRepositoryLocation("http://artifacts.pidome.org:9000/repository/pidome-components-releases/");
            productionComponentsRepo.setLocalRepositoryLocation("pidome-releases");
            productionComponentsRepo.setRepositoryDescription("PiDome official releases repository which contains released PiDome libraries.");
            autoManager.getManager().persist(productionComponentsRepo);

            repoComponentsContainer.setRepositories(List.of(productionComponentsRepo, snapshotComponentRepo));

            autoManager.getManager().persist(repoComponentsContainer);

            autoManager.getManager().getTransaction().commit();
        } catch (Exception ex) {
            LOG.error("Could not create initial repositories [{}].", ex.getMessage(), ex);
        }
    }

    /**
     * Returns a single package based on the given package id.
     *
     * @param packageGroup Group of the package.
     * @param packageName Name of the package.
     * @param packageVersion Version of the package.
     * @return Package found by the given group, name and version.
     */
    public final ServerPackage getPackage(final String packageGroup, final String packageName, final String packageVersion) {
        try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {

            CriteriaBuilder builder = autoManager.getManager().getCriteriaBuilder();
            CriteriaQuery<ServerPackage> criteria = builder.createQuery(ServerPackage.class);
            Root<ServerPackage> from = criteria.from(ServerPackage.class);

            criteria.where(
                    builder.equal(from.get("packageGroup"), packageGroup),
                    builder.equal(from.get("packageName"), packageName),
                    builder.equal(from.get("packageVersion"), packageVersion)
            );

            try {
                return autoManager.getManager().createQuery(criteria).getSingleResult();
            } catch (NoResultException nr) {
                LOG.warn("No package found with group [{}], name [{}] and version [{}] in local database, install if required.", packageGroup, packageName, packageVersion);
                return null;
            }
        }
    }

    /**
     * Returns a package by it's given id.
     *
     * @param packageId The package id to get the package for.
     * @return The package requested, null if not found.
     */
    public ServerPackage getPackageById(final UUID packageId) {
        try (Database.AutoClosableEntityManager autoManager = Database.getInstance().getNewAutoClosableManager()) {
            return autoManager.getManager().find(ServerPackage.class, packageId);
        } catch (NoResultException nr) {
            LOG.warn("Package with id [{}] not found in repository", packageId, nr);
        }
        return null;
    }

    /**
     * Start the service.
     *
     * @param startPromise to signal startup done.
     */
    @Override
    public void start(final Promise<Void> startPromise) {
        createRequiredRepositories();
        try {
            PackageStore.collect();
        } catch (IOException ex) {
            LOG.error("Unable to gather installed packages", ex);
        }
        startPromise.complete();
    }

    /**
     * Stop the service.
     *
     * @param stopPromise To signal the service is stopped.
     */
    @Override
    public void stop(final Promise<Void> stopPromise) {
        stopPromise.complete();
    }

}
