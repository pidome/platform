/**
 * Installer service.
 * <p>
 * Provides services to install, delete and other package related actions.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.server.services.installer;
