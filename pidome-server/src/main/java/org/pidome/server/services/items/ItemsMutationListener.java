/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.items;

/**
 * Listener for items mutation events.
 *
 * @author johns
 */
public interface ItemsMutationListener {

    /**
     * When a mutation is done by a container.
     *
     * @param event The mutation event.
     */
    void mutated(ItemsMutationEvent event);

}
