/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.hardware;

import java.util.UUID;
import javax.persistence.Entity;
import org.pidome.platform.hardware.driver.Transport;
import org.pidome.server.services.definitions.GenericMetaDefinition;

/**
 * A driver definition for an attached peripheral.
 *
 * @author John Sirach
 */
@Entity
public class DriverDefinition extends GenericMetaDefinition {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The server package providing this driver.
     */
    private UUID providedBy;

    /**
     * The class for the driver.
     */
    private String driver;

    /**
     * the transport subsystem used.
     */
    private Transport.SubSystem transport;

    /**
     * @return the driver
     */
    public String getDriver() {
        return driver;
    }

    /**
     * @param driver the driver to set
     */
    public void setDriver(final String driver) {
        this.driver = driver;
    }

    /**
     * @return the providedBy
     */
    public UUID getProvidedBy() {
        return providedBy;
    }

    /**
     * @param providedBy the providedBy to set
     */
    public void setProvidedBy(final UUID providedBy) {
        this.providedBy = providedBy;
    }

    /**
     * the transport subsystem used.
     *
     * @return the transport
     */
    public Transport.SubSystem getTransport() {
        return transport;
    }

    /**
     * the transport subsystem used.
     *
     * @param transport the transport to set
     */
    public void setTransport(final Transport.SubSystem transport) {
        this.transport = transport;
    }

    /**
     * String representation of this class.
     *
     * @return This class as String.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("DriverDefinition:[");
        builder.append("name:").append(this.getName()).append(",");
        builder.append("description:").append(this.getDescription()).append(",");
        builder.append("transport:").append(transport).append(",");
        builder.append("driver:").append(driver).append("]");
        return builder.toString();
    }

    /**
     * Calculates hash.
     *
     * @return The hash.
     */
    @Override
    @SuppressWarnings("CPD-START")
    public int hashCode() {
        return super.hashCode();
    }

    /**
     * Equality check.
     *
     * @param obj the object to check.
     * @return If equals.
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return super.equals(obj);
    }
}
