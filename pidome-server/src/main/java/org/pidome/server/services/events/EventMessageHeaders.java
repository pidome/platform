/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.events;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.vertx.core.eventbus.DeliveryOptions;
import java.util.Optional;
import org.pidome.server.entities.users.Role;

/**
 * Class used to set and get headers being set on messages.
 *
 * @author johns
 */
public class EventMessageHeaders extends DeliveryOptions {

    /**
     * Acknowledgement required header.
     */
    private static final String HEADER_ACK = "ha";

    /**
     * The role target header.
     */
    private static final String TARGET_ROLE = "tr";

    /**
     * The uid target header.
     */
    private static final String TARGET_UID = "ti";

    /**
     * The severity of the event message.
     */
    private static final String TARGET_SEVERITY = "ts";

    /**
     * The type of event.
     */
    private static final String EVENT_TYPE = "ev";

    /**
     * The event domain.
     */
    private static final String EVENT_DOMAIN = "ed";

    /**
     * The event domain.
     */
    private static final String EVENT_MESSAGE = "em";

    /**
     * Constructs headers with a minimum role target.
     *
     * Message headers are default constructed with
     * <code>EventSeverity.TRIVIAL</code> and <code>EventType.NOTIFY</code>
     *
     * @param minimumroleTarget The minimum role target required.
     */
    public EventMessageHeaders(final Role minimumroleTarget) {
        this.addHeader(TARGET_ROLE, minimumroleTarget.name());
        this.setSeverity(EventSeverity.TRIVIAL);
        this.setType(EventType.NOTIFY);
    }

    /**
     * Implementation to make final.
     *
     * @param name The parameter name.
     * @param value The parameter value.
     * @return this for fluent.
     */
    @Override
    public final DeliveryOptions addHeader(final String name, final String value) {
        if (super.getHeaders() == null) {
            super.addHeader(name, value);
        } else {
            super.getHeaders().set(name, value);
        }
        return this;
    }

    /**
     * Set ack requirement to true or false.
     *
     * @param ackRequired Set ack response required or not.
     */
    public final void setAckRequired(final boolean ackRequired) {
        this.addHeader(HEADER_ACK, String.valueOf(ackRequired));
    }

    /**
     * Returns true or false if an acknowledgement is required or not.
     *
     * If an ack is required, please consult the message on the type that is
     * expected to be returned.
     *
     * @return If an ack is required or not.
     */
    @JsonIgnore
    public final boolean isAckRequired() {
        return Boolean.valueOf(this.getHeaders().get(HEADER_ACK));
    }

    /**
     * Set ack requirement to true or false.
     *
     * @param uidTarget Set uid targeted.
     */
    public final void setTargetUid(final long uidTarget) {
        this.addHeader(TARGET_UID, String.valueOf(uidTarget));
    }

    /**
     * Returns an optional if the uid is targeted.
     *
     * When no uid is given an empty optional is returned.
     *
     * @return Optional with uid targeted.
     */
    @JsonIgnore
    public final Optional<Long> getTargetUid() {
        if (this.getHeaders().get(TARGET_UID) == null) {
            return Optional.empty();
        } else {
            return Optional.of(Long.valueOf(this.getHeaders().get(TARGET_UID)));
        }
    }

    /**
     * Returns the targeted role.
     *
     * @return The role targeted.
     */
    @JsonIgnore
    public final Role getMinimumRoleTarget() {
        return Role.valueOf(this.getHeaders().get(TARGET_ROLE));
    }

    /**
     * Sets the severity of a message.
     *
     * @param severity The severity weight.
     */
    public final void setSeverity(final EventSeverity severity) {
        this.addHeader(TARGET_SEVERITY, severity.name());
    }

    /**
     * Returns the message severity.
     *
     * @return The message severity.
     */
    public final EventSeverity getSeverity() {
        return EventSeverity.valueOf(this.getHeaders().get(TARGET_SEVERITY));
    }

    /**
     * Sets the message type.
     *
     * @param type The type of message.
     */
    public final void setType(final EventType type) {
        this.addHeader(EVENT_TYPE, type.name());
    }

    /**
     * Returns the message type.
     *
     * @return The message type.
     */
    public final EventType getType() {
        return EventType.valueOf(this.getHeaders().get(EVENT_TYPE));
    }

    /**
     * The event domain.
     *
     * @param domain The domain of the event.
     */
    public final void setDomain(final String domain) {
        this.addHeader(EVENT_DOMAIN, domain);
    }

    /**
     * Return the event domain.
     *
     * @return The event domain.
     */
    public final String getDomain() {
        return this.getHeaders().get(EVENT_DOMAIN);
    }

    /**
     * Adds an optional message.
     *
     * A message must be very short.
     *
     * @param message The message to set.
     */
    public final void setMessage(final String message) {
        this.addHeader(EVENT_MESSAGE, message);
    }

    /**
     * Return the event message.
     *
     * @return The event message.
     */
    @JsonIgnore
    public final String getMessage() {
        return this.getHeaders().get(EVENT_MESSAGE);
    }

    /**
     * Sets the codec identifying the object type to be send.
     *
     * @param klass The class used as the codec.
     */
    public final void setCodec(final Class klass) {
        super.setCodecName(klass.getCanonicalName());
    }

    /**
     * Sets the codec identifying the object type to be send.
     */
    @JsonIgnore
    @Override
    public final String getCodecName() {
        return super.getCodecName();
    }

    /**
     * Get the send timeout.
     *
     * @return the value of send timeout
     */
    @JsonIgnore
    @Override
    public final long getSendTimeout() {
        return super.getSendTimeout();
    }

    /**
     * @return whether the message should be delivered to local consumers only
     */
    @JsonIgnore
    @Override
    public boolean isLocalOnly() {
        return super.isLocalOnly();
    }

}
