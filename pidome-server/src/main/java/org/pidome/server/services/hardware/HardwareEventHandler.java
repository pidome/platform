/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.server.services.hardware;

import org.pidome.server.services.events.EventAddress;
import org.pidome.server.services.events.EventHandler;

/**
 * An hardware event.
 *
 * @author johns
 */
public class HardwareEventHandler extends EventHandler<HardwareEventBody> {

    /**
     * Constructor.
     */
    public HardwareEventHandler() {
        super(EventAddress.HARDWARE);
    }

    /**
     * Returns the event.
     *
     * @return The event.
     */
    public HardwareEventBody getEvent() {
        return this.getBody();
    }

}
