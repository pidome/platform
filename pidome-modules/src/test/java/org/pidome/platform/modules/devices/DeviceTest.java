/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

import org.pidome.platform.modules.items.ItemStringAddress;
import org.pidome.platform.modules.items.ItemAddress;
import io.vertx.core.Promise;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.pidome.platform.modules.devices.Device.DeviceStatus;
import org.pidome.platform.modules.devices.Device.DeviceStructureBuilder;
import org.pidome.platform.modules.items.Item;

/**
 *
 * @author johns
 */
public class DeviceTest {

    public DeviceTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of setName method, of class Device.
     */
    @Test
    public void testDeviceGenerics() throws Exception {
        ItemAddress<String> address = mockDeviceAddress();
        String name = "DeviceGroupByBuilderImpl";
        Device<ItemAddress<String>> device = new DeviceGroupByBuilderImpl();
        device.setAddress(address);

        assertThat(device.getAddress().getAddress(), is(equalTo(address.getAddress())));
        assertThat(device.getAddress().getDescription(), is(equalTo(address.getDescription())));
        assertThat(device.getAddress().getLabel(), is(equalTo(address.getLabel())));

        assertThat(device.getType(), is(equalTo(Item.ItemType.DEVICE)));
        assertThat(device.getDeviceStatus(), is(equalTo(DeviceStatus.OK)));

        assertThat(device.getName(), is(equalTo(name)));

    }

    /**
     * Test of setName method, of class Device.
     */
    @Test
    public void testCreateDeviceByBuilder() throws Exception {
        GenericDevice device = new DeviceGroupByBuilderImpl();
        device.build(new DeviceStructureBuilder());
    }

    /**
     * Generic reusable device mocked address.
     *
     * @return The device address.
     */
    private ItemAddress<String> mockDeviceAddress() throws Exception {
        String addressAddress = "Address";
        String description = "Address description";
        String label = "Address label";

        ItemAddress<String> address = new ItemStringAddress();
        address.setAddress(addressAddress);
        address.setDescription(description);
        address.setLabel(label);

        return address;
    }

    public class DeviceGroupByBuilderImpl extends GenericDevice<ItemAddress<String>> {

        @Override
        public void shutdownDevice() {
        }

        @Override
        public void startupDevice() {
        }

        @Override
        public void build(DeviceStructureBuilder builder) throws IllegalArgumentException {
            builder.group("groupid", "groupName", group -> {
                group.button("buttonId", button -> {
                    button.description("Button description")
                            .label("Button label")
                            .graphType(ControlGraphType.NONE)
                            .hidden(true)
                            .readOnly(true)
                            .retention(true)
                            .shortcutSlot(0)
                            .timeout(10)
                            .visualType(ControlVisualType.TEMPERATURE);
                }).colorPicker("colorId", colorPicker -> {
                    colorPicker.mode(ColorpickerMode.KELVIN)
                            .graphType(ControlGraphType.NONE)
                            .hidden(true)
                            .readOnly(true)
                            .retention(true)
                            .shortcutSlot(0)
                            .timeout(10)
                            .addCommand(new ControlCommand<>("commandId", "commandLabel"))
                            .visualType(ControlVisualType.TEMPERATURE);
                }).numberData("NumberControl", numberControl -> {
                    numberControl.description("A string control")
                            .graphType(ControlGraphType.SERIES)
                            .readOnly(true)
                            .retention(true)
                            .hidden(true)
                            .shortcutSlot(0)
                            .timeout(10)
                            .visualType(ControlVisualType.TEMPERATURE)
                            .suffix("Suffix")
                            .prefix("Control prefix");
                }).select("SelectControl", selectControl -> {
                    selectControl.addCommand(new ControlCommand<>("select id", "select label"))
                            .graphType(ControlGraphType.SERIES)
                            .readOnly(true)
                            .retention(true)
                            .hidden(true)
                            .shortcutSlot(0)
                            .timeout(10)
                            .visualType(ControlVisualType.TEMPERATURE);
                }).slider("SliderControl", sliderControl -> {
                    sliderControl.description("A sliding control")
                            .graphType(ControlGraphType.SERIES)
                            .readOnly(true)
                            .retention(true)
                            .hidden(true)
                            .shortcutSlot(0)
                            .timeout(10)
                            .visualType(ControlVisualType.TEMPERATURE)
                            .min(-10)
                            .max(10);
                }).stringData("StringControl", control -> {
                    control.description("A string control")
                            .graphType(ControlGraphType.SERIES)
                            .readOnly(true)
                            .retention(true)
                            .hidden(true)
                            .shortcutSlot(0)
                            .timeout(10)
                            .suffix("string suffix")
                            .prefix("String prefix")
                            .visualType(ControlVisualType.TEMPERATURE);
                }).toggle("ToggleId", toggleControl -> {
                    toggleControl.description("A sliding control")
                            .falseText("This is false")
                            .trueText("This is true")
                            .graphType(ControlGraphType.SERIES)
                            .readOnly(true)
                            .retention(true)
                            .hidden(true)
                            .shortcutSlot(0)
                            .timeout(10)
                            .visualType(ControlVisualType.TEMPERATURE);
                });
            });
        }

        @Override
        public void handleDeviceCommand(DevicesModule module, DeviceCommandRequest request, Promise promise) {
            /// To be implemented.
        }
    }

}
