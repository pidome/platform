/**
 * Package for discovery for devices.
 * <p>
 * Supplying discovery classes for devices.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.platform.modules.devices.discovery;
