/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.pidome.platform.hardware.driver.Transport;

/**
 * The annotation for modules.
 *
 * If you have a module, which is able to communicate over both network and a
 * serial implementation, you will need multiple classes annotated. Example:
 *
 * <code>MyBaseModule extends DeviceModule {}</code>
 *
 * <code>MySerialBasedModule extends MyBaseModule {}</code>
 *
 * <code>MyNetworkBasedModule extends MyBaseModule {}</code>
 *
 * Both <code>MySerialBasedModule</code> and <code>MyNetworkBasedModule</code>
 * would need to be annotated with <code>PidomeModule</code> and have configured
 * their own <code>Transport.SubSystem</code>.
 *
 * <code>MySerialBasedModule</code> would return
 * <code>Transport.SubSystem.SERIAL</code> and <code>MySerialBasedModule</code>
 * would return <code>Transport.SubSystem.NETWORK</code>.
 *
 * @author johns
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface PidomeModule {

    /**
     * If the module is enabled.
     *
     * @return true when enabled.
     */
    boolean enabled() default true;

    /**
     * The name of the module.
     *
     * @return The driver name.
     */
    String name();

    /**
     * The description of the module.
     *
     * @return The driver description.
     */
    String description();

    /**
     * The transport type being targeted.
     *
     * The transport target supports categorizing modules for specific transport
     * types like Serial devices.
     *
     * @return The transport type communicating with to expect from the module
     */
    Transport.SubSystem transport();

}
