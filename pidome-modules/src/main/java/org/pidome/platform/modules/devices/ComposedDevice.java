/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

import org.pidome.platform.modules.items.ItemAddress;

/**
 * A composed device.
 *
 * A composed device let's the end user create the device structure based on
 * given predefined controls.
 *
 * @author johns
 * @param <A> The address type.
 */
public abstract class ComposedDevice<A extends ItemAddress> extends Device<A> {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Public default constructor.
     */
    public ComposedDevice() {
        super();
    }

    /**
     * Returns the builder type.
     *
     * @return The builder type.
     */
    @Override
    public final DeviceBuilderType getDeviceBuilderType() {
        return DeviceBuilderType.PROVIDED;
    }

    /**
     * Called to supply the end user with a skeleton of controls.
     *
     * <p>
     * This method is called when the server wants to provide the end user with
     * a set of possible controls to create a device which is able to have a
     * specific set of controls.
     * <p>
     * The builder is the same builder as being used by a
     * <code>GenericDevice</code>. Where a generic device has a fixed structure
     * in both groups and controls, the custom device supplies a fixed amount of
     * possible groups, with the possible controls which can exist in these
     * groups.
     * <p>
     * Example:<br/>
     * KlikAanKlikUit devices uses the same signal and identification for both
     * switching a light switch (remote), as detection motion with a motion
     * sensor. This is why motion sensors for KlikAanKlikUit can act as a
     * "remote" for a light switch. Because switching and motion are exactly the
     * same, it is handled identically and for us impossible to say it's a
     * motion sensor or a switch. With custom building we can provide the user
     * with an option to use a switch device (like the remote) or have it being
     * displayed as motion detection device (the detector).
     *
     * @param builder The builder to build a device skeleton with.
     */
    public abstract void getProvidedControls(DeviceStructureBuilder builder); /// Use a device spec table for the device types.

}
