/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

/**
 * Enum for defining the color picker mode.
 *
 * This is just an hint for front-ends to be able to implement different color
 * picker modes. For example a device only supporting a limited set of colors
 * would use RGB. Devices with more granular control, for example a light
 * fixture, could use HSB. Light fixtures capable of doing white tints could use
 * kelvin.
 *
 * @author johns
 */
public enum ColorpickerMode {

    /**
     * Hsb color picker mode.
     */
    HSB("HSB mode"),
    /**
     * RGB color picker mode.
     */
    RGB("RGB mode"),
    /**
     * Kelvin mode.
     */
    KELVIN("Kelvin mode"),
    /**
     * CIE mode.
     */
    CIE("CIE mode");

    /**
     * The description of the enum type.
     */
    private final String description;

    /**
     * Enum constructor.
     *
     * @param description The description of the enum.
     */
    ColorpickerMode(final String description) {
        this.description = description;
    }

    /**
     * Returns the description of the enum.
     *
     * @return Enum description.
     */
    public String getDescription() {
        return this.description;
    }

}
