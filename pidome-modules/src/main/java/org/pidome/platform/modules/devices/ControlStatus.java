/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

/**
 * The status of a control.
 *
 * @author johns
 */
public enum ControlStatus {

    /**
     * The control is ok.
     */
    OK,
    /**
     * The control has encountered a data timeout.
     */
    TIMEOUT;

}
