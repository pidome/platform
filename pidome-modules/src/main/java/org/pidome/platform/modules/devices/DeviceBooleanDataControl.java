/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A boolean data control.
 *
 * @author johns
 */
public final class DeviceBooleanDataControl extends DeviceDataControl<Boolean> {

    /**
     * Class version.
     */
    public static final long serialVersionUID = 1L;

    /**
     * Constants for visualizing boolean values.
     *
     * This is heavily depending on the front-end implementation of a data
     * control. If a front-end implementation does not support colors they will
     * not be shown as such. These are just used for hinting.
     */
    public enum BooleanVisualType {
        /**
         * Keep as plain text.
         */
        TEXT("As text"),
        /**
         * Use a color.
         */
        COLOR("As color (true=green/false=red)"),
        /**
         * Use a color inverted.
         */
        COLOR_INVERTED("As color inverted (true=red/false=green)"),
        /**
         * Apply color to the text.
         *
         * True is positive, false is negative.
         */
        TEXT_COLOR("Text color (true=green/false=red)"),
        /**
         * Apply color to the text.
         *
         * True is negative, false is positive (for example for motion).
         */
        TEXT_COLOR_INVERTED("Text color (true=red/false=green)");

        /**
         * The description of the enum type.
         */
        private final String description;

        /**
         * Enum constructor.
         *
         * @param description The description of the enum.
         */
        BooleanVisualType(final String description) {
            this.description = description;
        }

        /**
         * Returns the description of the enum.
         *
         * @return Enum description.
         */
        public String getDescription() {
            return this.description;
        }
    }

    /**
     * Visual type for boolean values.
     */
    private BooleanVisualType booleanVisualType = BooleanVisualType.TEXT;

    /**
     * Text to show when boolean false is active and type is not COLOR.
     */
    private String falseText = "false";
    /**
     * Text to show when boolean true is active and type is not COLOR.
     */
    private String trueText = "true";

    /**
     * constructor.
     *
     * @param id The id of the control.
     */
    @JsonCreator
    public DeviceBooleanDataControl(@JsonProperty("id") final String id) {
        super(id);
        this.setValue(Boolean.FALSE);
    }

    /**
     * Set how the boolean data field should be visualized.
     *
     * @param visualType The visual type.
     */
    public void setBooleanVisualType(final BooleanVisualType visualType) {
        this.booleanVisualType = visualType;
    }

    /**
     * Returns the visual type for the boolean field.
     *
     * @return The visual type.
     */
    public BooleanVisualType getBooleanVisualType() {
        return this.booleanVisualType;
    }

    /**
     * Text to show when boolean false is active and type is not COLOR.
     *
     * @return the falseText
     */
    public String getFalseText() {
        return falseText;
    }

    /**
     * Text to show when boolean false is active and type is not COLOR.
     *
     * @param falseText the falseText to set
     */
    public void setFalseText(final String falseText) {
        this.falseText = falseText;
    }

    /**
     * Text to show when boolean true is active and type is not COLOR.
     *
     * @return the trueText
     */
    public String getTrueText() {
        return trueText;
    }

    /**
     * Text to show when boolean true is active and type is not COLOR.
     *
     * @param trueText the trueText to set
     */
    public void setTrueText(final String trueText) {
        this.trueText = trueText;
    }

    /**
     * Returns a new boolean data control builder.
     *
     * @param id The id of the control.
     * @return The builder.
     */
    public static BooleanDataControlBuilder booleanDataBuilder(final String id) {
        return new BooleanDataControlBuilder(id);
    }

    /**
     * Builder for the boolean data control.
     */
    @SuppressWarnings("PMD.AvoidFieldNameMatchingMethodName") //Builder pattern.
    public static class BooleanDataControlBuilder extends DeviceDataControl.DataControlBuilder<DeviceBooleanDataControl, BooleanDataControlBuilder> {

        /**
         * The control id.
         */
        private final String id;

        /**
         * The boolean type.
         */
        private BooleanVisualType type = BooleanVisualType.TEXT_COLOR;

        /**
         * Text to show when boolean false is active and type is not COLOR.
         */
        private String falseText = "false";

        /**
         * Text to show when boolean true is active and type is not COLOR.
         */
        private String trueText = "true";

        /**
         * Builder constructor.
         *
         * @param controlId The id of the control.
         */
        public BooleanDataControlBuilder(final String controlId) {
            this.id = controlId;
            this.setReference(this);
        }

        /**
         * Sets the prefix.
         *
         * @param booleanVisualType The visual type for this boolean.
         * @return the builder.
         */
        public BooleanDataControlBuilder booleanVisualType(final BooleanVisualType booleanVisualType) {
            this.type = booleanVisualType;
            return this;
        }

        /**
         * The text to display if mode is text and value is true.
         *
         * @param whenTrueText The text when true.
         * @return The Builder.
         */
        public BooleanDataControlBuilder trueText(final String whenTrueText) {
            this.trueText = whenTrueText;
            return this;
        }

        /**
         * The text to display if mode is text and value is false.
         *
         * @param whenFalseText The text when false.
         * @return The Builder.
         */
        public BooleanDataControlBuilder falseText(final String whenFalseText) {
            this.falseText = whenFalseText;
            return this;
        }

        /**
         * Builds the boolean data control.
         *
         * @return The boolean data control.
         */
        @Override
        @SuppressWarnings("unchecked")
        public DeviceBooleanDataControl build() {
            DeviceBooleanDataControl control = new DeviceBooleanDataControl(this.id);
            control.setControlType(DeviceControlType.DATA_BOOLEAN);
            super.build(control);
            control.setFalseText(this.falseText);
            control.setTrueText(this.trueText);
            control.setBooleanVisualType(type);
            return control;
        }

    }

}
