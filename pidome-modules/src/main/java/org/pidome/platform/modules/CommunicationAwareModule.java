/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import org.apache.logging.log4j.LogManager;
import org.pidome.platform.hardware.driver.types.consumers.ProsumerInterface;

/**
 * Base for communication aware modules.
 *
 * A prosumer is provided for a module which communicates with external
 * peripherals or network based services. Depending on the prosumer type
 * different communication methods are available.
 *
 * @author johns
 * @param <P> The Prosumer interface to be used within the module.
 * @todo: Add dataflow optimization with enums and local threads.
 */
public abstract class CommunicationAwareModule<P extends ProsumerInterface> extends ModuleBase {

    /**
     * Hardware root logger.
     */
    static final org.apache.logging.log4j.Logger LOG = LogManager.getLogger(CommunicationAwareModule.class);

    /**
     * The prosumer available for the module.
     */
    @JsonIgnore
    private P prosumer;

    /**
     * Constructor used to determine and create the provided prosumer.
     */
    @SuppressWarnings("unchecked")
    public CommunicationAwareModule() {
        Class<P> prosumerClass = (Class<P>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        try {
            this.prosumer = prosumerClass.getConstructor().newInstance();
        } catch (InvocationTargetException | IllegalAccessException | InstantiationException | NoSuchMethodException | SecurityException ex) {
            LOG.error("Unable to instantiate a prosumer for [{}]", this, ex);
        }
    }

    /**
     * The module can use this to consume dat from a driver and produce data for
     * a driver.
     *
     * @return The available prosumer of type P.
     */
    public final P getProsumer() {
        return this.prosumer;
    }

}
