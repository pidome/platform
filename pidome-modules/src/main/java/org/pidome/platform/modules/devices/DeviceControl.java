/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.tools.properties.ObjectPropertyBinding;

/**
 * The base definition of a control in a device.
 *
 * @author John
 * @param <T> The data type used in this control's value.
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.EXISTING_PROPERTY,
        property = "controlType",
        visible = true)
@JsonSubTypes({
    @Type(value = DeviceToggleControl.class, name = DeviceControlType.Name.TOGGLE),
    @Type(value = DeviceSliderControl.class, name = DeviceControlType.Name.SLIDER),
    @Type(value = DeviceSelectControl.class, name = DeviceControlType.Name.SELECT),
    @Type(value = DeviceBooleanDataControl.class, name = DeviceControlType.Name.DATA_BOOLEAN),
    @Type(value = DeviceNumberDataControl.class, name = DeviceControlType.Name.DATA_NUMBER),
    @Type(value = DeviceStringDataControl.class, name = DeviceControlType.Name.DATA_STRING),
    @Type(value = DeviceColorPickerControl.class, name = DeviceControlType.Name.COLORPICKER),
    @Type(value = DeviceButtonControl.class, name = DeviceControlType.Name.BUTTON)
})
@SuppressWarnings({"PMD.AbstractClassWithoutAbstractMethod"}) /// Only generic required methods, extended by json subtypes with domain specific methods.
public abstract class DeviceControl<T> implements Serializable {

    /**
     * Class version.
     */
    public static final long serialVersionUID = 1L;

    /**
     * Device logger.
     */
    private static final Logger LOG = LogManager.getLogger(DeviceControl.class);

    /**
     * The length of a second in miliseconds.
     */
    private static final int SECONDS_IN_MILISECONDS = 1000;

    /**
     * The current control status.
     */
    private ControlStatus controlStatus = ControlStatus.OK;

    /**
     * The control's id.
     */
    private final String id;

    /**
     * The control's description.
     */
    private String description;

    /**
     * The control's data type.
     */
    private final DeviceControlDataType dataType;

    /**
     * The device control group member.
     */
    @JsonIgnore
    private transient DeviceControlGroup group;

    /**
     * If the control is an hidden control.
     *
     * An hidden control is only used in code and is not exposed to any other
     * mechanism.
     */
    private boolean hidden;

    /**
     * If retention is requested or not.
     *
     * A control without retention will not store it's last known value in a
     * retention file. Hence a restart of the device will start with an initial
     * value if known.
     */
    private boolean retention = false;

    /**
     * A read only control is a control that can only be changed by data from
     * devices.
     *
     * A read only control will not be able to be updated from any external
     * interface.
     */
    private boolean readOnly = true;

    /**
     * The timeout limit of the control in seconds.
     *
     * When this timeout is reached, the control will expose it is timed out.
     */
    private int timeout = 0;

    /**
     * The shortcut order of the control.
     *
     * A shortcut control is used in device lists. Normally a control is only
     * shown when someone clicks on a device which will show all details. Set a
     * value to have it appear in lists. Controls are by default horizontally
     * from left to right in lists. Vertically from top to bottom.
     */
    private int shortcutSlot = 0;

    /**
     * The visual type of the control.
     */
    private ControlVisualType visualType = ControlVisualType.NONE;

    /**
     * Graph type for data.
     */
    private ControlGraphType graphType = ControlGraphType.NONE;

    /**
     * The control type.
     */
    private DeviceControlType controlType;

    /**
     * The UTC data when the last data was received.
     */
    private Date lastDataChange;

    /**
     * The current value of the control.
     */
    @JsonIgnore
    private final ObjectPropertyBinding<T> controlValueProperty = new ObjectPropertyBinding<>();

    /**
     * Contains the timout executor.
     */
    @JsonIgnore
    private ScheduledExecutorService timeoutService;

    /**
     * Constructor.
     *
     * @param controlId The id of the control.
     * @throws IllegalArgumentException When the constructor is unable to set
     * defaults and/or with the given values.
     */
    @SuppressWarnings("unchecked")
    public DeviceControl(final String controlId) throws IllegalArgumentException {
        if (controlId == null || controlId.length() < 1) {
            throw new IllegalArgumentException("Control id is incorrect (length or unavailable)");
        }
        this.id = controlId;
        Class<T> type = ((Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]);
        switch (type.getSimpleName()) {
            case "String":
                this.dataType = DeviceControlDataType.STRING;
                break;
            case "Number":
                this.dataType = DeviceControlDataType.NUMBER;
                break;
            case "Boolean":
                this.dataType = DeviceControlDataType.BOOLEAN;
                break;
            case "DeviceColorPickerControlColorData":
                this.dataType = DeviceControlDataType.COLOR;
                break;
            default:
                this.dataType = DeviceControlDataType.CUSTOM;
                break;
        }
    }

    /**
     * Sets the control group reference.
     *
     * @param group The control group to reference to.
     */
    public final void setGroup(final DeviceControlGroup group) {
        this.group = group;
    }

    /**
     * Returns the control group this control belongs to.
     *
     * @return The control group.
     */
    public final DeviceControlGroup getGroup() {
        return this.group;
    }

    /**
     * Returns the control id.
     *
     * @return The id of the control.
     */
    public final String getId() {
        return this.id;
    }

    /**
     * Returns the control type.
     *
     * @return The type of control.
     */
    public final DeviceControlType getControlType() {
        return this.controlType;
    }

    /**
     * Set the control type.
     *
     * @param controlType The control type to set.
     */
    public final void setControlType(final DeviceControlType controlType) {
        this.controlType = controlType;
    }

    /**
     * Sets the description.
     *
     * @param description The description to set, try to keep this short.
     */
    protected final void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Returns the description.
     *
     * @return The control description.
     */
    public final String getDescription() {
        return this.description;
    }

    /**
     * If this control has retention active.
     *
     * When retention is enabled, the control's value is kept between device
     * restarts.
     *
     * @param retention The retention to set.
     */
    public final void setRetention(final boolean retention) {
        this.retention = retention;
    }

    /**
     * Returns true if this control has retention set.
     *
     * @return If retention is set.
     */
    public final boolean isRetention() {
        return retention;
    }

    /**
     * Sets a control read only.
     *
     * @param readOnly If to set read only or not.
     */
    public final void setReadOnly(final boolean readOnly) {
        this.readOnly = readOnly;
    }

    /**
     * Returns true if a control is read only. This only applies to data
     * controls.
     *
     * @return True if read only.
     */
    public final boolean isReadOnly() {
        return readOnly;
    }

    /**
     * Hides a control from visual output.
     *
     * @param hidden If a control should be hidden in any interface.
     */
    protected final void setHidden(final boolean hidden) {
        this.hidden = hidden;
    }

    /**
     * Returns true if the group is hidden.
     *
     * @return If a control is hidden or not.
     */
    public final boolean isHidden() {
        return hidden;
    }

    /**
     * Returns true if this control has a timeout configured.
     *
     * Calling this method starts the timout scheduled when
     * <code>timeout &gt; 0</code>
     *
     * @param timeout The timeout in seconds.
     */
    public final void setTimeout(final int timeout) {
        this.timeout = timeout;
        if (this.timeout != 0) {
            startTimeOutScheduler();
        }
    }

    /**
     * Returns the timeout in seconds.
     *
     * @return The timeout length in seconds.
     */
    public final int getTimeout() {
        return this.timeout;
    }

    /**
     * Sets the shortcut slot.
     *
     * @param shortcutSlot The shortcut slot to set.
     */
    public void setShortcutSlot(final int shortcutSlot) {
        this.shortcutSlot = shortcutSlot;
    }

    /**
     * Returns the shortcut position.
     *
     * A shortcut slot position of 0 or lower is not a slot position.
     *
     * @return The shortcut position.
     */
    public final int getShortcutSlot() {
        return shortcutSlot;
    }

    /**
     * Sets the visual parameter.
     *
     * @param type The type of visual
     */
    public final void setVisualType(final ControlVisualType type) {
        visualType = type;
    }

    /**
     * Returns if there is a visual parameter.
     *
     * @return The visual type.
     */
    public final ControlVisualType getVisualType() {
        return visualType;
    }

    /**
     * Graph type for numeric data.
     *
     * @return the graphType.
     */
    public ControlGraphType getGraphType() {
        return graphType;
    }

    /**
     * Graph type for numeric data.
     *
     * @param graphType the graphType to set
     */
    public void setGraphType(final ControlGraphType graphType) {
        this.graphType = graphType;
    }

    /**
     * Returns the control data type.
     *
     * @return The data type of the control.
     */
    public final DeviceControlDataType getDataType() {
        return this.dataType;
    }

    /**
     * Returns the current status of the control.
     *
     * @return The status of the control.
     */
    public final ControlStatus getControlStatus() {
        return this.controlStatus;
    }

    /**
     * returns the button set value.
     *
     * @return The value.
     */
    public final T getValue() {
        return controlValueProperty.getValue();
    }

    /**
     * Sets the last known data.
     *
     * @param value The value to set in the control.
     */
    public void setValue(final T value) {
        updateLastDataChange();
        this.controlValueProperty.setValue(value);
    }

    /**
     * Returns the underlying control data.
     *
     * If a control has special cases where the underlying data is not the same
     * as the real data for semantic reasons, override this method.
     *
     * @return The underlying control data values.
     */
    @JsonIgnore
    public Object getValueData() {
        return controlValueProperty.getValue();
    }

    /**
     * Returns the value property to bind to.
     *
     * @return The object property binding for the value.
     */
    public final ObjectPropertyBinding<T> getControlValueProperty() {
        return this.controlValueProperty;
    }

    /**
     * Method executed when data updates.
     *
     * This method is always called, als when previous data is the same as the
     * new data.
     */
    private void updateLastDataChange() {
        this.lastDataChange = new Date();
        if (this.getTimeout() != 0) {
            timeoutCheck().run();
            startTimeOutScheduler();
        }
    }

    /**
     * Starts the timeout scheduler.
     */
    private void startTimeOutScheduler() {
        stopTimeOutScheduler();
        this.timeoutService = Executors.newScheduledThreadPool(1);
        this.timeoutService.scheduleAtFixedRate(this.timeoutCheck(), timeout, timeout, TimeUnit.SECONDS);
    }

    /**
     * Stops the timeout scheduler.
     */
    public final void stopTimeOutScheduler() {
        if (this.timeoutService != null) {
            this.timeoutService.shutdownNow();
        }
    }

    /**
     * Returns the timeout check runnable.
     *
     * @return The timeout runnable
     */
    private Runnable timeoutCheck() {
        return () -> {
            try {
                if (this.lastDataChange == null) {
                    this.controlStatus = ControlStatus.TIMEOUT;
                } else if (this.controlStatus != ControlStatus.TIMEOUT && (this.lastDataChange.getTime() < (new Date().getTime() - (this.timeout * SECONDS_IN_MILISECONDS)))) {
                    LOG.warn("Timeout status for control '{}', in group '{}' for device '{}'", this.getDescription(), this.getGroup().getTitle(), this.getGroup().getDevice().getName());
                    this.controlStatus = ControlStatus.TIMEOUT;
                } else if (this.controlStatus != ControlStatus.OK && (this.lastDataChange.getTime() > (new Date().getTime() - (this.timeout * SECONDS_IN_MILISECONDS)))) {
                    LOG.warn("Timeout recovery for control '{}', in group '{}' for device '{}'", this.getDescription(), this.getGroup().getTitle(), this.getGroup().getDevice().getName());
                    this.controlStatus = ControlStatus.OK;
                }
                this.group.getDevice().checkControlsStatus();
            } catch (Exception ex) {
                //// This mainly happens when a device is not ready yet as the device link has not been finalized yet.
                LOG.warn("Check of device control availability failed: {} ({})", ex.getMessage(), ex.getCause());
            }
        };
    }

    /**
     * Custom equals.
     *
     * @param o The object to check.
     * @return true or false if the data value contents are the same or not.
     */
    public abstract boolean valueEquals(Object o);

    /**
     * Interface for final builders.
     *
     * @param <C> The type to build.
     */
    public interface DeviceControlBuilder<C extends DeviceControl> {

        /**
         * To execute the builder's build.
         *
         * @return The control.
         */
        C build();
    }

    /**
     * Control base builder.
     *
     * @param <T> Builder extending the control builder.
     */
    @SuppressWarnings("PMD.AvoidFieldNameMatchingMethodName") //Builder pattern.
    public abstract static class ControlBuilder<T extends ControlBuilder> {

        /**
         * The control's description.
         */
        private String description = "";

        /**
         * If the control is an hidden control.
         *
         * An hidden control is only used in code and is not exposed to any
         * other mechanism.
         */
        private boolean hidden = false;

        /**
         * If retention is requested or not.
         *
         * A control without retention will not store it's last known value in a
         * retention file. Hence a restart of the device will start with an
         * initial value if known.
         */
        private boolean retention = false;

        /**
         * A read only control is a control that can only be changed by data
         * from devices.
         *
         * A read only control will not be able to be updated from any external
         * interface.
         */
        private boolean readOnly = false;
        /**
         * The timeout limit of the control in seconds.
         *
         * When this timeout is reached, the control will expose it is timed
         * out.
         */
        private int timeout = 0;

        /**
         * The shortcut order of the control.
         *
         * A shortcut control is used in device lists. Normally a control is
         * only shown when someone clicks on a device which will show all
         * details. Set a value to have it appear in lists. Controls are by
         * default horizontally from left to right in lists. Vertically from top
         * to bottom.
         */
        private int shortcutSlot = 0;

        /**
         * The visual type of the control.
         */
        private ControlVisualType visualType = ControlVisualType.NONE;

        /**
         * Graph type for data.
         */
        private ControlGraphType graphType = ControlGraphType.NONE;

        /**
         * The builder reference.
         */
        private T reference;

        /**
         * Builder constructor. Default constructor.
         */
        public ControlBuilder() {
        }

        /**
         * Creates a reference to the original builder for type reference
         * purposes.
         *
         * @param builderReference The builder extending this builder.
         */
        protected void setReference(final T builderReference) {
            this.reference = builderReference;
        }

        /**
         * control description.
         *
         * @param controlDescription The description of the control.
         * @return Builder
         */
        public T description(final String controlDescription) {
            this.description = controlDescription;
            return reference;
        }

        /**
         * If control is hidden.
         *
         * @param isHidden if hidden.
         * @return Builder.
         */
        public T hidden(final boolean isHidden) {
            this.hidden = isHidden;
            return reference;
        }

        /**
         * If value rertention is enabled.
         *
         * @param retentionEnabled if enabled.
         * @return Builder.
         */
        public T retention(final boolean retentionEnabled) {
            this.retention = retentionEnabled;
            return reference;
        }

        /**
         * If control is read only.
         *
         * @param isReadOnly if read only.
         * @return Builder.
         */
        public T readOnly(final boolean isReadOnly) {
            this.readOnly = isReadOnly;
            return reference;
        }

        /**
         * Time for timeout to occur on the control in seconds.
         *
         * Timeout is the time it takes to receive data in seconds when passed
         * and no data is received the control is in timeout.
         *
         * @param controlTimout timeout period in seconds, keep 0 for disabled.
         * @return Builder.
         */
        public T timeout(final int controlTimout) {
            this.timeout = controlTimout;
            return reference;
        }

        /**
         * The position a control takes in lists.
         *
         * @param displaySlot Position in lists from 1 to 3. Some controls take
         * 2 positions like the slider and color picker.
         * @return The builder.
         */
        public T shortcutSlot(final int displaySlot) {
            this.shortcutSlot = displaySlot;
            return reference;
        }

        /**
         * How the control should be visualized.
         *
         * @param controlVisualType The visual type.
         * @return Builder
         */
        public T visualType(final ControlVisualType controlVisualType) {
            this.visualType = controlVisualType;
            return reference;
        }

        /**
         * The type of graph for this control.
         *
         * @param controlGraphType The graph type.
         * @return the builder.
         */
        public T graphType(final ControlGraphType controlGraphType) {
            this.graphType = controlGraphType;
            return reference;
        }

        /**
         * Builds the control.To be implemented by Builder extenders.
         *
         *
         * @param <C> The type to be returned, which is in this case the
         * DeviceControl extending class.
         * @param childControl The control created by the implementing class.
         * @return The build control.
         */
        protected <C extends DeviceControl> C build(final C childControl) {
            childControl.setDescription(this.description);
            childControl.setHidden(this.hidden);
            childControl.setRetention(this.retention);
            childControl.setReadOnly(this.readOnly);
            childControl.setTimeout(this.timeout);
            childControl.setShortcutSlot(this.shortcutSlot);
            childControl.setVisualType(this.visualType);
            childControl.setGraphType(this.graphType);
            return childControl;
        }

    }

}
