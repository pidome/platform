/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

import org.pidome.platform.modules.items.ItemAddress;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import org.pidome.platform.modules.items.Item;
import org.pidome.platform.modules.items.ItemDataNotification;

/**
 * Class for creating a device notification to be send out to be passed to the
 * server.
 *
 * @author John
 */
public class DeviceDataNotification implements ItemDataNotification {

    /**
     * Collection of data to be send out.
     */
    private final List<GroupData> groupData = new ArrayList<>();

    /**
     * The notification device address.
     */
    private final ItemAddress address;

    /**
     * The item type of the notification.
     */
    private final Item.ItemType itemType = Item.ItemType.DEVICE;

    /**
     * The id of the device.
     *
     * Set by the server when the notification is received.
     */
    private UUID deviceId;

    /**
     * Constructor.
     *
     * @param deviceAddress The address for which device this is.
     */
    public DeviceDataNotification(final ItemAddress deviceAddress) {
        this.address = deviceAddress;
    }

    /**
     * Adds data to the group to be send out.
     *
     * @param groupId The group id for which the data needs to be updated.
     * @param controlId The control id for which the data needs to be updated.
     * @param value The value to set in the update.
     */
    public final void addData(final String groupId, final String controlId, final Object value) {
        addData(groupId, controlId, value, false);
    }

    /**
     * Adds data to the notification.
     *
     * This method supports interpreting the value even when it is the same as
     * previously. This should only be used in special cases. It is not required
     * to set <code>ignoreSameValue</code> to <code>true</code> to force a
     * storage mechanism to store the data. All data is always stored even when
     * it's the same as previously set.
     *
     * @param groupId The group id for which the data needs to be updated.
     * @param controlId The control id for which the data needs to be updated.
     * @param value The value to set in the update.
     * @param ignoreSameValue When set to true, all mechanisms are forced to use
     * the value, even when it's the same as previously.
     */
    public final void addData(final String groupId, final String controlId, final Object value, final boolean ignoreSameValue) {
        boolean found = false;
        ControlData controlData = new ControlData(controlId, value, ignoreSameValue);
        for (GroupData messageStructSet : groupData) {
            if (messageStructSet.getId().equals(groupId)) {
                found = true;
                messageStructSet.addControlData(controlData);
            }
        }
        if (!found) {
            GroupData group = new GroupData(groupId);
            group.addControlData(controlData);
            groupData.add(group);
        }
    }

    /**
     * @return The device address.
     */
    public final ItemAddress getAddress() {
        return this.address;
    }

    /**
     * The id of the device.
     *
     * Set by the server when the notification is received.
     *
     * @return the deviceId
     */
    public UUID getDeviceId() {
        return deviceId;
    }

    /**
     * The id of the device.
     *
     * Set by the server when the notification is received.
     *
     * @param deviceId the deviceId to set
     */
    public void setDeviceId(final UUID deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * Returns the list with group data.
     *
     * @return Group data list.
     */
    public final List<GroupData> getGroupData() {
        return groupData;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Item.ItemType getItemType() {
        return itemType;
    }

    /**
     * Class for group data.
     */
    public static final class GroupData {

        /**
         * Group Id.
         */
        private final String id;

        /**
         * Control data.
         */
        private final List<ControlData> controlData = new ArrayList<>();

        /**
         * Constructor.
         *
         * @param id The id of the group.
         */
        private GroupData(final String id) {
            this.id = id;
        }

        /**
         * Adds control data.
         *
         * Subsequent calls to this method will overwrite the previously set
         * data.
         *
         * @param control The controldata to add.
         */
        private void addControlData(final ControlData control) {
            Iterator<ControlData> iterator = this.controlData.iterator();
            while (iterator.hasNext()) {
                ControlData data = iterator.next();
                if (data.getId().equals(control.getId())) {
                    iterator.remove();
                    break;
                }
            }
            controlData.add(control);
        }

        /**
         * Returns the group id.
         *
         * @return The id of the group.
         */
        public String getId() {
            return this.id;
        }

        /**
         * Returns all the controls data added.
         *
         * @return Control data list.
         */
        public List<ControlData> getControlData() {
            return controlData;
        }

    }

    /**
     * Holds control data.
     */
    public static final class ControlData {

        /**
         * Id of the control.
         */
        private final String controlId;
        /**
         * the value to set for this control.
         */
        private final Object value;

        /**
         * Read only value to determine if the original value in the device's
         * control is the same as the value being given now.
         */
        private boolean valueIsSameAsPrevious = false;

        /**
         * Indicator to ignore any tests and forces handling of the new value.
         */
        private boolean ignoreSameValue = false;

        /**
         * Set control data to be send as notification or handled by plugins. By
         * default when using this constructor you allow the server to hint
         * implementing handlers the current value to be the same as the
         * previous value if this is determined by the server.
         *
         * @param id id of the control.
         * @param value value to be notified for.
         */
        public ControlData(final String id, final Object value) {
            this(id, value, false);
        }

        /**
         * Set control data to be send as notification or handled by plugins.
         *
         * When using this constructor the server will set the value as always
         * to be fresh. Even when the previous value has been the same. The use
         * of this constructor is heavily discouraged as this may cause unneeded
         * data traffic. This constructor can be determined to be used with
         * getIfPreviousValueIsAlwaysFresh if unmodifiablePrevious is set to
         * true (by default false).
         *
         * @param id id of the control.
         * @param value value to be notified for.
         * @param ignoreSameValue Set to true to let implementations ignore the
         * valueIsSameAsPrevious member.
         */
        public ControlData(final String id, final Object value, final boolean ignoreSameValue) {
            this.controlId = id;
            this.value = value;
            this.ignoreSameValue = ignoreSameValue;
        }

        /**
         * Returns the id.
         *
         * @return The id of the control.
         */
        public String getId() {
            return this.controlId;
        }

        /**
         * Returns the value.
         *
         * @return The value set for the control.
         */
        public Object getValue() {
            return this.value;
        }

        /**
         * Returns false when a value MUST be handled.
         *
         * Even when the value is the same, this method indicates if the value
         * should still be handled. All involved mechanisms will honour this
         * setting except for automation rules.
         *
         * @return true when must be send, false when optional.
         */
        public boolean getValueIsSame() {
            return this.ignoreSameValue ? false : this.valueIsSameAsPrevious;
        }

        /**
         * When a value is set optional it is not required to be handled. IF the
         * server determines the value contained is the same as a previously
         * send value.
         */
        public void setValueIsSame() {
            if (!this.ignoreSameValue) {
                valueIsSameAsPrevious = true;
            }
        }
    }
}
