/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import org.pidome.platform.modules.items.ItemPropertyInterface;

/**
 * A device parameters.
 *
 * Device parameters are used by code, and can be set by code. When Device
 * parameters are used in a discovered device, they will be included in the
 * device created.
 *
 * @author johns
 */
public class DeviceParameter implements ItemPropertyInterface, Serializable {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Name of the parameter.
     */
    private String name;

    /**
     * Parameter value.
     */
    private String value;

    /**
     * Constructor for the device parameter.
     *
     * @param name The name of the parameter.
     * @param value The value of the parameter.
     */
    @JsonCreator
    public DeviceParameter(final @JsonProperty("name") String name, final @JsonProperty("value") String value) {
        this.name = name;
        this.value = value;
    }

    /**
     * Sets the name.
     *
     * @param name The name to set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Sets the value.
     *
     * @param value The value to set.
     */
    public void setValue(final String value) {
        this.value = value;
    }

    /**
     * Name of the parameter.
     *
     * @return the name
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Parameter value.
     *
     * @return the value
     */
    @Override
    public String getValue() {
        return value;
    }

}
