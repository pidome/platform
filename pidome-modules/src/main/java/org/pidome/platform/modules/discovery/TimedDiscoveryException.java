/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.discovery;

/**
 * Exception thrown by discovery services based on periods.
 *
 * @author John
 */
public class TimedDiscoveryException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>TimedDiscoveryException</code> without
     * detail message.
     */
    public TimedDiscoveryException() {
    }

    /**
     * Constructs an instance of <code>TimedDiscoveryException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public TimedDiscoveryException(final String msg) {
        super(msg);
    }
}
