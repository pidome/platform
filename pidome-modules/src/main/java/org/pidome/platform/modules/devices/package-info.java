/**
 * Module types package.
 * <p>
 * Supplying module types.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.platform.modules.devices;
