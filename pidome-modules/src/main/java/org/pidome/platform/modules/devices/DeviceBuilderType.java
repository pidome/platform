/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

/**
 * The device builder type.
 *
 * A device builder type determines if a builder via code, a file or database
 * should be used.
 *
 * @author johns
 */
public enum DeviceBuilderType {

    /**
     * The builder type.
     *
     * The builder type requires a device to implement build in which a builder
     * is provided so groups and controls can be created.
     */
    BUILDER(false),
    /**
     * The provided builder type.
     *
     * A provided builder type has a device which supplies the server a fixed
     * set of groups and the controls which are able to exist in this group.
     * <p>
     * By using the builder the implementer builds all the groups with their
     * possible controls.
     * <p>
     * Example:<br/>
     * KlikAanKlikUit devices uses the same signal and identification for both
     * switching a light switch (remote), as detection motion with a motion
     * sensor. This is why motion sensors for KlikAanKlikUit can act as a
     * "remote" for a light switch. Because switching and motion are exactly the
     * same, it is handled identically and for us impossible to say it's a
     * motion sensor or a switch. With custom building we can provide the user
     * with an option to use a device with a switch (like the remote), or have
     * it being displayed as motion detected (the detector). We can supply the
     * user with two different controls, but these interpret the signal
     * identically, it is only showed differently.
     */
    PROVIDED(true),
    /**
     * A custom device.
     *
     * A device providing USER is a custom device. A custom device does not have
     * a fixed structure nor provides a possible structure as with PROVIDED. The
     * groups and control are dynamic and are created using a web interface by
     * the user.
     */
    USER(true),
    /**
     * An unknown building type.
     *
     * When a device does not correctly implement on of the above the unknown is
     * set for a front-end or development tool to identify an incorrect builder
     * type.
     */
    UNKNOWN(false);

    /**
     * If custom building is supported on the type.
     */
    private final boolean supportCustomBuild;

    /**
     * Constructor for the enum.
     *
     * @param supportsCustom If custom building is supported.
     */
    DeviceBuilderType(final boolean supportsCustom) {
        this.supportCustomBuild = supportsCustom;
    }

    /**
     * If the type supports custom building.
     *
     * @return boolean.
     */
    public final boolean getSupportsCustomBuild() {
        return this.supportCustomBuild;
    }

}
