/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.discovery;

import io.vertx.core.Promise;
import org.pidome.platform.modules.items.DiscoveredItem;

/**
 * Interface for modules supporting items discovery.
 *
 * To be informated as a module discovery is enabled override
 * <code>discover(Promise<Void> promise)</code> with a promise to
 * <code>promise.complete()</code> when discovery has completed.
 *
 * @author johns
 */
public interface ItemDiscovery {

    /**
     * Returns if discovery is enabled or not.
     *
     * When in the capabilities <code>Capabilities.DISCOVERY</code> is returned
     * and the end user has turned on device discovery this is set to true.
     *
     * @return If discovery is enabled or not.
     */
    boolean isDiscoveryEnabled();

    /**
     * Enables or disables discovery, called by the server.
     *
     * It has no use to set this from a module.
     *
     * When the server calls this method the internal list with found items is
     * cleared.
     *
     * @param enabled set to true to enable, false to disable.
     */
    void setDiscoveryEnabled(boolean enabled);

    /**
     * Called by the server to inform a module discovery is enabled.
     *
     * On the promise set an handler with
     * <pre>{@code
     * promise.future().setHandler(result -> {
     *      ///code to execute when discovery has ended.
     * });
     * }</pre>
     *
     * There are two ways of ending a discovery session. The server/user cancels
     * and thus calls <code>Promise.complete()</code>, by default the case with
     * discovery. If you must scan a bus or scan by other means you can call
     * <code>promise.complete()</code> when done. When the promise is completed
     * the code set by <code>promise.future().setHandler</code> will be
     * executed. After a completed promise, any discovered item will be
     * discarded.
     *
     * @param promise The promise to complete when discovery has completed.
     */
    default void discover(Promise<Void> promise) {
    }

    /**
     * Method to be called by a module when a device has been discovered.
     *
     * Calling this method does not guarantee the order of devices to be shown
     * to an end user.
     *
     * @param discoveredItem The discovered item.
     */
    void discovered(DiscoveredItem discoveredItem);

}
