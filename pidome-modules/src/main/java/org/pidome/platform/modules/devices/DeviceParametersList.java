/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

import java.util.ArrayList;
import java.util.Optional;
import org.pidome.platform.modules.items.ItemPropertiesListInterface;

/**
 * List of device parameters.
 *
 * Typed array list.
 *
 * @author johns
 */
public class DeviceParametersList extends ArrayList<DeviceParameter> implements ItemPropertiesListInterface<DeviceParameter> {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Returns a parameter by it's name.
     *
     * @param parameterName The name to return for.
     * @return The parameter requested.
     */
    @Override
    public Optional<DeviceParameter> getParameter(final String parameterName) {
        return this.stream().filter(parameter -> parameter.getName().equals(parameterName)).findFirst();
    }

}
