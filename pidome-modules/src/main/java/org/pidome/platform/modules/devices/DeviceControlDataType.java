/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

/**
 * The data types inside a control.
 *
 * @author John
 */
public enum DeviceControlDataType {
    /**
     * String type.
     */
    STRING,
    /**
     * Any number type.
     */
    NUMBER,
    /**
     * Hex value type.
     */
    HEX,
    /**
     * A boolean type.
     */
    BOOLEAN,
    /**
     * A color type.
     */
    COLOR,
    /**
     * A custom type.
     */
    CUSTOM;
}
