/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices.discovery;

import io.vertx.core.Promise;
import java.util.List;
import java.util.Map;
import org.pidome.platform.modules.devices.Device;
import org.pidome.platform.modules.items.ItemAddress;
import org.pidome.platform.modules.devices.DeviceDataNotification;

/**
 * Listener interface for discovered devices.
 *
 * @author John
 */
public interface DevicesServerProxy {

    /**
     * Handle a newly discovered device. Do not try to remove a device using
     * this handle.
     *
     * @param device The discovered device.
     */
    void handleNewDiscoveredDevice(DiscoveredDevice device);

    /**
     * Returns a map of device addresses currently active in the module.
     *
     * @return Map of device types with their addresses currently running.
     */
    Map<Class<? extends Device>, List<ItemAddress>> getRunningAddresses();

    /**
     * Notifies the host of new data.
     *
     * @param notification The notification to send.
     * @param sendResult Promise to report success or failure back to the
     * module.
     */
    void handleDeviceDataNotification(DeviceDataNotification notification, Promise sendResult);

}
