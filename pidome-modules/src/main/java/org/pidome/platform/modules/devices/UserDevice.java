/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

import org.pidome.platform.modules.items.ItemAddress;

/**
 * A custom device.
 *
 * A custom device let's the end user create the device structure.
 *
 * @author johns
 * @param <A> The address type.
 */
public abstract class UserDevice<A extends ItemAddress> extends Device<A> {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Returns the builder type.
     *
     * @return The builder type.
     */
    @Override
    public final DeviceBuilderType getDeviceBuilderType() {
        return DeviceBuilderType.USER;
    }

}
