/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules;

/**
 * Capabilities set for modules.
 *
 * @author johns
 */
public enum ModuleCapabilities {

    /**
     * The module supports automatic items discovery.
     *
     * When returned, the end user will get the option to enable item discovery
     * for the module. By default a new item is discovered when an item with an
     * unknown address is received or by scanning for new items.
     * <p>
     * Discovery of new items comes with options not exposed to a module, these
     * can be, but are not limited to:
     * <ul>
     * <li>Stop after one device is found</li>
     * <li>discovery is enabled for a limited set of time</li>
     * </ul>
     * <p>
     * A module will be informed when discovery has been enabled. By calling
     * <code>isDiscoveryEnabled()</code> when data is received an adhoc check
     * can be done to identify if discovery is enabled or not. Discovery is a
     * non server blocking method and can be cancelled by the server or user at
     * any time.
     * <p>
     * Example pseudo code:
     * <pre>{@code
     * public void handleDeviceData(device){
     *     if(isDiscoveryEnabled() && !device address is known){
     *         DiscoveredDevice newDevice = new DiscoveredDevice();
     *         discovered(newDevice);
     *     }
     * }
     * }</pre> Refer to
     * {@link org.pidome.platform.modules.discovery.ItemDiscovery#discover(Promise)}
     * on how to be informed when discovery is enabled and disabled.
     */
    DISCOVERY;
}
