/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

/**
 * The visual type of a control.
 *
 * Using this helps a front-end to decide what kind of visual control type the
 * control is. This is mainly used with data controls.
 *
 * @author johns
 */
public enum ControlVisualType {

    /**
     * A non defined visual type.
     *
     * This is the default as mostly visual types would only be used with data
     * controls.
     */
    NONE("No visual presentation"),
    /**
     * Motion based control.
     *
     * Motion based controls are re-used in maps.
     */
    MOVE("Motion detection"),
    /**
     * Temperature type.
     *
     * Temperature is always in Celsius and used in maps.
     */
    TEMPERATURE("Temperature"),
    /**
     * Light is always in LUX and used in maps.
     */
    LIGHT("Light intensity"),
    /**
     * A Computer type.
     */
    COMPUTER("Computer or peripheral"),
    /**
     * Pressure.
     */
    PRESSURE("Pressure"),
    /**
     * Fluid.
     */
    FLUID("Fluid type"),
    /**
     * Wind speed.
     */
    WIND("Wind"),
    /**
     * Battery.
     */
    BATTERY("Battery level"),
    /**
     * Signal strength type.
     */
    SIGNAL("Signal strength"),
    /**
     * Compass.
     */
    COMPASS("Compass (direction)");

    /**
     * The description of the enum type.
     */
    private final String description;

    /**
     * Enum constructor.
     *
     * @param description The description of the enum.
     */
    ControlVisualType(final String description) {
        this.description = description;
    }

    /**
     * Returns the description of the enum.
     *
     * @return Enum description.
     */
    public String getDescription() {
        return this.description;
    }

}
