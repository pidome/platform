/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * A group of device controls.
 *
 * @author John
 */
public class DeviceControlGroup implements Serializable {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The group id.
     */
    private final String id;
    /**
     * The group title.
     */
    private final String title;
    /**
     * If the group is hidden.
     */
    private boolean hidden = false;

    /**
     * The controls in the group.
     */
    private List<DeviceControl> controls;

    /**
     * The device this group is linked in.
     */
    @JsonIgnore
    private transient Device device;

    /**
     * Constructor.
     *
     * @param groupId The id of the group.
     * @param title The title of the group.
     * @throws IllegalArgumentException When the group is unable to be
     * constructed properly.
     */
    @JsonCreator
    public DeviceControlGroup(@JsonProperty("id") final String groupId, @JsonProperty("title") final String title) throws IllegalArgumentException {
        if (groupId == null || groupId.length() < 1) {
            throw new IllegalArgumentException("Invalid group id set (unavailable or length)");
        }
        this.id = groupId;
        this.title = title;
    }

    /**
     * Returns the group id.
     *
     * @return The id of the group.
     */
    public String getId() {
        return this.id;
    }

    /**
     * Returns the group title.
     *
     * @return The title of the group.
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Set the device link.
     *
     * @param device the device this group belongs to.
     */
    protected void setDevice(final Device device) {
        this.device = device;
    }

    /**
     * The device this group belongs to.
     *
     * @return The device.
     */
    protected Device getDevice() {
        return this.device;
    }

    /**
     * Hides a group from visual output.
     *
     * @param hidden Set if the control should be hidden.
     */
    protected void setHidden(final boolean hidden) {
        this.hidden = hidden;
    }

    /**
     * Returns true if the group is hidden.
     *
     * @return If the group should be hidden.
     */
    public boolean isHidden() {
        return hidden;
    }

    /**
     * Set the controls.
     *
     * @param controls The controls to set.
     */
    public void setControls(final List<DeviceControl> controls) {
        this.controls = controls;
    }

    /**
     * Returns the controls.
     *
     * @return The controls.
     */
    public List<DeviceControl> getControls() {
        return this.controls;
    }

    /**
     * Returns the control by id.
     *
     * @param controlId The id of the control to retrieve.
     * @return The control requested, empty optional when not found.
     */
    public Optional<DeviceControl> getControl(final String controlId) {
        return this.controls.stream()
                .filter(control -> control.getId().equals(controlId))
                .findFirst();
    }

}
