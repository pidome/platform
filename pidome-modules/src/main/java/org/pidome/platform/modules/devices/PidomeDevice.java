/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

/**
 * Annotation used to identify devices.
 *
 * @author johns
 */
public @interface PidomeDevice {

}
