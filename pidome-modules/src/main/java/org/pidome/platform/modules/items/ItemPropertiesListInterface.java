/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.items;

import java.util.List;
import java.util.Optional;

/**
 * Interface for option lists.
 *
 * @author johns
 * @param <T> The type in the option list.
 */
public interface ItemPropertiesListInterface<T extends ItemPropertyInterface> extends List<T> {

    /**
     * Returns the option as an optional.
     *
     * @param parameterName The name of the option.
     * @return The option requested by name, otherwise an empty optional.
     */
    Optional<? extends ItemPropertyInterface> getParameter(String parameterName);

}
