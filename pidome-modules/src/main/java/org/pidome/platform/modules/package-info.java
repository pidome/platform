/**
 * Module implemntation package.
 * <p>
 * Supplying base classes to be used for module implementations.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.platform.modules;
