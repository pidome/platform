/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The color picker control.
 *
 * The color picker is of complex type using an
 * <code>DeviceColorPickerControlColorData</code> for it's value.
 *
 * @author John
 */
public final class DeviceColorPickerControl extends DeviceControl<DeviceColorPickerControlColorData> {

    /**
     * Class version.
     */
    public static final long serialVersionUID = 1L;

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(DeviceColorPickerControl.class);

    /**
     * The buttons list to set any control command.
     */
    private ControlCommandSet<String> commandSet;

    /**
     * The default color picker mode.
     */
    private ColorpickerMode mode = ColorpickerMode.HSB;

    /**
     * Constructor.
     *
     * @param id The id of the control.
     */
    @JsonCreator
    public DeviceColorPickerControl(@JsonProperty("id") final String id) {
        super(id);
        Map<String, Object> initialValues = new HashMap<>();
        initialValues.put("h", 0.0F);
        initialValues.put("s", 0.0F);
        initialValues.put("b", 0.0F);
        LOG.debug("Setting initial value for color picker [{}]", initialValues);
        String initial = UUID.randomUUID().toString();
        this.setValue(new DeviceColorPickerControlColorData(new ControlCommand<>(initial, initial), initialValues));
    }

    /**
     * Set the color picker mode.
     *
     * @param mode The mode.
     */
    public void setMode(final ColorpickerMode mode) {
        this.mode = mode;
    }

    /**
     * Returns the color picker mode.
     *
     * See <code>ColorpickerMode</code> for details.
     *
     * @return The picker mode.
     */
    public ColorpickerMode getMode() {
        return this.mode;
    }

    /**
     * Returns the full buttons list.
     *
     * @return The command set.
     */
    public ControlCommandSet getCommandSet() {
        return this.commandSet;
    }

    /**
     * Returns the full buttons list.
     *
     * @param commandSet The command set.
     */
    public void setCommandSet(final ControlCommandSet<String> commandSet) {
        this.commandSet = commandSet;
    }

    /**
     * Return the RGB values This function returns a map with the keys r,g and
     * b.
     *
     * @return The color RGB values.
     */
    public Map<String, Integer> getRGB() {
        return super.getValue().getRGB();
    }

    /**
     * Return the RGB values This function returns a map with the keys h,s and
     * b.
     *
     * @return The color HSB values.
     */
    public Map<String, Double> getHSB() {
        return super.getValue().getHSB();
    }

    /**
     * Returns current kelvin value.
     *
     * Kelvin value is only available when kelvin is used to set the data.
     * Otherwise always returns 0.
     *
     * @return The kelvin value.
     */
    public Long getKelvin() {
        return super.getValue().getKelvin();
    }

    /**
     * Returns current hex value.
     *
     * Returns the hex values as #000000.
     *
     * @return The hex value.
     */
    @JsonIgnore
    public String getHex() {
        return super.getValue().getHex();
    }

    /**
     * Content equality check.
     *
     * Currently equality will mostly be false unless color = 0. We need a
     * better check on the values with rounding checks.
     *
     * @param o The content to check
     * @return true if equal
     */
    @Override
    public boolean valueEquals(final Object o) {
        /// We will do a comparison on the HSB values as these have the highest resolution.
        if (o == null) {
            return false;
        } else if (!(o instanceof DeviceColorPickerControlColorData)) {
            return false;
        } else if (this.getValueData() == null) {
            return false;
        } else {
            try {
                Map<String, Double> curData = this.getHSB();
                Map<String, Double> newData = ((DeviceColorPickerControlColorData) o).getHSB();
                if (newData != null) {
                    for (Map.Entry<String, Double> checkData : curData.entrySet()) {
                        if (newData.get(checkData.getKey()) == null || checkData.getValue().doubleValue() != newData.get(checkData.getKey()).doubleValue()) {
                            return false;
                        }
                    }
                    return true;
                } else {
                    return false;
                }
            } catch (Exception ex) {
                /// unable to compare, 99.99999% cause is possible missing data.
                return false;
            }
        }
    }

    /**
     * Returns a new button control builder.
     *
     * @param id The id of the control.
     * @return The button builder.
     */
    public static ColorPickerBuilder colorpickerBuilder(final String id) {
        return new ColorPickerBuilder(id);
    }

    /**
     * Builder for the device button control.
     */
    @SuppressWarnings("PMD.AvoidFieldNameMatchingMethodName") //Builder pattern.
    public static class ColorPickerBuilder extends DeviceControl.ControlBuilder<ColorPickerBuilder> implements DeviceControlBuilder<DeviceColorPickerControl> {

        /**
         * The control id.
         */
        private final String id;

        /**
         * Color picker mode.
         */
        private ColorpickerMode mode = ColorpickerMode.HSB;

        /**
         * The command set for the color picker.
         */
        private final ControlCommandSet<String> commandSet = new ControlCommandSet<>();

        /**
         * Builder constructor.
         *
         * @param controlId The id of the control.
         */
        public ColorPickerBuilder(final String controlId) {
            this.id = controlId;
            this.setReference(this);
        }

        /**
         * the color picker mode.
         *
         * @param pickerMode The color picker mode.
         * @return Builder.
         */
        public ColorPickerBuilder mode(final ColorpickerMode pickerMode) {
            this.mode = pickerMode;
            return this;
        }

        /**
         * Adds a command to the color picker.
         *
         * @param command The command to add.
         * @return The builder.
         */
        public ColorPickerBuilder addCommand(final ControlCommand<String> command) {
            this.commandSet.getSet().add(command);
            return this;
        }

        /**
         * Builds the color picker control.
         *
         * @return The color picker control.
         */
        @Override
        public final DeviceColorPickerControl build() {
            DeviceColorPickerControl control = new DeviceColorPickerControl(this.id);
            control.setControlType(DeviceControlType.COLORPICKER);
            super.build(control);
            control.setCommandSet(this.commandSet);
            control.setMode(this.mode);
            return control;
        }

    }

}
