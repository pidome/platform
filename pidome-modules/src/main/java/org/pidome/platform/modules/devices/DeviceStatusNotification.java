/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

import org.pidome.platform.modules.items.ItemAddress;

/**
 * Device status message.
 *
 * @author johns
 */
public final class DeviceStatusNotification extends DeviceDataNotification {

    /**
     * The device status holder.
     */
    private Device.DeviceStatus status = null;

    /**
     * The reason for the notification.
     */
    private String statusReason = null;

    /**
     * The control it is about.
     */
    private String statusControl = null;

    /**
     * The control group it is about.
     */
    private String statusControlGroup = null;

    /**
     * Set's a device status.
     *
     * @param address The address of the device affected.
     * @param deviceStatus The new status of the device.
     * @param reason The reason for the current status.
     * @param groupId The id of the control group.
     * @param controlId The id of the control.
     */
    protected DeviceStatusNotification(final ItemAddress address, final Device.DeviceStatus deviceStatus, final String reason, final String groupId, final String controlId) {
        super(address);
        this.status = deviceStatus;
        this.statusReason = reason;
        this.statusControlGroup = groupId;
        this.statusControl = controlId;
    }

    /**
     * @return True if the notification is a device status notification.
     */
    public boolean isDeviceStatusNotification() {
        return true;
    }

    /**
     * Returns the device status.
     *
     * @return The device status, null when there is no status info.
     */
    public Device.DeviceStatus getStatus() {
        return this.status;
    }

    /**
     * Returns a device status reason.
     *
     * @return The reason, null when there is none.
     */
    public String getStatusReason() {
        return this.statusReason;
    }

    /**
     * Returns the status control.
     *
     * @return The control id of the status update notification.
     */
    public String getStatusControl() {
        return this.statusControl;
    }

    /**
     * Returns the status control group.
     *
     * @return The group id of the status control notification.
     */
    public String getStatusGroup() {
        return this.statusControlGroup;
    }

}
