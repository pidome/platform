/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.tools.utilities.ColorUtil;

/**
 * Color command object.
 *
 * @author John
 */
public class DeviceColorPickerControlColorData {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(DeviceColorPickerControlColorData.class);

    /**
     * the length of a color array.
     */
    private static final byte COLOR_ARRAY_LENGTH = 3;

    /**
     * Kelvin name.
     */
    private static final String KELVIN_NAME = "kelvin";

    /**
     * HEX name.
     */
    private static final String HEX_NAME = "hex";
    /**
     * RGB name.
     */
    private static final String RGB_NAME = "rgb";
    /**
     * HSB name.
     */
    private static final String HSB_NAME = "hsb";

    /**
     * RGB red.
     */
    private static final String RED = "r";
    /**
     * RGB Green.
     */
    private static final String GREEN = "g";
    /**
     * RGB Blue.
     */
    private static final String BLUE = "b";

    /**
     * HSB Hue.
     */
    private static final String HUE = "h";
    /**
     * HSB Saturation.
     */
    private static final String SATURATION = "s";
    /**
     * HSB Brightness.
     */
    private static final String BRIGHTNESS = "b";

    /**
     * The color currently active.
     */
    private final Map<String, Object> color = new HashMap<>();

    /**
     * the action to take with the given color.
     */
    private final ControlCommand action;

    /**
     * Constructor to set color data.
     *
     * @param action The action to perform.
     * @param colorSet The color map value.
     */
    @JsonCreator
    public DeviceColorPickerControlColorData(final @JsonProperty("action") ControlCommand action, final @JsonProperty("color") Map<String, Object> colorSet) {

        this.action = action;

        Map<String, Double> finalHsb = new HashMap<>();
        finalHsb.put(HUE, 0.0d);
        finalHsb.put(SATURATION, 0.0d);
        finalHsb.put(BRIGHTNESS, 0.0d);

        Map<String, Integer> finalRGB = new HashMap<>();
        finalRGB.put(RED, 0);
        finalRGB.put(GREEN, 0);
        finalRGB.put(BLUE, 0);

        String finalHex = "#000000";

        Long finalKelvin = 0L;
        // CPD-OFF
        try {
            if (colorSet.containsKey(HEX_NAME)) {
                finalHex = (String) colorSet.get(HEX_NAME);
                int[] rgbFromHex = ColorUtil.hexToRgb((String) colorSet.get(HEX_NAME));
                finalRGB.put(RED, rgbFromHex[0]);
                finalRGB.put(GREEN, rgbFromHex[1]);
                finalRGB.put(BLUE, rgbFromHex[2]);
                float[] fromHex = ColorUtil.rgbToHsb(rgbFromHex[0], rgbFromHex[1], rgbFromHex[2]);
                finalHsb.put(HUE, fromHex[0] / 1d);
                finalHsb.put(SATURATION, fromHex[1] / 1d);
                finalHsb.put(BRIGHTNESS, fromHex[2] / 1d);
            } else if (colorSet.containsKey(RED) && colorSet.containsKey(GREEN) && colorSet.containsKey(BLUE)) {
                finalRGB.put(RED, ((Number) colorSet.get(RED)).intValue());
                finalRGB.put(GREEN, ((Number) colorSet.get(GREEN)).intValue());
                finalRGB.put(BLUE, ((Number) colorSet.get(BLUE)).intValue());
                int[] list = new int[COLOR_ARRAY_LENGTH];
                list[0] = finalRGB.get(RED);
                list[1] = finalRGB.get(GREEN);
                list[2] = finalRGB.get(BLUE);
                finalHex = ColorUtil.rgbToHex(list[0], list[1], list[2]);
                float[] fromRGB = ColorUtil.rgbToHsb(list[0], list[1], list[2]);
                finalHsb.put(HUE, fromRGB[0] / 1d);
                finalHsb.put(SATURATION, fromRGB[1] / 1d);
                finalHsb.put(BRIGHTNESS, fromRGB[2] / 1d);
            } else if (colorSet.containsKey(HUE) && colorSet.containsKey(SATURATION) && colorSet.containsKey(BRIGHTNESS)) {
                finalHsb.put(HUE, ((Number) colorSet.get(HUE)).doubleValue());
                finalHsb.put(SATURATION, ((Number) colorSet.get(SATURATION)).doubleValue());
                finalHsb.put(BRIGHTNESS, ((Number) colorSet.get(BRIGHTNESS)).doubleValue());
                int[] rgbFromHSB = ColorUtil.hsbToRgb((float) (finalHsb.get(HUE) / 1f), (float) (finalHsb.get(SATURATION) / 1f), (float) (finalHsb.get(BRIGHTNESS) / 1f));
                finalRGB.put(RED, rgbFromHSB[0]);
                finalRGB.put(GREEN, rgbFromHSB[1]);
                finalRGB.put(BLUE, rgbFromHSB[2]);
                finalHex = ColorUtil.rgbToHex(rgbFromHSB[0], rgbFromHSB[1], rgbFromHSB[2]);
            } else if (colorSet.containsKey(KELVIN_NAME)) {
                finalKelvin = (Long) colorSet.get(KELVIN_NAME);
                float[] fromKelvin = ColorUtil.kelvinToHsb((Long) colorSet.get(KELVIN_NAME));
                finalHsb.put(HUE, fromKelvin[0] / 1d);
                finalHsb.put(SATURATION, fromKelvin[1] / 1d);
                finalHsb.put(BRIGHTNESS, fromKelvin[2] / 1d);
                int[] rgbFromHSB = ColorUtil.hsbToRgb((float) (finalHsb.get(HUE) / 1f), (float) (finalHsb.get(SATURATION) / 1f), (float) (finalHsb.get(BRIGHTNESS) / 1f));
                finalRGB.put(RED, rgbFromHSB[0]);
                finalRGB.put(GREEN, rgbFromHSB[1]);
                finalRGB.put(BLUE, rgbFromHSB[2]);
                finalHex = ColorUtil.rgbToHex(rgbFromHSB[0], rgbFromHSB[1], rgbFromHSB[2]);
            }
        } catch (Exception ex) {
            LOG.error("Could not convert: {}", ex.getMessage(), ex);
        }
        // CPD-ON
        color.put(HEX_NAME, finalHex);
        color.put(RGB_NAME, finalRGB);
        color.put(HSB_NAME, finalHsb);
        color.put(KELVIN_NAME, finalKelvin);
    }

    /**
     * The action to take.
     *
     * @return The action to take.
     */
    public final ControlCommand getAction() {
        return this.action;
    }

    /**
     * Returns the full color map.
     *
     * @return The color map.
     */
    public final Map<String, Object> getColor() {
        return color;
    }

    /**
     * Returns the raw color map to be used for notifications.
     *
     * @return The color map to be used for notifications.
     */
    public final Map<String, Object> getNotificationColorMap() {
        return Collections.unmodifiableMap(color);
    }

    /**
     * Return the RGB values This function returns a map with the keys r,g and
     * b.
     *
     * @return The RGB value.
     */
    @SuppressWarnings("unchecked")
    public final Map<String, Integer> getRGB() {
        return (Map<String, Integer>) color.get(RGB_NAME);
    }

    /**
     * Return the RGB values This function returns a map with the keys h,s and
     * b.
     *
     * @return The HSB value.
     */
    @SuppressWarnings("unchecked")
    public final Map<String, Double> getHSB() {
        return (Map<String, Double>) color.get(HSB_NAME);
    }

    /**
     * Returns current kelvin value. Kelvin value is only available when kelvin
     * is used to set the data. Otherwise always returns 0.
     *
     * @return Kelvin value
     */
    @SuppressWarnings("unchecked")
    public final Long getKelvin() {
        return (Long) color.get(KELVIN_NAME);
    }

    /**
     * Returns current hex value. Returns the hex values as #000000
     *
     * @return Hex value
     */
    @SuppressWarnings("unchecked")
    public final String getHex() {
        return (String) color.get(HEX_NAME);
    }

}
