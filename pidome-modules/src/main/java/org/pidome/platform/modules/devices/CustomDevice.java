/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

import org.pidome.platform.modules.items.ItemAddress;

/**
 * A custom device is a device where the end user is in total control of how a
 * device is build.A custom device lets the end user define it's controls.
 *
 * This provides an unlimited setup for the end user. A custom device does not
 * have a builder nor provides pre-defined controls.
 *
 * @author johns
 * @param <A> The address type.
 */
public abstract class CustomDevice<A extends ItemAddress> extends Device<A> {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Public default constructor.
     */
    public CustomDevice() {
        super();
    }

    /**
     * Returns the builder type.
     *
     * @return The builder type.
     */
    @Override
    public final DeviceBuilderType getDeviceBuilderType() {
        return DeviceBuilderType.USER;
    }

}
