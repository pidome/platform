/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.items;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * The device address.
 *
 * @author John
 * @param <T> The datatype for the address.
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.EXISTING_PROPERTY,
        property = "dataType",
        visible = true)
@JsonSubTypes({
    @JsonSubTypes.Type(value = ItemHexAddress.class, name = "HEX_ADDRESS"),
    @JsonSubTypes.Type(value = ItemBooleanAddress.class, name = "BOOLEAN_ADDRESS"),
    @JsonSubTypes.Type(value = ItemFloatAddress.class, name = "FLOAT_ADDRESS"),
    @JsonSubTypes.Type(value = ItemIntegerAddress.class, name = "INTEGER_ADDRESS"),
    @JsonSubTypes.Type(value = ItemStringAddress.class, name = "STRING_ADDRESS"),
    @JsonSubTypes.Type(value = ItemIpAddress.class, name = "IP_ADDRESS")
})
@SuppressWarnings("PMD.AbstractClassWithoutAbstractMethod")
public abstract class ItemAddress<T> implements Cloneable {

    /**
     * The address data type.
     */
    private String dataType;
    /**
     * A description for the address.
     */
    private String description;
    /**
     * The input type description.
     */
    private String label;

    /**
     * The current device address.
     */
    private T address;

    /**
     * Abstract constructor.
     *
     * @param type The type of field.
     */
    ItemAddress(final String type) {
        this.setDataType(type);
    }

    /**
     * Sets the address data type.
     *
     * @param dataType The datatype to set.
     */
    public final void setDataType(final String dataType) {
        this.dataType = dataType;
    }

    /**
     * Returns the data type.
     *
     * @return The data type.
     */
    public final String getDataType() {
        return this.dataType;
    }

    /**
     * Sets the address.
     *
     * @param address The address to set.
     */
    public final void setAddress(final T address) {
        this.address = address;
    }

    /**
     * Sets an address based on string input.
     *
     * @param stringAddress The string based address.
     * @return The address typed as expected.
     */
    @SuppressWarnings({"unchecked"})
    public final T setAddressFromString(final String stringAddress) {
        switch (this.dataType) {
            case "HEX_ADDRESS":
            case "STRING_ADDRESS":
            case "IP_ADDRESS":
                this.setAddress((T) stringAddress);
                break;
            case "BOOLEAN_ADDRESS":
                this.setAddress((T) Boolean.valueOf(stringAddress));
                break;
            case "FLOAT_ADDRESS":
                this.setAddress((T) Float.valueOf(stringAddress));
                break;
            case "INTEGER_ADDRESS":
                this.setAddress((T) Integer.valueOf(stringAddress));
                break;
            default:
                this.setAddress((T) stringAddress);
                break;
        }
        return address;
    }

    /**
     * Returns the device address.
     *
     * @return The address.
     */
    public final T getAddress() {
        return address;
    }

    /**
     * Sets the description.
     *
     * @param description The description to set.
     */
    public final void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Returns the main address description.
     *
     * @return The address description.
     */
    public final String getDescription() {
        return this.description;
    }

    /**
     * Sets the input description.
     *
     * @param label the address input description.
     */
    public final void setLabel(final String label) {
        this.label = label;
    }

    /**
     * Returns the input description.
     *
     * @return the address input description.
     */
    public final String getLabel() {
        return this.label;
    }

    /**
     * Returns true if an device has an address.
     *
     * @return true if an address is available.
     */
    public boolean hasAddress() {
        return this.address == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ItemAddress clone() throws CloneNotSupportedException {
        return (ItemAddress) super.clone();
    }

}
