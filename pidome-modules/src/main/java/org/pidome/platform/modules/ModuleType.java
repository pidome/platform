/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules;

import org.pidome.platform.modules.devices.DevicesModule;

/**
 * The available module types.
 *
 * @author johns
 */
public enum ModuleType {

    /**
     * Devices.
     */
    DEVICES("Devices module", DevicesModule.class),
    /**
     * Fallback for unknown modules.
     */
    UNKNOWN("Unknown module", UnknownModule.class);

    /**
     * The description of the communication subsystem enum.
     */
    private final String description;

    /**
     * The implementation required for the specific transport.
     */
    private final Class<? extends ModuleBase> implementation;

    /**
     * The enum constructor.
     *
     * @param fieldDescription The description of the enum.
     * @param implementation The implementation of the module.
     */
    ModuleType(final String fieldDescription, final Class<? extends ModuleBase> implementation) {
        this.description = fieldDescription;
        this.implementation = implementation;
    }

    /**
     * Returns the description of the enum.
     *
     * @return The enum description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Returns the implementation required for the module type.
     *
     * @return An implementation.
     */
    public final Class<? extends ModuleBase> getImplementation() {
        return implementation;
    }

    /**
     * Returns the Type by the given implementation class.
     *
     * @param implementationClass The raw class extending the required
     * interface.
     * @return The Type of the given class.
     */
    public static ModuleType getByImplementationClass(final Class<? extends ModuleBase> implementationClass) {
        return getByImplementationClass(implementationClass.getSimpleName());
    }

    /**
     * Returns the Type by the given implementation class.
     *
     * @param classSimpleName The <code>Class.getSimpleName</code> string.
     * @return The subsystem of the given class.
     */
    public static ModuleType getByImplementationClass(final String classSimpleName) {
        for (ModuleType type : ModuleType.values()) {
            if (type.getImplementation().getSimpleName().equals(classSimpleName)) {
                return type;
            }
        }
        return ModuleType.UNKNOWN;
    }

    /**
     * Returns the enum by the given name.
     *
     * @param enumName The name given to retrieve the enum for.
     * @return The enum found by it's name. If not found <code>UNKNOWN</code> is
     * returned.
     */
    public static ModuleType getByName(final String enumName) {
        for (ModuleType type : ModuleType.values()) {
            if (type.name().equals(enumName)) {
                return type;
            }
        }
        return ModuleType.UNKNOWN;
    }

}
