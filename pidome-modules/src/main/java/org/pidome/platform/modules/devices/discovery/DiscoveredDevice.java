/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices.discovery;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.UUID;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.modules.devices.Device;
import org.pidome.platform.modules.devices.DeviceBuilderType;
import org.pidome.platform.modules.devices.DeviceParameter;
import org.pidome.platform.modules.devices.DeviceParametersList;
import org.pidome.platform.modules.items.DiscoveredItem;
import org.pidome.platform.modules.items.Item;
import org.pidome.platform.modules.items.ItemAddress;
import org.pidome.platform.modules.items.ItemPropertiesListInterface;
import org.pidome.platform.presentation.Presentation;
import org.pidome.platform.presentation.components.list.DescriptionList;
import org.pidome.platform.presentation.components.list.DescriptionListItem;

/**
 * Information about a discovered device.With this class a user is presented a
 * device in the discovered devices list.
 *
 * @author John
 * @param <A> The address type used by the device.
 */
public class DiscoveredDevice<A extends ItemAddress> implements DiscoveredItem {

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(DiscoveredDevice.class);

    /**
     * Visual presentative information.
     *
     * This can contain additional information for the end user.
     */
    private final Presentation information = new Presentation("Device information");

    /**
     * The description list for the <code>information</code>.
     */
    @JsonIgnore
    private final DescriptionList descriptionList = new DescriptionList();

    /**
     * Parameters specific for a device.
     *
     * Parameters can be used for identical structured devices, but have some
     * differences. for example exact the same groups and controls but a
     * different protocol. You would put some protocol identifiable information
     * in the parameters.
     *
     * These parameters are unmodifiable once put in a discovered device.
     */
    private final DeviceParametersList parameters = new DeviceParametersList();

    /**
     * Discovered device address.
     */
    private A address;

    /**
     * Discovered device address.
     */
    private final String name;

    /**
     * The device builder type.
     */
    private DeviceBuilderType builderType = DeviceBuilderType.UNKNOWN;

    /**
     * The device driver when set.
     */
    private final Class<? extends Device<A>> device;

    /**
     * When the device was discovered. Created in the constructor.
     */
    private final Date discoveryDateTime;

    /**
     * Reference id.
     */
    private UUID id;

    /**
     * Constructing a new found device.
     *
     * @param deviceClass The class definition of this device.
     * @param identifyingName A name to give so an end user can find out what
     * kind of device it is.
     */
    public DiscoveredDevice(final Class<? extends Device<A>> deviceClass, final String identifyingName) {
        this.name = identifyingName;
        this.device = deviceClass;
        information.addSection(descriptionList);
        this.discoveryDateTime = new Date();
        this.id = UUID.randomUUID();
        try {
            this.builderType = deviceClass.getConstructor().newInstance().getDeviceBuilderType();
        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            LOG.error("Unable to set builder type on device type [{}]", deviceClass.getClass().getName(), ex);
        }
    }

    /**
     * Returns the reference id.
     *
     * @return an id for reference.
     */
    @Override
    public final UUID getId() {
        return this.id;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final Item.ItemType getType() {
        return Item.ItemType.DEVICE;
    }

    /**
     * @return Returns the device discovered.
     */
    public final Class<? extends Device<A>> getDevice() {
        return this.device;
    }

    /**
     * Returns the builder type in this discovered device.
     *
     * @return The builder type.
     */
    public final DeviceBuilderType getDeviceBuilderType() {
        return this.builderType;
    }

    /**
     * @return Returns the discovery time.
     */
    @Override
    public final Date getDiscoveryDateTime() {
        return (Date) this.discoveryDateTime.clone();
    }

    /**
     * @return Returns a name which should hint an end user what kind of device
     * it is.
     */
    @Override
    public final String getName() {
        return this.name;
    }

    /**
     * Sets the device address.
     *
     * @param address The address to set.
     */
    public final void setAddress(final A address) {
        this.address = address;
    }

    /**
     * @return Returns the device address.
     */
    public final A getAddress() {
        return this.address;
    }

    /**
     * Returns the given address as a string.
     *
     * @return The address as string.
     */
    public final String getAddressAsString() {
        return this.address.getAddress().toString();
    }

    /**
     * Sets information as device details.
     *
     * No need to set the address as this is already done in the constructor.
     *
     * Convenience method for
     * <code>getInformation.getSection(0).addDescriptionListItem(item);</code>
     *
     * @param item A descriptive item for the end user.
     */
    public final void addDescriptionListItem(final DescriptionListItem item) {
        descriptionList.addListItem(item);
    }

    /**
     * @return Returns the presentation for the end user to support identifying
     * the device.
     */
    public final Presentation getInformation() {
        return this.information;
    }

    /**
     * Sets the information the driver will receive when an user adds a device.
     * Parameters are ALWAYS set to lower case and the address is already set.
     *
     * @param parameterName The name of the parameter.
     * @param parameterValue The value of the parameter.
     */
    public final void addParameter(final String parameterName, final String parameterValue) {
        this.parameters.add(new DeviceParameter(parameterName, parameterValue));
    }

    /**
     * Sets the parameters.
     *
     * Could also use addParameter which is a shortcut for quickly adding
     * parameters.
     *
     * @param parameters The parameters list to set.
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T extends ItemPropertiesListInterface> void setParameters(final T parameters) {
        this.parameters.clear();
        this.parameters.addAll(parameters);
    }

    /**
     * Returns the parameters set.
     *
     * @return The set parameters.
     */
    @Override
    public DeviceParametersList getParameters() {
        return this.parameters;
    }

}
