/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

/**
 * Enum for identifying control types.
 *
 * @author johns
 */
public enum DeviceControlType {
    /**
     * A toggle control.
     */
    TOGGLE,
    /**
     * A slider control.
     */
    SLIDER,
    /**
     * A select control.
     */
    SELECT,
    /**
     * A data control acting on boolean values.
     */
    DATA_BOOLEAN,
    /**
     * A data control acting on number values.
     */
    DATA_NUMBER,
    /**
     * A data control acting on string values.
     */
    DATA_STRING,
    /**
     * A color picker control.
     */
    COLORPICKER,
    /**
     * A button control.
     */
    BUTTON;

    /**
     * Type enum as a string constant.
     */
    public static class Name {

        /**
         * A toggle control.
         */
        public static final String TOGGLE = "TOGGLE";
        /**
         * A slider control.
         */
        public static final String SLIDER = "SLIDER";
        /**
         * A select control.
         */
        public static final String SELECT = "SELECT";
        /**
         * A data control acting on boolean values.
         */
        public static final String DATA_BOOLEAN = "DATA_BOOLEAN";
        /**
         * A data control acting on number values.
         */
        public static final String DATA_NUMBER = "DATA_NUMBER";
        /**
         * A data control acting on string values.
         */
        public static final String DATA_STRING = "DATA_STRING";
        /**
         * A color picker control.
         */
        public static final String COLORPICKER = "COLORPICKER";
        /**
         * A button control.
         */
        public static final String BUTTON = "BUTTON";
    }

}
