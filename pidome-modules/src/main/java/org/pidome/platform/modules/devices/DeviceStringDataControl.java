/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * String based data control.
 *
 * @author johns
 */
public final class DeviceStringDataControl extends DeviceDataControl<String> {

    /**
     * Class version.
     */
    public static final long serialVersionUID = 1L;

    /**
     * Constructor.
     *
     * @param id The id of the control.
     */
    @JsonCreator
    public DeviceStringDataControl(@JsonProperty("id") final String id) {
        super(id);
        this.setValue("");
    }

    /**
     * Returns a new string data control builder.
     *
     * @param id The id of the control.
     * @return The builder.
     */
    public static StringDataControlBuilder stringDataBuilder(final String id) {
        return new StringDataControlBuilder(id);
    }

    /**
     * Builder for the string data control.
     */
    public static class StringDataControlBuilder extends DeviceDataControl.DataControlBuilder<DeviceStringDataControl, StringDataControlBuilder> {

        /**
         * The control id.
         */
        private final String id;

        /**
         * Builder constructor.
         *
         * @param controlId The id of the control.
         */
        @SuppressWarnings("unchecked")
        public StringDataControlBuilder(final String controlId) {
            this.id = controlId;
            this.setReference(this);
        }

        /**
         * Builds the string data control.
         *
         * @return The string data control.
         */
        @Override
        @SuppressWarnings("unchecked")
        public DeviceStringDataControl build() {
            DeviceStringDataControl control = new DeviceStringDataControl(this.id);
            control.setControlType(DeviceControlType.DATA_STRING);
            super.build(control);
            return control;
        }

    }

}
