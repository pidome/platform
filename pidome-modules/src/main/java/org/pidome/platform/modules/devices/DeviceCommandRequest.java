/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

/**
 * Device command request.
 *
 * The device command request is a literal request as created by a call to the
 * server.
 *
 * @author John
 * @todo: Transform object to literal object as provided by a control.
 */
public final class DeviceCommandRequest {

    /**
     * The command value.
     */
    private final Object commandValue;

    /**
     * The device control for which this command is.
     */
    private final DeviceControl deviceControl;

    /**
     * The address where the command is originating from.
     */
    private final Device device;

    /**
     * Constructor.
     *
     * @param device The device where this command is coming from.
     * @param deviceControl for which device control this is.
     * @param value The value in the command.
     */
    public DeviceCommandRequest(final Device device, final DeviceControl deviceControl, final Object value) {
        this.deviceControl = deviceControl;
        this.device = device;
        this.commandValue = value;
    }

    /**
     * @return The device address from which the command is from.
     */
    public Device getDevice() {
        return this.device;
    }

    /**
     * Returns the command group.
     *
     * @return The command group.
     */
    public String getGroupId() {
        return this.deviceControl.getGroup().getId();
    }

    /**
     * Returns the datatype of the control.
     *
     * Convenience method for <code>getcontrol().getId();</code>
     *
     * @return The data type.
     */
    public String getControlId() {
        return this.deviceControl.getId();
    }

    /**
     * Returns the datatype of the control.
     *
     * Convenience method for <code>getcontrol().getDataType();</code>
     *
     * @return The data type.
     */
    public DeviceControlDataType getControlDataType() {
        return this.deviceControl.getDataType();
    }

    /**
     * Returns the command value.
     *
     * @return The command value.
     */
    public Object getCommandValue() {
        return this.commandValue;
    }

    /**
     * Returns the control where it's all about.
     *
     * @return The device control.
     */
    public DeviceControl getControl() {
        return this.deviceControl;
    }

    /**
     * this object to String.
     *
     * @return The string output as json.
     */
    @Override
    public String toString() {
        return new StringBuilder("DeviceCommandRequest: ")
                .append("[device: ").append(this.device.getName()).append(", ")
                .append("group: ").append(this.getGroupId()).append(",")
                .append("control: ").append(this.getControlId()).append(",")
                .append("value: ").append(this.getCommandValue()).append("]")
                .toString();
    }

}
