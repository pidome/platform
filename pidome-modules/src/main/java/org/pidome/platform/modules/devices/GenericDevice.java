/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

import org.pidome.platform.modules.items.ItemAddress;

/**
 * A generic device.
 *
 * A generic device requires the implementer use a builder to create the device.
 *
 * @author johns
 * @param <A> Address type.
 */
public abstract class GenericDevice<A extends ItemAddress> extends Device<A> {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Default public constructor.
     */
    public GenericDevice() {
        super();
    }

    /**
     * Returns the builder type.
     *
     * @return The builder type.
     */
    @Override
    public final DeviceBuilderType getDeviceBuilderType() {
        return DeviceBuilderType.BUILDER;
    }

    /**
     * Called to build a device's structure.
     *
     * @param builder The builder to build the device's groups and controls.
     */
    public abstract void build(DeviceStructureBuilder builder);

}
