/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.items;

/**
 * Exception thrown when a device address is incorrect or a problem with the
 * address exists.
 *
 * @author John
 */
public class ItemAddressException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>DeviceAddressException</code> without
     * detail message.
     */
    public ItemAddressException() {
    }

    /**
     * Constructs an instance of <code>DeviceAddressException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public ItemAddressException(final String msg) {
        super(msg);
    }

    /**
     * Constructs an instance of <code>DeviceAddressException</code> with the
     * specified throwable.
     *
     * @param ex the rethrown trhowable.
     */
    public ItemAddressException(final Throwable ex) {
        super(ex);
    }

}
