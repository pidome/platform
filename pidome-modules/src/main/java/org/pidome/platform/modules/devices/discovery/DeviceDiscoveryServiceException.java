/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices.discovery;

/**
 * Generic exception thrown when there are device discovery issues.
 *
 * @author John
 */
public class DeviceDiscoveryServiceException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>DeviceDiscoveryServiceException</code>
     * without detail message.
     */
    public DeviceDiscoveryServiceException() {
    }

    /**
     * Constructs an instance of <code>DeviceDiscoveryServiceException</code>
     * with the specified detail message.
     *
     * @param msg the detail message.
     */
    public DeviceDiscoveryServiceException(final String msg) {
        super(msg);
    }
}
