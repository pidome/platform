/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules;

import io.vertx.core.Promise;
import org.pidome.platform.hardware.driver.Transport;
import org.pidome.platform.presentation.Presentation;
import org.pidome.platform.presentation.input.InputForm;

/**
 * Fallback for incorrectly configured modules.
 *
 * Generic implementation is used to be able to trackback to the module that's
 * wrongly configured. Future implementation of this module will be for
 * development purposes.
 *
 * @author johns
 */
@PidomeModule(
        name = "Unknown module",
        description = "Incorrect configured module",
        transport = Transport.SubSystem.UNKNOWN
)
public final class UnknownModule extends ModuleBase {

    /**
     * Wrongly configured literal.
     */
    private static final String INCORRECT_CONFIGURED = " module is wrongly configured";

    /**
     * Starts the module.
     *
     * @param promise To succeed or fail.
     */
    @Override
    public void startModule(final Promise promise) {
        promise.fail(new UnsupportedOperationException(this + INCORRECT_CONFIGURED));
    }

    /**
     * Stops the module.
     *
     * @param promise To succeed or fail.
     */
    @Override
    public void stopModule(final Promise promise) {
        promise.fail(new UnsupportedOperationException(this + INCORRECT_CONFIGURED));
    }

    /**
     * Composes a configuration.
     *
     * @param promise To succeed or fail.
     */
    @Override
    public void composeConfiguration(final Promise promise) {
        promise.fail(new UnsupportedOperationException(this + INCORRECT_CONFIGURED));
    }

    /**
     * Applies a configuration.
     *
     * @param promise To succeed or fail.
     */
    @Override
    public void configure(final InputForm configurationResult, final Promise promise) {
        promise.fail(new UnsupportedOperationException(this + INCORRECT_CONFIGURED));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Presentation getModuleInfo() {
        throw new UnsupportedOperationException("Not supported.");
    }

}
