/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.items;

/**
 * A hex based address.
 *
 * @author johns
 */
public class ItemHexAddress extends ItemAddress<String> {

    /**
     * Constructor setting hex type.
     */
    public ItemHexAddress() {
        super("HEX_ADDRESS");
    }

}
