/*
 * Copyright 2019 John Sirach <john.sirach@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pidome.platform.modules.devices;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.vertx.core.Promise;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.modules.items.Item;
import org.pidome.platform.modules.items.ItemAddress;
import org.pidome.platform.modules.items.ItemPropertiesListInterface;

/**
 * Base class for any device.
 *
 * @author John Sirach
 * @param <A> The device address type.
 */
public abstract class Device<A extends ItemAddress> implements Item, Serializable {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The logger.
     */
    private static final Logger LOG = LogManager.getLogger(Device.class);

    /**
     * For tracking the device status.
     */
    public enum DeviceStatus {
        /**
         * The device is ok.
         */
        OK,
        /**
         * The device needs some attention.
         */
        WARNING,
        /**
         * There is a serious device issue.
         */
        ERROR,
        /**
         * Status is known and is dead.
         */
        DEAD,
        /**
         * One of the controls is timing out.
         */
        CONTROL_TIMEOUT;
    }

    /**
     * The current device status.
     */
    private DeviceStatus status = DeviceStatus.OK;

    /**
     * List of control groups.
     */
    private List<DeviceControlGroup> groups;

    /**
     * The device status reason.
     */
    private String statusText = "No problems found";

    /**
     * Protected default constructor.
     *
     * The generic device is abstract and package extended. For extending device
     * see:
     * <ul>
     * <li><code>ComposedDevice</code></li>
     * <li><code>CustomDevice</code></li>
     * <li><code>GenericDevice</code></li>
     * </ul>
     */
    protected Device() {
    }

    /**
     * List of device parameters.
     */
    @JsonIgnore
    @SuppressWarnings("unchecked")
    private final DeviceParametersList parameters = new DeviceParametersList();

    /**
     * The device's address.
     */
    private A address;

    /**
     * Returns the type the Item expects.
     *
     * @return The type expected.
     */
    @Override
    public final ItemType getType() {
        return ItemType.DEVICE;
    }

    /**
     * Returns the device simple name.
     *
     * Shortcut for <code>this.getClass().getSimpleName();</code>
     *
     * @return The device simple name.
     */
    public final String getName() {
        return this.getClass().getSimpleName();
    }

    /**
     * Returns the device status.
     *
     * @return The current status of the device.
     */
    public final DeviceStatus getDeviceStatus() {
        return this.status;
    }

    /**
     * Returns the status textual reason as set by the devices/drivers.
     *
     * @return The textual representation of the device.
     */
    public final String getStatusText() {
        return this.statusText;
    }

    /**
     * Sets the control groups.
     *
     * @param groups The control groups to set.
     */
    public void setGroups(final List<DeviceControlGroup> groups) {
        this.groups = groups;
    }

    /**
     * Returns the groups in the device.
     *
     * @return The device groups.
     */
    public List<DeviceControlGroup> getGroups() {
        return this.groups;
    }

    /**
     * Returns an optional with the group if present.
     *
     * @param groupId The id of the group.
     * @return The group if present, otherwise empty optional.
     */
    public Optional<DeviceControlGroup> getGroup(final String groupId) {
        return getGroups().stream()
                .filter(group -> group.getId().equals(groupId))
                .findFirst();
    }

    /**
     * Checks the status of the controls.
     *
     * When there is a control found which is in timeout, it will modify device
     * status to timeout, otherwise ok.
     */
    public void checkControlsStatus() {
        Optional<DeviceControl> timedOutControl = this.groups.stream()
                .flatMap(group -> group.getControls().stream())
                .filter(control -> control.getControlStatus().equals(ControlStatus.TIMEOUT)).findFirst();
        timedOutControl.ifPresentOrElse(control -> {
            setDeviceControlStatus(DeviceStatus.CONTROL_TIMEOUT, control.getDescription() + " timed out", control.getGroup().getId(), control.getId());
        }, () -> {
            this.status = DeviceStatus.OK;
        });
    }

    /**
     * Sets the device status based on the given control.
     *
     * @param deviceStatus The new device status.
     * @param reason The reason for this status.
     * @param groupId For which group id this status message is
     * @param controlId For which control this is.
     */
    private void setDeviceControlStatus(final DeviceStatus deviceStatus, final String reason, final String groupId, final String controlId) {
        this.status = deviceStatus;
        this.statusText = reason;
        dispatchToHost(
                new DeviceStatusNotification(this.address, status, reason, groupId, controlId)
        );
    }

    /**
     * @return The device address.
     */
    public final A getAddress() {
        return this.address;
    }

    /**
     * Sets the device address.
     *
     * @param address The address to set.
     */
    public final void setAddress(final A address) {
        this.address = address;
    }

    /**
     * Sets the device parameters.
     *
     * @param parameters Parameters.
     */
    @Override
    @SuppressWarnings("unchecked")
    public final void setParameters(final ItemPropertiesListInterface parameters) {
        this.parameters.clear();
        this.parameters.addAll(parameters);
    }

    /**
     * @return Returns the list of device parameters.
     */
    @Override
    @SuppressWarnings("unchecked")
    public final DeviceParametersList getParameters() {
        return this.parameters;
    }

    /**
     * Notifies the host of new data.
     *
     * @param notification The notification to send.
     */
    public final void dispatchToHost(final DeviceDataNotification notification) {
        dispatchToHost(notification, true);
    }

    /**
     * Notifies the host of new data but with the option if it is an user
     * intention or automated dispatch. When the user intent is false it means
     * it is an automated process.
     *
     * @param notification The notification to send.
     * @param userIntent If the notification was intended by a user, which means
     * non automated.
     */
    public final void dispatchToHost(final DeviceDataNotification notification, final boolean userIntent) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Sending notification [{}] with user intent [{}]", notification, userIntent);
        }
    }

    /**
     * Used to shutdown devices.
     */
    public abstract void shutdownDevice();

    /**
     * Used to shutdown devices.
     */
    public abstract void startupDevice();

    /**
     * Handle a command for a device and sends this to the module to be send to
     * the peripheral.
     *
     * @param module The module for this device.
     * @param request The request done.
     * @param promise The promise to fulfil with <code>succeed()</code> or fail
     * with <code>fail(new Exception("reason"))</code>
     */
    public abstract void handleDeviceCommand(DevicesModule module, DeviceCommandRequest request, Promise<Void> promise);

    /**
     * Returns the device structure builder method.
     *
     * @return The device builder method.
     */
    public abstract DeviceBuilderType getDeviceBuilderType();

    /**
     * Builder helper for creating devices by code.
     */
    public static class DeviceStructureBuilder {

        /**
         * List of group builders.
         */
        private final List<GroupBuilder> groupBuilders = new ArrayList<>();

        /**
         * Create a new group builder.
         *
         * @param id The id of the group.
         * @param title The group title.
         * @param provider The provider to build the group.
         * @return The builder.
         */
        public DeviceStructureBuilder group(final String id, final String title, final GroupProvider provider) {
            GroupBuilder groupBuilder = new GroupBuilder(id, title);
            provider.accept(groupBuilder);
            this.groupBuilders.add(groupBuilder);
            return this;
        }

        /**
         * Builds the device control groups.
         *
         * @return List of device control groups.
         */
        public List<DeviceControlGroup> build() {
            List<DeviceControlGroup> buildList = new ArrayList<>();
            this.groupBuilders.stream().forEach(groupBuilder -> {
                buildList.add(groupBuilder.build());
            });
            return buildList;
        }

    }

    /**
     * Group builder helper.
     */
    public static class GroupBuilder {

        /**
         * The group created.
         */
        private final DeviceControlGroup group;

        /**
         * The controls to build.
         */
        private final List<DeviceControl.DeviceControlBuilder> controlBuilders = new ArrayList<>();

        /**
         * Constructor.
         *
         * @param id The group id.
         * @param title The group description.
         */
        public GroupBuilder(final String id, final String title) {
            this.group = new DeviceControlGroup(id, title);
        }

        /**
         * Set hidden or not.
         *
         * @param hidden If the whole group with the controls should be hidden
         * from communicated implementation.
         * @return The builder.
         */
        public GroupBuilder setHidden(final boolean hidden) {
            this.group.setHidden(hidden);
            return this;
        }

        /**
         * Add a button control.
         *
         * @param id The button id.
         * @param provider The builder provider.
         * @return The group.
         */
        public GroupBuilder button(final String id, final ButtonProvider provider) {
            DeviceButtonControl.ButtonControlBuilder builder = DeviceButtonControl.buttonBuilder(id);
            provider.accept(builder);
            this.controlBuilders.add(builder);
            return this;
        }

        /**
         * Add a color picker control.
         *
         * @param id The id of the control.
         * @param provider The provider of the control.
         * @return The group builder.
         */
        public GroupBuilder colorPicker(final String id, final ColorPickerProvider provider) {
            DeviceColorPickerControl.ColorPickerBuilder builder = DeviceColorPickerControl.colorpickerBuilder(id);
            provider.accept(builder);
            this.controlBuilders.add(builder);
            return this;
        }

        /**
         * Add a select control.
         *
         * @param id The id of the control.
         * @param provider The provider of the control.
         * @return The group builder.
         */
        public GroupBuilder select(final String id, final SelectProvider provider) {
            DeviceSelectControl.SelectControlBuilder builder = DeviceSelectControl.selectBuilder(id);
            provider.accept(builder);
            this.controlBuilders.add(builder);
            return this;
        }

        /**
         * Add a slider control.
         *
         * @param id The id of the control.
         * @param provider The provider of the control.
         * @return The group builder.
         */
        public GroupBuilder slider(final String id, final SliderProvider provider) {
            DeviceSliderControl.SliderControlBuilder builder = DeviceSliderControl.sliderDataBuilder(id);
            provider.accept(builder);
            this.controlBuilders.add(builder);
            return this;
        }

        /**
         * Add a toggle control.
         *
         * @param id The id of the control.
         * @param provider The provider of the control.
         * @return The group builder.
         */
        public GroupBuilder toggle(final String id, final ToggleProvider provider) {
            DeviceToggleControl.ToggleControlBuilder builder = DeviceToggleControl.toggleBuilder(id);
            provider.accept(builder);
            this.controlBuilders.add(builder);
            return this;
        }

        /**
         * Add a boolean data control.
         *
         * @param id The id of the control.
         * @param provider The provider of the control.
         * @return The group builder.
         */
        public GroupBuilder booleanData(final String id, final BooleanDataProvider provider) {
            DeviceBooleanDataControl.BooleanDataControlBuilder builder = DeviceBooleanDataControl.booleanDataBuilder(id);
            provider.accept(builder);
            this.controlBuilders.add(builder);
            return this;
        }

        /**
         * Add the number data control.
         *
         * @param id The id of the control.
         * @param provider The provider of the control.
         * @return The group builder.
         */
        public GroupBuilder numberData(final String id, final NumberDataProvider provider) {
            DeviceNumberDataControl.NumberDataControlBuilder builder = DeviceNumberDataControl.numberDataBuilder(id);
            provider.accept(builder);
            this.controlBuilders.add(builder);
            return this;
        }

        /**
         * The string data control.
         *
         * @param id The id of the control.
         * @param provider The provider of the control.
         * @return The group builder.
         */
        public GroupBuilder stringData(final String id, final StringDataProvider provider) {
            DeviceStringDataControl.StringDataControlBuilder builder = DeviceStringDataControl.stringDataBuilder(id);
            provider.accept(builder);
            this.controlBuilders.add(builder);
            return this;
        }

        /**
         * Builds the control group.
         *
         * @return The control group.
         * @throws IllegalArgumentException on build failure.
         */
        public DeviceControlGroup build() throws IllegalArgumentException {
            List<DeviceControl> controls = new ArrayList<>();
            controlBuilders.stream().forEach(controlBuilder -> {
                controls.add(controlBuilder.build());
            });
            this.group.setControls(controls);
            return this.group;
        }

    }

    /**
     * Provider for the group builder.
     */
    @FunctionalInterface
    public interface GroupProvider {

        /**
         * Handle the builder.
         *
         * @param builder The builder to handle.
         */
        void accept(GroupBuilder builder);
    }

    /**
     * Provider for button builder.
     */
    @FunctionalInterface
    public interface ButtonProvider {

        /**
         * Handle the builder.
         *
         * @param builder The builder to handle.
         */
        void accept(DeviceButtonControl.ButtonControlBuilder builder);
    }

    /**
     * Provider for color picker builder.
     */
    @FunctionalInterface
    public interface ColorPickerProvider {

        /**
         * Handle the builder.
         *
         * @param builder The builder to handle.
         */
        void accept(DeviceColorPickerControl.ColorPickerBuilder builder);
    }

    /**
     * Provider for the select builder.
     */
    @FunctionalInterface
    public interface SelectProvider {

        /**
         * Handle the builder.
         *
         * @param builder The builder to handle.
         */
        void accept(DeviceSelectControl.SelectControlBuilder builder);
    }

    /**
     * Provider for the slider builder.
     */
    @FunctionalInterface
    public interface SliderProvider {

        /**
         * Handle the builder.
         *
         * @param builder The builder to handle.
         */
        void accept(DeviceSliderControl.SliderControlBuilder builder);
    }

    /**
     * Provider for the toggle builder.
     */
    @FunctionalInterface
    public interface ToggleProvider {

        /**
         * Handle the builder.
         *
         * @param builder The builder to handle.
         */
        void accept(DeviceToggleControl.ToggleControlBuilder builder);
    }

    /**
     * Provider for boolean data builder.
     */
    @FunctionalInterface
    public interface BooleanDataProvider {

        /**
         * Handle the builder.
         *
         * @param builder The builder to handle.
         */
        void accept(DeviceBooleanDataControl.BooleanDataControlBuilder builder);
    }

    /**
     * Provider for number data builder.
     */
    @FunctionalInterface
    public interface NumberDataProvider {

        /**
         * Handle the builder.
         *
         * @param builder The builder to handle.
         */
        void accept(DeviceNumberDataControl.NumberDataControlBuilder builder);
    }

    /**
     * Provider for string data provider.
     */
    @FunctionalInterface
    public interface StringDataProvider {

        /**
         * Handle the builder.
         *
         * @param builder The builder to handle.
         */
        void accept(DeviceStringDataControl.StringDataControlBuilder builder);
    }

}
