/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.items;

import org.pidome.platform.modules.items.Item.ItemType;

/**
 * Interface for notifications being send out by items, or for items.
 *
 * @author johns
 */
public interface ItemDataNotification {

    /**
     * Returns the item type for the notification.
     *
     * @return The item type.
     */
    ItemType getItemType();

}
