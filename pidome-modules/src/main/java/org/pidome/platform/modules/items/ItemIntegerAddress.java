/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.items;

/**
 * A integer based address.
 *
 * @author johns
 */
public class ItemIntegerAddress extends ItemAddress<Integer> {

    /**
     * Constructor setting integer type.
     */
    public ItemIntegerAddress() {
        super("INTEGER_ADDRESS");
    }

}
