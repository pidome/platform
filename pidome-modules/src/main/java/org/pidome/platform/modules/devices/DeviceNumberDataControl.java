/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Number based data control.
 *
 * @author johns
 */
public final class DeviceNumberDataControl extends DeviceDataControl<Number> {

    /**
     * Class version.
     */
    public static final long serialVersionUID = 1L;

    /**
     * Minimum value for numeric values.
     */
    private Number minValue;

    /**
     * Critical threshold for a low numeric value.
     */
    private Number lowCriticalValue;

    /**
     * Warning threshold for a low numeric value.
     */
    private Number lowWarningValue;

    /**
     * Warning threshold for a high numeric value.
     */
    private Number highWarningValue;

    /**
     * Critical threshold for a high numeric value.
     */
    private Number highCriticalValue;

    /**
     * Maximum value for numeric values.
     */
    private Number maxValue;

    /**
     * Constructor.
     *
     * @param id The id of the control.
     */
    @JsonCreator
    public DeviceNumberDataControl(@JsonProperty("id") final String id) {
        super(id);
    }

    /**
     * Minimum value for numeric values.
     *
     * @return the minValue
     */
    public Number getMinValue() {
        return minValue;
    }

    /**
     * Minimum value for numeric values.
     *
     * @param minValue the minValue to set
     */
    public void setMinValue(final Number minValue) {
        this.minValue = minValue;
        this.setValue(minValue);
    }

    /**
     * Critical threshold for a low numeric value.
     *
     * @return the lowCriticalValue
     */
    public Number getLowCriticalValue() {
        return lowCriticalValue;
    }

    /**
     * Critical threshold for a low numeric value.
     *
     * @param lowCriticalValue the lowCriticalValue to set
     */
    public void setLowCriticalValue(final Number lowCriticalValue) {
        this.lowCriticalValue = lowCriticalValue;
    }

    /**
     * Warning threshold for a low numeric value.
     *
     * @return the lowWarningValue
     */
    public Number getLowWarningValue() {
        return lowWarningValue;
    }

    /**
     * Warning threshold for a low numeric value.
     *
     * @param lowWarningValue the lowWarningValue to set
     */
    public void setLowWarningValue(final Number lowWarningValue) {
        this.lowWarningValue = lowWarningValue;
    }

    /**
     * Warning threshold for a high numeric value.
     *
     * @return the highWarningValue
     */
    public Number getHighWarningValue() {
        return highWarningValue;
    }

    /**
     * Warning threshold for a high numeric value.
     *
     * @param highWarningValue the highWarningValue to set
     */
    public void setHighWarningValue(final Number highWarningValue) {
        this.highWarningValue = highWarningValue;
    }

    /**
     * Critical threshold for a high numeric value.
     *
     * @return the highCriticalValue
     */
    public Number getHighCriticalValue() {
        return highCriticalValue;
    }

    /**
     * Critical threshold for a high numeric value.
     *
     * @param highCriticalValue the highCriticalValue to set
     */
    public void setHighCriticalValue(final Number highCriticalValue) {
        this.highCriticalValue = highCriticalValue;
    }

    /**
     * Maximum value for numeric values.
     *
     * @return the maxValue
     */
    public Number getMaxValue() {
        return maxValue;
    }

    /**
     * Maximum value for numeric values.
     *
     * @param maxValue the maxValue to set
     */
    public void setMaxValue(final Number maxValue) {
        this.maxValue = maxValue;
    }

    /**
     * Returns a new number data control builder.
     *
     * @param id The id of the control.
     * @return The builder.
     */
    public static NumberDataControlBuilder numberDataBuilder(final String id) {
        return new NumberDataControlBuilder(id);
    }

    /**
     * Builder for the number data control.
     */
    @SuppressWarnings("PMD.AvoidFieldNameMatchingMethodName") //Builder pattern.
    public static class NumberDataControlBuilder extends DeviceDataControl.DataControlBuilder<DeviceNumberDataControl, NumberDataControlBuilder> {

        /**
         * The control id.
         */
        private final String id;

        /**
         * Minimum value for numeric values.
         */
        private Number minValue;

        /**
         * Critical threshold for a low numeric value.
         */
        private Number lowCriticalValue;

        /**
         * Warning threshold for a low numeric value.
         */
        private Number lowWarningValue;

        /**
         * Warning threshold for a high numeric value.
         */
        private Number highWarningValue;

        /**
         * Critical threshold for a high numeric value.
         */
        private Number highCriticalValue;

        /**
         * Maximum value for numeric values.
         */
        private Number maxValue;

        /**
         * Builder constructor.
         *
         * @param controlId The id of the control.
         */
        @SuppressWarnings("unchecked")
        public NumberDataControlBuilder(final String controlId) {
            this.id = controlId;
            this.setReference(this);
        }

        /**
         * Minimum value for numeric values.
         *
         * @param value The value to set
         * @return The builder
         */
        public NumberDataControlBuilder minValue(final Number value) {
            this.minValue = value;
            return this;
        }

        /**
         * Minimum value for numeric values.
         *
         * @param value The value to set
         * @return The builder
         */
        public NumberDataControlBuilder lowCriticalValue(final Number value) {
            this.lowCriticalValue = value;
            return this;
        }

        /**
         * Minimum value for numeric values.
         *
         * @param value The value to set
         * @return The builder
         */
        public NumberDataControlBuilder lowCWarnValue(final Number value) {
            this.lowWarningValue = value;
            return this;
        }

        /**
         * Minimum value for numeric values.
         *
         * @param value The value to set
         * @return The builder
         */
        public NumberDataControlBuilder highWarnValue(final Number value) {
            this.highWarningValue = value;
            return this;
        }

        /**
         * Minimum value for numeric values.
         *
         * @param value The value to set
         * @return The builder
         */
        public NumberDataControlBuilder highCriticalValue(final Number value) {
            this.highCriticalValue = value;
            return this;
        }

        /**
         * Maximum value for numeric values.
         *
         * @param value The value to set
         * @return The builder
         */
        public NumberDataControlBuilder maxValue(final Number value) {
            this.maxValue = value;
            return this;
        }

        /**
         * Builds the number data control.
         *
         * @return The number data control.
         */
        @Override
        @SuppressWarnings("unchecked")
        public DeviceNumberDataControl build() {
            DeviceNumberDataControl control = new DeviceNumberDataControl(this.id);
            control.setControlType(DeviceControlType.DATA_NUMBER);
            super.build(control);
            control.setMinValue(this.minValue);
            control.setLowWarningValue(this.lowWarningValue);
            control.setLowCriticalValue(this.lowCriticalValue);
            control.setHighWarningValue(this.highWarningValue);
            control.setHighCriticalValue(this.highCriticalValue);
            control.setMaxValue(this.maxValue);
            return control;
        }

    }

}
