/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The device toggle control.The toggle control is of a complex type.
 *
 * It's value contents is based on true or false control value. A true value
 * contains a label and a value belonging to the internal true value. As same it
 * is for false.
 *
 * The above results in when the toggle control is set to true, it is able to
 * return a value of any type which is mapped to this true setting. If you only
 * need to know if the value is true or false use <code>getValue</code> If you
 * want to know which data is bound to the true state use
 * <code>getValueData</code>.
 *
 * @author John
 */
public final class DeviceToggleControl extends DeviceControl<Boolean> {

    /**
     * Class version.
     */
    public static final long serialVersionUID = 1L;

    /**
     * Text to show when boolean false is active.
     */
    private String falseText = "false";

    /**
     * Text to show when boolean true is active.
     */
    private String trueText = "true";

    /**
     * Constructor.
     *
     * @param id The id of the control.
     */
    @JsonCreator
    public DeviceToggleControl(@JsonProperty("id") final String id) {
        super(id);
        this.setValue(Boolean.FALSE);
    }

    /**
     * Text to show when boolean false is active.
     *
     * @return the falseText
     */
    public String getFalseText() {
        return falseText;
    }

    /**
     * Text to show when boolean false is active.
     *
     * @param falseText the falseText to set
     */
    public void setFalseText(final String falseText) {
        this.falseText = falseText;
    }

    /**
     * Text to show when boolean true is active.
     *
     * @return the trueText
     */
    public String getTrueText() {
        return trueText;
    }

    /**
     * Text to show when boolean true is active.
     *
     * @param trueText the trueText to set
     */
    public void setTrueText(final String trueText) {
        this.trueText = trueText;
    }

    /**
     * Content equality check.
     *
     * @param o The content to check
     * @return true if equal
     */
    @Override
    public boolean valueEquals(final Object o) {
        if (o == null) {
            return false;
        } else if (!(o instanceof Boolean)) {
            return false;
        } else if (this.getValueData() == null) {
            return false;
        } else {
            /// Boolean can be represented both in string and boolean types.
            return this.getValue().equals(o);
        }
    }

    /**
     * Returns a new toggle control builder.
     *
     * @param id The id of the control.
     * @return The builder.
     */
    public static ToggleControlBuilder toggleBuilder(final String id) {
        return new ToggleControlBuilder(id);
    }

    /**
     * Builder for the toggle control.
     */
    @SuppressWarnings("PMD.AvoidFieldNameMatchingMethodName") //Builder pattern.
    public static class ToggleControlBuilder extends DeviceControl.ControlBuilder<ToggleControlBuilder> implements DeviceControlBuilder {

        /**
         * The control id.
         */
        private final String id;

        /**
         * Text to show when boolean false is active.
         */
        private String falseText = "false";

        /**
         * Text to show when boolean true is active.
         */
        private String trueText = "true";

        /**
         * Builder constructor.
         *
         * @param controlId The id of the control.
         */
        public ToggleControlBuilder(final String controlId) {
            this.id = controlId;
            this.setReference(this);
        }

        /**
         * The text to display if value is true.
         *
         * @param whenTrueText The text when true.
         * @return The Builder.
         */
        public ToggleControlBuilder trueText(final String whenTrueText) {
            this.trueText = whenTrueText;
            return this;
        }

        /**
         * The text to display if value is false.
         *
         * @param whenFalseText The text when false.
         * @return The Builder.
         */
        public ToggleControlBuilder falseText(final String whenFalseText) {
            this.falseText = whenFalseText;
            return this;
        }

        /**
         * Builds the toggle control.
         *
         * @return The toggle control.
         */
        @Override
        public DeviceToggleControl build() {
            DeviceToggleControl control = new DeviceToggleControl(this.id);
            control.setControlType(DeviceControlType.TOGGLE);
            super.build(control);
            control.setFalseText(this.falseText);
            control.setTrueText(this.trueText);
            return control;
        }

    }

}
