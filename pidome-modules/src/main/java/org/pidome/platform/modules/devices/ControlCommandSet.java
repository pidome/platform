/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A set of commands for a control.
 *
 * Used in various controls where there are multiple commands able to be send
 * such as the color picker and the select control.
 *
 * @author johns
 * @param <T> The type to expect in the value.
 */
public class ControlCommandSet<T> implements Serializable {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The command set label.
     */
    private String label;

    /**
     * The command set description.
     */
    private String description;

    /**
     * The list of commands in this set.
     */
    private Set<ControlCommand<T>> set = new HashSet<>();

    /**
     * The command set label.
     *
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * The command set label.
     *
     * @param label the label to set
     */
    public void setLabel(final String label) {
        this.label = label;
    }

    /**
     * The command set description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * The command set description.
     *
     * @param description the description to set
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * The list of commands in this set.
     *
     * @return the commands
     */
    public Set<ControlCommand<T>> getSet() {
        return set;
    }

    /**
     * The list of commands in this set.
     *
     * @param set the commands to set
     */
    public void setSet(final Set<ControlCommand<T>> set) {
        this.set = set;
    }

}
