/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Objects;

/**
 * A single control command to be used in command sets.
 *
 * @author johns
 * @param <T> The type in the command action.
 */
public class ControlCommand<T> implements Serializable {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The command label.
     */
    private final String label;

    /**
     * The command action.
     */
    private final T action;

    /**
     * Command constructor.
     *
     * @param label The label to show.
     * @param action The action to perform.
     */
    @JsonCreator
    public ControlCommand(final @JsonProperty("label") String label, final @JsonProperty("action") T action) {
        this.label = label;
        this.action = action;
    }

    /**
     * The command label.
     *
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * The command action.
     *
     * @return the action
     */
    public T getAction() {
        return action;
    }

    /**
     * Generates the hash code.
     *
     * @return The generated hash code.
     */
    @Override
    @SuppressWarnings("CPD-START")
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.label);
        return hash;
    }

    /**
     * Object equality check.
     *
     * @param obj The object to check against.
     * @return If equal or not.
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ControlCommand<?> other = (ControlCommand<?>) obj;
        return Objects.equals(this.label, other.label);
    }

}
