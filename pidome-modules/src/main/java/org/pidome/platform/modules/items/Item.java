/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.items;

/**
 * The base interface for an item.
 *
 * An item is an object that will be presented to the end user. The server
 * handles the item identifications and categorizing.
 *
 * @author johns
 */
public interface Item {

    /**
     * The item types identifiable for the server.
     */
    enum ItemType {
        /**
         * An item which does not follow set structures.
         */
        GENERIC,
        /**
         * A device item.
         */
        DEVICE;
    }

    /**
     * Returns the item type.
     *
     * @return The item type.
     */
    ItemType getType();

    /**
     * Item parameters.Parameters are item private parameters.
     *
     * Items could be equal is structure, where parameters come in
     *
     * @param <T> The type.
     * @param parameters The parameters to set.
     */
    <T extends ItemPropertiesListInterface> void setParameters(T parameters);

    /**
     * Returns set parameters.
     *
     * @return The parameters.
     */
    ItemPropertiesListInterface getParameters();

}
