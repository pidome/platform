/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

/**
 * Graph types.
 *
 * @author johns
 */
public enum ControlGraphType {
    /**
     * No graph implemented.
     */
    NONE("No graph"),
    /**
     * Series data. One plot point per data refresh.
     */
    SERIES("Line (Numeric series data over time)"),
    /**
     * Totals data. Values are accumulated over time periods.
     */
    TOTALS("Bar (Occurences in time frames)"),
    /**
     * Logarithmic values on a series set.
     */
    LOG("Line (Logarithmic scale numeric series)");

    /**
     * The description of the enum type.
     */
    private final String description;

    /**
     * Enum constructor.
     *
     * @param description The description of the enum.
     */
    ControlGraphType(final String description) {
        this.description = description;
    }

    /**
     * Returns the description of the enum.
     *
     * @return Enum description.
     */
    public String getDescription() {
        return this.description;
    }
}
