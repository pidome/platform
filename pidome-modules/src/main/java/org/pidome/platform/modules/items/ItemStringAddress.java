/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.items;

/**
 * A string based address.
 *
 * @author johns
 */
public class ItemStringAddress extends ItemAddress<String> {

    /**
     * Constructor setting String type.
     */
    public ItemStringAddress() {
        super("STRING_ADDRESS");
    }

}
