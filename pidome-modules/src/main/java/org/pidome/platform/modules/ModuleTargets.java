/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to support preferable and targeted attention for end users.
 * <p>
 * A target will show up in the front-end as a preferable driver for the
 * hardware connected to the server. It does not provide exclusivity as such it
 * will not show other drivers, but will hint the end user this is a module
 * preferably capable to handle the hardware connected.
 * <p>
 * By default the module will be limited to be shown only when the specific
 * hardware is connected to the server limited to the given targets. If your
 * module is more generic, you would be able to set <code>exclusive</code> to
 * <code>false</code>. This will show always show the module. The preferable
 * notation will stay for the given targets.
 * <p>
 * Example: If you have an USB hid module which would react with numeric parts
 * of a keyboard, there are hundreds of different keyboards, but will mostly
 * react to the same HID send bytes, you would just omit this annotation. But if
 * you are targeting a specific keyboard type, you would set the target so it
 * becomes a preferable module. The module but will still be applicable to all
 * those other generic types if <code>exclusive</code> is set to
 * <code>false</code>.
 * <p>
 * This annotation is not required when you write a module which is generic to
 * all kind of similar devices. It will show up always.
 * <p>
 * NOTE: This annotation only works on devices connected to a physical port on
 * the server. Networked devices are not identifiable. Possibly a future
 * implementation will be done when there is a requirement for network devices
 * which broadcast their presence in whatever way.
 *
 * @author johns
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ModuleTargets {

    /**
     * In case of writing a module which targets a specific set of connected
     * hardware it is optionally possible to provide an array of USB vid and
     * pid's.
     *
     * Example: "VID_0000&amp;PID_0000", "VID_0001&amp;PID_0001".
     *
     * @return Array hardware being targeted by this module.
     */
    String[] targets();

    /**
     * If the module is exclusive for the vid&amp;pid being targeted in the
     * targets method.
     *
     * @return If the module is exclusive for the targeted hardware.
     */
    boolean exclusive() default false;

}
