/**
 * Base for items.
 * <p>
 * Supplying base classes for produced by modules to be identified as items.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.platform.modules.items;
