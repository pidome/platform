/**
 * Generic discovery classes.
 * <p>
 * Supplying generic classes for discovery.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.platform.modules.discovery;
