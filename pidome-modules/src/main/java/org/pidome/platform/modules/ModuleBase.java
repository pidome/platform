/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules;

import io.vertx.core.Promise;
import org.pidome.platform.presentation.Presentation;
import org.pidome.platform.presentation.input.InputForm;

/**
 * Minimal lowest base class for modules.
 *
 * @author johns
 * @param <T> The configuration class being provided, if your module does not
 * provide a configuration use type "Void".
 */
public abstract class ModuleBase<T extends InputForm> implements ModuleConfiguration<T> {

    /**
     * The configuration presentation of the module.
     */
    private T configuration;

    /**
     * Sets the configuration.
     *
     * This method must be called from <code>composeConfiguration</code> which
     * is called when the module enters configuration mode.
     *
     * To create the configuration you use
     * {@link org.pidome.platform.presentation.input.InputSection} for each
     * configuration section.
     *
     * Inside these sections you place
     * {@link org.pidome.platform.presentation.input.InputField} which will be
     * shown to the end user. Refer to the pidome-presentation package for more
     * information on how to create a configuration shown to the end user.
     * <p>
     * To set a configuration create a class which extends
     * <code>InputForm</code> and create the configuration in the constructor.
     * <p>
     *
     * @param configuration The configuration to apply.
     */
    public final void setConfiguration(final T configuration) {
        this.configuration = configuration;
    }

    /**
     * Returns the configuration form of this module.
     *
     * This method returns the form used to configure this module. When you want
     * to use the configuration you need to create your form in the
     * <code>composeConfiguration</code> method in your driver.
     *
     * @return the configuration object.
     * @see org.pidome.platform.presentation.input.InputForm
     */
    public final T getConfiguration() {
        return configuration;
    }

    /**
     * Returns information about a module when it has been started.
     *
     * This is a free form presentation in which a developer is free into put
     * what a developers finds usable information to show the end user about the
     * module. For example the PiDome provided RFXCom module shows information
     * about the RFXCom device and the active protocols.
     *
     * @return The module information.
     */
    public abstract Presentation getModuleInfo();

    /**
     * Starts a module.
     *
     * @param promise The promise to succeed or fail.
     */
    public abstract void startModule(Promise promise);

    /**
     * Stops a module.
     *
     * @param promise The promise to succeed or fail.
     */
    public abstract void stopModule(Promise promise);

}
