/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules;

import io.vertx.core.Promise;
import org.pidome.platform.presentation.input.InputForm;
import org.pidome.platform.presentation.types.ConfigurationPresentation;

/**
 * Interface for implementing a configurable module.
 *
 * @author johns
 * @param <T> The configuration type.
 */
public interface ModuleConfiguration<T extends InputForm> extends ConfigurationPresentation {

    /**
     * Method called for a module to create a configuration.
     *
     * When configuration is done call
     * <code>promise.complete(YourConfigurationObject);</code>. When the
     * creation of your configuration fails call
     * <code>promise.fail("Your message");</code> or
     * <code>promise.fail(YourException);</code>
     *
     * @param promise The promise to let the supplier know configuration has
     * been applied/created.
     */
    void composeConfiguration(Promise<T> promise);

    /**
     * Called when the user provides the configuration.
     *
     * This method is called when a user has entered the configuration needed,
     * and if a configuration has been created. When your configuration is set,
     * call promise.complete(); or when it fails call
     * <code>promise.fail("Your message");</code> or
     * <code>promise.fail(YourException);</code>
     *
     * @param configurationResult The configuration as created by
     * <code>composeConfiguration</code> but with selected values by the end
     * user.
     * @param promise The promise to notify the server the configuration has
     * been completed.
     */
    void configure(T configurationResult, Promise<Void> promise);

}
