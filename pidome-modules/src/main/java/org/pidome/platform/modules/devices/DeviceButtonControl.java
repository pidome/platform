/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A simple button in the controls.
 *
 * When a button is pressed it will ALWAYS be honoured.
 *
 * @author John
 */
public final class DeviceButtonControl extends DeviceControl<Boolean> {

    /**
     * Class version.
     */
    public static final long serialVersionUID = 1L;

    /**
     * The button label.
     */
    private String label;

    /**
     * Constructor.
     *
     * @param id The field id of the control.
     */
    @JsonCreator
    protected DeviceButtonControl(@JsonProperty("id") final String id) {
        super(id);
        this.setValue(false);
    }

    /**
     * Set the button label.
     *
     * @param label The button label.
     */
    public void setLabel(final String label) {
        this.label = label;
    }

    /**
     * Returns the button label.
     *
     * @return The button label.
     */
    public String getLabel() {
        return this.label;
    }

    /**
     * Content equality check.
     *
     * A button always returns false because it is a one shot single action
     * which should always be honoured.
     *
     * @param o The content to check
     * @return true if equal
     */
    @Override
    public boolean valueEquals(final Object o) {
        return false;
    }

    /**
     * Returns a new button control builder.
     *
     * @param id The id of the control.
     * @return The button builder.
     */
    public static ButtonControlBuilder buttonBuilder(final String id) {
        return new ButtonControlBuilder(id);
    }

    /**
     * Builder for the device button control.
     */
    @SuppressWarnings("PMD.AvoidFieldNameMatchingMethodName") //Builder pattern.
    public static final class ButtonControlBuilder extends DeviceControl.ControlBuilder<ButtonControlBuilder> implements DeviceControlBuilder<DeviceButtonControl> {

        /**
         * the label on the button.
         */
        private String label;

        /**
         * Id of the control.
         */
        private String id;

        /**
         * Build constructor.
         *
         * @param controlId The id of the control.
         */
        private ButtonControlBuilder(final String controlId) {
            this.id = controlId;
            this.setReference(this);
        }

        /**
         * Set the button label.
         *
         * @param buttonLabel the button label.
         * @return The Builder.
         */
        public ButtonControlBuilder label(final String buttonLabel) {
            this.label = buttonLabel;
            return this;
        }

        /**
         * Builds the button control.
         *
         * @return The button control.
         */
        @Override
        public DeviceButtonControl build() {
            DeviceButtonControl control = new DeviceButtonControl(this.id);
            control.setControlType(DeviceControlType.BUTTON);
            super.build(control);
            control.setLabel(this.label);
            return control;
        }

    }

}
