/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

/**
 * Exception thrown when a group is not functioning correctly.
 *
 * @author John
 */
public class DeviceControlsGroupException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>DeviceControlsGroupException</code>
     * without detail message.
     */
    public DeviceControlsGroupException() {
    }

    /**
     * Constructs an instance of <code>DeviceControlsGroupException</code> with
     * the specified detail message.
     *
     * @param msg the detail message.
     */
    public DeviceControlsGroupException(final String msg) {
        super(msg);
    }
}
