/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

import org.pidome.tools.comparators.NumberComparator;

/**
 * Control used for displaying data.
 *
 * @author John
 * @param <T> The type of data inside the control.
 */
public abstract class DeviceDataControl<T> extends DeviceControl<T> {

    /**
     * Class version.
     */
    public static final long serialVersionUID = 1L;

    /**
     * Data prefix.
     */
    private String prefix;

    /**
     * Data suffix.
     */
    private String suffix;

    /**
     * Constructor.
     *
     * @param id The control's id.
     */
    protected DeviceDataControl(final String id) {
        super(id);
    }

    /**
     * Data prefix.
     *
     * @param prefix the prefix to set
     */
    public void setPrefix(final String prefix) {
        this.prefix = prefix;
    }

    /**
     * Returns the prefix.
     *
     * @return The prefix.
     */
    public final String getPrefix() {
        return this.prefix;
    }

    /**
     * Data suffix.
     *
     * @param suffix the suffix to set
     */
    public void setSuffix(final String suffix) {
        this.suffix = suffix;
    }

    /**
     * Returns the suffix.
     *
     * @return The suffix.
     */
    public final String getSuffix() {
        return this.suffix;
    }

    /**
     * Content equality check.
     *
     * @param o The content to check.
     * @return true if equal.
     */
    @Override
    public final boolean valueEquals(final Object o) {
        if (o == null) {
            return false;
        } else if (this.getValueData() == null) {
            return false;
        } else {
            switch (getDataType()) {
                case STRING:
                    return ((String) o).equals((String) this.getValueData());
                case BOOLEAN:
                    return (boolean) o == (boolean) this.getValueData();
                case NUMBER:
                    return new NumberComparator().compare((Number) this.getValueData(), (Number) o) == 0;
                default:
                    return false;
            }
        }
    }

    /**
     * Builder for the device button control.
     *
     * @param <C> The type of data control.
     * @param <B> The type of control builder.
     */
    @SuppressWarnings("PMD.AvoidFieldNameMatchingMethodName") //Builder pattern.
    public abstract static class DataControlBuilder<C extends DeviceControl, B extends DataControlBuilder>
            extends DeviceControl.ControlBuilder<B> implements DeviceControlBuilder<C> {

        /**
         * The data prefix.
         */
        private String prefix;

        /**
         * The data suffix.
         */
        private String suffix;

        /**
         * The reference to the builder.
         */
        private B reference;

        /**
         * Builder constructor.
         */
        public DataControlBuilder() {
            /// Default constructor
        }

        /**
         * Creates a reference to the original builder for type reference
         * purposes.
         *
         * @param builderReference The builder extending this builder.
         */
        @Override
        protected void setReference(final B builderReference) {
            this.reference = builderReference;
            super.setReference(builderReference);
        }

        /**
         * Sets the prefix.
         *
         * @param controlPrefix The prefix.
         * @return the builder.
         */
        public B prefix(final String controlPrefix) {
            this.prefix = controlPrefix;
            return this.reference;
        }

        /**
         * Sets the data suffix.
         *
         * @param controlSuffix The data suffix.
         * @return The Builder.
         */
        public B suffix(final String controlSuffix) {
            this.suffix = controlSuffix;
            return this.reference;
        }

        /**
         * Builds the data control.
         *
         * @param <C> The type of data control.
         * @param control The control.
         * @return The data control of type C.
         */
        @SuppressWarnings("unchecked")
        public <C extends DeviceDataControl> C build(final C control) {
            super.build(control);
            control.setPrefix(this.prefix);
            control.setSuffix(this.suffix);
            return control;
        }

    }

}
