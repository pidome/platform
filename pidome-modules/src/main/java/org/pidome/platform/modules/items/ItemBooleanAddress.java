/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.items;

/**
 * A boolean based address.
 *
 * @author johns
 */
public class ItemBooleanAddress extends ItemAddress<Boolean> {

    /**
     * Constructor setting boolean type.
     */
    public ItemBooleanAddress() {
        super("BOOLEAN_ADDRESS");
    }

}
