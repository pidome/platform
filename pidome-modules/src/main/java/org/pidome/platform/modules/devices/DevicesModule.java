/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

import org.pidome.platform.modules.items.ItemAddress;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.hardware.driver.types.consumers.ProsumerInterface;
import org.pidome.platform.modules.CommunicationAwareModule;
import org.pidome.platform.modules.ModuleCapabilities;
import org.pidome.platform.modules.devices.discovery.DevicesServerProxy;
import org.pidome.platform.modules.devices.discovery.DiscoveredDevice;
import org.pidome.platform.modules.discovery.ItemDiscovery;
import org.pidome.platform.modules.items.DiscoveredItem;
import org.pidome.tools.utilities.Serialization;

/**
 * The base class for device modules.
 *
 * @author johns
 * @param <P> The expected prosumer.
 * @todo: Generic implementation re-usable for other modules.
 */
public abstract class DevicesModule<P extends ProsumerInterface> extends CommunicationAwareModule<P> implements ItemDiscovery {

    /**
     * Class root logger.
     */
    private final Logger instanceLogger = LogManager.getLogger(this.getClass());

    /**
     * Listener for discovered devices.
     */
    private DevicesServerProxy serverProxy;

    /**
     * If the proxy has been set.
     */
    private boolean proxySet = false;

    /**
     * If discovery is enabled or not.
     */
    private final AtomicBoolean discoveryEnabled = new AtomicBoolean(Boolean.FALSE);

    /**
     * Constructor.
     */
    public DevicesModule() {
        super();
    }

    /**
     * Proxy set by the server to communicate with the module.
     *
     * @param proxy The server proxy.
     */
    @SuppressWarnings("PMD.NullAssignment")
    public final void setServerProxy(final DevicesServerProxy proxy) {
        if (!proxySet) {
            proxySet = true;
            this.serverProxy = proxy;
        } else if (proxySet && proxy == null) {
            this.serverProxy = null;
        }
    }

    /**
     * Returns an array of capabilities on top of the default functionalities.
     *
     * @return An array of capabilities.
     */
    public abstract ModuleCapabilities[] capabilities();

    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean isDiscoveryEnabled() {
        return discoveryEnabled.getAcquire();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void setDiscoveryEnabled(final boolean enabled) {
        this.discoveryEnabled.compareAndSet(!enabled, enabled);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void discovered(final DiscoveredItem discoveredItem) {
        if (instanceLogger.isDebugEnabled()) {
            try {
                instanceLogger.debug("Discovered device producing by module: [{}]", Serialization.getDefaultObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(discoveredItem));
            } catch (JsonProcessingException ex) {
                instanceLogger.error("Coud not serialize discovered device [{}]", discoveredItem);
            }
        }
        serverProxy.handleNewDiscoveredDevice((DiscoveredDevice) discoveredItem);
    }

    /**
     * Returns a map with device class references and their addresses.
     *
     * @return Map with device class references and their addresses.
     */
    public final Map<Class<? extends Device>, List<ItemAddress>> getRunningDevices() {
        return serverProxy.getRunningAddresses();
    }

    /**
     * Sends a data update notification for a device.
     *
     * @param notification The notification to send.
     * @return Future to indicate success or failure in publishing the data.
     */
    public final Future<Void> sendDeviceDataNotification(final DeviceDataNotification notification) {
        Promise<Void> sendSuccess = Promise.promise();
        serverProxy.handleDeviceDataNotification(notification, sendSuccess);
        return sendSuccess.future();
    }

    /**
     * Handles the command request which hsa been send by a device to the
     * module.
     *
     * @param command The command send.
     * @param promise Promise to let the server know it has been handled or it
     * failed.
     * @return The future to determine if a device command has been handled
     * successfully.
     */
    public abstract Future<Void> handleReceivedFromDevice(DeviceCommandRequest command, Promise<Void> promise);

}
