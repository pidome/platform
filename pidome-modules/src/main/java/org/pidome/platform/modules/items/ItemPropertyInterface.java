/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.items;

/**
 * Interface for item option sets.
 *
 * @author johns
 */
public interface ItemPropertyInterface {

    /**
     * Name of the option.
     *
     * @return the name
     */
    String getName();

    /**
     * Option value.
     *
     * @return the value
     */
    String getValue();

}
