/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.items;

/**
 * Address based on ip address.
 *
 * @author johns
 */
public class ItemIpAddress extends ItemAddress<String> {

    /**
     * Constructor setting String type.
     */
    public ItemIpAddress() {
        super("IP_ADDRESS");
    }

}
