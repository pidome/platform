/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.items;

/**
 * A float based address.
 *
 * @author johns
 */
public class ItemFloatAddress extends ItemAddress<Float> {

    /**
     * constructor setting float type.
     */
    public ItemFloatAddress() {
        super("FLOAT_ADDRESS");
    }

}
