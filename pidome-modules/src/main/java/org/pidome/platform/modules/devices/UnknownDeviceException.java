/*
 * Copyright 2013 John Sirach <john.sirach@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.pidome.platform.modules.devices;

/**
 * Exception thrown when there is an unknown device.
 *
 * @author John Sirach
 */
public class UnknownDeviceException extends Exception {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructor with message.
     *
     * @param message The message to set.
     */
    public UnknownDeviceException(final String message) {
        super(message);
    }

    /**
     * constructor with cause.
     *
     * @param cause The cause to set.
     */
    public UnknownDeviceException(final Throwable cause) {
        super(cause);
    }

    /**
     * Exception with message and cause.
     *
     * @param message The message to set.
     * @param cause The cause to set.
     */
    public UnknownDeviceException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
