/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.pidome.tools.comparators.NumberComparator;

/**
 * The slider control.
 *
 * The slider control implements a <code>Number</code> type.
 *
 * @author John
 */
public final class DeviceSliderControl extends DeviceControl<Number> {

    /**
     * Class version.
     */
    public static final long serialVersionUID = 1L;

    /**
     * The default minimum value.
     */
    private Number min = 0;

    /**
     * The default maximum value.
     */
    private Number max = 0;

    /**
     * Constructor.
     *
     * @param id The control id.
     */
    @JsonCreator
    public DeviceSliderControl(@JsonProperty("id") final String id) {
        super(id);
    }

    /**
     * Set the minimum value.
     *
     * @param min The minimum value to set.
     */
    public void setMin(final Number min) {
        this.min = min;
        this.setValue(min);
    }

    /**
     * Returns the minimal value.
     *
     * @return The minimum value.
     */
    public Number getMin() {
        return this.min;
    }

    /**
     * Set the minimum value.
     *
     * @param max The maximum value.
     */
    public void setMax(final Number max) {
        this.max = max;
    }

    /**
     * Returns the maximum value.
     *
     * @return The maximum value.
     */
    public Number getMax() {
        return this.max;
    }

    /**
     * Content equality check.
     *
     * @param o The content to check.
     * @return true if equal.
     */
    @Override
    public boolean valueEquals(final Object o) {
        if (o == null) {
            return false;
        } else if (!(o instanceof Number)) {
            return false;
        } else if (this.getValueData() == null) {
            return false;
        } else {
            return new NumberComparator().compare(this.getValue(), (Number) o) == 0;
        }
    }

    /**
     * Returns a new slider control builder.
     *
     * @param id The id of the control.
     * @return The builder.
     */
    public static SliderControlBuilder sliderDataBuilder(final String id) {
        return new SliderControlBuilder(id);
    }

    /**
     * Builder for the slider control.
     */
    @SuppressWarnings("PMD.AvoidFieldNameMatchingMethodName") //Builder pattern.
    public static class SliderControlBuilder extends DeviceControl.ControlBuilder<SliderControlBuilder> implements DeviceControlBuilder {

        /**
         * The control id.
         */
        private final String id;

        /**
         * The default minimum value.
         */
        private Number min = 0;

        /**
         * The default maximum value.
         */
        private Number max = 0;

        /**
         * Builder constructor.
         *
         * @param controlId The id of the control.
         */
        public SliderControlBuilder(final String controlId) {
            this.id = controlId;
            this.setReference(this);
        }

        /**
         * Set the minimum value.
         *
         * @param minValue The minimum value.
         * @return The builder
         */
        public SliderControlBuilder min(final Number minValue) {
            this.min = minValue;
            return this;
        }

        /**
         * Set the maximum value.
         *
         * @param maxValue The maximum value.
         * @return The builder
         */
        public SliderControlBuilder max(final Number maxValue) {
            this.max = maxValue;
            return this;
        }

        /**
         * Builds the slider control.
         *
         * @return The slider control.
         */
        @Override
        public DeviceSliderControl build() {
            DeviceSliderControl control = new DeviceSliderControl(this.id);
            control.setControlType(DeviceControlType.SLIDER);
            super.build(control);
            control.setMin(this.min);
            control.setMax(this.max);
            return control;
        }

    }

}
