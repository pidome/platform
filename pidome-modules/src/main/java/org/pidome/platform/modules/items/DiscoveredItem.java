/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.items;

import java.util.Date;
import java.util.UUID;

/**
 * A single discovered item.
 *
 * @author johns
 */
public interface DiscoveredItem extends Item {

    /**
     * A reference id to identify a discovered item.
     *
     * @return The reference id of the discovered item.
     */
    UUID getId();

    /**
     * The name of the discovered item.
     *
     * @return The discovered item name.
     */
    String getName();

    /**
     * The moment the discovery was done.
     *
     * @return Date object with discovery moment.
     */
    Date getDiscoveryDateTime();

}
