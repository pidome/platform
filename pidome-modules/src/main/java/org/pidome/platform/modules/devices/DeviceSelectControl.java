/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.modules.devices;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The select control.
 *
 * The select control is an Integer list indexed based control.
 *
 * When you want the real data which is available on the selected value use
 * <code>getValueData</code>
 *
 * @author John
 */
public final class DeviceSelectControl extends DeviceControl<ControlCommand<String>> {

    /**
     * Class version.
     */
    public static final long serialVersionUID = 1L;

    /**
     * List of available options.
     */
    private ControlCommandSet<String> optionList;

    /**
     * Constructor.
     *
     * @param id The id of the control.
     */
    @JsonCreator
    protected DeviceSelectControl(@JsonProperty("id") final String id) {
        super(id);
    }

    /**
     * Sets the option list.
     *
     * @param optionList The option list to set.
     */
    public void setOptionList(final ControlCommandSet<String> optionList) {
        this.optionList = optionList;
        if (!this.optionList.getSet().isEmpty()) {
            this.setValue(this.optionList.getSet().iterator().next());
        }
    }

    /**
     * Returns the option list.
     *
     * @return The option list.
     */
    public ControlCommandSet<String> getOptionList() {
        return this.optionList;
    }

    /**
     * Content equality check.
     *
     * @param o The content to check
     * @return true if equal
     */
    @Override
    public boolean valueEquals(final Object o) {
        if (o == null) {
            return false;
        } else if (!(o instanceof ControlCommand)) {
            return false;
        } else if (this.getValue() == null) {
            return false;
        } else {
            try {
                return o == this.getValue();
            } catch (Exception ex) {
                /// Could not match contents.
                return false;
            }
        }
    }

    /**
     * Returns a new select control builder.
     *
     * @param id The id of the control.
     * @return The builder.
     */
    public static SelectControlBuilder selectBuilder(final String id) {
        return new SelectControlBuilder(id);
    }

    /**
     * Builder for the select control.
     */
    @SuppressWarnings("PMD.AvoidFieldNameMatchingMethodName") //Builder pattern.
    public static class SelectControlBuilder extends DeviceControl.ControlBuilder implements DeviceControlBuilder<DeviceSelectControl> {

        /**
         * The control id.
         */
        private final String id;

        /**
         * List of available options.
         */
        private final ControlCommandSet<String> options = new ControlCommandSet<>();

        /**
         * Builder constructor.
         *
         * @param controlId The id of the control.
         */
        @SuppressWarnings("unchecked")
        public SelectControlBuilder(final String controlId) {
            this.id = controlId;
            this.setReference(this);
        }

        /**
         * Adds a command to the select.
         *
         * @param command The command to add.
         * @return The builder.
         */
        public SelectControlBuilder addCommand(final ControlCommand<String> command) {
            this.options.getSet().add(command);
            return this;
        }

        /**
         * Builds the select control.
         *
         * @return The select control.
         */
        @Override
        @SuppressWarnings("unchecked")
        public DeviceSelectControl build() {
            DeviceSelectControl control = new DeviceSelectControl(this.id);
            control.setControlType(DeviceControlType.SELECT);
            super.build(control);
            control.setOptionList(this.options);
            return control;
        }

    }

}
