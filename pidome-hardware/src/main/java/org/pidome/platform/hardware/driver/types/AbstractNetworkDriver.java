/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.hardware.driver.types;

import org.pidome.platform.hardware.driver.HardwareDriver;
import org.pidome.platform.hardware.driver.config.DummyConfigInterface;
import org.pidome.platform.hardware.driver.types.consumers.DummyProsumer;
import org.pidome.platform.presentation.input.InputForm;

/**
 * This is used as a dummy interface.The network adapter is utilized using an
 * hardware interface to have a hardware compatible event bus to its
 * availability.
 *
 * It is not exposed.
 *
 * @author John Sirach
 * @param <T> The configuration class to be applied.
 */
public abstract class AbstractNetworkDriver<T extends InputForm> extends HardwareDriver<DummyConfigInterface, T, DummyProsumer> {

}
