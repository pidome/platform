/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.hardware.driver.types.consumers;

/**
 * Interface for the serial data prosumer implementation in a driver.
 *
 * @author johns
 */
public final class SerialDataProsumer implements ProsumerInterface {

    /**
     * Consumer for the driver to read the data coming from the server.
     */
    private SerialDataConsumer moduleConsumer;

    /**
     * Consumer for the server to read the data coming from a driver.
     */
    private SerialDataConsumer driverConsumer;

    /**
     * Dispatches data to the module/server.
     *
     * @param data The data to send to the module/server.
     */
    public void produceByDriver(final byte[] data) {
        if (this.driverConsumer != null) {
            this.driverConsumer.accept(data);
        }
    }

    /**
     * Supplies the driver with data from a module.
     *
     * @param data The data to supply.
     */
    public void produceByModule(final byte[] data) {
        if (this.moduleConsumer != null) {
            this.moduleConsumer.accept(data);
        }
    }

    /**
     * The server uses this to receive the data from a driver and pass it to the
     * module.
     *
     * @param driverConsumer Consumer to read data from a driver.
     */
    public void setDriverConsumer(final SerialDataConsumer driverConsumer) {
        this.driverConsumer = driverConsumer;
    }

    /**
     * Driver should call this to receive data from the module/server.
     *
     * @param moduleConsumer Consumer to read from the module/server.
     */
    public void setModuleConsumer(final SerialDataConsumer moduleConsumer) {
        this.moduleConsumer = moduleConsumer;
    }

}
