/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.hardware.driver.types;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.pidome.platform.hardware.driver.HardwareDriver;
import org.pidome.platform.hardware.driver.config.SerialDeviceConfiguration;
import org.pidome.platform.hardware.driver.types.consumers.SerialDataProsumer;
import org.pidome.platform.presentation.input.InputForm;

/**
 * The interface for hardware serial drivers to implement.
 * <p>
 * An implementation of this class should use the following methods to receive
 * and send data:
 * <ul>
 * <li><code>sendDataToModule</code> To send data to a module</li>
 * <li><code>setModuleDataListener</code> To receive data from a module</li>
 * </ul>
 *
 * @author John Sirach
 * @param <T> The <code>InputForm</code> being extended which is returned by the
 * driver.
 */
public abstract class AbstractSerialDriver<T extends InputForm> extends HardwareDriver<SerialDeviceConfiguration, T, SerialDataProsumer> {

    /**
     * The prosumer for a serial driver.
     */
    @JsonIgnore
    private SerialDataProsumer prosumer = new SerialDataProsumer();

    /**
     * The driver should call this to retrieve the prosumer to be able to send
     * and receive data from the server.
     *
     * @return
     */
    @Override
    public final SerialDataProsumer getProsumer() {
        return this.prosumer;
    }

}
