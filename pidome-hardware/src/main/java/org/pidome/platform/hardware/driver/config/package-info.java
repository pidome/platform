/**
 * Hardware driver configuration.
 * <p>
 * Provides abstract and interface hardware driver configuration classes.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.platform.hardware.driver.config;
