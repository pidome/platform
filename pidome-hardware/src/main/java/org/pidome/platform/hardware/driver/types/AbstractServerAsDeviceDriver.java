/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.hardware.driver.types;

import org.pidome.platform.hardware.driver.HardwareDriver;
import org.pidome.platform.hardware.driver.config.DummyConfigInterface;
import org.pidome.platform.hardware.driver.types.consumers.DummyProsumer;
import org.pidome.platform.presentation.input.InputForm;

/**
 * This interface is used internally to be able to expose server information.
 *
 * It is not exposed.
 *
 * @author John Sirach
 * @param <T> The configuration class to be applied.
 */
public abstract class AbstractServerAsDeviceDriver<T extends InputForm> extends HardwareDriver<DummyConfigInterface, T, DummyProsumer> {

}
