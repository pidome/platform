/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.hardware.driver.types.consumers;

import static java.util.Objects.requireNonNull;
import java.util.function.Consumer;

/**
 * Consumer for byte arrays.
 *
 * @author johns
 */
public interface SerialDataConsumer extends Consumer<Byte[]> {

    /**
     * Performs this operation on the given byte value.
     *
     * @param value The input value
     */
    void accept(byte[] value);

    /**
     * Performs this operation on the given byte value.
     *
     * @param value The input value
     */
    @Override
    default void accept(final Byte[] value) {
        byte[] bytes = new byte[value.length];
        for (int i = 0; i < value.length; i++) {
            bytes[i] = value[i].byteValue();
        }
        accept(bytes);
    }

    /**
     * @param after To apply after current.
     * @return ByteArrayConsumer the consumer.
     */
    default SerialDataConsumer andThen(final SerialDataConsumer after) {
        requireNonNull(after, "after");
        return value -> {
            accept(value);
            after.accept(value);
        };
    }

}
