/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.hardware.driver;

import org.pidome.platform.hardware.driver.types.AbstractBluetoothDriver;
import org.pidome.platform.hardware.driver.types.AbstractDummyDriver;
import org.pidome.platform.hardware.driver.types.AbstractHidDriver;
import org.pidome.platform.hardware.driver.types.AbstractI2CDriver;
import org.pidome.platform.hardware.driver.types.AbstractNetworkDriver;
import org.pidome.platform.hardware.driver.types.AbstractSerialDriver;
import org.pidome.platform.hardware.driver.types.AbstractServerAsDeviceDriver;
import org.pidome.platform.hardware.driver.types.HardwareDriverInterface;

/**
 * Interface for the transport used.
 *
 * @author John Sirach
 */
public interface Transport {

    /**
     * The sub system used to communicate with the peripheral.
     */
    enum SubSystem {
        /**
         * An unknown subsystem.
         *
         * Used to inform an end user the driver is incorrectly configured.
         */
        UNKNOWN("Unknown", AbstractDummyDriver.class),
        /**
         * A Server internally used communication channel.
         */
        SERVER("Server", AbstractServerAsDeviceDriver.class),
        /**
         * I2C Communication subsystem.
         */
        I2C("I2C", AbstractI2CDriver.class),
        /**
         * Network based communication channel.
         */
        NETWORK("Network", AbstractNetworkDriver.class),
        /**
         * Serial communication channel.
         */
        SERIAL("Serial", AbstractSerialDriver.class),
        /**
         * USB HID communication channel.
         */
        HID("USB HID", AbstractHidDriver.class),
        /**
         * Bluetooth communication channel.
         */
        BLUETOOTH("Bluetooth", AbstractBluetoothDriver.class);

        /**
         * The description of the communication subsystem enum.
         */
        private final String description;

        /**
         * The implementation required for the specific transport.
         */
        private final Class<? extends HardwareDriverInterface> implementation;

        /**
         * The enum constructor.
         *
         * @param fieldDescription The description of the enum.
         * @param implementation The implementation of the subsystem.
         */
        SubSystem(final String fieldDescription, final Class<? extends HardwareDriverInterface> implementation) {
            this.description = fieldDescription;
            this.implementation = implementation;
        }

        /**
         * Returns the description of the enum.
         *
         * @return The enum description.
         */
        public String getDescription() {
            return description;
        }

        /**
         * Returns the implementation required for the transport type.
         *
         * @return An implementation.
         */
        public final Class<? extends HardwareDriverInterface> getImplementation() {
            return implementation;
        }

        /**
         * Returns the subsystem by the given implementation class.
         *
         * @param implementationClass The raw class extending the required
         * interface.
         * @return The subsystem of the given class.
         */
        public static SubSystem getByImplementationClass(final Class<? extends HardwareDriverInterface> implementationClass) {
            return getByImplementationClass(implementationClass.getSimpleName());
        }

        /**
         * Returns the subsystem by the given implementation class.
         *
         * @param classSimpleName The <code>Class.getSimpleName</code> string.
         * @return The subsystem of the given class.
         */
        public static SubSystem getByImplementationClass(final String classSimpleName) {
            for (SubSystem sub : SubSystem.values()) {
                if (sub.getImplementation().getSimpleName().equals(classSimpleName)) {
                    return sub;
                }
            }
            return SubSystem.UNKNOWN;
        }

        /**
         * Returns the enum by the given name.
         *
         * @param enumName The name given to retrieve the enum for.
         * @return The enum found by it's name. If not found
         * <code>UNKNOWN</code> is returned.
         */
        public static SubSystem getByName(final String enumName) {
            for (SubSystem sub : SubSystem.values()) {
                if (sub.name().equals(enumName)) {
                    return sub;
                }
            }
            return SubSystem.UNKNOWN;
        }

    }

    /*
    public default SubSystem getTransportSubSystem() {
        return SubSystem.getByName(((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0].getTypeName());
    }
     */
}
