/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.hardware.driver;

import io.vertx.core.Promise;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicBoolean;
import org.pidome.platform.hardware.driver.config.HardwareConfiguration;
import org.pidome.platform.hardware.driver.types.HardwareDriverInterface;
import org.pidome.platform.hardware.driver.types.consumers.ProsumerInterface;
import org.pidome.platform.presentation.Presentation;
import org.pidome.platform.presentation.components.list.DescriptionList;
import org.pidome.platform.presentation.components.list.DescriptionListItem;
import org.pidome.platform.presentation.input.InputForm;
import org.pidome.tools.utilities.DataUtils;

/**
 * The base for the hardware driver.
 *
 * <p>
 * This is a live object. This means that you are capable of keeping state of
 * your implementation between start and stop methods. Use the provided
 * <code>Promise</code>'s in the start and stop methods to indicate success or
 * failure.
 * <p>
 * The <code>Promise</code> implementation used is provided by the Eclipse
 * Vert.X project. Please check https://vertx.io/ for more information.
 * <p>
 * A driver has multiple phases:
 * <li>Generate configuration phase during <code>composeConfiguration</code>.
 * <li>Configure the driver and prepare any hardware to be used during
 * <code>configure</code>
 * <li>Start the driver and use the configuration set.
 * <li>Stop the driver and release.
 *
 * <p>
 * The <code>configure</code> and <code>startDriver</code> methods are being
 * executed after each other as long as the future of <code>configure</code>
 * succeeds. When the <code>configure</code> fails, stopDriver will be called to
 * release any references.
 * <p>
 * The method <code>stopDriver</code> will always be called when there is some
 * sort of failure or when the driver is being stopped. This IS the method to
 * release all references.
 *
 * @author John Sirach
 * @param <C> The configuration being supplied by the server to the driver.
 * @param <T> The configuration to be created by the driver creator, this object
 * is returned during configuration phase.
 * @param <P> The prosumer used to communicate between server and driver.
 */
public abstract class HardwareDriver<C, T extends InputForm, P extends ProsumerInterface> implements HardwareConfiguration<C, T>, HardwareDriverInterface<P> {

    /**
     * How many bytes have been send to hardware.
     */
    private BigInteger dataSendCounter = BigInteger.ZERO;
    /**
     * When the last data has been send to the device.
     */
    private final DescriptionListItem dataSend = new DescriptionListItem("Data send", "0");
    /**
     * When the last data has been send to the device.
     */
    private final DescriptionListItem lastDataSend = new DescriptionListItem("Last data send", "00-00-00 00:00:00");
    /**
     * how many bytes have been received from hardware.
     */
    private BigInteger dataRecievedCounter = BigInteger.ZERO;
    /**
     * When the last data has been send to the device.
     */
    private final DescriptionListItem dataReceived = new DescriptionListItem("Data received", "0");
    /**
     * When the last data has been send to the device.
     */
    private final DescriptionListItem lastDataReceived = new DescriptionListItem("Last data received", "00-00-00 00:00:00");

    /**
     * Calendar for time/date registration.
     */
    private final Calendar cal = new GregorianCalendar(TimeZone.getDefault(), Locale.getDefault());

    /**
     * Date format used for last data send/received.
     */
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault(Locale.Category.FORMAT));

    /**
     * If the driver currently is stopping.
     */
    private AtomicBoolean stopping = new AtomicBoolean(Boolean.FALSE);

    /**
     * Presentation for data utilization.
     */
    private final Presentation dataPresentation = new Presentation("Data transfered")
            .addSections(new DescriptionList("Send")
                    .addListItems(lastDataSend, dataSend),
                    new DescriptionList("Recieved")
                            .addListItems(lastDataReceived, dataReceived)
            );

    /**
     * Increment the amount of bytes send to hardware.
     *
     * @param byteSendToDevice The amount send.
     */
    public final void updateSendCounter(final long byteSendToDevice) {
        cal.setTime(new Date());
        dataSendCounter = dataSendCounter.add(BigInteger.valueOf(byteSendToDevice));
        lastDataSend.setDd(dateFormat.format(cal.getTime()));
    }

    /**
     * Increment the amount of bytes received from the hardware.
     *
     * @param bytesReceived The amount received.
     */
    public final void updateReceivedCounter(final long bytesReceived) {
        cal.setTime(new Date());
        dataRecievedCounter = dataRecievedCounter.add(BigInteger.valueOf(bytesReceived));
        lastDataReceived.setDd(dateFormat.format(cal.getTime()));
    }

    /**
     * Returns the information about data being send / received from hardware.
     *
     * @return Presentation containing data transfer information.
     */
    public final Presentation getDataInfo() {
        dataSend.setDd(DataUtils.dataSizeToHumanReadable(dataSendCounter, 2));
        dataReceived.setDd(DataUtils.dataSizeToHumanReadable(dataRecievedCounter, 2));
        return dataPresentation;
    }

    /**
     * The configuration presentation of the driver.
     */
    private T configuration;

    /**
     * Sets the configuration.
     *
     * This method is called from <code>composeConfiguration</code> which is
     * called when the driver enters configuration mode.
     *
     * To create the configuration you use
     * {@link org.pidome.platform.presentation.input.InputSection} for each
     * configuration section.
     *
     * Inside these sections you place
     * {@link org.pidome.platform.presentation.input.InputField} which will be
     * shown to the end user. Refer to the pidome-presentation package for more
     * information on how to create a configuration shown to the end user.
     * <p>
     * To set a configuration create a class which extends
     * <code>InputForm</code> and create the configuration in the constructor.
     * <p>
     *
     * @param configuration The configuration to apply.
     */
    public final void setConfiguration(final T configuration) {
        this.configuration = configuration;
    }

    /**
     * Returns the configuration form of this driver.
     *
     * This method returns the form used to configure this driver. When you want
     * to use the configuration you need to create your form in the
     * <code>composeConfiguration</code> method in your driver.
     *
     * @return the configuration object.
     * @see org.pidome.platform.presentation.input.InputForm
     */
    public final T getConfiguration() {
        return configuration;
    }

    /**
     * Starts the driver implementation.
     *
     * This method is called after <code>setOpts()</code> when the future has
     * been completed.
     *
     * The future is used as an indicator so the server knows the driver is
     * active. It does not prevent from <code>stopDriver()</code> being called.
     *
     * @param promise The promise to complete or fail when a driver wants to
     * start or not.
     */
    public abstract void startDriver(Promise<Void> promise);

    /**
     * Stops the driver implementation.
     *
     * @param promise The promise to let know if the driver is successfully
     * stopped.
     */
    public abstract void stopDriver(Promise<Void> promise);

    /**
     * If the driver currently is stopping.
     *
     * @return the stopping
     */
    public boolean isStopping() {
        return stopping.getAcquire();
    }

    /**
     * If the driver currently is stopping.
     */
    public void setStopping() {
        this.stopping.compareAndSet(Boolean.FALSE, Boolean.TRUE);
    }

    /**
     * Returns the name of the class from the instance.
     *
     * The class instance name is returned, which means it returns the canonical
     * name of the object implementing the <code>HardwareDriver</code>
     *
     * @return The class canonical name.
     */
    @Override
    public String toString() {
        return new StringBuilder("HardwareDriver:[").append(this.getClass().getCanonicalName()).append("],")
                .append("configuration:[").append(configuration).append("]")
                .toString();
    }

}
