/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.hardware.driver.config;

import io.vertx.core.Promise;
import org.pidome.platform.presentation.types.ConfigurationPresentation;

/**
 * Interface required to be able to supply a hardware configuration to an
 * implementation.
 *
 * @author John Sirach
 * @param <C> The configuration supplied by the server to support the driver.
 * @param <T> The configuration object type returned by the driver.
 */
public interface HardwareConfiguration<C, T> extends ConfigurationPresentation {

    /**
     * Supplies the configuration of a peripheral attached to the system.
     *
     * @param configuration The configuration.
     * @param promise The promise to let the supplier know configuration has
     * been applied/created.
     */
    void composeConfiguration(C configuration, Promise<Void> promise);

    /**
     * Called when the user provides the configuration.This method is called
     * when a user has entered the configuration needed, and if a configuration
     * has been created.
     *
     * @param configurationResult The configuration as created by
     * <code>composeConfiguration</code> but with selected values by the end
     * user.
     * @param promise The promise to notify PiDome the configuration has been
     * completed.
     */
    void configure(T configurationResult, Promise<Void> promise);

}
