/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.hardware.driver.types;

import org.pidome.platform.hardware.driver.types.consumers.ProsumerInterface;

/**
 * Interface for hardware drivers.
 *
 * @author John Sirach
 * @param <P> The prosumer used to communicate between server and driver.
 */
public interface HardwareDriverInterface<P extends ProsumerInterface> {

    /**
     * The driver should call this to retrieve the prosumer to be able to send
     * and receive data from the server.
     *
     * @return The prosumer.
     */
    P getProsumer();

}
