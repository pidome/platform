/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.hardware.driver;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Enum for discovery purposes.
 *
 * @author John Sirach
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface HardwareDriverDiscovery {

    /**
     * If the driver discovery has been enabled.
     *
     * @return true when enabled.
     */
    boolean enabled() default true;

    /**
     * The name of the driver.
     *
     * @return The driver name.
     */
    String name();

    /**
     * The description of the driver.
     *
     * @return The driver description.
     */
    String description();

}
