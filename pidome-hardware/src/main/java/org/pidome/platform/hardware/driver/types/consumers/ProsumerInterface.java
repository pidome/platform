/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.hardware.driver.types.consumers;

/**
 * Base interface for Prosumer implementations.
 *
 * A prosumer both consumes and produces data.
 *
 * @author johns
 */
public interface ProsumerInterface {

}
