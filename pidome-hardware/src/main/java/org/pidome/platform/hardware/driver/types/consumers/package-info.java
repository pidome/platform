/**
 * Consumers for the driver implementations.
 * <p>
 * This package contains consumers specialized for transfering data between
 * drivers and server.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.platform.hardware.driver.types.consumers;
