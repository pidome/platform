/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.hardware.driver.types;

import org.pidome.platform.hardware.driver.HardwareDriver;
import org.pidome.platform.hardware.driver.config.DummyConfigInterface;
import org.pidome.platform.hardware.driver.types.consumers.DummyProsumer;
import org.pidome.platform.presentation.input.InputForm;

/**
 * The HID driver.
 *
 * @author John Sirach
 * @param <T> The <code>InputForm</code> being extended which is returned by the
 * driver.
 */
public abstract class AbstractHidDriver<T extends InputForm> extends HardwareDriver<DummyConfigInterface, T, DummyProsumer> {

}
