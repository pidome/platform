/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.hardware.driver.types;

import io.vertx.core.Promise;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.pidome.tools.properties.BooleanPropertyBinding;

/**
 *
 * @author johns
 */
public class AbstractSerialDriverTest {

    public AbstractSerialDriverTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of dispatchToServer method, of class AbstractSerialDriver.
     */
    @Test
    public void testDispatchToServer() {
        final byte[] bytesExpected = new byte[]{1, 2, 3};
        /// Driver implementation to set to true when data is recieved.
        final BooleanPropertyBinding driverRecieved = new BooleanPropertyBinding(Boolean.FALSE);
        /// Module implementation to set to true when data is recieved.
        final BooleanPropertyBinding moduleRecieved = new BooleanPropertyBinding(Boolean.FALSE);

        AbstractSerialDriver driver = new TestDriver();

        /// Recieve data from th server.
        driver.getProsumer().setModuleConsumer(data -> {
            assertThat(data, is(equalTo(bytesExpected)));
            driverRecieved.setValue(Boolean.TRUE);
            /// Send data to a module.
            driver.getProsumer().produceByDriver(data);
        });
        /// Data send from a driver to the server which listens.
        driver.getProsumer().setDriverConsumer(data -> {
            moduleRecieved.setValue(Boolean.TRUE);
            assertThat(data, is(equalTo(bytesExpected)));
        });
        /// A module sends data to a driver.
        driver.getProsumer().produceByModule(bytesExpected);
        assertThat(driverRecieved.get(), is(equalTo(Boolean.TRUE)));
        assertThat(moduleRecieved.get(), is(equalTo(Boolean.TRUE)));
    }

    static class TestDriver extends AbstractSerialDriver {

        @Override
        public void startDriver(Promise promise) {
            promise.complete();
        }

        @Override
        public void stopDriver(Promise promise) {
            promise.complete();
        }

        @Override
        public void composeConfiguration(Object configuration, Promise promise) {
            throw new UnsupportedOperationException("Not supported.");
        }

        @Override
        public void configure(Object configurationResult, Promise promise) {
            throw new UnsupportedOperationException("Not supported.");
        }

    }

}
