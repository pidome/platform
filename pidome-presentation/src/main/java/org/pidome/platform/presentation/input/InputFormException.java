/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input;

import java.util.List;

/**
 * Input form errors.
 *
 * @author johns
 */
public final class InputFormException extends Exception {

    /**
     * Class version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The input fields error handler.
     */
    private final List<InputFormError> inputErrors;

    /**
     * Constructs an instance of <code>InputFormException</code> with the
     * specified detail message.
     *
     * @param handler The handler containing the errors.
     */
    public InputFormException(final InputFieldRequirementsHandler handler) {
        super("Errors are present in the form");
        this.inputErrors = handler.getErrors();
    }

    /**
     * Returns the list of errors in the input form.
     *
     * @return List of input field errors.
     */
    public List<InputFormError> getInputErrors() {
        return this.inputErrors;
    }

}
