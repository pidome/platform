/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Deserializer for enum values.
 *
 * @author johns
 * @param <T> The enum.
 */
public class InputEnumDeserializer<T extends Enum<T>> extends JsonDeserializer<T> {

    /**
     * Return the enum.
     *
     * @param parser Parsed used for reading JSON content.
     * @param context Context that can be used to access information about this
     * deserialization activity.
     * @return the enum deserialized.
     * @throws IOException When
     * @throws JsonProcessingException
     */
    @Override
    @SuppressWarnings({"unchecked", "PMD.UseProperClassLoader"})
    public T deserialize(final JsonParser parser, final DeserializationContext context) throws IOException, JsonProcessingException {
        JsonNode node = parser.getCodec().readTree(parser);
        String enumStringClass = node.get("path").asText();
        try {
            return Enum.valueOf((Class<T>) Class.forName(enumStringClass, true, context.getTypeFactory().getClassLoader()), node.get("name").asText());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(InputEnumDeserializer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
