/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.components.list;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.pidome.platform.presentation.properties.Collapsible;
import org.pidome.platform.presentation.skeleton.PresentationSection;

/**
 * HTML dl like equivalent.
 *
 * @author johns
 */
public final class DescriptionList extends PresentationSection implements Collapsible {

    /**
     * the items in the list.
     */
    private final List<DescriptionListItem> listItems = new ArrayList<>();

    /**
     * Set a description list collapsible.
     */
    private boolean collapsible = false;

    /**
     * Constructor.
     */
    public DescriptionList() {
        this(null, null);
    }

    /**
     * Constructor.
     *
     * @param sectionName The name of the section.
     */
    public DescriptionList(final String sectionName) {
        this(sectionName, null);
    }

    /**
     * Constructor.
     *
     * @param sectionName The name of the section.
     * @param sectionDescription The description of the section.
     */
    public DescriptionList(final String sectionName, final String sectionDescription) {
        super(PresentationComponent.DESCRIPTION_LIST, sectionName, sectionDescription);
    }

    /**
     * Adds a single list item.
     *
     * @param item The item to add.
     * @return this for fluent design.
     */
    public DescriptionList addListItem(final DescriptionListItem item) {
        this.listItems.add(item);
        return this;
    }

    /**
     * Removes a list item.
     *
     * @param item the item to remove.
     * @return this for fluent design.
     */
    public DescriptionList removeListItem(final DescriptionListItem item) {
        this.listItems.remove(item);
        return this;
    }

    /**
     * Adds multiple items to the list.
     *
     * @param items The items to add.
     * @return this for fluent design.
     */
    public DescriptionList addListItems(final DescriptionListItem... items) {
        this.addListItems(Arrays.asList(items));
        return this;
    }

    /**
     * Adds multiple items to the list.
     *
     * @param items The items to add.
     * @return this for fluent design.
     */
    public DescriptionList addListItems(final List<DescriptionListItem> items) {
        this.listItems.addAll(items);
        return this;
    }

    /**
     * Replaces the items in the list with the given items.
     *
     * @param items The items to replace the list with.
     * @return this for fluent design.
     */
    public DescriptionList setListItems(final DescriptionListItem... items) {
        this.setListItems(Arrays.asList(items));
        return this;
    }

    /**
     * Replaces the items in the list with the given items.
     *
     * @param items The items to replace the list with.
     * @return this for fluent design.
     */
    public DescriptionList setListItems(final List<DescriptionListItem> items) {
        this.clearListItems();
        this.listItems.addAll(items);
        return this;
    }

    /**
     * Clears the list.
     *
     * @return this for fluent design.
     */
    public DescriptionList clearListItems() {
        this.listItems.clear();
        return this;
    }

    /**
     * Returns the list items.
     *
     * @return The items in the list.
     */
    public List<DescriptionListItem> getListItems() {
        return this.listItems;
    }

    /**
     * Set a description list collapsible.
     *
     * @return the collapsible
     */
    @Override
    public boolean isCollapsible() {
        return collapsible;
    }

    /**
     * Set a description list collapsible.
     *
     * When a list is collapsible, it will start closed and displays a clickable
     * title
     *
     * @param collapsible the collapsible to set
     */
    public void setCollapsible(final boolean collapsible) {
        this.collapsible = collapsible;
    }

}
