/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

/**
 * An error in the InputForm.
 *
 * @author johns
 */
public final class InputFormError implements Serializable {

    /**
     * Class version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The label the error occurred.
     */
    @JsonProperty("field")
    private final String id;

    /**
     * The label the error occurred.
     */
    @JsonProperty("label")
    private final String label;

    /**
     * The message of the error.
     */
    @JsonProperty("message")
    private final String message;

    /**
     * Error message constructor.
     *
     * @param id The id of the field.
     * @param label The label.
     * @param message The message.
     */
    protected InputFormError(final String id, final String label, final String message) {
        this.id = id;
        this.label = label;
        this.message = message;
    }

    /**
     * @return the id
     */
    protected String getId() {
        return id;
    }

    /**
     * @return the label
     */
    protected String getLabel() {
        return label;
    }

    /**
     * @return the message
     */
    protected String getMessage() {
        return message;
    }

}
