/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class used to determine any errors in an InputField.
 *
 * @author johns
 */
public final class InputFieldRequirementsHandler {

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(InputFieldRequirementsHandler.class);

    /**
     * List containing the errors.
     */
    private final List<InputFormError> requirementErrors = new ArrayList<>();

    /**
     * Method which checks for requirements and gathers exception messages.
     *
     * @param inputField The input field to check.
     */
    public void checkForRequirements(final InputField inputField) {
        try {
            inputField.checkRequirements();
        } catch (InputValueException ex) {
            requirementErrors.add(new InputFormError(ex.getId(), ex.getLabel(), ex.getMessage()));
            LOG.warn("Input field [{}, {}] did not met requirements [{}]", ex.getId(), ex.getLabel(), ex.getMessage());
        }
    }

    /**
     * Returns if there are errors found in the input field.
     *
     * @return true when there are errors, false when all ok.
     */
    public boolean hasErrors() {
        return !this.requirementErrors.isEmpty();
    }

    /**
     * Returns a list of errors gathered.
     *
     * @return The list of errors.
     */
    public List<InputFormError> getErrors() {
        return this.requirementErrors;
    }

}
