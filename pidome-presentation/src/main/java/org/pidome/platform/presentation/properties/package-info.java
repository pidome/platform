/**
 * Properties for components.
 * <p>
 * Properties implementable by presentations. These properties are set on base
 * components and are not user implementable. Not all components support all
 * properties.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.platform.presentation.properties;
