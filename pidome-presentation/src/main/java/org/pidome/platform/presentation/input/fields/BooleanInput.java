/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input.fields;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.pidome.platform.presentation.input.InputField;
import org.pidome.platform.presentation.input.InputValueException;

/**
 * Checkbox to select a true or false value.
 *
 * @author John Sirach
 */
public final class BooleanInput extends InputField<Boolean> {

    /**
     * Constructor setting id, name and description.
     *
     * @param fieldId The id of the field.
     * @param fieldLabel The label of the field.
     * @param fieldDescription The description of the field.
     */
    @JsonCreator
    public BooleanInput(@JsonProperty("id") final String fieldId,
            @JsonProperty("label") final String fieldLabel,
            @JsonProperty("description") final String fieldDescription) {
        super(fieldId, fieldLabel, fieldDescription, InputFieldType.BOOLEAN_FIELD);
    }

    /**
     * Validate the input.
     *
     * @throws InputValueException When validation fails.
     */
    @Override
    public void validateInput() throws InputValueException {

    }

}
