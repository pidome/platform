/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input.fields;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.regex.Matcher;
import org.pidome.platform.presentation.input.InputField;
import org.pidome.platform.presentation.input.InputValueException;
import org.pidome.tools.utilities.NetworkUtils;

/**
 * Input field for ip addresses.
 *
 * @author John Sirach
 */
@SuppressWarnings("CPD-START")
public final class IpAddressInput extends InputField<String> {

    /**
     * Constructor setting id, name and description.
     *
     * @param fieldId The id of the field.
     * @param fieldLabel The label of the field.
     * @param fieldDescription The description of the field.
     */
    @JsonCreator
    public IpAddressInput(@JsonProperty("id") final String fieldId,
            @JsonProperty("label") final String fieldLabel,
            @JsonProperty("description") final String fieldDescription) {
        super(fieldId, fieldLabel, fieldDescription, InputFieldType.IP_ADDRESS_FIELD);
    }

    /**
     * Validate the input.
     *
     * @throws InputValueException When validation fails.
     */
    @Override
    public void validateInput() throws InputValueException {
        Matcher ipMatch = NetworkUtils.PATTERN_IS_IP.matcher(this.getValue());
        if (!ipMatch.matches()) {
            throw new InputValueException(this, "Given ip [" + this.getValue() + "] is not a valid ip address");
        }
    }

}
