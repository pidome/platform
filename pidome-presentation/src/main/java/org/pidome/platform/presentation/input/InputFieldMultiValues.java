/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Base class for multi value inputs.
 *
 * @author johns
 * @param <T> The type implemented for the input field.
 */
public abstract class InputFieldMultiValues<T> extends InputField<T> {

    /**
     * The values to be displayed in the select list.
     */
    private final Set<T> listValues = new HashSet<>();

    /**
     * Constructor for the multi value input.
     *
     * @param fieldId The id of the field.
     * @param fieldLabel The label fo the field.
     * @param fieldDescription The description of the field.
     * @param fieldInputType The field input type.
     */
    protected InputFieldMultiValues(final String fieldId, final String fieldLabel, final String fieldDescription, final InputElement.InputFieldType fieldInputType) {
        super(fieldId, fieldLabel, fieldDescription, fieldInputType);
    }

    /**
     * Sets the list of <code>Enum</code> items to be displayed in the select
     * field.
     *
     * @param items <code>List</code> of items of type <code>Enum</code>
     */
    @JsonProperty("elements")
    public final void setListValues(final List<T> items) {
        if (items != null && !items.isEmpty()) {
            this.listValues.addAll(items);
        }
    }

    /**
     * Sets the list of <code>Enum</code> items to be displayed in the select
     * field.
     *
     * @param items <code>List</code> of items of type <code>Enum</code>
     */
    @SuppressWarnings({"unchecked", "varargs"})
    @JsonIgnore
    public final void setListValues(final T... items) {
        if (items != null) {
            this.listValues.addAll(Set.of(items));
        }
    }

    /**
     * Adds a single item to the list to be displayed.
     *
     * @param item The item to add of type <code>Enum</code>
     */
    public final void addItem(final T item) {
        if (item != null) {
            this.listValues.add(item);
        }
    }

    /**
     * Validates the input given.
     *
     * @throws InputValueException When validation fails
     */
    @Override
    public final void validateInput() throws InputValueException {
        if (this.getValue() == null) {
            throw new InputValueException(this, "Value is required");
        } else if (!this.listValues.contains(this.getValue())) {
            throw new InputValueException(this, "Given value [" + this.getValue() + "] is not in the list");
        }
    }

    /**
     * Returns the list of values to be displayed.
     *
     * @return The list with type <code>Enum</code> to be displayed.
     */
    public final Set<T> getListValues() {
        return this.listValues;
    }

}
