/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input;

/**
 * Exception thrown when there is an invalid value set by the end user.
 *
 * @author John Sirach
 */
public final class InputValueException extends Exception {

    /**
     * Class version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The label involved in the message.
     */
    private final String label;

    /**
     * The id of the field the error occurred on.
     */
    private final String id;

    /**
     * Constructs an instance of <code>WebInputInvalidValueException</code> with
     * the specified detail message.
     *
     * @param field The input field causing the exception.
     * @param message the detail message.
     */
    public InputValueException(final InputField field, final String message) {
        super(message);
        this.id = field.getId();
        this.label = field.getLabel();
    }

    /**
     * Returns the label on which the exception occurred.
     *
     * @return The input field label.
     */
    public String getLabel() {
        return this.label;
    }

    /**
     * Returns the id of the field.
     *
     * @return The field id.
     */
    public String getId() {
        return this.id;
    }
}
