/**
 * Presentation types.
 * <p>
 * Provides interfaces to identify presentation types.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.platform.presentation.types;
