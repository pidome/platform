/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.properties;

/**
 * Interface for components which should support collapsible elements.
 *
 * @author johns
 */
public interface Collapsible {

    /**
     * If collapsible or not.
     */
    boolean COLLAPSIBLE = false;

    /**
     * If a component is initially an open collapsible.
     */
    boolean COLLAPSIBLE_OPEN = false;

    /**
     * If a component is collapsible or not.
     *
     * @return If the component is collapsible.
     */
    default boolean isCollapsible() {
        return COLLAPSIBLE;
    }

    /**
     * @return If a component is initially an open collapsible.
     */
    default boolean isCollapsibleOpen() {
        return COLLAPSIBLE_OPEN;
    }

}
