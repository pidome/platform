/**
 * List components.
 * <p>
 * Components for constructing list based structure on the front-end.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.platform.presentation.components.list;
