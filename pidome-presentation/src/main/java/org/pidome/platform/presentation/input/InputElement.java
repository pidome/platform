/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input;

/**
 * Interface for web elements.
 *
 * @author John Sirach
 * @param <T> The type inside the element.
 */
public interface InputElement<T> {

    /**
     * Possible field types for displaying.
     */
    enum InputFieldType {
        /**
         * URL input.
         */
        URL_FIELD,
        /**
         * Ip address input.
         */
        IP_ADDRESS_FIELD,
        /**
         * Text input string, arbitrary input.
         */
        STRING_FIELD,
        /**
         * Text input string, arbitrary input.
         */
        STRING_MULTILINE_FIELD,
        /**
         * Password input field.
         */
        PASSWORD_FIELD,
        /**
         * Integer input field.
         */
        INT_FIELD,
        /**
         * Input field with decimals.
         */
        DOUBLE_FIELD,
        /**
         * Device data input. This is a combined input field.
         */
        DEVICEDATA_FIELD,
        /**
         * A select input field, enum and <code>WebInputField</code> compatible.
         */
        SELECT_FIELD,
        /**
         * A checkbox input field, enum and <code>WebInputField</code>
         * compatible.
         *
         * A multi boolean field.
         */
        CHECKBOX_FIELD,
        /**
         * A Single boolean input field.
         */
        BOOLEAN_FIELD,
        /**
         * A time input field.
         */
        TIME_FIELD,
        /**
         * A date input field.
         */
        DATE_FIELD,
        /**
         * Date and time input.
         */
        DATE_TIME_FIELD,
        /**
         * Ordinary text.
         */
        TEXT_FIELD;
    }

    /**
     * Gets the input id.
     *
     * @return The id of the input.
     */
    String getId();

    /**
     * Gets the input label.
     *
     * @return The label of the input.
     */
    String getLabel();

    /**
     * Get the input description.
     *
     * @return The description of the input.
     */
    String getDescription();

    /**
     * @return the inputFieldType
     */
    InputFieldType getInputFieldType();

    /**
     * Set's an options default value.
     *
     * @param asDefault The defaultValue to set.
     */
    void setDefaultValue(T asDefault);

    /**
     * Returns the default value. Defaults to empty string.
     *
     * @return The default value set by the user.
     */
    T getDefaultValue();

    /**
     * Sets the input value.
     *
     * @param userValue The value entered by the user.
     */
    void setValue(T userValue);

    /**
     * Returns the option value.
     *
     * @return The value set by the user.
     */
    T getValue();

    /**
     * Checks of the requirements are met.
     *
     * @return true when the requirements are met.
     * @throws InputValueException Throw when the requirements are not met.
     */
    boolean checkRequirements() throws InputValueException;

    /**
     * Returns if the value of the input is required.
     *
     * @return If required or not.
     */
    boolean isRequired();

    /**
     * Set the input field required or not.
     *
     * By default the value is false meaning not required.
     *
     * @param setRequired Set to true to set this field required.
     */
    void setRequired(boolean setRequired);

    /**
     * Validates the end user input.
     *
     * This only checks for input validity, not value validity. This method
     * throws the <code>WebInputInvalidValueException</code> when the input is
     * invalid and before it reaches any code requesting the input value
     *
     * @throws InputValueException When the input is invalid.
     */
    void validateInput() throws InputValueException;

}
