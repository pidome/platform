/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.components.list;

/**
 * HTML dt,dl equivalent.
 *
 * This class is useful for displaying name value pairs. It is constructed as as
 * horizontal and not below each other and without indentation.
 *
 * Currently dd is limited to be only a text string.
 *
 * @author johns
 */
public class DescriptionListItem {

    /**
     * dt (name) element.
     */
    private final String dt;

    /**
     * dd (value) element.
     */
    private Object dd;

    /**
     * Constructor.
     *
     * @param dtItem The dt item.
     * @param ddItem The dd item.
     */
    public DescriptionListItem(final String dtItem, final Object ddItem) {
        this.dt = dtItem;
        this.dd = ddItem;
    }

    /**
     * dt element.
     *
     * @return the dt
     */
    public String getDt() {
        return dt;
    }

    /**
     * dd element.
     *
     * @return the dd
     */
    public Object getDd() {
        return dd;
    }

    /**
     * Sets the dd Value.
     *
     * @param ddValue The value to set for dd.
     */
    public final void setDd(final String ddValue) {
        this.dd = ddValue;
    }

}
