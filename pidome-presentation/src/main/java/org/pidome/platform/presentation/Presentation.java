/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.pidome.platform.presentation.skeleton.PresentationSection;

/**
 * Base class for displating information on the frontend.
 *
 * @author johns
 */
public class Presentation {

    /**
     * The name of the form.
     */
    private final String title;

    /**
     * The description of the form.
     */
    private final String description;

    /**
     * List of sections in the form.
     */
    private List<PresentationSection> sections;

    /**
     * Constructor of a presentation.
     */
    public Presentation() {
        this(null, null);
    }

    /**
     * Constructor of a presentation.
     *
     * @param presentationTitle The title of the presentation.
     */
    public Presentation(final String presentationTitle) {
        this(presentationTitle, null);
    }

    /**
     * Constructor of a presentation.
     *
     * @param presentationTitle The title of the presentation.
     * @param presentationDescription The description of the presentation.
     */
    public Presentation(final String presentationTitle, final String presentationDescription) {
        this(presentationTitle, presentationDescription, new ArrayList<>());
    }

    /**
     * Constructor of a presentation.
     *
     * @param presentationTitle The title of the presentation.
     * @param presentationDescription The description of the presentation.
     * @param sections The section list to set.
     */
    public Presentation(final String presentationTitle, final String presentationDescription, final List<PresentationSection> sections) {
        this.title = presentationTitle;
        this.description = presentationDescription;
        this.sections = sections;
    }

    /**
     * @return the name of the form.
     */
    public final String getTitle() {
        return title;
    }

    /**
     * @return the description
     */
    public final String getDescription() {
        return description;
    }

    /**
     * @return the sectionList
     */
    public final List<PresentationSection> getSections() {
        return sections;
    }

    /**
     * Adds a single list item.
     *
     * @param item The item to add.
     * @return this for fluent design.
     */
    public Presentation addSection(final PresentationSection item) {
        this.sections.add(item);
        return this;
    }

    /**
     * Removes a list item.
     *
     * @param item the item to remove.
     * @return this for fluent design.
     */
    public Presentation removeSection(final PresentationSection item) {
        this.sections.remove(item);
        return this;
    }

    /**
     * Adds multiple items to the list.
     *
     * @param items The items to add.
     * @return this for fluent design.
     */
    public Presentation addSections(final PresentationSection... items) {
        this.addSections(Arrays.asList(items));
        return this;
    }

    /**
     * Adds multiple items to the list.
     *
     * @param items The items to add.
     * @return this for fluent design.
     */
    public Presentation addSections(final List<PresentationSection> items) {
        this.sections.addAll(items);
        return this;
    }

    /**
     * Replaces the items in the list with the given items.
     *
     * @param items The items to replace the list with.
     * @return this for fluent design.
     */
    public Presentation setSections(final PresentationSection... items) {
        this.setSections(Arrays.asList(items));
        return this;
    }

    /**
     * Replaces the items in the list with the given items.
     *
     * @param items The items to replace the list with.
     * @return this for fluent design.
     */
    public Presentation setSections(final List<PresentationSection> items) {
        this.clearSections();
        this.sections.addAll(items);
        return this;
    }

    /**
     * Clears the list.
     *
     * @return this for fluent design.
     */
    public Presentation clearSections() {
        this.sections.clear();
        return this;
    }

}
