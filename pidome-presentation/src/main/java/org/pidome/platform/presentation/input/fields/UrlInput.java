/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input.fields;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.net.URL;
import java.util.regex.Pattern;
import org.pidome.platform.presentation.input.InputField;
import org.pidome.platform.presentation.input.InputValueException;

/**
 * An input field expecting an URL.
 *
 * URL as described as {@link java.net.URL java.net.URL}
 *
 * @author John Sirach
 */
public final class UrlInput extends InputField<URL> {

    /**
     * Url pattern.
     */
    private static final Pattern PATTERN_URL = Pattern.compile("^(https?|ftp)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");

    /**
     * Constructor setting id, name and description.
     *
     * @param fieldId The id of the field.
     * @param fieldLabel The label of the field.
     * @param fieldDescription The description of the field.
     */
    @JsonCreator
    public UrlInput(@JsonProperty("id") final String fieldId,
            @JsonProperty("label") final String fieldLabel,
            @JsonProperty("description") final String fieldDescription) {
        super(fieldId, fieldLabel, fieldDescription, InputFieldType.URL_FIELD);
    }

    /**
     * Validate the input.
     *
     * @throws InputValueException When validation fails.
     */
    @Override
    public void validateInput() throws InputValueException {
        /// We do not allow specific urls which are accepted by default within the JDK, for example we do not allow file and jar etc.
        if (this.getValue() != null && !PATTERN_URL.matcher(this.getValue().toExternalForm()).matches()) {
            throw new InputValueException(this, "Given url [" + this.getValue().toExternalForm() + "] is incorrect");
        }
    }

}
