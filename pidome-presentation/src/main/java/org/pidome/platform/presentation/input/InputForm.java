/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The input form containing sections with input fields.
 *
 * @author John Sirach
 */
@SuppressWarnings("PMD.AbstractClassWithoutAbstractMethod")
public abstract class InputForm {

    /**
     * List of sections in the form.
     */
    private List<InputSection> sections;

    /**
     * The name of the form.
     */
    private final String name;

    /**
     * The description of the form.
     */
    private final String description;

    /**
     * Constructor of a web form.
     */
    public InputForm() {
        this(null, null);
    }

    /**
     * Constructor of a web form.
     *
     * @param formName The name of the form.
     */
    public InputForm(final String formName) {
        this(formName, null);
    }

    /**
     * Constructor of a web form.
     *
     * @param formName The name of the form.
     * @param formDescription The description of the form.
     */
    public InputForm(final String formName, final String formDescription) {
        this(formName, formDescription, new ArrayList<>());
    }

    /**
     * Constructor of a web form.
     *
     * @param formName The name of the form.
     * @param formDescription The description of the form.
     * @param sections The section list to set.
     */
    @JsonCreator
    public InputForm(@JsonProperty("name") final String formName, @JsonProperty("description") final String formDescription, @JsonProperty("sections") final List<InputSection> sections) {
        this.name = formName;
        this.description = formDescription;
        this.sections = sections;
    }

    /**
     * @return the name of the form.
     */
    public final String getName() {
        return name;
    }

    /**
     * @return the description
     */
    public final String getDescription() {
        return description;
    }

    /**
     * @return the sectionList
     */
    public final List<InputSection> getSections() {
        return sections;
    }

    /**
     * @param sections the sectionList to set
     */
    public final void setSections(final List<InputSection> sections) {
        this.sections = sections;
    }

    /**
     * @param section the sectionList to set.
     * @return This object.
     */
    @JsonIgnore
    public final InputForm addSection(final InputSection section) {
        this.sections.add(section);
        return this;
    }

    /**
     * @param sectionsList the sectionList to set.
     * @return This object.
     */
    @JsonIgnore
    public final InputForm addSections(final List<InputSection> sectionsList) {
        this.sections.addAll(sectionsList);
        return this;
    }

    /**
     * @param sectionsList the sectionList to set.
     * @return This object.
     */
    @JsonIgnore
    public final InputForm addSections(final InputSection... sectionsList) {
        return addSections(Arrays.asList(sectionsList));
    }

    /**
     * @param section the sectionList to set.
     * @return This object.
     */
    @JsonIgnore
    public final InputForm removeSection(final InputSection section) {
        this.sections.remove(section);
        return this;
    }

    /**
     * @param sectionsList the sectionList to set.
     * @return This object.
     */
    @JsonIgnore
    public final InputForm removeSections(final List<InputSection> sectionsList) {
        this.sections.removeAll(sectionsList);
        return this;
    }

    /**
     * @param sectionsList the sectionList to set.
     * @return This object.
     */
    @JsonIgnore
    public final InputForm removeSections(final InputSection... sectionsList) {
        return removeSections(Arrays.asList(sectionsList));
    }

    /**
     * Performs a check on all required fields which are marked required.
     *
     * @throws InputFormException thrown when there are errors in the input
     * field.
     */
    @JsonIgnore
    public final void checkRequiredFields() throws InputFormException {
        final InputFieldRequirementsHandler errorHandler = new InputFieldRequirementsHandler();
        if (sections != null && !sections.isEmpty()) {
            sections.stream().forEach(section -> {
                section.getElements().stream().forEach(inputField -> errorHandler.checkForRequirements(inputField));
            });
        }
        if (errorHandler.hasErrors()) {
            throw new InputFormException(errorHandler);
        }
    }

}
