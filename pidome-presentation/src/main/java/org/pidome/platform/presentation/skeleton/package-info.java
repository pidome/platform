/**
 * Skeletons to help construct for the front-end.
 * <p>
 * Classes providing skeleton structure setup for front-end.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.platform.presentation.skeleton;
