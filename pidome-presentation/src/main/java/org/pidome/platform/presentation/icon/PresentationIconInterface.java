/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.icon;

/**
 * Presentation class for icons.
 *
 * @author John Sirach
 */
public interface PresentationIconInterface {

    /**
     * @return the iconType
     */
    IconType getIconType();

    /**
     * @param iconType the iconType to set
     */
    void setIconType(IconType iconType);

    /**
     * @return the icon
     */
    String getIcon();

    /**
     * @param icon the icon to set
     */
    void setIcon(String icon);

}
