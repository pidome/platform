/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.components.list;

/**
 * Presentation component types.
 *
 * @author johns The type of components available in generic presentations.
 */
public enum PresentationComponent {

    /**
     * The HTML dl description list.
     */
    DESCRIPTION_LIST;

}
