/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.icon;

/**
 * To display an icon.
 *
 * @author John Sirch
 */
public final class PresentationIcon implements PresentationIconInterface {

    /**
     * The icon type.
     */
    private IconType iconType;

    /**
     * The icon.
     */
    private String icon;

    /**
     * The icon type.
     *
     * @return The icon type.
     */
    @Override
    public IconType getIconType() {
        return iconType;
    }

    /**
     * Sets the icon type.
     *
     * @param type The type of icon.
     */
    @Override
    public void setIconType(final IconType type) {
        this.iconType = type;
    }

    /**
     * The icon available in type.
     *
     * @return The icon.
     */
    @Override
    public String getIcon() {
        return this.icon;
    }

    /**
     * Sets the icon available in icon type.
     *
     * @param presentationIcon the icon.
     */
    @Override
    public void setIcon(final String presentationIcon) {
        this.icon = presentationIcon;
    }

}
