/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.skeleton;

import org.pidome.platform.presentation.components.list.PresentationComponent;

/**
 * A basic section.
 *
 * @author johns
 */
@SuppressWarnings("PMD.AbstractClassWithoutAbstractMethod")
public abstract class PresentationSection {

    /**
     * The name of the section.
     */
    private final String name;

    /**
     * The description of the section.
     */
    private final String description;

    /**
     * Presentation component type.
     */
    private final PresentationComponent type;

    /**
     * Constructor.
     *
     * @param type The component type.
     */
    public PresentationSection(final PresentationComponent type) {
        this(type, null, null);
    }

    /**
     * Constructor.
     *
     * @param type The component type.
     * @param sectionName The name of the section.
     */
    public PresentationSection(final PresentationComponent type, final String sectionName) {
        this(type, sectionName, null);
    }

    /**
     * Constructor.
     *
     * @param type The component type.
     * @param sectionName The name of the section.
     * @param sectionDescription The description of the section.
     */
    public PresentationSection(final PresentationComponent type, final String sectionName, final String sectionDescription) {
        this.name = sectionName;
        this.description = sectionDescription;
        this.type = type;
    }

    /**
     * Returns the component type.
     *
     * @return The type of component.
     */
    public final PresentationComponent getType() {
        return this.type;
    }

    /**
     * Returns the name of the section.
     *
     * @return Section name.
     */
    public final String getName() {
        return this.name;
    }

    /**
     * Returns the description.
     *
     * @return the description.
     */
    public final String getDescription() {
        return this.description;
    }

}
