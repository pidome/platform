/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input.fields;

import com.fasterxml.jackson.databind.ObjectMapper;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.Test;
import org.pidome.platform.presentation.input.InputElement.InputFieldType;
import org.pidome.tools.utilities.Serialization;

/**
 * Test boolean input.
 *
 * @author johns
 */
@SuppressWarnings("CPD-START")
public class BooleanInputTest {

    private static BooleanInput instance;

    private static final String ID = "boolean-input";
    private static final String LABEL = "boolean-label";
    private static final String DESC = "boolean-description";

    private void createDefaultInstance() {
        instance = new BooleanInput(ID, LABEL, DESC);
        instance.setRequired(true);
    }

    /**
     * Test of validateInput method, of class BooleanInput.
     */
    @Test
    public void testDefaultValue() throws Exception {
        createDefaultInstance();
        instance.setDefaultValue(Boolean.TRUE);
        assertThat(null, is(equalTo(instance.getValue())));
        assertThat(Boolean.TRUE, is(equalTo(instance.getDefaultValue())));
        instance.setValue(Boolean.FALSE);
        assertThat(instance.getValue(), not(equalTo(instance.getDefaultValue())));
    }

    /**
     * Test of validateInput method, of class BooleanInput.
     */
    @Test
    public void testGetType() throws Exception {
        createDefaultInstance();
        assertThat(InputFieldType.BOOLEAN_FIELD, is(equalTo(instance.getInputFieldType())));
    }

    /**
     * Test of validateInput method, of class BooleanInput.
     */
    @Test
    public void testGetBaseParameters() throws Exception {
        createDefaultInstance();
        assertThat(ID, is(equalTo(instance.getId())));
        assertThat(LABEL, is(equalTo(instance.getLabel())));
        assertThat(DESC, is(equalTo(instance.getDescription())));
    }

    /**
     * Test of validateInput method, of class BooleanInput.
     */
    @Test
    public void testValidateInput() throws Exception {
        createDefaultInstance();
        instance.setValue(Boolean.TRUE);
        instance.validateInput();
    }

    @Test
    public void testSerialization() throws Exception {
        createDefaultInstance();
        instance.setDefaultValue(Boolean.FALSE);
        instance.setValue(Boolean.TRUE);
        String expectedResult = "{\"@class\":\"org.pidome.platform.presentation.input.fields.BooleanInput\",\"id\":\"boolean-input\",\"label\":\"boolean-label\",\"description\":\"boolean-description\",\"inputFieldType\":\"BOOLEAN_FIELD\",\"required\":true,\"defaultValue\":false,\"value\":true}";
        ObjectMapper mapper = Serialization.getDefaultObjectMapper();
        assertThat(expectedResult, is(equalTo(mapper.writeValueAsString(instance))));
    }

}
