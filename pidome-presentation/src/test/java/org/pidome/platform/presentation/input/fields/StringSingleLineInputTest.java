/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input.fields;

import com.fasterxml.jackson.databind.ObjectMapper;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.Test;
import org.pidome.platform.presentation.input.InputElement.InputFieldType;
import org.pidome.platform.presentation.input.InputValueException;
import org.pidome.tools.utilities.Serialization;

/**
 *
 * @author johns
 */
@SuppressWarnings("CPD-START")
public class StringSingleLineInputTest {

    private static StringSingleLineInput instance;

    private static final String ID = "string-single-input";
    private static final String LABEL = "string-single-label";
    private static final String DESC = "string-single-description";

    private void createDefaultInstance() {
        instance = new StringSingleLineInput(ID, LABEL, DESC);
        instance.setRequired(true);
    }

    /**
     * Test of validateInput method, of class BooleanInput.
     */
    @Test
    public void testDefaultValue() throws Exception {
        createDefaultInstance();
        instance.setDefaultValue("default-value");
        assertThat(null, is(equalTo(instance.getValue())));
        assertThat("default-value", is(equalTo(instance.getDefaultValue())));
        instance.setValue("new-value");
        assertThat(instance.getValue(), not(equalTo(instance.getDefaultValue())));
    }

    /**
     * Test of validateInput method, of class BooleanInput.
     */
    @Test
    public void testGetType() throws Exception {
        createDefaultInstance();
        assertThat(InputFieldType.STRING_FIELD, is(equalTo(instance.getInputFieldType())));
    }

    /**
     * Test of validateInput method, of class BooleanInput.
     */
    @Test
    public void testGetBaseParameters() throws Exception {
        createDefaultInstance();
        assertThat(ID, is(equalTo(instance.getId())));
        assertThat(LABEL, is(equalTo(instance.getLabel())));
        assertThat(DESC, is(equalTo(instance.getDescription())));
    }

    /**
     * Test of validateInput method, of class BooleanInput.
     */
    @Test
    public void testValidateInput() throws Exception {
        createDefaultInstance();
        instance.setValue("single-line");
        instance.validateInput();

        createDefaultInstance();
        instance.setValue("multi\nline");
        try {
            instance.validateInput();
            fail("InputValueException should have been thrown");
        } catch (Exception ex) {
            assertThat(ex, is(instanceOf(InputValueException.class)));
        }
    }

    @Test
    public void testSerialization() throws Exception {
        createDefaultInstance();
        instance.setDefaultValue("default-value");
        instance.setValue("new-value");
        String expectedResult = "{\"@class\":\"org.pidome.platform.presentation.input.fields.StringSingleLineInput\",\"id\":\"string-single-input\",\"label\":\"string-single-label\",\"description\":\"string-single-description\",\"inputFieldType\":\"STRING_FIELD\",\"required\":true,\"defaultValue\":\"default-value\",\"value\":\"new-value\"}";
        ObjectMapper mapper = Serialization.getDefaultObjectMapper();
        assertThat(expectedResult, is(equalTo(mapper.writeValueAsString(instance))));
    }

}
