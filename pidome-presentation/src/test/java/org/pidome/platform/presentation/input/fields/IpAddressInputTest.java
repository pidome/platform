/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input.fields;

import com.fasterxml.jackson.databind.ObjectMapper;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;
import org.pidome.platform.presentation.input.InputValueException;
import org.pidome.tools.utilities.Serialization;

/**
 *
 * @author johns
 */
public class IpAddressInputTest {

    private static IpAddressInput instance;

    private static final String ID = "ipaddress-input";
    private static final String LABEL = "ipaddress-single-label";
    private static final String DESC = "ipaddress-single-description";

    private void createDefaultInstance() {
        instance = new IpAddressInput(ID, LABEL, DESC);
        instance.setRequired(true);
    }

    /**
     * Test of validateInput method, of class IpAddressInput.
     */
    @Test
    public void testValidateInputCorrect() throws Exception {
        createDefaultInstance();
        instance.setValue("127.0.0.1");
        instance.setRequired(true);
        instance.validateInput();
    }

    /**
     * Test of validateInput method, of class IpAddressInput.
     */
    @Test
    public void testValidateInputInCorrect() throws Exception {
        createDefaultInstance();
        instance.setValue("123.456.7.890");
        instance.setRequired(true);
        assertThrows(InputValueException.class, () -> {
            instance.validateInput();
        });

    }

    @Test
    public void testSerialization() throws Exception {
        createDefaultInstance();
        instance.setDefaultValue("127.0.0.1");
        instance.setValue("10.0.0.1");
        String expectedResult = "{\"@class\":\"org.pidome.platform.presentation.input.fields.IpAddressInput\",\"id\":\"ipaddress-input\",\"label\":\"ipaddress-single-label\",\"description\":\"ipaddress-single-description\",\"inputFieldType\":\"IP_ADDRESS_FIELD\",\"required\":true,\"defaultValue\":\"127.0.0.1\",\"value\":\"10.0.0.1\"}";
        ObjectMapper mapper = Serialization.getDefaultObjectMapper();
        System.err.println(mapper.writeValueAsString(instance));
        assertThat(expectedResult, is(equalTo(mapper.writeValueAsString(instance))));
    }

}
