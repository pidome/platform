/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input.fields;

import com.fasterxml.jackson.databind.ObjectMapper;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.Test;
import org.pidome.platform.presentation.input.InputElement.InputFieldType;
import org.pidome.tools.utilities.Serialization;

/**
 * Check the enum input box.
 *
 * @author johns
 */
@SuppressWarnings("CPD-START")
public class CheckboxEnumInputTest {

    private static CheckboxEnumInput<EnumNumberHelper> instance;

    private static final String ID = "enum-check-input";
    private static final String LABEL = "enum-check-label";
    private static final String DESC = "enum-check-description";

    private void createDefaultInstance() {
        instance = new CheckboxEnumInput<EnumNumberHelper>(ID, LABEL, DESC);
        instance.setListValues(EnumNumberHelper.values());
        instance.setRequired(true);
    }

    /**
     * Test of validateInput method, of class BooleanInput.
     */
    @Test
    public void testDefaultValue() throws Exception {
        createDefaultInstance();
        instance.setDefaultValue(EnumNumberHelper.ENUM_ONE);
        assertThat(null, is(equalTo(instance.getValue())));
        assertThat(EnumNumberHelper.ENUM_ONE, is(equalTo(instance.getDefaultValue())));
        instance.setValue(EnumNumberHelper.ENUM_TWO);
        assertThat(instance.getValue(), not(equalTo(instance.getDefaultValue())));
    }

    /**
     * Test of validateInput method, of class BooleanInput.
     */
    @Test
    public void testGetType() throws Exception {
        createDefaultInstance();
        assertThat(InputFieldType.CHECKBOX_FIELD, is(equalTo(instance.getInputFieldType())));
    }

    /**
     * Test of validateInput method, of class BooleanInput.
     */
    @Test
    public void testGetBaseParameters() throws Exception {
        createDefaultInstance();
        assertThat(ID, is(equalTo(instance.getId())));
        assertThat(LABEL, is(equalTo(instance.getLabel())));
        assertThat(DESC, is(equalTo(instance.getDescription())));
    }

    /**
     * Test of validateInput method, of class BooleanInput.
     */
    @Test
    public void testValidateInput() throws Exception {
        createDefaultInstance();
        instance.setValue(EnumNumberHelper.ENUM_ONE);
        instance.validateInput();
    }

    @Test
    public void testSerialization() throws Exception {
        createDefaultInstance();
        instance.setDefaultValue(EnumNumberHelper.ENUM_ONE);
        instance.setValue(EnumNumberHelper.ENUM_TWO);
        String expectedResult1 = "{\"@class\":\"org.pidome.platform.presentation.input.fields.CheckboxEnumInput\",\"id\":\"enum-check-input\",\"label\":\"enum-check-label\",\"description\":\"enum-check-description\",\"inputFieldType\":\"CHECKBOX_FIELD\",\"required\":true,\"defaultValue\":{\"@class\":\"org.pidome.platform.presentation.input.fields.EnumNumberHelper\",\"label\":\"One\",\"value\":1,\"name\":\"ENUM_ONE\",\"path\":\"org.pidome.platform.presentation.input.fields.EnumNumberHelper\"},\"value\":{\"@class\":\"org.pidome.platform.presentation.input.fields.EnumNumberHelper\",\"label\":\"Two\",\"value\":2,\"name\":\"ENUM_TWO\",\"path\":\"org.pidome.platform.presentation.input.fields.EnumNumberHelper\"},\"elements\":[{\"@class\":\"org.pidome.platform.presentation.input.fields.EnumNumberHelper\",\"label\":\"One\",\"value\":1,\"name\":\"ENUM_ONE\",\"path\":\"org.pidome.platform.presentation.input.fields.EnumNumberHelper\"},{\"@class\":\"org.pidome.platform.presentation.input.fields.EnumNumberHelper\",\"label\":\"Two\",\"value\":2,\"name\":\"ENUM_TWO\",\"path\":\"org.pidome.platform.presentation.input.fields.EnumNumberHelper\"}]}";
        String expectedResult2 = "{\"@class\":\"org.pidome.platform.presentation.input.fields.CheckboxEnumInput\",\"id\":\"enum-check-input\",\"label\":\"enum-check-label\",\"description\":\"enum-check-description\",\"inputFieldType\":\"CHECKBOX_FIELD\",\"required\":true,\"defaultValue\":{\"@class\":\"org.pidome.platform.presentation.input.fields.EnumNumberHelper\",\"label\":\"One\",\"value\":1,\"name\":\"ENUM_ONE\",\"path\":\"org.pidome.platform.presentation.input.fields.EnumNumberHelper\"},\"value\":{\"@class\":\"org.pidome.platform.presentation.input.fields.EnumNumberHelper\",\"label\":\"Two\",\"value\":2,\"name\":\"ENUM_TWO\",\"path\":\"org.pidome.platform.presentation.input.fields.EnumNumberHelper\"},\"elements\":[{\"@class\":\"org.pidome.platform.presentation.input.fields.EnumNumberHelper\",\"label\":\"Two\",\"value\":2,\"name\":\"ENUM_TWO\",\"path\":\"org.pidome.platform.presentation.input.fields.EnumNumberHelper\"},{\"@class\":\"org.pidome.platform.presentation.input.fields.EnumNumberHelper\",\"label\":\"One\",\"value\":1,\"name\":\"ENUM_ONE\",\"path\":\"org.pidome.platform.presentation.input.fields.EnumNumberHelper\"}]}";
        ObjectMapper mapper = Serialization.getDefaultObjectMapper();
        assertThat(mapper.writeValueAsString(instance), anyOf(equalTo(expectedResult1), equalTo(expectedResult2)));
    }

}
