/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input.fields;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.URL;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.Test;
import org.pidome.platform.presentation.input.InputElement.InputFieldType;
import org.pidome.platform.presentation.input.InputValueException;
import org.pidome.tools.utilities.Serialization;

/**
 *
 * @author johns
 */
@SuppressWarnings("CPD-START")
public class UrlInputTest {

    private static UrlInput instance;

    private static final String ID = "url-input";
    private static final String LABEL = "url-label";
    private static final String DESC = "url-description";

    private void createDefaultInstance() {
        instance = new UrlInput(ID, LABEL, DESC);
        instance.setRequired(true);
    }

    /**
     * Test of validateInput method, of class BooleanInput.
     */
    @Test
    public void testDefaultValue() throws Exception {
        createDefaultInstance();
        instance.setDefaultValue(new URL("http://pidome.org"));
        assertThat(null, is(equalTo(instance.getValue())));
        assertThat("http://pidome.org", is(equalTo(instance.getDefaultValue().toExternalForm())));
        instance.setValue(new URL("https://pidome.org"));
        assertThat(instance.getValue(), not(equalTo(instance.getDefaultValue())));
    }

    /**
     * Test of validateInput method, of class BooleanInput.
     */
    @Test
    public void testGetType() throws Exception {
        createDefaultInstance();
        assertThat(InputFieldType.URL_FIELD, is(equalTo(instance.getInputFieldType())));
    }

    /**
     * Test of validateInput method, of class BooleanInput.
     */
    @Test
    public void testGetBaseParameters() throws Exception {
        createDefaultInstance();
        assertThat(ID, is(equalTo(instance.getId())));
        assertThat(LABEL, is(equalTo(instance.getLabel())));
        assertThat(DESC, is(equalTo(instance.getDescription())));
    }

    /**
     * Test of validateInput method, of class BooleanInput.
     */
    @Test
    public void testValidateInput() throws Exception {
        createDefaultInstance();
        instance.setValue(new URL("https://pidome.org"));
        instance.validateInput();

        createDefaultInstance();
        instance.setValue(new URL("file://pidome.org"));
        try {
            instance.validateInput();
            fail("InputValueException should have been thrown");
        } catch (Exception ex) {
            assertThat(ex, is(instanceOf(InputValueException.class)));
            assertThat(((InputValueException) ex).getLabel(), is(equalTo(LABEL)));
        }

        createDefaultInstance();
        instance.setValue(new URL("ftp://user:withpass@example.org/"));
        instance.validateInput();

        createDefaultInstance();
        instance.setValue(new URL("https://pidome.org/search/?q=tree+-swingaround&z=yadadada&d=a-date-20190101-at-13:34&blah=0#targetanchor"));
        instance.validateInput();

    }

    @Test
    public void testSerialization() throws Exception {
        createDefaultInstance();
        instance.setDefaultValue(new URL("https://www.pidome.org"));
        instance.setValue(new URL("https://docs.pidome.org"));
        String expectedResult = "{\"@class\":\"org.pidome.platform.presentation.input.fields.UrlInput\",\"id\":\"url-input\",\"label\":\"url-label\",\"description\":\"url-description\",\"inputFieldType\":\"URL_FIELD\",\"required\":true,\"defaultValue\":\"https://www.pidome.org\",\"value\":\"https://docs.pidome.org\"}";
        ObjectMapper mapper = Serialization.getDefaultObjectMapper();
        assertThat(expectedResult, is(equalTo(mapper.writeValueAsString(instance))));
    }

}
