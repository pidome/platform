/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input.fields;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.Test;
import org.pidome.platform.presentation.input.InputElement;

/**
 *
 * @author johns
 */
@SuppressWarnings("CPD-START")
public class DisplayInputTest {

    private static final String ID = "string-display-input-input";
    private static final String LABEL = "This is the label to show";

    private static final String SHOW_VALUE = "This is the value to show";

    /**
     * Test of getContent method, of class DisplayInput.
     */
    @Test
    public void testGetContent() {
        DisplayInput instance = new DisplayInput(ID, LABEL);
        instance.setValue(SHOW_VALUE);

        assertThat(ID, is(equalTo(instance.getId())));
        assertThat(LABEL, is(equalTo(instance.getLabel())));
        assertThat(SHOW_VALUE, is(equalTo(instance.getValue())));

        assertThat(InputElement.InputFieldType.TEXT_FIELD, is(equalTo(instance.getInputFieldType())));

    }

}
