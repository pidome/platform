/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input.fields;

import com.fasterxml.jackson.databind.ObjectMapper;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.Test;
import org.pidome.platform.presentation.input.InputElement.InputFieldType;
import org.pidome.tools.utilities.Serialization;

/**
 *
 * @author johns
 */
@SuppressWarnings("CPD-START")
public class IntegerInputTest {

    private static IntegerInput instance;

    private static final String ID = "integer-input";
    private static final String LABEL = "integer-label";
    private static final String DESC = "integer-description";

    private void createDefaultInstance() {
        instance = new IntegerInput(ID, LABEL, DESC);
        instance.setRequired(true);
    }

    /**
     * Test of validateInput method, of class BooleanInput.
     */
    @Test
    public void testDefaultValue() throws Exception {
        createDefaultInstance();
        instance.setDefaultValue(1);
        assertThat(null, is(equalTo(instance.getValue())));
        assertThat(1, is(equalTo(instance.getDefaultValue())));
        instance.setValue(2);
        assertThat(instance.getValue(), not(equalTo(instance.getDefaultValue())));
    }

    /**
     * Test of validateInput method, of class BooleanInput.
     */
    @Test
    public void testGetType() throws Exception {
        createDefaultInstance();
        assertThat(InputFieldType.INT_FIELD, is(equalTo(instance.getInputFieldType())));
    }

    /**
     * Test of validateInput method, of class BooleanInput.
     */
    @Test
    public void testGetBaseParameters() throws Exception {
        createDefaultInstance();
        assertThat(ID, is(equalTo(instance.getId())));
        assertThat(LABEL, is(equalTo(instance.getLabel())));
        assertThat(DESC, is(equalTo(instance.getDescription())));
    }

    /**
     * Test of validateInput method, of class BooleanInput.
     */
    @Test
    public void testValidateInput() throws Exception {
        createDefaultInstance();
        instance.setValue(Integer.MAX_VALUE);
        instance.validateInput();
    }

    @Test
    public void testSerialization() throws Exception {
        createDefaultInstance();
        instance.setDefaultValue(1);
        instance.setValue(2);
        String expectedResult = "{\"@class\":\"org.pidome.platform.presentation.input.fields.IntegerInput\",\"id\":\"integer-input\",\"label\":\"integer-label\",\"description\":\"integer-description\",\"inputFieldType\":\"INT_FIELD\",\"required\":true,\"defaultValue\":1,\"value\":2}";
        ObjectMapper mapper = Serialization.getDefaultObjectMapper();
        assertThat(expectedResult, is(equalTo(mapper.writeValueAsString(instance))));
    }

}
