/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input.fields;

import org.pidome.platform.presentation.input.InputEnum;

/**
 * Helper for enum values in tests.
 *
 * @author johns
 */
public enum EnumNumberHelper implements InputEnum<Integer> {

    ENUM_ONE("One", 1),
    ENUM_TWO("Two", 2);

    /**
     * The description of this enum.
     */
    private final String label;

    /**
     * The stop bits asosciated with this enum.
     */
    private final int value;

    /**
     * Enum constructor.
     *
     * @param enumLabel The label.
     * @param p The value.
     */
    EnumNumberHelper(final String enumLabel, final int p) {
        this.label = enumLabel;
        this.value = p;
    }

    @Override
    public String getLabel() {
        return this.label;
    }

    @Override
    public Integer getValue() {
        return this.value;
    }

}
