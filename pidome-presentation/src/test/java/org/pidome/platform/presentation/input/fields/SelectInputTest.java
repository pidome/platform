/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input.fields;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.Test;
import org.pidome.platform.presentation.input.InputElement.InputFieldType;
import org.pidome.platform.presentation.input.InputValueException;
import org.pidome.tools.utilities.Serialization;

/**
 *
 * @author johns
 */
@SuppressWarnings("CPD-START")
public class SelectInputTest {

    private static SelectInput<String> instance;

    private static final String ID = "string-select-input";
    private static final String LABEL = "string-select-label";
    private static final String DESC = "string-select-description";

    private static final List<String> items = List.of("One", "Two");

    private void createDefaultInstance() {
        instance = new SelectInput<String>(ID, LABEL, DESC);
        instance.setListValues(items);
        instance.setRequired(true);
    }

    /**
     * Test of validateInput method, of class BooleanInput.
     */
    @Test
    public void testDefaultValue() throws Exception {
        createDefaultInstance();
        instance.setDefaultValue("One");
        assertThat(null, is(equalTo(instance.getValue())));
        assertThat("One", is(equalTo(instance.getDefaultValue())));
        instance.setValue("Two");
        assertThat(instance.getValue(), not(equalTo(instance.getDefaultValue())));
    }

    /**
     * Test of validateInput method, of class BooleanInput.
     */
    @Test
    public void testGetType() throws Exception {
        createDefaultInstance();
        assertThat(InputFieldType.SELECT_FIELD, is(equalTo(instance.getInputFieldType())));
    }

    /**
     * Test of validateInput method, of class BooleanInput.
     */
    @Test
    public void testGetBaseParameters() throws Exception {
        createDefaultInstance();
        assertThat(ID, is(equalTo(instance.getId())));
        assertThat(LABEL, is(equalTo(instance.getLabel())));
        assertThat(DESC, is(equalTo(instance.getDescription())));
    }

    /**
     * Test of validateInput method, of class BooleanInput.
     */
    @Test
    public void testValidateInput() throws Exception {
        createDefaultInstance();
        instance.setValue("One");
        instance.validateInput();
        instance.setValue("Three");
        try {
            instance.validateInput();
            fail("InputValueException should have been thrown");
        } catch (Exception ex) {
            assertThat(ex, is(instanceOf(InputValueException.class)));
        }
    }

    @Test
    public void testSerialization() throws Exception {
        createDefaultInstance();
        instance.setDefaultValue("One");
        instance.setValue("Two");
        String expectedResult1 = "{\"@class\":\"org.pidome.platform.presentation.input.fields.SelectInput\",\"id\":\"string-select-input\",\"label\":\"string-select-label\",\"description\":\"string-select-description\",\"inputFieldType\":\"SELECT_FIELD\",\"required\":true,\"defaultValue\":\"One\",\"value\":\"Two\",\"elements\":[\"One\",\"Two\"]}";
        String expectedResult2 = "{\"@class\":\"org.pidome.platform.presentation.input.fields.SelectInput\",\"id\":\"string-select-input\",\"label\":\"string-select-label\",\"description\":\"string-select-description\",\"inputFieldType\":\"SELECT_FIELD\",\"required\":true,\"defaultValue\":\"One\",\"value\":\"Two\",\"elements\":[\"Two\",\"One\"]}";
        ObjectMapper mapper = Serialization.getDefaultObjectMapper();
        System.out.println(mapper.writeValueAsString(instance));
        assertThat(mapper.writeValueAsString(instance), anyOf(equalTo(expectedResult1), equalTo(expectedResult2)));
    }

}
