/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.presentation.input.fields;

import com.fasterxml.jackson.databind.ObjectMapper;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.Test;
import org.pidome.platform.presentation.input.InputElement.InputFieldType;
import org.pidome.tools.utilities.Serialization;

/**
 *
 * @author johns
 */
@SuppressWarnings("CPD-START")
public class StringMultiLineInputTest {

    private static StringMultiLineInput instance;

    private static final String ID = "string-multiline-input";
    private static final String LABEL = "string-multiline-label";
    private static final String DESC = "string-multiline-description";

    private void createDefaultInstance() {
        instance = new StringMultiLineInput(ID, LABEL, DESC);
        instance.setRequired(true);
    }

    /**
     * Test of validateInput method, of class BooleanInput.
     */
    @Test
    public void testDefaultValue() throws Exception {
        createDefaultInstance();
        instance.setDefaultValue("default-value");
        assertThat(null, is(equalTo(instance.getValue())));
        assertThat("default-value", is(equalTo(instance.getDefaultValue())));
        instance.setValue("new-value");
        assertThat(instance.getValue(), not(equalTo(instance.getDefaultValue())));
    }

    /**
     * Test of validateInput method, of class BooleanInput.
     */
    @Test
    public void testGetType() throws Exception {
        createDefaultInstance();
        assertThat(InputFieldType.STRING_MULTILINE_FIELD, is(equalTo(instance.getInputFieldType())));
    }

    /**
     * Test of validateInput method, of class BooleanInput.
     */
    @Test
    public void testGetBaseParameters() throws Exception {
        createDefaultInstance();
        assertThat(ID, is(equalTo(instance.getId())));
        assertThat(LABEL, is(equalTo(instance.getLabel())));
        assertThat(DESC, is(equalTo(instance.getDescription())));
    }

    /**
     * Test of validateInput method, of class BooleanInput.
     */
    @Test
    public void testValidateInput() throws Exception {
        createDefaultInstance();
        instance.setValue("single-line");
        instance.validateInput();

        createDefaultInstance();
        instance.setValue("multi\nline");
        instance.validateInput();
    }

    @Test
    public void testSerialization() throws Exception {
        createDefaultInstance();
        instance.setDefaultValue("default-value");
        instance.setValue("new-value");
        String expectedResult = "{\"@class\":\"org.pidome.platform.presentation.input.fields.StringMultiLineInput\",\"id\":\"string-multiline-input\",\"label\":\"string-multiline-label\",\"description\":\"string-multiline-description\",\"inputFieldType\":\"STRING_MULTILINE_FIELD\",\"required\":true,\"defaultValue\":\"default-value\",\"value\":\"new-value\"}";
        ObjectMapper mapper = Serialization.getDefaultObjectMapper();
        assertThat(expectedResult, is(equalTo(mapper.writeValueAsString(instance))));
    }

}
