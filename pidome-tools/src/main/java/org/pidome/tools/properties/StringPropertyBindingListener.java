package org.pidome.tools.properties;

/**
 * The String property binding.
 *
 * @author John
 */
public interface StringPropertyBindingListener extends ObjectPropertyBindingListener<String> {

}
