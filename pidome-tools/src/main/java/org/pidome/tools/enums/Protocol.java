/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.enums;

/**
 * Collection of protocols.
 *
 * @author johns
 */
public enum Protocol {
    /**
     * TCP.
     */
    TCP,
    /**
     * UDP.
     */
    UDP,
    /**
     * HTTP.
     */
    HTTP,
    /**
     * HTTPS.
     */
    HTTPS,
    /**
     * Local socket.
     */
    SOCKET,
    /**
     * A file.
     */
    FILE,

}
