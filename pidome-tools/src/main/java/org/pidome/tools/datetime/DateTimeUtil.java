/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.datetime;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Date time utility class.
 *
 * @author johns
 */
public class DateTimeUtil {

    /**
     * Default constructor.
     *
     * @throws UnsupportedOperationException Class should not be instantiated.
     */
    protected DateTimeUtil() {
        throw new UnsupportedOperationException();
    }

    /**
     * Calendar used in the system.
     */
    private static GregorianCalendar cal;

    /**
     * The current time zone.
     */
    private static TimeZone timeZone = TimeZone.getDefault();

    /**
     * Date format for a long date.
     */
    public static final String LONG_DATE_TIME_FORMAT = "dd-MM-yyyy HH:mm:ss";
    /**
     * Military date format.
     */
    public static final String MILITARY_FORMAT = "HH:mm:ss";
    /**
     * Default format.
     */
    public static final String DEFAULT_FORMAT = "hh:mm:ss a";

    /**
     * Service for updating time every minute.
     */
    private static final ScheduledExecutorService TIME_UPDATE_SERVICE = Executors.newSingleThreadScheduledExecutor();

    /**
     * Initialize the scheduled thread executor to update every minute.
     */
    static {
        TIME_UPDATE_SERVICE.scheduleAtFixedRate(updateTime(), 0, 1, TimeUnit.MINUTES);
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                TIME_UPDATE_SERVICE.shutdownNow();
            }
        });
    }

    /**
     * Sets the new status.
     *
     * @return The runnable to update every minute.
     */
    private static Runnable updateTime() {
        return () -> {
            timeZone = TimeZone.getDefault();
            cal = new GregorianCalendar(timeZone);
        };
    }

    /**
     * Returns a long date time representation.
     *
     * @return long representation of the current time.
     */
    public static String getLongDateTime() {
        return getLongDatetimeFormat().format(cal.getTime());
    }

    /**
     * Returns 24 hours military time.
     *
     * @return Military notation of the current time.
     */
    public static String getMilitaryTime() {
        return getMilitaryDateTimeFormat().format(cal.getTime());
    }

    /**
     * Returns default time format.
     *
     * @return The default dat time formatted of the current time.
     */
    public static String getDefaultDateTime() {
        return getDefaultDateTimeFormat().format(cal.getTime());
    }

    /**
     * Returns a date and time format in a long notation.
     *
     * @return Date time formatter
     */
    private static SimpleDateFormat getLongDatetimeFormat() {
        return new SimpleDateFormat(LONG_DATE_TIME_FORMAT, Locale.getDefault(Locale.Category.FORMAT));
    }

    /**
     * Returns a date and time format in a military notation.
     *
     * @return Date time formatter
     */
    private static SimpleDateFormat getMilitaryDateTimeFormat() {
        return new SimpleDateFormat(MILITARY_FORMAT, Locale.getDefault(Locale.Category.FORMAT));
    }

    /**
     * Returns a date and time format in a default notation.
     *
     * @return Date time formatter
     */
    private static SimpleDateFormat getDefaultDateTimeFormat() {
        return new SimpleDateFormat(DEFAULT_FORMAT, Locale.getDefault(Locale.Category.FORMAT));
    }

}
