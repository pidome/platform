/**
 * Package for date time utilities.
 * <p>
 * Provides date and time utilities.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.tools.datetime;
