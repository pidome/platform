/**
 * Utility comparators.
 * <p>
 * Collection of non default reusable comparators.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.tools.comparators;
