/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.comparators;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Comparator;

/**
 * A number comparator for types extending number.
 *
 * @author johns
 */
public class NumberComparator implements Comparator<Number>, Serializable {

    /**
     * Serial version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * compare two numbers.
     *
     * @param a To compare.
     * @param b To compare to.
     * @return Compare result.
     */
    @Override
    public int compare(final Number a, final Number b) {
        return new BigDecimal(a.toString()).compareTo(new BigDecimal(b.toString()));
    }

}
