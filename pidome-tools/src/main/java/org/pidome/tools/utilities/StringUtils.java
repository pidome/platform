/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.utilities;

/**
 * String based utilities.
 *
 * @author johns
 */
public final class StringUtils {

    /**
     * Utils class private constructor.
     */
    private StringUtils() {
        /// default constructor.
    }

    /**
     * Capitalizes the first character in a string.
     *
     * @param string The string to capitalize.
     * @return The capitalized String.
     */
    public static String capitalise(final String string) {
        if (string == null) {
            return null;
        } else if (string.isBlank()) {
            return "";
        } else {
            return new StringBuilder(string.length())
                    .append(Character.toTitleCase(string.charAt(0)))
                    .append(string, 1, string.length())
                    .toString();
        }
    }

}
