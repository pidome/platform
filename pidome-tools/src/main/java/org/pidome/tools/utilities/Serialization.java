/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.utilities;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

/**
 * Utility class which supplies serialization objects based on the Jackson
 * ObjectMapper.
 *
 * @author John Sirach
 */
public final class Serialization {

    /**
     * Private constructor, no initialization.
     */
    private Serialization() {
    }

    /**
     * The <code>ObjectMapper</code> supporting the <code>Optional</code>
     * interfaces.
     */
    private static final ObjectMapper OPTIONAL_OBJECT_MAPPER;

    /**
     * A default <code>ObjectMapper</code>.
     */
    private static final ObjectMapper OBJECT_MAPPER;

    /**
     * Static initializer for creating the object mappers.
     */
    static {
        OBJECT_MAPPER = new ObjectMapper();
        setDefaultsOnObjectMapper(OBJECT_MAPPER);

        OPTIONAL_OBJECT_MAPPER = new ObjectMapper();
        OPTIONAL_OBJECT_MAPPER.registerModule(new Jdk8Module());
        setDefaultsOnObjectMapper(OPTIONAL_OBJECT_MAPPER);
    }

    /**
     * Set default values across all created mappers.
     *
     * @param mapper The mapper to set the defaults on.
     */
    private static void setDefaultsOnObjectMapper(final ObjectMapper mapper) {
        // Don't throw an exception when json has extra fields you are
        // not serializing on. This is useful when you want to use a pojo
        // for deserialization and only care about a portion of the json
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        // Ignore null values when writing json.
        mapper.setSerializationInclusion(Include.ALWAYS);

        // Write times as a String instead of a Long so its human readable.
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        mapper.registerModule(new JavaTimeModule());
    }

    /**
     * Returns an object mapper which supports the <code>Optional</code>
     * interface.
     *
     * @return Jackson object mapper with optional support.
     */
    public static ObjectMapper getDefaultObjectMapper() {
        return OPTIONAL_OBJECT_MAPPER;
    }

    /**
     * Returns an object mapper which supports the <code>Optional</code>
     * interface.
     *
     * @return Jackson object mapper with optional support.
     */
    public static ObjectMapper getOptionalObjectMapper() {
        return OPTIONAL_OBJECT_MAPPER;
    }

}
