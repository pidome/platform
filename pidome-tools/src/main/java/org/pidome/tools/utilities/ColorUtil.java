/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.utilities;

import java.awt.Color;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.pidome.tools.enums.ColorType;

/**
 * Simple color utilities.
 *
 * @author John Sirach
 */
@SuppressWarnings("checkstyle:MagicNumber")
public final class ColorUtil {

    /**
     * Private constructor.
     */
    private ColorUtil() {
    }

    /**
     * RGB comma separated pattern.
     */
    private static final String COMMA_SSEPARATED_RGB_PATTERN = "^(\\d{3}),(\\d{3}),(\\d{3})$";
    /**
     * Length of color hex value without # sign.
     */
    static final int HEXLENGTH = 6;
    /**
     * Decimal hex pattern supporting alpha (8 positions).
     */
    private static final String HEX_COLOR_PATTERN = "^#([\\da-fA-F]{1,8})$";

    /**
     * When kelvin color is requested this value is threshold for the lower
     * bounds.
     */
    private static final double LOWER_TEMPERATURE_THRESHOLD = 15.0;

    /**
     * When kelvin color is requested this value is threshold for the lower
     * bounds.
     */
    private static final long LOWER_TEMPERATURE_THRESHOLD_KELVIN = 2700;

    /**
     * When kelvin color is requested this value is threshold for the upper
     * bounds.
     */
    private static final double HIGHER_TEMPERATURE_THRESHOLD = 27.0;

    /**
     * When kelvin color is requested this value is threshold for the lower
     * bounds.
     */
    private static final long HIGHER_TEMPERATURE_THRESHOLD_KELVIN = 27000;

    /**
     * The upper bound for the upper range kelvin calculation.
     */
    private static final long KELVIN_TO_COLOR_UPPER_BOUND = 40000;

    /**
     * The lower bound for the lower range kelvin calculation.
     */
    private static final long KELVIN_TO_COLOR_LOWER_BOUND = 1000;

    /**
     * Maximum color value.
     */
    private static final float COLOR_BIT_UPPER_BOUND = 255;
    /**
     * Maximum color int value.
     */
    private static final int COLOR_BIT_UPPER_BOUND_INT = 255;

    /**
     * Calculate The HSB value based on a temperature, below 15 degrees is fixed
     * 3800 and above 27 degrees is fixed 40000.
     *
     * @param temp temperature in degrees.
     * @return Array[0]=H, Array[1]=S and Array[2]=B
     */
    public static float[] tempToHsb(final String temp) {
        return kelvinToHsb(getKelvinFromTemp(temp));
    }

    /**
     * Calculate The HSB value based on a temperature, below 15 degrees is fixed
     * 3800 and above 27 degrees is fixed 40000.
     *
     * @param temp temperature in degrees.
     * @return Array[0]=H, Array[1]=S and Array[2]=B
     */
    public static float[] tempToRgb(final String temp) {
        return kelvinToRgb(getKelvinFromTemp(temp));
    }

    /**
     * Return the kelvins from a given temperature.
     *
     * @param temp The temperature as string.
     * @return The mapped kelvin value (may not be as exact as the real kelvins
     * scale)
     */
    public static long getKelvinFromTemp(final String temp) {
        return getKelvinFromTemp(Double.valueOf(temp));
    }

    /**
     * Return the kelvins from a given temperature.
     *
     * @param temp The temperature as string.
     * @return The mapped kelvin value (may not be as exact as the real kelvins
     * scale)
     */
    public static long getKelvinFromTemp(final double temp) {
        long kelvin;
        if (temp <= LOWER_TEMPERATURE_THRESHOLD) {
            kelvin = LOWER_TEMPERATURE_THRESHOLD_KELVIN;
        } else if (temp >= LOWER_TEMPERATURE_THRESHOLD && temp <= HIGHER_TEMPERATURE_THRESHOLD) {
            kelvin = (int) MathUtil.map(temp, LOWER_TEMPERATURE_THRESHOLD, HIGHER_TEMPERATURE_THRESHOLD, LOWER_TEMPERATURE_THRESHOLD_KELVIN, HIGHER_TEMPERATURE_THRESHOLD_KELVIN);
        } else {
            kelvin = HIGHER_TEMPERATURE_THRESHOLD_KELVIN;
        }
        return kelvin;
    }

    /**
     * Converts kelving color value to RGB color value.
     *
     * @param kelvin The kelvin value.
     * @return Array[0]=R, Array[1]=G and Array[2]=B as colorType RGB
     */
    public static float[] kelvinToRgb(final long kelvin) {
        return kelvinToColor(kelvin, ColorType.RGB);
    }

    /**
     * Converts kelving color value to RGB color value.
     *
     * @param kelvin The kelvin value.
     * @return Array[0]=R, Array[1]=G and Array[2]=B as colorType HSB
     */
    public static float[] kelvinToHsb(final long kelvin) {
        return kelvinToColor(kelvin, ColorType.HSB);
    }

    /**
     * Convert an rgb to hsb.
     *
     * @param red The red color.
     * @param green The green color.
     * @param blue The blue color.
     * @return HSB value [h,s,b]
     */
    public static float[] rgbToHsb(final int red, final int green, final int blue) {
        return java.awt.Color.RGBtoHSB(red, green, blue, null);
    }

    /**
     * Calculate th HSB based on Kelvin degrees.
     *
     * reasonable OK between the 1000 and 40000 kelvin.
     *
     * @param kelvinValue Kelvin value
     * @param colorType type color type applied to be returned.
     * @return Array[0]=H, Array[1]=S and Array[2]=B when colorType HSB
     * otherwise Array[0]=R, Array[1]=G and Array[2]=B when colorType RGB
     */
    public static float[] kelvinToColor(final long kelvinValue, final ColorType colorType) {
        double tmpCalc;
        double r;
        double g;
        double b;
        long kelvin;

        final long divideSmaller = 100;
        final long privateKelvinThreshold = 66;
        final long kelvinBlueThreshold = 19;

        if (kelvinValue > KELVIN_TO_COLOR_UPPER_BOUND) {
            kelvin = KELVIN_TO_COLOR_UPPER_BOUND / divideSmaller;
        } else if (kelvinValue < KELVIN_TO_COLOR_LOWER_BOUND) {
            kelvin = KELVIN_TO_COLOR_LOWER_BOUND / divideSmaller;
        } else {
            kelvin = kelvinValue / divideSmaller;
        }

        /// red
        if (kelvin <= privateKelvinThreshold) {
            r = COLOR_BIT_UPPER_BOUND;
        } else {
            tmpCalc = kelvin - 60;
            tmpCalc = 329.698727446 * Math.pow(tmpCalc, -0.1332047592);
            r = tmpCalc;
            if (r < 0) {
                r = 0;
            }
            if (r > COLOR_BIT_UPPER_BOUND) {
                r = COLOR_BIT_UPPER_BOUND;
            }
        }
        /// green
        if (kelvin <= privateKelvinThreshold) {
            tmpCalc = kelvin;
            tmpCalc = 99.4708025861 * Math.log(tmpCalc) - 161.1195681661;
            g = tmpCalc;
            if (g < 0) {
                g = 0;
            }
            if (g > COLOR_BIT_UPPER_BOUND) {
                g = COLOR_BIT_UPPER_BOUND;
            }
        } else {
            tmpCalc = kelvin - 60;
            tmpCalc = 288.1221695283 * Math.pow(tmpCalc, -0.0755148492);
            g = tmpCalc;
            if (g < 0) {
                g = 0;
            }
            if (g > COLOR_BIT_UPPER_BOUND) {
                g = COLOR_BIT_UPPER_BOUND;
            }
        }
        /// blue
        if (kelvin >= privateKelvinThreshold) {
            b = COLOR_BIT_UPPER_BOUND;
        } else if (kelvin <= kelvinBlueThreshold) {
            b = 0;
        } else {
            tmpCalc = kelvin - 10;
            tmpCalc = 138.5177312231 * Math.log(tmpCalc) - 305.0447927307;
            b = tmpCalc;
            if (b < 0) {
                b = 0;
            }
            if (b > COLOR_BIT_UPPER_BOUND) {
                b = COLOR_BIT_UPPER_BOUND;
            }
        }
        switch (colorType) {
            case HSB:
                return java.awt.Color.RGBtoHSB((int) r, (int) g, (int) b, null);
            default:
                return new float[]{(int) r, (int) g, (int) b};
        }
    }

    /**
     * Converts HEX to RGB.
     *
     * @param hexForRGBConversion Hex color including the # sign.
     * @return Array[0]=R, Array[1]=G and Array[2]=B
     */
    public static int[] hexToRgb(final String hexForRGBConversion) {
        Pattern hexPattern = Pattern.compile(HEX_COLOR_PATTERN);
        Matcher hexMatcher = hexPattern.matcher(hexForRGBConversion);

        if (hexMatcher.find()) {
            int hexInt = Integer.valueOf(hexForRGBConversion.substring(1), 16);

            int r = (hexInt & 0xFF0000) >> 16;
            int g = (hexInt & 0xFF00) >> 8;
            int b = (hexInt & 0xFF);

            return new int[]{r, g, b};

        } else {
            return new int[]{0, 0, 0};
        }

    }

    /**
     * Converts RGB to a HEX value including the # sign.
     *
     * @param rgbForHexConversion a comma separated RGB pattern.
     * @return The HEX value of the given comma separated RGB value
     * @see ColorUtil#COMMA_SSEPARATED_RGB_PATTERN
     */
    public static String rgbToHex(final String rgbForHexConversion) {
        String hexValue;
        Pattern rgbPattern = Pattern.compile(COMMA_SSEPARATED_RGB_PATTERN);
        Matcher rgbMatcher = rgbPattern.matcher(rgbForHexConversion);

        int red;
        int green;
        int blue;
        if (rgbMatcher.find()) {
            red = Integer.parseInt(rgbMatcher.group(1));
            green = Integer.parseInt(rgbMatcher.group(2));
            blue = Integer.parseInt(rgbMatcher.group(3));
            Color color = new Color(red, green, blue);
            hexValue = Integer.toHexString(color.getRGB() & 0xffffff);
            int numberOfZeroesNeededForPadding = HEXLENGTH - hexValue.length();
            StringBuilder buf = new StringBuilder();
            for (int i = 0; i < numberOfZeroesNeededForPadding; i++) {
                buf.append("0");
            }
            String zeroPads = buf.toString();
            hexValue = "#" + zeroPads + hexValue;

        } else {
            hexValue = "#000000";
        }
        return hexValue;
    }

    /**
     * RGB to hex.
     *
     * @param red The red int value
     * @param green The green int value
     * @param blue The blue int value
     * @return String hex value including # sign.
     */
    public static String rgbToHex(final int red, final int green, final int blue) {
        String hexValue;
        Color color = new Color(red, green, blue);
        hexValue = Integer.toHexString(color.getRGB() & 0xffffff);
        int numberOfZeroesNeededForPadding = HEXLENGTH - hexValue.length();
        String zeroPads = "";
        for (int i = 0; i < numberOfZeroesNeededForPadding; i++) {
            zeroPads += "0";
        }
        hexValue = "#" + zeroPads + hexValue;
        return hexValue;
    }

    /**
     * Returns the rgb values of an hsb color.
     *
     * @param hue The hue.
     * @param saturation The saturation.
     * @param brightness The brightness.
     * @return int[0] = red,int[1] = green,int[2] = blue.
     */
    public static int[] hsbToRgb(final float hue, final float saturation, final float brightness) {
        int[] rgb = new int[3];
        int color = java.awt.Color.HSBtoRGB(hue, saturation, brightness);
        rgb[0] = (color >> 16) & COLOR_BIT_UPPER_BOUND_INT;
        rgb[1] = (color >> 8) & COLOR_BIT_UPPER_BOUND_INT;
        rgb[2] = color & COLOR_BIT_UPPER_BOUND_INT;
        return rgb;
    }

}
