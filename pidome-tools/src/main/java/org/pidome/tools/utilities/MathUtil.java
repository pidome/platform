/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.utilities;

/**
 * Simple math utilities.
 *
 * @author John Sirach
 * @since 1.0
 */
public final class MathUtil {

    /**
     * Private constructor, only static methods.
     */
    private MathUtil() {
    }

    /**
     * Returns the mapped range from lower bound to higher bound.
     *
     * Calculation result 60 = map(6, 0, 10, 0, 100);
     *
     * @param curValue The known value between minCurValue and maxCurvalue
     * @param minCurValue The minimum value of the lower bound range range
     * @param maxCurValue The maximum value of the lower bound range
     * @param minValue The minimum value of the higher bound range
     * @param maxValue The maximum value of the higher bound range
     * @return The calculated value between minValue and maxValue
     */
    public static double map(final double curValue, final double minCurValue, final double maxCurValue, final double minValue, final double maxValue) {
        return (curValue - minCurValue) / (maxCurValue - minCurValue) * (maxValue - minValue) + minValue;
    }

    /**
     * Returns the inverse mapped range from lower bound to higher bound.
     * Calculation result 40 = map(6, 0, 10, 0, 100);
     *
     * @param curValue The known value between minCurValue and maxCurvalue
     * @param minCurValue The minimum value of the lower bound range range
     * @param maxCurValue The maximum value of the lower bound range
     * @param minValue The minimum value of the higher bound range
     * @param maxValue The maximum value of the higher bound range
     * @return The calculated value between minValue and maxValue
     */
    public static double mapInverse(final double curValue, final double minCurValue, final double maxCurValue, final double minValue, final double maxValue) {
        return maxValue - ((curValue - minCurValue) / (maxCurValue - minCurValue) * (maxValue - minValue) + minValue) + minValue;
    }
}
