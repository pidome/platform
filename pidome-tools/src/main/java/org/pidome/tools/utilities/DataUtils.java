/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.utilities;

import java.math.BigInteger;
import java.util.Locale;

/**
 * some utilities based on data.
 *
 * @author johns
 */
public final class DataUtils {

    /**
     * Private constructor utility class.
     */
    private DataUtils() {
        /// default private constructor.
    }

    /**
     * Simple dictionary for data sizes.
     */
    private static final String[] DATA_SIZE_DICTIONAIRY = {"bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"};

    /**
     * Divider value for calculating bytes.
     */
    private static final BigInteger BYTE_BASE_DIVIDER = BigInteger.valueOf(1024);

    /**
     * Create an human readable string for the amount of bytes.
     *
     * @param bytes The amount of bytes to return an human readable string from.
     * @param digits the amount of digits to use.
     * @return Human readable string of a data size.
     */
    public static String dataSizeToHumanReadable(final BigInteger bytes, final int digits) {
        int dictIndex;
        BigInteger calculationBytes = bytes;
        for (dictIndex = 0; dictIndex < DATA_SIZE_DICTIONAIRY.length; dictIndex++) {
            if (calculationBytes.compareTo(BYTE_BASE_DIVIDER) == -1) {
                break;
            }
            calculationBytes = calculationBytes.divide(BYTE_BASE_DIVIDER);
        }
        return String.format(Locale.getDefault(), "%." + digits + "f", calculationBytes.doubleValue()) + " " + DATA_SIZE_DICTIONAIRY[dictIndex];
    }

}
