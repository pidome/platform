/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.utilities;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.jupiter.api.AfterAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 *
 * @author johns
 */
public class NetworkUtilsTest {

    /**
     * Server test socket.
     */
    private static ServerSocket serverSocket;

    /**
     * Port number to test on.
     */
    private static final int CORRECT_PORT = 65535;

    /**
     * Port number to test on.
     */
    private static final String CORRECT_HOST = "127.0.0.1";

    /**
     * Port number to test on.
     */
    private static final String CORRECT_HOST_PORT = CORRECT_HOST + ":" + String.valueOf(CORRECT_PORT);

    /**
     * Port number to test on.
     */
    private static final int WRONG_PORT = 65534;

    /**
     * Port number to test on.
     */
    private static final String WRONG_HOST = "127.0.0.2";

    /**
     * Port number to test on.
     */
    private static final String WRONG_HOST_CORRECT_PORT = WRONG_HOST + ":" + String.valueOf(CORRECT_PORT);

    /**
     * Port number to test on.
     */
    private static final String CORRECT_HOST_WRONG_PORT = CORRECT_HOST + ":" + String.valueOf(WRONG_PORT);

    /**
     * Port number to test on.
     */
    private static final String WRONG_HOST_WRONG_PORT = WRONG_HOST + ":" + String.valueOf(WRONG_PORT);

    /**
     * Sets up server socket for tests.
     *
     * @throws IOException When server socket is unable to be created.
     */
    @BeforeAll
    public static void setUpClass() throws IOException {
        serverSocket = new ServerSocket(CORRECT_PORT, 50, InetAddress.getByName("localhost"));
    }

    /**
     * Shuts down the server socket.
     */
    @AfterAll
    public static void tearDownClass() {
        if (serverSocket != null && !serverSocket.isClosed()) {
            try {
                serverSocket.close();
            } catch (IOException ex) {
                Logger.getLogger(NetworkUtilsTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Test of remotePortAvailable method, of class NetworkUtils.
     */
    @Test
    public void testRemoteCorrectHostCorrectPortAvailable_String() {
        assertEquals(true, NetworkUtils.remotePortAvailable(CORRECT_HOST_PORT));
    }

    /**
     * Test of remotePortAvailable method, of class NetworkUtils.
     */
    @Test
    public void testRemoteWrongHostCorrectPortAvailable_String() {
        assertEquals(false, NetworkUtils.remotePortAvailable(WRONG_HOST_CORRECT_PORT));
    }

    /**
     * Test of remotePortAvailable method, of class NetworkUtils.
     */
    @Test
    public void testRemoteCorrectHostWrongPortAvailable_String() {
        assertEquals(false, NetworkUtils.remotePortAvailable(CORRECT_HOST_WRONG_PORT));
    }

    /**
     * Test of remotePortAvailable method, of class NetworkUtils.
     */
    @Test
    public void testRemoteWrongHostWrongPortAvailable_String() {
        assertEquals(false, NetworkUtils.remotePortAvailable(WRONG_HOST_WRONG_PORT));
    }

    /**
     * Test of remotePortAvailable method, of class NetworkUtils.
     */
    @Test
    public void testRemoteCorrectHostCorrectPortAvailable_String_int() {
        assertEquals(true, NetworkUtils.remotePortAvailable(CORRECT_HOST, CORRECT_PORT));
    }

    /**
     * Test of remotePortAvailable method, of class NetworkUtils.
     */
    @Test
    public void testRemoteWrongHostCorrectPortAvailable_String_int() {
        assertEquals(false, NetworkUtils.remotePortAvailable(WRONG_HOST, CORRECT_PORT));
    }

    /**
     * Test of remotePortAvailable method, of class NetworkUtils.
     */
    @Test
    public void testRemoteCorrectHostWrongPortAvailable_String_int() {
        assertEquals(false, NetworkUtils.remotePortAvailable(CORRECT_HOST, WRONG_PORT));
    }

    /**
     * Test of remotePortAvailable method, of class NetworkUtils.
     */
    @Test
    public void testRemoteWrongHostWrongPortAvailable_String_int() {
        assertEquals(false, NetworkUtils.remotePortAvailable(WRONG_HOST, WRONG_PORT));
    }

    /**
     * Test of remotePortAvailable method, of class NetworkUtils.
     */
    @Test
    public void testRemoteCorrectRemoteCorrectPortAvailable_3args() {
        assertEquals(true, NetworkUtils.remotePortAvailable(CORRECT_HOST, CORRECT_PORT, 5));
    }

    /**
     * Test of remotePortAvailable method, of class NetworkUtils.
     */
    @Test
    public void testRemoteWrongRemoteCorrectPortAvailable_3args() {
        assertEquals(false, NetworkUtils.remotePortAvailable(WRONG_HOST, CORRECT_PORT, 5));
    }

    /**
     * Test of remotePortAvailable method, of class NetworkUtils.
     */
    @Test
    public void testRemoteCorrectRemoteWrongPortAvailable_3args() {
        assertEquals(false, NetworkUtils.remotePortAvailable(CORRECT_HOST, WRONG_PORT, 5));
    }

    /**
     * Test of remotePortAvailable method, of class NetworkUtils.
     */
    @Test
    public void testRemoteWrongRemoteWrongPortAvailable_3args() {
        assertEquals(false, NetworkUtils.remotePortAvailable(WRONG_HOST, WRONG_PORT, 5));
    }

}
