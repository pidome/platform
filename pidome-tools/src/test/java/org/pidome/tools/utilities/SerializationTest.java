/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.utilities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Date;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.Test;

/**
 *
 * @author johns
 */
public class SerializationTest {

    /**
     * Test of getDefaultObjectMapper method, of class Serialization.
     */
    @Test
    public void testGetDefaultObjectMapper() {
        Date isNow = new Date();

        Weird weirdPojo = new Weird();
        weirdPojo.setWeirdValue("funny");

        GenericPojo generic = new GenericPojo();
        generic.setDate(isNow);
        generic.setInteger(10);
        generic.setText("Some text");
        generic.setWeird(weirdPojo);

        ObjectMapper mapper = Serialization.getDefaultObjectMapper();

        try {
            String serialized = mapper.writeValueAsString(generic);
            GenericPojo deserialized = mapper.readValue(serialized, GenericPojo.class);
            assertEquals(deserialized.getWeird(), null);
            assertEquals(deserialized.getDate(), isNow);
            assertEquals(deserialized.getText(), "Some text");
        } catch (IOException ex) {
            fail(ex);
        }
    }

    /**
     * Test of getOptionalObjectMapper method, of class Serialization.
     */
    @Test
    public void testGetOptionalObjectMapperWithFilledOptionalData() {
        TestOptionalMapperClass optionalTestFilled = new TestOptionalMapperClass();
        optionalTestFilled.setField(Optional.of("testInput"));
        ObjectMapper mapper = Serialization.getOptionalObjectMapper();
        try {
            String serialized = mapper.writeValueAsString(optionalTestFilled);
            assertEquals("{\"field\":\"testInput\"}", serialized);
            TestOptionalMapperClass deserialized = mapper.readValue(serialized, TestOptionalMapperClass.class);
            deserialized.getField().ifPresent(value -> {
                assertEquals("testInput", value);
            });
        } catch (IOException ex) {
            fail(ex);
        }
    }

    /**
     * Test of getOptionalObjectMapper method, of class Serialization.
     */
    @Test
    public void testGetOptionalObjectMapperWithEmptyOptionalData() {
        TestOptionalMapperClass optionalTestEmpty = new TestOptionalMapperClass();
        ObjectMapper mapper = Serialization.getOptionalObjectMapper();
        try {
            String serialized = mapper.writeValueAsString(optionalTestEmpty);
            assertEquals("{\"field\":null}", serialized);
            TestOptionalMapperClass deserialized = mapper.readValue(serialized, TestOptionalMapperClass.class);
            assertEquals(deserialized.getField(), Optional.empty());
        } catch (IOException ex) {
            fail(ex);
        }
    }

    static class GenericPojo {

        private Date date;

        private String text;

        private int integer;

        @JsonIgnore
        private Weird weird;

        /**
         * @return the date
         */
        public Date getDate() {
            return date;
        }

        /**
         * @param date the date to set
         */
        public void setDate(Date date) {
            this.date = date;
        }

        /**
         * @return the text
         */
        public String getText() {
            return text;
        }

        /**
         * @param text the text to set
         */
        public void setText(String text) {
            this.text = text;
        }

        /**
         * @return the integer
         */
        public int getInteger() {
            return integer;
        }

        /**
         * @param integer the integer to set
         */
        public void setInteger(int integer) {
            this.integer = integer;
        }

        /**
         * @return the weird
         */
        public Weird getWeird() {
            return weird;
        }

        /**
         * @param weird the weird to set
         */
        public void setWeird(Weird weird) {
            this.weird = weird;
        }

    }

    static class Weird {

        private String weirdValue;

        /**
         * @return the weirdValue
         */
        public String getWeirdValue() {
            return weirdValue;
        }

        /**
         * @param weirdValue the weirdValue to set
         */
        public void setWeirdValue(String weirdValue) {
            this.weirdValue = weirdValue;
        }

    }

    /**
     * Class used for optional mapper.
     */
    static class TestOptionalMapperClass {

        public TestOptionalMapperClass() {
        }

        /**
         * Pojo member.
         */
        private Optional<String> field;

        /**
         * Set pojo value.
         *
         * @param field The value to set.
         */
        public void setField(Optional<String> field) {
            this.field = field;
        }

        /**
         * Pojo getter for member.
         *
         * @return The member optional.
         */
        public Optional<String> getField() {
            return field;
        }
    }

}
