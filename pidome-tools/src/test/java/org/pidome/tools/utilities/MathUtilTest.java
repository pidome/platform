/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.utilities;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 *
 * @author johns
 */
public class MathUtilTest {

    /**
     * Test of map method, of class MathUtil.
     */
    @Test
    public void testMap() {
        assertEquals(10, MathUtil.map(1, 0, 10, 0, 100), 0.0);
    }

    /**
     * Test of mapInverse method, of class MathUtil.
     */
    @Test
    public void testMapInverse() {
        assertEquals(90, MathUtil.mapInverse(1, 0, 10, 0, 100), 0.0);
    }

}
