/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.tools.enums;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * Resource types.
 *
 * @author johns
 */
@DisplayName("Available resources")
public class ResourceTypeTest {

    /**
     * Test of values method, of class ResourceType.
     */
    @Test
    @DisplayName("Test available resource types")
    public void testValues() {
        ResourceType[] expResult = {ResourceType.FILESYSTEM, ResourceType.NETWORK, ResourceType.MEMORY, ResourceType.SOCK};
        ResourceType[] result = ResourceType.values();
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of valueOf method, of class ResourceType.
     */
    @Test
    @DisplayName("Test discovery by string")
    public void testValueOf() {
        ResourceType expResult = ResourceType.MEMORY;
        ResourceType result = ResourceType.valueOf("MEMORY");
        assertEquals(expResult, result);
    }

}
